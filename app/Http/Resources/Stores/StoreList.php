<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use Carbon\Carbon;
use App\Model\StoreBranch;
use App\Models\StoreLike;
use App\Models\StoreFollowing;
use App\Models\StoreSchedule;
use App\Models\StoreTiming;
class StoreList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hash_id'                   => $this->hashid,
            'is_main_branch'            => $this->is_main_branch,
            'is_actived'                => $this->is_active,
            'business_type'             => $this->businessType ? $this->businessType->name : null,
            'name'                      => $this->name,
            'street'                    => $this->street,
            'city'                      => $this->city,
            'is_opened'                 => $this->is_opened,
            'status'                    => $this->when(auth()->user() ? auth()->user()->type_info != 'customer' : false, $this->status_info),
            'total_like'                => $this->likes->count(),
            'total_following'           => $this->follows->count(),
            'is_liked'                  => $this->when(auth()->user() ? auth()->user()->type_info === 'customer' : false, $this->isLiked()),
            'is_followed'               => $this->when(auth()->user() ? auth()->user()->type_info === 'customer' : false, $this->isFollowed()),
            'created_at'                => $this->created_at,
            'timestamp'                 =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'profile_photo_full'        => asset($this->profilePhoto()->full),
            'profile_photo_thumbnail'   => asset($this->profilePhoto()->thumbnail),
            'cover_photo_full'          => $this->coverPhoto()->full ? asset($this->coverPhoto()->full) : null,
            'cover_photo_thumbnail'     => $this->coverPhoto()->thumbnail ? asset($this->coverPhoto()->thumbnail) : null,
        ];
    }

    public function profilePhoto()
    {
        $full = public_path().'/images/stores/' . $this->hashid . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hashid . '/profile-photo-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . $this->hashid . '/profile-photo-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . $this->hashid . '/profile-photo-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }

    public function coverPhoto()
    {
        $full = public_path().'/images/stores/' . $this->hashid . '/cover-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hashid . '/cover-photo-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . $this->hashid . '/cover-photo-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . $this->hashid . '/cover-photo-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }

    public function isLiked()
    {
       $storeLiked = StoreLike::where('store_id', $this->id)->where('user_id', auth()->id())->where('is_liked', 1)->first();
       return $storeLiked ? 1 : 0;
    }

    public function isFollowed()
    {
       $storeFollowed = StoreFollowing::where('store_id', $this->id)->where('user_id', auth()->id())->where('is_followed', 1)->first();
       return $storeFollowed ? 1 : 0;
    }
}
