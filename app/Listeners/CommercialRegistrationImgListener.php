<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use App\Models\CommercialRegistrationImage;
use Illuminate\Support\Facades\File;
class CommercialRegistrationImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data->model;

        $commercial_registration_images = json_decode($event->data->commercial_registration_images, true);

        foreach ($commercial_registration_images as $image){
            $new = new CommercialRegistrationImage;
            $new->store_id = $store->id;
            $new->base64 = $image;
            $new->save();

            $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';
        
            $path = $store_path . 'commercial-registration-images/';

            if(!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/stores/' . encode($store->id, 'uuid') . '/commercial-registration-images', $mode = 0777, true, true);
            }
            
            $image = Image::make($new->base64);
            $image->save($path . $new->id .'-commercial-registration-full.png');
            $image->resize(150, 150);
            $image->save($path . $new->id .'-commercial-registration-thumbnail.png');
        }

    }
}
