<?php

namespace App\Http\Resources\Stores;
use App\Http\Resources\Stores\StoreList;
use App\Models\Store;

use stdClass;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    
        return [
            'hashid'        => $this->hashid,
            'name'          => $this->name,
            'street'        => $this->street,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'status'        => $this->status ? 'active' : 'inactive',
            'is_approved'   => $this->approve(),
            //'main_branch' => $this->store()
        ];
    }

    public function store()
    {
        
        $ss = Store::where('id', $this->store_id)->first();
        if(!$ss) return 'Invalid Store';
        
        $data = new stdClass;
        $data->hashid = $ss->hash_id;
        $data->name = $ss->name;
        
        return $data;
    }

    public function status()
    {
        if($this->status === 1)return 'active';
        if($this->status === 0)return 'inactive';
    }

    public function approve()
    {
        if($this->is_approved === 1)return 'approved';
        if($this->is_declined === 0)return 'declined';
        if($this->is_declined === 0 && $this->is_approved === 0)return 'pending';
    }
}
