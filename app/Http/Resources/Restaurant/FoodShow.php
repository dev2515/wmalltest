<?php

namespace App\Http\Resources\Restaurant;
use App\Models\Meal;
use App\User;
use App\FoodCategory;
use stdClass;
use Illuminate\Http\Resources\Json\JsonResource;

class FoodShow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'hashid' =>                     $this->hash_id,
            'name' =>                       $this->name,
            'price' =>                      centToPrice($this->price),
            'description' =>                $this->description, 
            'status' =>                     $this->status(),
            'is_available' =>               $this->available(),
            'full' =>                       $this->photo()->full,
            'thumbnail' =>                  $this->photo()->thumbnail,
            'variation' =>                  $this->variation,
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function status()
    {
        if($this->is_approved === 1) return 'approved';
        if($this->is_declined === 1) return 'declined';
        if($this->is_approved === 0 && $this->is_declined === 0) return 'pending';
    }

    public function available()
    {
        if($this->is_available === 1) return 'available';
        if($this->is_available === 0) return 'unavailable';
    }

}
