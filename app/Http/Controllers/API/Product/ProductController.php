<?php

namespace App\Http\Controllers\API\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ProductInterface;

class ProductController extends Controller
{
    protected $req;
    protected $product;

    public function __construct(Request $req, ProductInterface $product)
    {
        $this->req = $req;
        $this->product = $product;
    }

    public function mostPopular()
    {
        return $this->product->mostPopular($this->req);
    }

    public function toggleToFavorite()
    {
        return $this->product->toggleToFavorite($this->req);
    }

    public function details()
    {
        return $this->product->details($this->req);
    }

    public function items()
    {
        return $this->product->items($this->req);
    }

    public function updateStatus()
    {
        return $this->product->updateStatus($this->req);
    }

    public function customerReviews()
    {
        return $this->product->customerReviews($this->req);
    }

    public function searchProduct()
    {
        return $this->product->searchProduct($this->req);
    }

    public function listByTags()
    {
        return $this->product->listByTags($this->req);
    }

    public function listByCategory()
    {
        return $this->product->listByCategory($this->req);
    }

    public function searchProductImage()
    {
        return $this->product->searchProductImage($this->req);  
    }

    public function listByName()
    {
        return $this->product->listByName($this->req);
    }

    public function listByPrice()
    {
        return $this->product->listByPrice($this->req);
    }

    public function listBySort()
    {
        return $this->product->listBySort();
    }

    public function listOfTags()
    {
        return $this->product->listOfTags($this->req);
    }

    public function addBrand()
    {
        return $this->product->addBrand($this->req);
    }

    public function updateBrand()
    {
        return $this->product->updateBrand($this->req);
    }

    public function listBrand()
    {
        return $this->product->listBrand($this->req);
    }

    public function searchBrand()
    {
        return $this->product->searchBrand($this->req);
    }

    public function brandListByName()
    {
        return $this->product->brandListByName($this->req);
    }

    public function brandListBySort()
    {
        return $this->product->brandListBySort();
    }

    public function listByStatus()
    {
        return $this->product->listByStatus($this->req);
    }

    public function totalBrands()
    {
        return $this->product->totalBrands();
    }

    public function getBrands()
    {
        return $this->product->getBrands($this->req);
    }

    public function addCategory()
    {
        return $this->product->addCategory($this->req);
    }

    public function updateCategory()
    {
        return $this->product->updateCategory($this->req);
    }

    public function deleteCategory()
    {
        return $this->product->deleteCategory($this->req);
    }

    public function searchTags()
    {
        return $this->product->searchTags($this->req);
    }

    public function updateTags()
    {
        return $this->product->updateTags($this->req);
    }

    public function addTags()
    {
        return $this->product->addTags($this->req);
    }

    public function brandShow()
    {
        return $this->product->brandShow($this->req);
    }

    public function productChangesStatus()
    {
        return $this->product->productChangesStatus($this->req);
    }

    public function brandChangeStatus()
    {
        return $this->product->brandChangeStatus($this->req);
    }
    
    public function productShowInOpenAPI()
    {
        return $this->product->productShowInOpenAPI($this->req);
    }
    
    public function productTypeList()
    {
        return $this->product->productTypeList();
    }

    public function open_product_list()
    {
        return $this->product->open_product_list($this->req);
    }
}
