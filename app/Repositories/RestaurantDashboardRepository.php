<?php

namespace App\Repositories;

use App\Interfaces\RestaurantDashboardInterface;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use App\User;
use Carbon\Carbon;

class RestaurantDashboardRepository implements RestaurantDashboardInterface
{

    /**
     * Get total order received
     * @param $store_id
     * @return JsonResponse
     */
    public function getOrderReceived($store_id):int
    {

        $cart_ids = RestaurantCart::where('restaurant_id', $store_id)->pluck('id');
        return RestaurantOrder::whereIn('cart_id', $cart_ids)->count();
    }

    /**
     * Get total customer
     * @param $store_id
     * @return JsonResponse
     */
    public function getTotalCustomer($store_id):int
    {
        return User::where('type', 0)->where('status', 1)->count();
    }

    /**
     * Get total delivery
     * @param $store_id
     * @return JsonResponse
     */
    public function getTotalDelivery($store_id):int
    {
        $cart_ids = RestaurantCart::where('restaurant_id', $store_id)->pluck('id');
        return RestaurantOrder::whereIn('cart_id', $cart_ids)->where('order_type', 2)->where('is_completed', 1)
        ->where('created_at', '>=', Carbon::now()->startOfDay())->where('created_at', '<=', Carbon::now()->endOfDay())->count();
    }
    
}