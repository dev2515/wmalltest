<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentsFileNameToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'cc_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('cc_file_name')->nullable()->after('signature_status');
            });
        }

        if(!Schema::hasColumn('stores', 'cr_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('cr_file_name')->nullable()->after('cc_file_name');
            });
        }

        if(!Schema::hasColumn('stores', 'cp_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('cp_file_name')->nullable()->after('cr_file_name');
            });
        }

        if(!Schema::hasColumn('stores', 'sq_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('sq_file_name')->nullable()->after('cp_file_name');
            });
        }

        if(!Schema::hasColumn('stores', 'signature_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('signature_file_name')->nullable()->after('sq_file_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'cc_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('cc_file_name');
            });
        }

        if(Schema::hasColumn('stores', 'cr_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('cr_file_name');
            });
        }

        if(Schema::hasColumn('stores', 'cp_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('cp_file_name');
            });
        }

        if(Schema::hasColumn('stores', 'sq_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('sq_file_name');
            });
        }

        if(Schema::hasColumn('stores', 'signature_file_name')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_file_name');
            });
        }
    }
}
