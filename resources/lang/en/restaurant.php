<?php

return [
    'not_found'         => 'Restaurant not found', 
    'not_restaurant'    => 'This store is not restaurant',
    'services' => [
        'not_found'  => 'Service not found',
        'not_binded' => 'Restaurant services pivot not found'
    ],
    'order' => [
        'not_found' => 'Order information not found',
    ]
];