<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class SeedBrands extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Brand::count();

        if($count == 0){
            Brand::firstOrCreate(['id' => 1, 'name' => 'OEM', 'is_approved' => 1, 'status' => 1, 'profile_base64' => '']);
        }
    }
}
