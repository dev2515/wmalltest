<?php

namespace App\Interfaces;

interface BloggerInterface
{
    public function blogger_list($req);

    public function category_list($req);

    public function product_list($req);

    public function add_product($req);

    public function add_boutique_shop($req);

    public function add_product_category_image($req);
}