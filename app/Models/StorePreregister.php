<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StorePreregister extends Model
{
    protected $appends = ['status_info'];

    public function getStatusInfoAttribute()
    {
        switch ($this->status) {
            case 2 :
            return $status = 'Registered';
            break;

            case 1 :
            return $status = 'Pending';
            break;

            case 0 :
            return $status = 'Cancelled';
            break;
        }
    }
}
