<?php

namespace App\Http\Resources\Restaurant;

use App\Models\FoodCategory;
use App\Models\RestaurantFood;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class MenuListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            //'Food Type' => $this->food_type(),
            'hash_id' =>                    encode($this->id,'uuid'),
            'Name' =>                       $this->name,
            'Price' =>                      centToPrice($this->price),
            'full' =>                       asset($this->photo()->full),
            'thumbnail' =>                  asset($this->photo()->thumbnail),
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . encode($this->id,'uuid') . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . encode($this->id,'uuid') . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . encode($this->id,'uuid') . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . encode($this->id,'uuid') . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
