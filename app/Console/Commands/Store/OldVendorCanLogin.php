<?php

namespace App\Console\Commands\Store;

use Illuminate\Console\Command;
use App\Models\Store;
use App\User;

class OldVendorCanLogin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vendor:login';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Old stores owner that have pending status can be log-in';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = Store::where('is_approved', 0)->where('is_declined', 0)->where('ban', 0)->pluck('user_id');

        $bar = $this->output->createProgressBar(count($ids));
        $bar->start();

        $users = User::whereIn('id', $ids)->update(array('status' => 1));
        $bar->finish();
    }
}
