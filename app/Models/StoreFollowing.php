<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class StoreFollowing extends Model
{
    protected $table = 'store_followings';
    protected $fillable = ['store_id', 'user_id', 'is_followed', 'status'];
    protected $appends = ['hash_id'];


    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
