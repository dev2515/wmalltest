<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use Illuminate\Http\Resources\Json\JsonResource;

class ScanCartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'cart_hash_id' =>                   $this->hash_id,
            'food' =>                           $this->foods_info,
            'total_info' =>                     $this->totalinfo(),
        ];
    }

    public function totalinfo()
    {
        $select_all = false;
        $cart_count = RestaurantCart::where('user_id', auth()->user()->id)->where('is_deleted', 0)->count();
        $on_checkout_count = RestaurantCart::where('user_id', auth()->user()->id)->where('is_deleted', 0)->where('on_checkout', 1)->count();
        
        if ($on_checkout_count >= $cart_count) {
            $select_all = true;
        }

        $total = $this->calculateTotalAmount();
        $collect = collect([$select_all, $total]);

        return $collect;
    }

    public function calculateTotalAmount() {
        $total = 0;
        $total_count = 0;
        $user_id = auth()->user()->id;
       
        $items = RestaurantCartItem::where('cart_id', $this->id)->get();
        
        foreach ($items as $item) {
            $total += $item->food->price * $item->quantity;
            $total_count += $total_count + 1;
        }

        $t = array(['total' => $total, 'total_count' => $total_count]);
        return $t;
    }
}
