<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQRPointersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('q_r_pointers')) {
            Schema::create('q_r_pointers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->text('hash');
                $table->integer('transaction_type_id')->unsigned();
                $table->integer('business_type_id')->default(0);
                $table->integer('transaction_number')->default(0);
                $table->integer('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('q_r_pointers');
    }
}
