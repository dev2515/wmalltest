<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('user_addresses', ['nickname', 'area', 'house', 'additional_directions', 'mobile', 'landline', 'building', 'floor', 'apartment_number', 'office'])){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->string('nickname')->nullable()->after('user_id');
                $table->string('area')->nullable()->after('city');
                $table->string('house')->nullable()->after('area');
                $table->string('additional_directions')->nullable()->after('house');
                $table->string('mobile')->nullable()->after('other_address_info');
                $table->string('landline')->nullable()->after('mobile');
                $table->string('building')->nullable()->after('landline');
                $table->string('floor')->nullable()->after('building');
                $table->string('apartment_number')->nullable()->after('floor');
                $table->string('office')->nullable()->after('apartment_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumns('user_addresses', ['nickname', 'area', 'house', 'additional_directions', 'mobile', 'landline', 'building', 'floor', 'apartment_number', 'office'])){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->dropColumn('nickname');
                $table->dropColumn('area');
                $table->dropColumn('house');
                $table->dropColumn('additional_directions');
                $table->dropColumn('mobile');
                $table->dropColumn('landline');
                $table->dropColumn('building');
                $table->dropColumn('floor');
                $table->dropColumn('apartment_number');
                $table->dropColumn('office');
            });
        }
    }
}
