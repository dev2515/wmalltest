<?php

namespace App\Repositories;

use App\User;
use App\Models\State;
use App\Models\Product;
use App\Models\WishList;
use App\Models\ProductType;
use App\Models\StockStatus;
use App\Models\ProductImage;
use App\Models\ShippingRate;
use App\Models\ProductReview;
use App\Models\ProductCategory;
use App\Models\ProductShipping;
use App\Models\ProductAttribute;
use App\Models\ShippingCategory;
use Illuminate\Support\Facades\DB;
use App\Interfaces\ProductInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use App\Models\ProductTag;
use App\Models\Tag;
use App\Models\StoreChangeField;
use App\Models\Category;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Product\ProductListCollection;

use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\File;
use App\Models\Brand;
use App\Http\Resources\Product\Type;
use App\Http\Resources\Product\TagCollection;
use App\Http\Resources\Product\BrandShow;
use App\Http\Resources\Product\BrandCollection;
use App\Http\Resources\Stores\BrandChangeFieldCollection;

class ProductRepository implements ProductInterface
{
    public function list($req)
    {
        $permissions = ['vendor', 'vendor staff', 'agent', 'administrator'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        if(!$req->limit) return res('limit is required', null, 400);

        $store_id = decode($req->store_hash_id, 'uuid');

        $products = Product::where('store_id', $store_id)->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->filter === 'approved') $products = Product::where('store_id', $store_id)->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->filter === 'declined') $products = Product::where('store_id', $store_id)->where('is_declined', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->filter === 'pending')  $products = Product::where('store_id', $store_id)->where('is_approved', 0)->where('is_declined', 0)->orderBy('created_at', 'desc')->paginate($req->limit);
        
        $data = new ProductListCollection($products);
        return res('success', $data, 200);
    }

    public function open_product_list($req)
    {
        $v = Validator::make($req->all(),[
            'store_hash_id' => 'required',
            'limit' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $store_id = decode($req->store_hash_id, 'uuid');

        $products = Product::where('store_id', $store_id)->where('is_approved', 1)->orderBy('name', 'ASC')->paginate($req->limit);
        
        $data = new ProductListCollection($products);
        return res('success', $data, 200);
    }

    public function show($req)
    {
        if (!$req->has('product_hash_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $product_id = decode($req->product_hash_id, 'uuid');

        $product = Product::find($product_id);
        if (!$product) {
            return res(__('product.not_found'), null, 400);
        }
        
        $data= new ProductResource($product);
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalProducts($req)
    {
        if (!$req->has('store_hashid')) {
            return res(__('validation.identifier'), null, 400);
        }

        $store_hashid = decode($req->store_hashid, 'uuid');
        
        $products = product::where('store_id', $store_hashid)->count();
        return res('success', $products, 200);
    }

    public function add($req)
    {
        DB::beginTransaction();

        $permissions = ['vendor', 'vendor staff', 'agent', 'administrator', 'blogger'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('vendor.not_allowed'), null, 400);
            DB::rollBack();
        }
    
        $validator = Validator::make($req->all(), [
            'store_id'          => 'required',
            'product_type_id'   => 'required',
            'category_ids'      => 'required',
            'name'              => 'required',
            'price'             => 'required',
            'short_description' => 'required|max:500',
            'tags'              => 'required',
            'delivery_option'   => 'required',
            'length'            => 'required',
            'width'             => 'required',
            'height'            => 'required',
            'weight'            => 'required'
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $store_id       = decode($req->store_id, 'uuid');
        $brand_id       = 0;
        $branch_ids     = [];

        $store_check = DB::table('stores')->select('id','business_type_id')->where('id', $store_id)->first();

        foreach (json_decode($req->branch_ids, true) as $branch_id){ array_push($branch_ids, $id = decode($branch_id, 'uuid')); }

        if($req->brand_hashid != 0) $brand_id = decode($req->brand_hashid, 'uuid');

        $has_product = Product::where('store_id', $store_id)->where('name', $req->name)->count();

        if ($has_product > 0) {
            DB::rollBack();
            return res(__('product.existed'), null, 400);
        }

        // $tags = json_decode($req->tags, true);
        // if (count($tags) > 5 && count($tags) < 0) {
        //     DB::rollBack();
        //     return res(__('product.tags'), null, 400);
        // }

        $new                = new StoreChangeField;
        $new->store_id      = $store_id;
        $new->type          = 'add product';
        $new->data          = $req->only('store_id', 'name', 'price', 'on_sale', 'sale_price', 'short_description');
        $new->request_by    = auth()->id();
        $new->is_approved   = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new->process_by    = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;

        $product                    = new Product;
        $product->store_id          = $store_id;
        $product->brand_id          = $brand_id;
        $product->product_type_id   = $req->product_type_id;
        $product->branch_ids        = $branch_ids;
        $product->category_ids      = $req->category_ids;
        $product->name              = $req->name;
        $product->price             = $req->price;
        $product->short_description = $req->short_description;
        $product->quick_details     = $req->quick_details;
        $product->tags              = $req->tags;
        $product->uuid              = $req->uuid;
        $product->days_of_return    = 7; // @AES Default wblue days of return;
        $product->years_of_warranty = 1; // @AES Default years of waranty
        $product->length            = $req->length;
        $product->width             = $req->width;
        $product->height            = $req->height;
        $product->weight            = $req->weight;
        $product->delivery_option   = $req->delivery_option;
        $product->dimensions        = 0;
        $product->is_approved       = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $product->process_by        = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $product->request_by        = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $product->status            = auth()->user()->type_info == 'administrator' ? 1 : 0;

        if ($req->has('used_owned') && $req->used_owned){
            $product->days_of_return    = $req->days_of_return;
            $product->years_of_waranty  = $req->years_of_waranty;
        }

        if ($req->has('on_sale') && $req->on_sale) {
            if ($req->sale_price <= 0) {
                DB::rollBack();
                return res(__('product.invalid_price'), null, 400);
            }

            $validator = Validator::make($req->all(), [
                'sale_date_from'    => 'required',
                'sale_date_to'      => 'required',
            ]);

            if ($validator->fails()) {
                DB::rollBack();
                return res('Failed', $validator->errors(), 412);
            }

            $product->sale_price        = $req->sale_price;
            $product->sale_date_from    = $req->sale_date_from;
            $product->sale_date_to      = $req->sale_date_to;
        }
        

        if ($req->has('stock_status_id')) {
            $product->stock_status_id = $req->stock_status_id;
        }
        if ($req->has('sku')) {
            $product->sku = $req->sku;
        }
        
        if ($req->has('processing_time')) {
            $product->processing_time = $req->processing_time;
        }
        $product->save();
        $new->product_id = $product->id;
        $new->save();
        
        $this->saveCategoryRelation($product->id, $req->category_ids);
        $this->saveTagRelation($product->id, $req->tags);
        // $this->saveDeliveryDetails($product->id, $req->delivery_options); @AES comment out for big changes

        if ($req->has('quantity')) {
            $req->request->add(['product_id' => $product->id]);
            (new InventoryRepository)->add($req);
        }

        if ($req->has('variations')) {
            $this->saveVariations($product->id, $req->variations);
        }
        $images_base64 = json_decode($req->images_base64, true); // ------------- comment for testing--------
        // $images_base64 = $req->images_base64; // ------------- uncomment for testing--------
        if ($req->has('images_base64') && count($images_base64) > 0) {

            $path = public_path() . '/images/products';
            if(!file_exists($path)){
                File::makeDirectory(base_path() . '/public/images/products');
            }
            File::makeDirectory(base_path() . '/public/images/products/' . encode($product->id, 'uuid'), $mode = 0777, true, true);
            foreach ($images_base64 as $key => $value) {
                $pi                 = new ProductImage;
                $pi->product_id     = $product->id;
                $pi->base64         = $value;
                $pi->file_name      = $product->name . '-' . $key;
                $pi->save();

                $product_path = public_path() . '/images/products/' . encode($product->id, 'uuid') . '/';

                $make = Image::make($value);
                $make->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $make->save($product_path . encode($pi->id, 'uuid') . '-full.png');

                $make->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $make->save($product_path . encode($pi->id, 'uuid') . '-thumbnail.png');
            }
        }
        DB::commit();
        return res('Success', $product->hashid, 201);
    }

    public function update($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        if (!$req->has('user_id')) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if ($req->user_id != auth()->user()->id) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'product_type_id'   => 'required',
            'category_ids'      => 'required',
            'name'              => 'required',
            'price'             => 'required',
            'short_description' => 'required',
            'long_description'  => 'required',
            'tags'              => 'required',
            'delivery_options'  => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $product = Product::find($req->product_id);
        if (!$product) {
            return res(__('product.not_found'), null, 400);
        }

        $product->store_id          = $req->store_id;
        $product->product_type_id   = $req->product_type_id;
        $product->category_ids      = $req->category_ids;
        $product->name              = $req->name;
        $product->price             = $req->price;
        $product->short_description = $req->short_description;
        $product->long_description  = $req->long_description;
        $product->tags              = $req->tags;
        $product->status            = $req->status;
        $product->is_requested      = $req->is_requested;
        $product->is_deleted        = $req->is_deleted;
        

        if ($req->has('sale_price')) {
            if ($req->sale_price > 0) {
                $validator = Validator::make($req->all(), [
                    'sale_date_from'    => 'required',
                    'sale_date_to'      => 'required',
                ]);
                if ($validator->fails()) {
                    return res('Failed', $validator->errors(), 412);
                }

                $product->sale_price        = $req->sale_price;
                $product->sale_date_from    = $req->sale_date_from;
                $product->sale_date_to      = $req->sale_date_to;
            }
        }

        if ($req->has('stock_status_id')) {
            $product->stock_status_id = $req->stock_status_id;
        }
        if ($req->has('sku')) {
            $product->sku = $req->sku;
        }
        if ($req->has('dimensions')) {
            $product->dimensions = $req->dimensions;
        }
        if ($req->has('weight')) {
            $product->weight = $req->weight;
        }
        if ($req->has('processing_time')) {
            $product->processing_time = $req->processing_time;
        }
        $product->save();

        $this->saveCategoryRelation($product->id, $req->category_ids);
        $this->saveDeliveryDetails($product->id, $req->delivery_options);

        if ($req->has('variations')) {
            $this->saveVariations($req->product_id, $req->variations, 'update');
        }

        return res('Success', $product);
    }

    private function saveDeliveryDetails($product_id, $delivery_options)
    {
        ProductShipping::where('product_id', $product_id)->update(['status' => 0]);
        foreach ($delivery_options as $value) {
            if ($value['active'] && $value['category_id']) {
                $ps = ProductShipping::where('product_id', $product_id)->where('shipping_category_id', $value['category_id'])->first();
                if (!$ps) {
                    $ps                         = new ProductShipping;
                    $ps->product_id             = $product_id;
                    $ps->shipping_category_id   = $value['category_id'];
                }
                $ps->shipping_id        = $value['shipping_id'];
                $ps->only_state_ids     = $value['only_ids'];
                $ps->except_state_ids   = $value['except_ids'];
                $ps->filter_by          = $value['filter_by'];
                $ps->status             = 1;
                $ps->save();
            }
        }
    }

    private function saveCategoryRelation(int $product_id, $category_ids)
    {
       // $arr = json_decode($category_ids, true); // ------------- comment for testing--------
        $arr = $category_ids; // ------------- uncomment for testing--------

        ProductCategory::where('product_id', $product_id)->delete();
        foreach ($arr as $id) {
            $pc                 = new ProductCategory;
            $pc->uid            = encode(now()->timestamp + $id, 'entry-id');
            $pc->product_id     = $product_id;
            $pc->category_id    = $id;
            $pc->save();
        }
    }

    private function saveTagRelation(int $product_id, $tags)
    {
        $arr = json_decode($tags, true); // ------------- comment for testing--------
        // $arr = $tags; // ------------- uncomment for testing--------

        ProductTag::where('product_id', $product_id)->delete();
        if (count($arr) > 5 && count($arr) < 0) return false;

        foreach ($arr as $tag){
            $pt             = new ProductTag;
            $pt->product_id = $product_id;
            $pt->name       = $tag;
            $pt->save();
        }
    }

    public function saveVariations($product_id, $variations, $transaction = 'add')
    {
        $arr = json_decode($variations, true); // ------------- comment for testing--------
        // $arr = $variations; // ------------- comment for testing--------

        $collection = collect($arr)->pluck('name');
        $table = (new ProductAttribute())->getTable();
        DB::table($table)->where('status', 1)->whereNotIn('name', $collection)->update(['status' => 0]);
        
        if (count($arr) > 0) {
            foreach ($arr as $value) {
                if ($transaction === 'add') {
                    $name = strtolower($value['name']);
                } else {
                    $name = strtolower($value['old_name']);
                }
                $var = ProductAttribute::where('product_id', $product_id)->where('name', $name)->where('status', 1)->first();
                if (!$var) {
                    $var = new ProductAttribute();
                    $var->product_id = $product_id;
                    $var->name = strtolower($value['name']);
                }
                $var->values = $value['values'];
                $var->save();
            }
        }
    }

    public function uploadImage($req)
    {
        // TODO: Main logic here that returns url
    }

    public function types($req)
    {
        $types = ProductType::where('status', 1)->get();
        $types->makeHidden(['created_at', 'updated_at', 'status']);
        return res('Success', $types);
    }

    public function stockStatus($req)
    {
        $list = StockStatus::where('status', 1)->get()->map(function ($item) {
            $item->name = title_case($item->name);
            return $item;
        });
        $list->makeHidden(['status', 'created_at', 'updated_at']);
        return res('Success', $list);
    }

    public function mostPopular($req)
    {
        $limit = 10;
        if ($req->has('limit')) {
            $limit = $req->limit;
        }
        if ($req->has('user_id') && $req->user_id !== null) {
            Auth::loginUsingId($req->user_id);
        }
        if ($req->has('store_id') && $req->store_id !== null) {
            $list = Product::with('reviews')->where('store_id', $req->store_id)->where('status', 1)->where('is_deleted', 0)->limit($limit)->get()->sortByDesc(function($product) {
                return $product->reviews->sum('rating');
            });
        } else {
            $list = Product::with('reviews')->where('status', 1)->where('is_deleted', 0)->limit($limit)->get()->sortByDesc(function($product) {
                return $product->reviews->sum('rating');
            });
        }
        $collection = collect($list);
        $list->makeHidden(['delivery_options']);
        $collection = $collection->filter(function ($item) {
            // dump(count($item->delivery_options_2));
            return count($item->delivery_options_2) > 0;
        })->values();

        return res(count($collection), $collection);
        // return res('Success', $list);
    }

    public function uploadImages($req)
    {
        if (!$req->has('uuid')) {
            return res('uuid is required', null, 400);
        }
        if (!$req->has('data')) {
            return res('data is required', null, 400);
        }
        if (count(json_decode($req->data)) > 8) {
            return res('Failed. Max of 8 images only', count(json_decode($req->data)), 400);
        }

        Redis::set('images:products:' . $req->uuid, $req->data);

        return res('Success', count(json_decode($req->data)));
    }

    public function add_image_product($req)
    {
        $permissions = ['administrator', 'admin Staff', 'vendor', 'vendor staff', 'blogger'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_found'), null, 402);
        }

        $v = Validator::make($req->all(),[
            'product_hash_id' => 'required',
            'base64'          => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $phi = decode($req->product_hash_id, 'uuid');

        $img                        = new ProductImage;
        $img-> product_id           = $phi;
        $img-> base64               = $req->base64;
        $img-> save();

        $full = public_path() . '/images/products/' . $req->product_hash_id . '-full.png';
        $thumbnail = public_path() . '/images/products/' . $req->product_hash_id . '-thumbnail.png';

        if(file_exists($full) && file_exists($thumbnail)) {
            File::delete($full);
            File::delete($thumbnail);
        }

        $local_path = public_path() . '/images/products/';
        File::makeDirectory($local_path, 0777, true, true);

        $image = Image::make($img->base64);
        $image->save($local_path . encode($phi, 'uuid') . '-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($local_path . encode($phi, 'uuid') . '-thumbnail.png');

        return res('success', null, 200);
    }

    public function getProductPhotos($req)
    {
        if (!$req->has('uuid')) {
            return res('uuid is required', null, 400);
        }
        $data = Redis::get('images:products:' . $req->uuid);
        if (!$data) {
            return res('Success', []);
        }

        return res('Success', json_decode($data));
    }

    public function toggleToFavorite($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'status' => 'required|min:0|max:1'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user_id = auth()->user()->id;
        $wish = WishList::where('user_id', $user_id)->where('product_id', $req->product_id)->first();
        if (!$wish) {
            $wish = new WishList();
            $wish->user_id = $user_id;
            $wish->product_id = $req->product_id;
        }
        $wish->status = $req->status;
        $wish->save();

        return res($wish->status == 1 ? 'Added to WishList' : 'Removed from WishList', (int)$wish->status);
    }

    public function details($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        
        // TODO:: get user location state for $state = all
        $state = 'all';
        if ($req->has('state')) {
            $state = $req->state;
        }
        if (!auth()->user()) {
            if ($req->has('user_id')) {
                Auth::loginUsingId($req->user_id);
            }
        }

        $product = Product::where('id', $req->product_id)->where('status', 1)->where('is_deleted', 0)->first();
        if (!$product) {
            return res(__('validation.not_found'), null, 400);
        }

        $stateObj = State::where('name', $state)->first();
        $delivery_options = deliveryOptions($product->id, $stateObj);
        $product->makeHidden(['delivery_options', 'delivery_options_2', 'store_info']);
        $collection = collect($product);
        $collection->put('delivery_options_2', $delivery_options);

        return res('Success', $collection);
    }

    public function items($req)
    {
        $limit = 10;

        if($req->has('limit')) $limit = $req->limit;

        if ($req->store_id) {
            $store_id = decode($req->store_id, 'uuid');
            $list = Product::where('store_id', $store_id)->where('is_approved', 1)->paginate($limit);
        } else {
            $list = Product::where('is_deleted', 0)->paginate($limit);
        }
        
        $data = new ProductListCollection($list);

        return res('Success', $data);
    }

    public function updateStatus($req)
    {
        if (auth()->user()->type_info != 'administrator') {
            return res(__('vendor.not_allowed'), null, 400);
        }
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'action' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        
        $p = Product::find($req->product_id);
        if (!$p) {
            return res('Invalid Product', null, 400);
        }
        if ($req->action == 'post') {
            $p->is_deleted = 0;
            $p->status = 1;
            $p->is_requested = 0;
            $p->save();
            \recordOnProduct($p->id, $req->action, 200);
            return res('Success', $p, 200);
        } elseif ($req->action == 'delete') {
            $p->is_deleted = 1;
            $p->is_requested = 0;
            $p->save();
            \recordOnProduct($p->id, $req->action, 200);
            return res('Success', $p, 200);
        }

        return res('Error! Invalid action', null, 400);
    }

    public function customerReviews($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $list = ProductReview::where('product_id', $req->product_id)->where('status', 1)->get();
        
        return res('Success', $list);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchProduct($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $product = Product::where('name', 'like', '%' . $req->key . '%')->where('status', 1)->paginate($req->limit);
        $product = new ProductListCollection($product);

        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listByTags($req)
    {
        if(!$req->tags) return res('tags is required', null, 400);

        if(!$req->limit) return res('limit is required', null, 400);

        $product = Product::where('tags', 'like', '%' . $req->tags . '%')->paginate($req->limit);
        $product = new ProductListCollection($product);

        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listByCategory($req)
    {
        if(!$req->category_ids) return res('category ID is required', null, 400);

        if(!$req->limit) return res('limit is required', null, 400);

        $product = Product::where('category_ids', 'like', '%' . $req->category_ids . '%')->paginate($req->limit);
        $product = new ProductListCollection($product);

        return res('success', $product, 200);
    }

    public function searchProductImage($req)
    {
        if(!$req->prod_image) return res('tags is required', null, 400);

        $prod_image_id = ProductImage::where('base64', $req->prod_image)
        ->first()->id;

        $prod_category_id = Product::where('product_id', $prod_image_id)
        ->where('status', 1)
        ->first()
        ->id;

        $products = Product::where('category_ids', $prod_category_id)
        ->where('status', 1)
        ->get();

        $products = new ProductListCollection($products);

        return res('success', $products, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listByName($req)
    {
        if(!$req->name) return res('name is required', null, 400);

        if(!$req->limit) return res('limit is required', null, 400);

        $product = Product::where('name', 'like', '%' . $req->name . '%')->paginate($req->limit);
        $product = new ProductListCollection($product);

        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listByPrice($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        if(is_null($req->max) && !is_null($req->min)){
            $product = Product::where('price', '>=', $req->min)->paginate($req->limit);
            $product = new ProductListCollection($product);
        }

        if(!is_null($req->max) && is_null($req->min)){
            $product = Product::where('price', '<=', $req->max)->paginate($req->limit);
            $product = new ProductListCollection($product);
        }

        if(!is_null($req->max) && !is_null($req->min)){
            $product = Product::where('price', '>=', $req->min)->where('price', '<=', $req->max)->paginate($req->limit);
            $product = new ProductListCollection($product);
        }

        else{

        $product = Product::paginate($req->limit);
        $product = new ProductListCollection($product);
        }
        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listBySort()
    {
        $limit = 10;

        $product = Product::orderByRaw('LENGTH(name)', 'ASC')->orderBy('name', 'ASC')->paginate($limit);
        $product = new ProductListCollection($product);

        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listOfTags($req)
    {
        $duplicates = ProductTag::selectRaw("name, count('name') as count")->groupBy('name')->Paginate($req->limit)->items();
        return res('success', $duplicates, 200);
    }
    

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addBrand($req)
    {
        $permissions = ['agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin and agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'name' => 'required|unique:brands,name',
            'base64' => 'required',
        ], [
            'name.unique' => 'The Brand Name has already been taken',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $new = new StoreChangeField;
        $new->type = 'add brand';
        $new->data = $req->except('base64');
        $new->request_by = auth()->id();
        $new->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;

        $brand = new Brand;
        $brand->name = $req->name;
        $brand->profile_base64 = $req->base64;
        $brand->is_premium = $req->isPremium;
        $brand->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $brand->request_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $brand->process_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $brand->status = auth()->user()->type_info == 'administrator' ? 1 : 0;

        $brand->save();
        $new->brand_id = $brand->id;
        $new->save();

        $path = public_path() . '/images/brands';

        if(!file_exists($path)) {
            File::makeDirectory(base_path() . '/public/images/brands', $mode = 0777, true, true);
        }

        $local_path = public_path() . '/images/brands/';

        $image = Image::make($brand->profile_base64);
        $image->save($local_path . encode($brand->id, 'uuid') . '-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($local_path . encode($brand->id, 'uuid') . '-thumbnail.png');

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function updateBrand($req)
    {
        $permissions = ['agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin and agent can do this process', null, 401);
        }

        $brand_id = decode($req->brand_hashid, 'uuid');

        $brand = Brand::where('id', $brand_id)->where('status', 1)->first();
        if(!$brand) return res(__('product.brand_not_found'), null, 404);

        if(auth()->user()->type_info == 'administrator'){

        if($req->name) $brand->name = $req->name;

        if($req->profile_base64) {
            $brand->profile_base64 = $req->profile_base64;

            $full = public_path() . '/images/brands/' . $brand->hashid . '-full.png';
            $thumbnail = public_path() . '/images/brands/' . $brand->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/brands/';

            $image = Image::make($brand->profile_base64);
            $image->save($local_path . encode($brand->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($brand->id, 'uuid') . '-thumbnail.png');
        }
        
            $brand->save();
            return res('success', null, 200);
        }

        if(auth()->user()->type_info == 'agent'){

            $new = new StoreChangeField;
            $new->brand_id = $brand->id;
            $new->type = 'update brand photo';
            $new->data = $req->except('profile_base64', 'brand_hashid');
            $new->request_by = auth()->id();
            $new->base64 = $req->profile_base64;
            $new->save();
            
            if($req->profile_base64){
            $path = public_path() . '/images/store-requests';
            if (!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/store-requests', $mode = 0777, true, true);
            }
            File::makeDirectory(base_path() . '/public/images/store-requests/' . encode($new->id, 'uuid'), $mode = 0777, true, true);
            
            $request_path = public_path() . '/images/store-requests/' . encode($new->id, 'uuid') . '/';
            
            $image = Image::make($new->base64);
            $image->save($request_path . 'profile-photo-full.png');
            $image->resize(150, 150);
            $image->save($request_path . 'profile-photo-thumbnail.png');
            
            }
            }
            
    
            return res('success', null, 200);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listBrand($req)
    {
        $validator = Validator::make($req->all(), ['status' => 'required','limit' => 'required',]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        if($req->status === 'approved') $brands = Brand::where('name', 'LIKE', '%' . $req->key . '%')->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->status === 'declined') $brands = Brand::where('name', 'LIKE', '%' . $req->key . '%')->where('is_declined', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->status === 'pending')  $brands = Brand::where('name', 'LIKE', '%' . $req->key . '%')->where('is_approved', 0)->orderBy('created_at', 'desc')->where('is_declined', 0)->paginate($req->limit);
        if($req->status === 'all')  $brands = Brand::where('name', 'LIKE', '%' . $req->key . '%')->orderBy('created_at', 'desc')->paginate($req->limit);
        $data = new BrandCollection($brands);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchBrand($req)
    {
        $brands = Brand::where('name', 'like', '%' . $req->key . '%')->paginate($req->limit);

        $data = new BrandCollection($brands);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandListByName($req)
    {
        $brands = Brand::where('name', 'like', '%' . $req->name . '%')->paginate($req->limit);

        $data = new BrandCollection($brands);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandListBySort()
    {
        $limit = 10;
        $brands = Brand::orderByRaw('LENGTH(name)', 'ASC')->orderBy('name', 'ASC')->paginate($limit);

        $data = new BrandCollection($brands);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listByStatus($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        if(!$req->status) return res('status is required', null, 400);

        switch($req->status){
            case 'pending' :
            $brand = StoreChangeField::where('type', "add brand")->where('data->name', 'like', '%' .$req->key. '%')->where('is_approved', 0)->where('is_declined', 0)->paginate($req->limit);
            $data = new BrandChangeFieldCollection($brand);
            return res('success', $data, 200);
            break;

            case 'approved' :
            $brand = StoreChangeField::where('type', "add brand")->where('data->name', 'like', '%' .$req->key. '%')->where('is_approved', 1)->paginate($req->limit);
            $data = new BrandChangeFieldCollection($brand);
            return res('success', $data, 200);
            break;

            case 'declined' :
            $brand = StoreChangeField::where('type', "add brand")->where('data->name', 'like', '%' .$req->key. '%')->where('is_declined', 1)->paginate($req->limit);
            $data = new BrandChangeFieldCollection($brand);
            return res('success', $data, 200);
            break;

            case 'all' :
            $brand = StoreChangeField::where('type', "add brand")->where('data->name', 'like', '%' .$req->key. '%')->paginate($req->limit);
            $data = new BrandChangeFieldCollection($brand);
            return res('success', $data, 200);
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalBrands()
    {
        $brands = Brand::count('id');
        return res('success', $brands, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function getBrands()
    {
        $brands = Brand::where('status', 1)->get();

        $data = new BrandCollection($brands);
        return res('success', $data, 200);
    }

    public function brandShow($req)
    {
        $brand_id = decode($req->brand_hashid, 'uuid');

        $brands = Brand::where('id', $brand_id)->first();
        if(!$brands) return res('brand not found', null, 400);

        $data = new BrandShow($brands);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addCategory($req)
    {
        if (auth()->user()->type_info != 'administrator') {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'parent_id' => 'required',
            'name' => 'required|unique:categories,name',
        ], [
            'name.unique' => 'The Category name has already been taken',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $category = new Category;
        $category->parent_id = $req->parent_id;
        $category->name = $req->name;
        $category->status = 1;
        $category->save();

        return res('success', encode($category->id, 'uuid'), 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function updateCategory($req)
    {
        if (auth()->user()->type_info != 'administrator') {
            return res(__('user.not_allowed'), null, 400);
        }
        if(!$req->category_hashid) return res('category hashid is required', null, 400);

        $id = decode($req->category_hashid, 'uuid');

        $cat = Category::where('id', $id)->first();

        $validator = Validator::make($req->all(), [
            'parent_id' => 'required',
            'name' => 'required|unique:categories,name',
            'status' => 'required',
        ], [
            'name.unique' => 'The Category name has already been taken',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        if($req->parent_id)$cat->parent_id = $req->parent_id;
        if($req->name)$cat->name = $req->name;
        if($req->status === 'active'){$cat->status = 1;}
        else{$cat->status = 0;}
        $cat->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function deleteCategory($req)
    {
        if (auth()->user()->type_info != 'administrator') {
            return res(__('user.not_allowed'), null, 400);
        }

        $id = decode($req->category_hashid, 'uuid');

        $cat = Category::where('id', $id)->first();
        if(!$cat) return res(__('category.not_found'), null, 404);

        $cat->delete();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addTags($req)
    {

        $validator = Validator::make($req->all(), [
            'name' => 'required|unique:tags,name',
        ], [
            'name.unique' => 'The Category name has already been taken',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $tag = new Tag;
        if($req->name)$tag->name = $req->name;
        $tag->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function updateTags($req)
    {
        if(!$req->tag_hashid) return res('tag hashid is required', null, 400);
        
        $id = decode($req->tag_hashid, 'uuid');

        $tag = Tag::where('id', $id)->first();
        if(!$tag) return res(__('tag.not_found'), null, 404);

        $validator = Validator::make($req->all(), [
            'tag_hashid' => 'required',
            'name' => 'required|unique:categories,name',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        if($req->name){$tag->name = $req->name;}
        if($req->status === 'active'){$tag->status = 1;}
        else{$tag->status = 0;}
        $tag->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchTags($req)
    {
        if($req->status === 'active'){
        $tag = Tag::where('name', 'like', '%' .$req->key. '%')->where('status', 1)->paginate($req->limit);
        $data = new TagCollection($tag);
        }

        if($req->status === 'inactive'){
        $tag = Tag::where('name', 'like', '%' .$req->key. '%')->where('status', 0)->paginate($req->limit);
        $data = new TagCollection($tag);
        }
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productChangesStatus($req)
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        if(!$req->product_hashid) return res(__('validation.identifier'), null, 400);

        $product_id = decode($req->product_hashid, 'uuid');

        $product = Product::where('id', $product_id)->first();
        if(!$product)  return res('invalid branch', null, 404);

        switch(auth()->user()->type_info){
            case 'administrator' :
            if($req->approve === 'approve'){ 
            $product->is_approved = 1;
            $product->is_declined = 0;
            $product->process_by = auth()->id();
            }
            if($req->approve === 'decline'){ 
            $product->is_declined = 1;
            $product->is_approved = 0;
            $product->process_by = auth()->id();
            }
            if($req->approve === 'pending'){ 
            $product->is_approved = 0;
            $product->is_declined = 0;
            $product->process_by = auth()->id();
            }
            if($req->status === 'active') $product->status = 1;
            if($req->status === 'inactive') $product->status = 0;
            $product->save();
            return res('success', null, 200);
            break;

            case 'vendor' :
            if($req->status === 'active') $product->status = 1;
            if($req->status === 'inactive') $product->status = 0;
            $product->save();
            return res('success', null, 200);
            break;

            case 'agent' :
            if($req->status === 'active') $product->status = 1;
            if($req->status === 'inactive') $product->status = 0;
            $product->save();
            return res('success', null, 200);
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandChangeStatus($req)
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        if(!$req->brand_hashid) return res(__('validation.identifier'), null, 400);

        $brandt_id = decode($req->brand_hashid, 'uuid');

        $brand = Brand::where('id', $brandt_id)->first();
        if(!$brand)  return res('invalid branch', null, 404);

        switch(auth()->user()->type_info){
            case 'administrator' :
            if($req->approve === 'approve'){ 
            $brand->is_approved = 1;
            $brand->is_declined = 0;
            $brand->process_by = auth()->id();
            }
            if($req->approve === 'decline'){ 
            $brand->is_declined = 1;
            $brand->is_approved = 0;
            $brand->process_by = auth()->id();
            }
            if($req->approve === 'pending'){ 
            $brand->is_approved = 0;
            $brand->is_declined = 0;
            $brand->process_by = auth()->id();
            }
            if($req->status === 'active') $brand->status = 1;
            if($req->status === 'inactive') $brand->status = 0;
            $brand->save();
            return res('success', null, 200);
            break;

            case 'vendor' :
            if($req->approve) return res('Only admin can approve', null, 200);
            if($req->status === 'active') $brand->status = 1;
            if($req->status === 'inactive') $brand->status = 0;
            $brand->save();
            return res('success', null, 200);
            break;

            case 'agent' :
            if($req->approve) return res('Only admin can approve', null, 200);
            if($req->status === 'active') $brand->status = 1;
            if($req->status === 'inactive') $brand->status = 0;
            $brand->save();
            return res('success', null, 200);
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productShowInOpenAPI($req)
    {
        if (!$req->has('product_hashid')) return res(__('validation.identifier'), null, 400);

        $product_id = decode($req->product_hashid, 'uuid');
        $product = Product::find($product_id);

        if (!$product) return res(__('product.not_found'), null, 400);
        
        $data= new ProductResource($product);
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productTypeList()
    {
        $productType = ProductType::all();
        $data = Type::collection($productType);
        return res('Success', $data, 200);
    }
}
