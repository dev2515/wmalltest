<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\StoreChangeField;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message' => $this->message,
            'status_type' => $this->status_type,
            'is_seen' => $this->is_seen,
            'changes' => $this->changes(),
        ];
    }

    public function changes()
    {
        $scf = StoreChangeField::where('id', $this->change_field_id)->first();
        if(!$scf) return null;

        switch ($scf->type) {
            case 'informations' :
                return  $scf->data;
            break;

            case 'locations' :
                return $scf->data;
            break;

            default :
                return null;
            break;
        }
    }
}
