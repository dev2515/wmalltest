<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantTable extends Model
{
    protected $table = 'restaurant_tables';
    protected $fillable = ['restaurant_id', 'name', 'description', 'floor', 'status', 'x_axis', 'y_axis', 'table_bg'];
    protected $appends = ['hash_id'];

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Restaurant::class, 'restaurant_id');
    }

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function reservation()
    {
        return $this->hasMany(\App\Models\TableReservation::class, 'table_id');
    }
}
