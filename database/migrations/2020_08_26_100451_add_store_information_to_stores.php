<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreInformationToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'store_information')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('store_information')->default('Welcome to my store!')->after('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'store_information')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('store_information');
            });
        }
    }
}
