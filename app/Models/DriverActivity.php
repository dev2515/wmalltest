<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class DriverActivity extends Model
{
    protected $fillable = ['user_id','total_income'];
    protected $appends = ['hashid'];
    
    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo(DriverStatus::class, 'status_id');
    }
}
