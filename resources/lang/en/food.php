<?php

return [
    'not_found' => 'Food information not found',
    'food_type' => [
        'not_found' => 'Food type information not found',
    ],
    'food_variation' => [
        'not_found' => 'Food variation selection information not found',
        'exists' => 'Food variation selection is already exists.',
        'child' => [
            'not_found' => 'Food variation type selection information not found'
        ]
    ],
    'food_not_bind' => 'Food not found on restaurant_food_available_foods table'  
];