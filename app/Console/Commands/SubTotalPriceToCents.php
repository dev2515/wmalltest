<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RestaurantOrder;
use App\Models\Command as ModelCommand;
use Carbon\Carbon;
class SubTotalPriceToCents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'price-to-cent:restaurant_orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $command = ModelCommand::where('name', $this->signature)->where('is_called', 1)->first();
        if($command){
            $this->info('This command should call at once only');
            return false;
        }

        $orders = RestaurantOrder::get();
        $bar = $this->output->createProgressBar(count($orders));
        $bar->start();
        foreach ($orders as $order){
            $order->sub_total = priceToCent($order->sub_total);
            $order->save();
            $bar->advance();
        }
        $bar->finish();

        $executed = new ModelCommand();
        $executed->name = $this->signature;
        $executed->is_multiple_called = 0;
        $executed->is_called = 1;
        $executed->executed_at = Carbon::now();
        $executed->save();
    }
}
