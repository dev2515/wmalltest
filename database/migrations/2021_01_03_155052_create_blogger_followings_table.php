<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloggerFollowingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('blogger_followings')){
            Schema::create('blogger_followings', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('blogger_id');
                $table->unsignedBigInteger('user_id');
                $table->boolean('is_followed')->default(0);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('blogger_followings')){
            Schema::dropIfExists('blogger_followings');
        }
    }
}
