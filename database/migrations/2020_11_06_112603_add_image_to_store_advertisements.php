<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToStoreAdvertisements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_advertisements', 'image')){
            Schema::table('store_advertisements', function (Blueprint $table) {
                $table->longText('image')->nullable()->after('store_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_advertisements', 'image')){
            Schema::table('store_advertisements', function (Blueprint $table) {
                $table->dropColumn('image');
            });
        }
    }
}
