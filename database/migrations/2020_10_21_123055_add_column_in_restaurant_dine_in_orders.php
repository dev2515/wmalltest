<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInRestaurantDineInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_dine_in_orders','is_paid')){
            Schema::table('restaurant_dine_in_orders', function (Blueprint $table) {
                $table->boolean('is_paid')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_dine_in_orders','is_paid')){
            Schema::table('restaurant_dine_in_orders', function (Blueprint $table) {
                $table->dropColumn('is_paid');
            });
        }
    }
}
