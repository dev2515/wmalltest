<?php

namespace App\Interfaces;

interface OrderInterface
{
    public function list($req);

    public function show($req);

    public function paymentMethods($req);

    public function place($req);

    public function orderHistory($req);

    public function trackOrder($req);
}
