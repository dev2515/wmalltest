<?php

namespace App\Console\Commands\Food;

use Illuminate\Console\Command;
use App\Models\RestaurantFood;

class UpdateStatusFood extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-food-status-to-active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the status to 1 of a food.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        RestaurantFood::where('status', 0)->update(['status' => 1]);
        $this->info('All food status hasbeen added');
    }
}
