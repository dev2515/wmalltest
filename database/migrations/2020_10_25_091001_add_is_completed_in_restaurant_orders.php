<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCompletedInRestaurantOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_orders','is_completed')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->boolean('is_completed')->default(0);
            });
        }

        if(!Schema::hasColumn('restaurant_order_lists','is_completed')){
            Schema::table('restaurant_order_lists', function (Blueprint $table) {
                $table->boolean('is_completed')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_orders','table_id')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('is_completed');
            });
        }

        if(Schema::hasColumn('restaurant_order_lists','table_id')){
            Schema::table('restaurant_order_lists', function (Blueprint $table) {
                $table->dropColumn('is_completed');
            });
        }
    }
}
