<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusIdToDriverActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_activities', 'status_id')){
            Schema::table('driver_activities', function (Blueprint $table) {
                $table->integer('status_id')->default(3)->after('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_activities', 'status_id')){
            Schema::table('driver_activities', function (Blueprint $table) {
                $table->dropColumn('status_id');
            });
        }
    }
}
