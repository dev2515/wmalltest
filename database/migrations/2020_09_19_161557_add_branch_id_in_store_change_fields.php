<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchIdInStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_change_fields', 'branch_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->integer('branch_id')->nullable()->after('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'branch_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('branch_id');
            });
        }
    }
}
