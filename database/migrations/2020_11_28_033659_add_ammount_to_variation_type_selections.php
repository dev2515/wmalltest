<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmmountToVariationTypeSelections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('variation_type_selections', 'amount')){
            Schema::table('variation_type_selections', function (Blueprint $table) {
                $table->integer('amount')->after('price')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('variation_type_selections', 'amount')){
            Schema::table('variation_type_selections', function (Blueprint $table) {
                $table->dropColumn('amount');
            });
        }
    }
}
