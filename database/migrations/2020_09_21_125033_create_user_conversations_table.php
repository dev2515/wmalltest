<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('user_conversations')){
            Schema::create('user_conversations', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('conversation_id');
                $table->string('text')->nullable();
                $table->integer('from_user_id')->nullable();
                $table->integer('from_store_id')->nullable();
                $table->string('type')->default('message');
                $table->integer('product_id')->nullable();
                $table->integer('order_id')->nullable();
                $table->longText('base64')->nullable();
                $table->boolean('is_new')->default(1);
                $table->boolean('deleted_store')->default(0);
                $table->boolean('deleted_user')->default(0);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_conversations');
    }
}
