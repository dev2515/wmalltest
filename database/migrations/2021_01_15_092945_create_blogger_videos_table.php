<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloggerVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogger_videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('video_title');
            $table->string('video_thumbnail');
            $table->string('video_url');
            $table->string('video_description')->nullable();
            $table->string('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogger_videos');
    }
}
