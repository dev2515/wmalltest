<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use App\User;
use App\Models\RestaurantCartItem;

class DriverHistoryList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'             => $this->hashid,
            'transaction_number' => $this->transaction_number,
            'customer_name'      => $this->customerName(),
            'package_type'       => $this->packageType->name,
            'payment_type'       => $this->order()->pay_method->name,
            'total_items'        => $this->numberOfItems(),
            'amount'             => $this->amount(),
            'status'             => $this->order()->status_info
        ];
    }

    public function order()
    {
        $cart = RestaurantCart::where('transaction_number', $this->transaction_number)->first();
        if(!$cart) return __('cart.not_found');

        $order = RestaurantOrder::where('cart_id', $cart->id)->first();
        if(!$order) return __('restaurant.order.not_found');
        
        return $order;
    }

    public function customerName():string
    {
        $user = User::where('id', $this->order()->customer_id)->first();
        if(!$user) return __('user.not_found');

        return $user->name;
    }

    public function numberOfItems():int
    {
        $cart  = RestaurantCart::where('transaction_number', $this->transaction_number)->first();
        if(!$cart) return __('cart.not_found');

        $items = RestaurantCartItem::where('cart_id', $cart->id)->sum('quantity');

        return $items;
    }

    public function amount():string
    {
        $cart  = RestaurantCart::where('transaction_number', $this->transaction_number)->first();
        if(!$cart) return __('cart.not_found');

        $order = RestaurantOrder::where('cart_id', $cart->id)->first();
        if(!$order) return __('restaurant.order.not_found');

        return centToPrice($order->sub_total);
    }
}
