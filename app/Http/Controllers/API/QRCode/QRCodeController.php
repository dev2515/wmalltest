<?php

namespace App\Http\Controllers\API\QRCode;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\QRCodeInterface;

class QRCodeController extends Controller
{
    protected $req;
    protected $qrcode;

    public function __construct(Request $req, QRCodeInterface $qrcode)
    {
        $this->req = $req;
        $this->qrcode = $qrcode;
    }

    public function scan()
    {
        return $this->qrcode->scan($this->req);
    }

    public function bind()
    {
        return $this->qrcode->bind($this->req);
    }
}
