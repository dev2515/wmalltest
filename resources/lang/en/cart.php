<?php

return [
    'item_not_found'        => 'Item not found in the cart',
    'subtract_not_apply'    => 'Subtract not applicable',
    'not_found'             => 'Cart information not found'
];