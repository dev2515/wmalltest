<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverNetIncome extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'credits' => $this->total_income(),
        ];
    }

    public function total_income()
    {
        $c = RestaurantCart::select('id', 'transaction_number')->where('transaction_number', $this->transaction_number)->first();
        $o = RestaurantOrder::select('id','sub_total','shipping_rate','cart_id')->where('cart_id', $c->id)->sum('shipping_rate');
        return $o;
    }
}
