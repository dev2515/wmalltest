<?php

namespace App\Interfaces;

interface RestaurantManagerInterface
{
    public function addFoodInFoodType($req);

    public function addFoodInFoodTypeSubBranch($req);

    public function foodList($req);

    public function addedFoods($req);

    public function notAddedFoods($req);

    public function removeFoodInFoodType($req);

    public function addFood($req);

    public function editFood($req);

    public function deleteFood($req);

    public function addVariationSelection($req);

    public function editVariationSelection($req);

    public function listVariationSelection($req);

    public function deleteVariationSelection($req);

    public function listOfServices($req);

    public function updateOfServices($req);

    public function showFood($req);

    public function addChildVariation($req);

    public function editChildVariation($req);

    public function deleteChildVariation($req);

    public function addBusyduration($req);

    public function stopBusyduration($req);

    public function closeOpen($req);
}