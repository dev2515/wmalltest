<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_category_images')){
            Schema::create('product_category_images', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('store_id');
                $table->unsignedBigInteger('category_id');
                $table->longText('base64_image');
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('product_category_images')){
            Schema::dropIfExists('product_category_images');
        }
    }
}
