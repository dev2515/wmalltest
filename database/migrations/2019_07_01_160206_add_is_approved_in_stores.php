<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsApprovedInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('stores', 'is_approved')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->integer('is_approved')->default(0);
            });
        }
        if (!Schema::hasColumn('stores', 'approved_at')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('approved_at')->default(null)->nullable();
            });
        }
        if (!Schema::hasColumn('stores', 'ban')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->integer('ban')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
