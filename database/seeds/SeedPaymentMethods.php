<?php

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class SeedPaymentMethods extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::firstOrCreate(['slug' => 'cash-on-delivery', 'name' => 'Cash on Delivery', 'instruction' => '']);
        PaymentMethod::firstOrCreate(['slug' => 'bank-transfer', 'name' => 'Bank Transfer', 'instruction' => '']);
        PaymentMethod::firstOrCreate(['slug' => 'paypal', 'name' => 'Paypal', 'instruction' => '']);
    }
}
