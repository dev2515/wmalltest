<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTheme extends Model
{
    protected $table = 'store_themes';
    protected $hidden = ['created_at', 'updated_at'];
    protected $append = ['hashid'];
    protected $casts = [
        'codes' => 'array',
    ];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

}
