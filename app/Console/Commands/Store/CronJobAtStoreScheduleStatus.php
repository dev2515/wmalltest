<?php

namespace App\Console\Commands\Store;

use App\Models\StoreSchedule;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CronJobAtStoreScheduleStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron-execute:store-busy-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and update the store schedule busy status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $schedules = StoreSchedule::where('is_busy', 1)->where('day_id', $now->dayOfWeekIso)->get();
        foreach ($schedules as $schedule){
            if($now > $schedule->is_busy_duration){
                $schedule->is_busy = 0;
                $schedule->is_busy_duration = null;
                $schedule->save();
            }
        }
        $this->info('Execute successfuly');
    }
}
