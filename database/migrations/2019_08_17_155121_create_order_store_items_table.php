<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStoreItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_store_items')) {
            Schema::create('order_store_items', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('order_store_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->string('name');
                $table->double('price');
                $table->integer('quantity');
                $table->double('commission');
                $table->integer('is_rated');
                $table->integer('status')->default(0); // 1:to-pay|2:to-ship|3:to-receive|4:completed|10:cancelled|11:return-refund
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_store_items');
    }
}
