<?php

namespace App\Http\Resources\Restaurant;

use App\Models\FoodReview;
use App\Models\Review;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use stdClass;

class FoodReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'hash_id'                       => encode($this->id, 'uuid'),
            'rate'                          => $this->name,
            'number_of_reviews'             =>  $this->ratings()['total_reviews'],
            'ratings'                       =>  $this->ratings()['total_rate'],
            'review'                        => $this->reviews(),
        ];
    }

    public function ratings()
    {
        $c = DB::table('reviews')->select('type_id')->where('type_id',$this->id)->where('category', 'Food')->count();
        $r = DB::table('reviews')->select('type_id')->where('type_id',$this->id)->where('category', 'Food')->average('rate');

        $collect = collect(['total_reviews' => $c, 'total_rate' => $r]);

        return $collect;
    }

    public function reviews()
    {
        $c = Review::select('id','type_id','rate','content','user_id')->where('type_id',$this->id)->where('category', 'Food')->where('status', 1)->get();
        if($c) $c->makeHidden(['id','type_id','user_id']);

        return $c;
    }
}
