<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsInRestaurantOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_orders', 'shipping_address_latitude')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->double('shipping_address_latitude');
            });
        }

        if(!Schema::hasColumn('restaurant_orders', 'shipping_address_longitude')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->double('shipping_address_longitude');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_orders', 'shipping_address_latitude')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('shipping_address_latitude');
            });
        }

        if(Schema::hasColumn('restaurant_orders', 'shipping_address_longitude')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('shipping_address_longitude');
            });
        }
    }
}
