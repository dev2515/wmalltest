<?php

namespace App\Repositories;

use App\Interfaces\RestaurantManagerInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Store;
use App\Models\FoodType;
use App\Models\RestaurantFood;
use App\Models\FoodTypePivot;
use stdClass;
use App\Http\Resources\RestaurantManager\FoodListCollection;
use App\Events\AddFoodEvent;
use App\Events\EditFoodEvent;
use App\Models\FoodVariationSelection;
use App\Http\Resources\Stores\RestaurantServiceCollection;
use App\Models\RestaurantService;
use App\Models\RestaurantServicePivot;
use App\Http\Resources\Restaurant\FoodRestaurant;
use App\Models\VariationTypeSelection;
use App\Http\Resources\RestaurantManager\FoodVariationCollection;
use App\Models\StoreSchedule;
use Carbon\Carbon;
class RestaurantManagerRepository implements RestaurantManagerInterface
{
    /**
     * @param $req
     * @return JsonResponse
     */
    public function addFoodInFoodType($req):object
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('Only admin and agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'food_type_hashid'      => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack(); // rollback transaction
            return res('Failed', $validator->errors(), 412);
        }

        $food_type_id = decode($req->food_type_hashid, 'uuid');
        $food_type = FoodType::where('id', $food_type_id)->first();
        if(!$food_type){
            DB::rollBack(); // rollback transaction
            return res(__('food.food_type.not_found'), null, 404);
        }

        foreach($req->food_hashids as $food_hashid){
            $food_id = decode($food_hashid, 'uuid');
            
            $food = RestaurantFood::where('id', $food_id)->first();
            if(!$food){
                DB::rollBack(); // rollback transaction
                return res(__('food.not_found') . ', One of your added foods is not found', null, 404);
            }

            $check_another_type = FoodTypePivot::where('store_id', $food->restaurant_id)->where('food_id', $food->id)->where('status', 1)->first();
            if($check_another_type){
                return res('Food name: ' . $food->name .' is already in another type', null, 404);
            }

            $check_food = FoodTypePivot::where('store_id', $food->restaurant_id)->where('food_type_id', $food_type->id)->where('food_id', $food->id)->first();
            if($check_food){
                if($check_food->status == 0){
                    $check_food->status = 1;
                    $check_food->save();
                }
            }else {
                $add                = new FoodTypePivot();
                $add->store_id      = $food->restaurant_id;
                $add->food_type_id  = $food_type->id;
                $add->food_id       = $food->id;
                $add->save();
            }
        }
        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addFoodInFoodTypeSubBranch($req):object
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('Only admin and agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(),
        [       
            'sub_store_hashid'  => 'required',
            'food_type_hashid'  => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack(); // rollback transaction
            return res('Failed', $validator->errors(), 412);
        }

        $food_type_id   = decode($req->food_type_hashid, 'uuid');
        $food_type      = FoodType::where('id', $food_type_id)->first();
        if(!$food_type){
            DB::rollBack(); // rollback transaction
            return res(__('food.food_type.not_found'), null, 404);
        }

        $sub_store_id   = decode($req->sub_store_hashid, 'uuid');
        $sub_store      = Store::where('id', $sub_store_id)->first();
        if(!$sub_store){
            DB::rollBack(); // rollback transaction
            return res(__('store.sub_branch.not_found'), null, 404);
        }

        if($sub_store->is_main_branch != 0){
            DB::rollBack(); // rollback transaction
            return res(__('store.sub_branch.not_authorized'), null, 404);
        }

        foreach($req->food_hashids as $food_hashid){

            $food_id = decode($food_hashid, 'uuid');
            $food = RestaurantFood::where('id', $food_id)->first();
            if(!$food){
                DB::rollBack(); // rollback transaction
                return res(__('food.not_found') . ', One of your added foods is not found', null, 404);
            }

            if($sub_store->main_store_id != $food->restaurant_id){
                DB::rollBack(); // rollback transaction
                return res(__('store.sub_branch.not_equal') . ' restaurant store id', null, 404);
            }

            $check_another_type = FoodTypePivot::where('store_id', $sub_store_id)->where('food_id', $food->id)->where('status', 1)->first();
            if($check_another_type){
                return res('Food name: ' . $food->name .' is already in another type', null, 404);
            }

            $check_food = FoodTypePivot::where('store_id', $sub_store_id)->where('food_type_id', $food_type->id)->where('food_id', $food->id)->first();
            if($check_food){
                if($check_food->status == 0){
                    $check_food->status = 1;
                    $check_food->save();
                }
            }else {
                $add                = new FoodTypePivot();
                $add->store_id      = $sub_store_id;
                $add->food_type_id  = $food_type->id;
                $add->food_id       = $food->id;
                $add->save();
            }
        }

        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function removeFoodInFoodType($req):object
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('Only admin and agent can do this process', null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'food_type_hashid' => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack(); // rollback transaction
            return res('Failed', $validator->errors(), 412);
        }

        $food_type_id   = decode($req->food_type_hashid, 'uuid');
        $food_type      = FoodType::where('id', $food_type_id)->first();
        if(!$food_type){
            DB::rollBack(); // rollback transaction
            return res(__('food.food_type.not_found'), null, 404);
        }
        
        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store){
            DB::rollBack(); // rollback transaction
            return res(__('store.not_found'), null, 404);
        }

        foreach($req->food_hashids as $food_hashid){
            $food_id = decode($food_hashid, 'uuid');
            $food = RestaurantFood::where('id', $food_id)->first();
            if(!$food){
                DB::rollBack(); // rollback transaction
                return res(__('food.not_found') . ', One of your added food is not found', null, 404);
            }

            $check_food = FoodTypePivot::where('store_id', $store_id)->where('food_type_id', $food_type->id)->where('food_id', $food->id)->where('status', 1)->first();
            if($check_food){
                $check_food->status = 0;
                $check_food->save();
            }
        }

        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function foodList($req):object
    {
        $validator = Validator::make($req->all(), [
            'store_hashid'      => 'required',
            'food_type_hashid'  => 'required',
            'limit'             => 'required|int'   
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $food_type_id = decode($req->food_type_hashid, 'uuid');
        $food_type = FoodType::where('id', $food_type_id)->first();
        if(!$food_type){
            return res(__('food.food_type.not_found'), null, 404);
        }

        $food_ids = FoodTypePivot::where('store_id', $store_id)->where('food_type_id', $food_type_id)->where('status', 1)->pluck('food_id');
        if($store->is_main_branch == 0){
            $store_id = $store->main_store_id;
        }
        $foods = RestaurantFood::whereIn('id', $food_ids)->where('restaurant_id', $store_id)->where('name', 'LIKE', '%' . $req->key . '%')->paginate($req->limit);

        $data = new FoodListCollection($foods);
        return res('success', $data, 200);

    } 

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addedFoods($req):object
    {
        $validator = Validator::make($req->all(), [
            'store_hashid'      => 'required',
            'limit'             => 'required'   
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $food_ids = FoodTypePivot::where('store_id', $store_id)->where('status', 1)->pluck('food_id');
        $foods = RestaurantFood::whereIn('id', $food_ids)->where('restaurant_id', $store_id)->where('name', 'LIKE', '%' . $req->key . '%')->paginate($req->limit);

        $data = new FoodListCollection($foods);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function notAddedFoods($req)
    {
        $validator = Validator::make($req->all(), [
            'store_hashid'      => 'required',
            'limit'             => 'required|int'   
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $food_ids = FoodTypePivot::where('store_id', $store_id)->where('status', 1)->pluck('food_id');
        if($store->is_main_branch == 0){
            $store_id = $store->main_store_id;
        }

        $foods = RestaurantFood::whereNotIn('id', $food_ids)->where('restaurant_id', $store_id)->where('name', 'LIKE', '%' . $req->key . '%')->paginate($req->limit);
        $data = new FoodListCollection($foods);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addFood($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'store_hashid'  => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store){
            DB::rollBack();
            return res(__('store.not_found'), null, 404);
        }

        if($store->is_main_branch != 1) {
            $store_id = $store->main_store_id;
        }

        foreach ($req->foods as $food){
            $validator = Validator::make($food, [
                'name'  => 'required',
                'price' => 'required'
            ]);
    
            if ($validator->fails()) {
                DB::rollBack();
                return res('Failed', $validator->errors(), 412);
            }

            $add_food                   = new RestaurantFood();
            $add_food->restaurant_id    = $store_id;
            $add_food->name             = $food['name'];
            $add_food->price            = priceToCent($food['price']);
            $add_food->description      = $food['description'] != null ? $food['description'] : '';
            $add_food->food_image       = $food['image'];
            $add_food->request_by       = auth()->id();   
            $add_food->process_by       = auth()->user()->type_info === 'administrator' ? auth()->id() : 0;
            $add_food->is_approved      = auth()->user()->type_info === 'administrator' ? 1 : 0;
            $add_food->is_declined      = 0;
            $add_food->is_available     = 1;
            $add_food->status           = 1;
            $add_food->category_id      = 0;
            $add_food->save();

            $data = new stdClass;
            $data->model                = $add_food;
            $data->variation_selections = $req->variation_selections;

            event(new AddFoodEvent($data));
        }

        DB::commit();
        return res('success', $add_food->hash_id, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function editFood($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'food_hashid'   => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $food_id = decode($req->food_hashid, 'uuid');
        $food = RestaurantFood::where('id', $food_id)->first();
        if(!$food){
            DB::rollBack();
            return res(__('food.not_found'), null, 401);
        }

        $data = new stdClass;
        $data->food = $food;
        $data->req  = $req->all();

        event(new EditFoodEvent($data));

        DB::commit();
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function deleteFood($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'food_hashid'   => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $food_id = decode($req->food_hashid, 'uuid');
        $food = RestaurantFood::where('id', $food_id)->first();
        if(!$food){
            DB::rollBack();
            return res(__('food.not_found'), null, 401);
        }

        $food->status = 0;
        $food->save();

        DB::commit();
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listVariationSelection($req):object
    {
        $validator = Validator::make($req->all(), [
            'food_hashid'   => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $food_id    = decode($req->food_hashid, 'uuid');
        $list       = FoodVariationSelection::where('food_id', $food_id)->where('status', 1)->get();
        $data       = new FoodVariationCollection($list);
        
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addVariationSelection($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'food_hashid'           => 'required|string',
            'variation_name'        => 'required|string',
            'is_required'           => 'required|boolean',
            'is_multiple'           => 'required|boolean',
            'selection_max_number'  => 'required|int',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $food_id    = decode($req->food_hashid, 'uuid');
        $food       = RestaurantFood::where('id', $food_id)->first();
        if(!$food){
            DB::rollBack();
            return res(__('food.not_found'), null, 404);
        }

        $variation = new FoodVariationSelection();
        $variation->food_id                = $food_id;
        $variation->name                   = $req->variation_name;
        $variation->is_required            = $req->is_required;
        $variation->is_multiple            = $req->is_multiple;
        $variation->selection_max_number   = $req->selection_max_number;
        $variation->save();

        foreach ($req->variation_type_selections as $selection_type){
            $validator = Validator::make($selection_type, [
                'name'      => 'required|string',
                'price'     => 'required',
                'is_added_price'  => 'required|boolean'
            ]);
    
            if ($validator->fails()) {
                DB::rollBack();
                return res('Failed', $validator->errors(), 412);
            }

            $variation_type = new VariationTypeSelection();
            $variation_type->food_variation_id  = $variation->id;
            $variation_type->name       = $selection_type['name'];
            $variation_type->amount      = priceToCent($selection_type['price']);
            $variation_type->is_added_price   = $selection_type['is_added_price'];
            $variation_type->save();
        }
        
        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function editVariationSelection($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'variation_hashid'      => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $variation_id       = decode($req->variation_hashid, 'uuid');
        $variation          = FoodVariationSelection::where('id', $variation_id)->where('status', 1)->first();
        if(!$variation){
            DB::rollBack();
            return res(__('food.food_variation.not_found'), null, 404);
        }

        if($req->name != null){
            $variation->name = $req->name;
        }

        if($req->is_required != null){
            $variation->is_required = $req->is_required;
        }

        if($req->is_multiple != null){
            $variation->is_multiple = $req->is_multiple;
        }

        if($req->selection_max_number != null){
            $variation->selection_max_number = $req->selection_max_number;
        }

        $variation->save();
        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function deleteVariationSelection($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'variation_hashid'      => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $variation_id       = decode($req->variation_hashid, 'uuid');
        $variation          = FoodVariationSelection::where('id', $variation_id)->where('status', 1)->first();
        if(!$variation){
            DB::rollBack();
            return res(__('food.food_variation.not_found'), null, 404);
        }

        $variation->status = 0;
        $type_selections   = VariationTypeSelection::where('food_variation_id', $variation_id)->where('status', 1)->update(['status' => 0]);

        $variation->save();
        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listOfServices($req):object
    {
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid' => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid ,'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $data = new RestaurantServiceCollection($store->restaurantServices);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function updateOfServices($req):object
    {
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid'      => 'required',
            'service_hashid'    => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id   = decode($req->store_hashid ,'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $service_id = decode($req->service_hashid, 'uuid');
        $service    = RestaurantService::where('id', $service_id)->first();
        if(!$service){
            return res(__('restaurant.services.not_found'), null, 404);
        } 

        $pivot      = RestaurantServicePivot::where('store_id', $store_id)->where('restaurant_service_id', $service_id)->first();
        if(!$pivot){
            return res(__('restaurant.services.not_binded'), null, 404);
        }

        $pivot->is_active = $pivot->is_active == 1 ? 0 : 1;
        $pivot->save();

        return res('success', $pivot->is_active, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function showFood($req):object
    {
        $validator = Validator::make($req->all(), [
            'food_hashid'      => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $food_id = decode($req->food_hashid, 'uuid');
        $food = RestaurantFood::where('id', $food_id)->first();
        if(!$food){
            return res(__('food.not_found'), null, 404);
        }

        $data = new FoodRestaurant($food);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addChildVariation($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'variation_hashid'  => 'required',
            'name'              => 'required|string',
            'price'             => 'required',
            'is_added_price'    => 'required|boolean',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $variation_id   = decode($req->variation_hashid, 'uuid');
        $variation      = FoodVariationSelection::where('id', $variation_id)->where('status', 1)->first();
        if(!$variation){
            DB::rollBack();
            return res(__('food.food_variation.not_found'), null, 404);
        }

        $add_child = new VariationTypeSelection();
        $add_child->food_variation_id   = $variation_id;
        $add_child->name                = $req->name;
        $add_child->amount              = priceToCent($req->price);
        $add_child->is_added_price      = $req->is_added_price;
        $add_child->save();

        DB::commit();
        return res('success', encode($add_child->id, 'uuid'), 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function editChildVariation($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'child_variation_hashid'  => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $child_variation_id   = decode($req->child_variation_hashid, 'uuid');
        $child_variation      = VariationTypeSelection::where('id', $child_variation_id)->where('status', 1)->first();
        if(!$child_variation){
            DB::rollBack();
            return res(__('food.food_variation.child.not_found'), null, 404);
        }

        if($req->name != null){
            $child_variation->name = $req->name;
        }

        if($req->price != null){
            $child_variation->amount = priceToCent($req->price);
        }

        if($req->is_added_price != null){
            $child_variation->is_added_price = $req->is_added_price;
        }

        $child_variation->save();

        DB::commit();
        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function deleteChildVariation($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'child_variation_hashid'  => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $child_variation_id   = decode($req->child_variation_hashid, 'uuid');
        $child_variation      = VariationTypeSelection::where('id', $child_variation_id)->where('status', 1)->first();
        if(!$child_variation){
            DB::rollBack();
            return res(__('food.food_variation.child.not_found'), null, 404);
        }

        $child_variation->status = 0;
        $child_variation->save();

        DB::commit();
        return res('success', null, 201);
    }

    /**
     * Add Minute duration of a store
     * @param $req
     * @return JsonResponse
     */
    public function addBusyduration($req):object
    {
        DB::beginTransaction();
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'store_hashid'      => 'required|string',
            'minute_duration'   => 'required|int'
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store    = Store::where('id', $store_id)->first();
        if(!$store){
            DB::rollBack();
            return res(__('store.not_found'), null, 401);
        }

        $now = Carbon::now(); // Get current time now.
        $schedule = StoreSchedule::where('store_id', $store_id)->where('day_id', $now->dayOfWeekIso)->first();
        if(!$schedule) {
            DB::rollBack();
            return __('schedule.not_found');
        }

        
        $schedule->is_busy = 1;
        if($schedule->is_busy_duration != null){
            $schedule->is_busy_duration = Carbon::parse($schedule->is_busy_duration)->addMinutes($req->minute_duration);
        }else{
            $schedule->is_busy_duration = $now->addMinutes($req->minute_duration);
        }
        $schedule->is_opened = 1;
        $schedule->save();
        DB::commit();
        return res('success', null, 200);
    }

    /**     
     * Add Minute duration of a store
     * @param $req
     * @return JsonResponse
     */
    public function stopBusyduration($req):object
    {
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'store_hashid'      => 'required|string',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store    = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 401);
        }

        $now = Carbon::now(); // Get current time now.
        $schedule = StoreSchedule::where('store_id', $store_id)->where('day_id', $now->dayOfWeekIso)->first();
        if(!$schedule) {
            DB::rollBack();
            return __('schedule.not_found');
        }

        $schedule->is_busy = 0;
        $schedule->is_busy_duration = null;
        $schedule->save();

        return res('success', null, 200);
    }

    /**     
     * Close or Open the store for this day.
     * @param $req
     * @return JsonResponse
     */
    public function closeOpen($req):object
    {
        $permissions = ['administrator', 'agent', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('auth.not_allowed'), null, 401);
        }
        
        $validator = Validator::make($req->all(), [
            'store_hashid'  => 'required|string',
            'is_opened'     => 'required|boolean'    
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store    = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 401);
        }

        $now = Carbon::now(); // Get current time now.
        $schedule = StoreSchedule::where('store_id', $store_id)->where('day_id', $now->dayOfWeekIso)->first();
        if(!$schedule) {
            DB::rollBack();
            return __('schedule.not_found');
        }

        $schedule->is_opened        = $req->is_opened;
        $schedule->is_busy          = 0;
        $schedule->is_busy_duration = null;
        $schedule->save();

        return res('success', null, 200);
    }
}