<?php

namespace App;

use App\Models\Store;
use App\Models\Address;
use App\Models\StoreFollowing;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\DriverLicense;
use App\Models\DriverVehicle;
use App\Models\DriverActivity;
use App\Models\ProductDetails;
use App\Models\UserAddress;
use App\Models\BloggerVideo;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    protected $appends = ['hash_id', 'current_address', 'status_info', 'mobile_prefix', 'type_info', 'store_info', 'default_billing_address', 'default_shipping_address', 'driver_license_status', 'vehicle_registration_status'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobile', 'password', 'profile_photo', 'type', 'iso'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_token', 'mobile_token', 'password_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //==================Function===============
    public function scopeCloseTo($query, $lat, $lon, $radius)
    {
        $haversine = "(6371 * acos(cos(radians($lat)) 
            * cos(radians(latitude)) 
            * cos(radians(longitude) 
            - radians($lon)) 
            + sin(radians($lat)) 
            * sin(radians(latitude))))";


        return $query
            ->select('id', 'name', 'latitude', 'longitude')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])
            ->where('type', 4)
            ->orderBy('distance')->get();
    }
    //====END OF FUNCTIONS

    //--------------------------- APPEND -------------------------------
    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function getMobilePrefixAttribute()
    {
        $prefix = null;
        if (isset($this->attributes['iso'])) {
            $prefix = getMobilePrefix(iso2ToIso3($this->attributes['iso'], true));
        }
        return $prefix;
    }

    public function getStatusInfoAttribute()
    {
        $status = [
            'text' => 'unverified',
            'type' => 'info',
        ];
        $verified = $this->attributes['verified_email'] == 1 || $this->attributes['verified_mobile'] == 1;
        if ($this->attributes['ban'] == 1) {
            $status = [
                'text' => 'banned',
                'type' => 'danger',
            ];
        } else {
            if ($this->attributes['status'] == 1 && $verified) {
                $status = [
                    'text' => 'active',
                    'type' => 'success',
                ];
            }
        }

        return $status;
    }

    public function getCurrentAddressAttribute()
    {
        $address = UserAddress::where('user_id', $this->id)->where('is_current', 1)->first();
        if(!$address) return __('address.not_found');

        return $address;
    }

    public function getTypeInfoAttribute()
    {
        switch ($this->type) {
            case 9 :
            return $type = 'administrator';
            break;

            case 8 :
            return $type = 'admin Staff';
            break;

            case 7 :
            return $type = 'agent';
            break;

            case 5 :
            return $type = 'tailor';
            break;

            case 4 :
            return $type = 'driver';
            break;

            case 3 :
            return $type = 'blogger';
            break;

            case 2 :
            return $type = 'vendor';
            break;

            case 1 :
            return $type = 'vendor staff';
            break;

            default :
            return $type = 'customer';
            break;
        }
    }

    public function getStoreInfoAttribute()
    {
        $info = null;
        $store = Store::where('user_id', $this->attributes['id'])->where('is_approved', 1)->where('ban', 0)->first();
        if ($store) {
            $info = $store;
        }
        return $info;
    }

    public function getDefaultBillingAddressAttribute()
    {
        $billing = Address::where('is_default', 1)->where('address_type', '<>', 1)->where('status', 1)->first();
        return $billing;
    }

    public function getDefaultShippingAddressAttribute()
    {
        $shipping = Address::where('is_default', 1)->where('address_type', '<>', 0)->where('status', 1)->first();
        return $shipping;
    }

    public function getDriverLicenseStatusAttribute() :String
    {
        if($this->type !== 4) return 'Not a driver';

        $license = DriverLicense::where('user_id', $this->id)->first();
        if(!$license) return 'Not submitted';

        if($license->is_approved == 1) return 'approved';
        if($license->is_declined == 1) return 'declined';

        return 'pending';

    }

    public function getVehicleRegistrationStatusAttribute() :String
    {
        if($this->type !== 4) return 'Not a driver';

        $vehicle = DriverVehicle::where('user_id', $this->id)->first();
        if(!$vehicle) return 'Vehicle information not found';

        return $vehicle->registration_status;
    }
    // -------------------------------------------------------------

    
    // -------------------- RELATIONSHIP ----------------------

    //-ONE TO MANY
    public function store()
    {
        return $this->hasMany(\App\Models\Store::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(\App\Models\Order::class, 'customer_id');
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\RestaurantReview::class, 'user_id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\TableBooking::class, 'user_id');
    }

    public function foodreviews()
    {
        return $this->hasMany(\App\Models\FoodReview::class, 'user_id');
    }

    public function driverreviews()
    {
        return $this->hasMany(\App\Models\DriverReview::class, 'user_id');
    }

    public function reservation()
    {
        return $this->hasMany(\App\Models\TableReservation::class, 'user_id');
    }

    public function resturant_carts()
    {
        return $this->hasMany(\App\Models\RestaurantCart::class, 'user_id');
    }

    public function rest_order()
    {
        return $this->hasMany(\App\Models\RestaurantOrder::class,'user_id');
    }

    public function favorites()
    {
        return $this->hasMany(\App\Models\FoodFavorite::class, 'user_id');
    }

    public function order_lists()
    {
        return $this->hasMany(\App\Models\RestaurantOrder::class, 'user_id');
    }

    public function following()
    {
        return $this->hasMany(StoreFollowing::class, 'user_id');
    }
    //

    public function driverActivity()
    {
        return $this->hasOne(DriverActivity::class, 'user_id');
    }

    public function blogger_products()
    {
        return $this->hasMany(ProductDetails::class, 'user_id');
    }

    public function blogger_details()
    {
        return $this->hasOne(bloggerDetails::class, 'user_id');
    }

    public function blogger_videos()
    {
        return $this->hasMany(BloggerVideo::class, 'user_id');
    }
    //-------------------------------------------------------
}
