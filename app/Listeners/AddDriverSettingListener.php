<?php

namespace App\Listeners;

use App\Events\AddDriver;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DriverSetting;

class AddDriverSettingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddDriver  $event
     * @return void
     */
    public function handle(AddDriver $event)
    {
        $data = $event->data;

        $setting = new DriverSetting;
        $setting->user_id = $data['user']['id'];
        $setting->notification_is_activated = 0;
        $setting->save();
        
    }
}
