<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderDurationToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'order_duration')){
            Schema::table('stores', function (Blueprint $table) {
                $table->json('order_duration')->nullable()->after('sell_on_country');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'order_duration')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('order_duration');
            });
        }
    }
}
