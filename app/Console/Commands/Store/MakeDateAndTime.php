<?php

namespace App\Console\Commands\Store;

use Illuminate\Console\Command;
use App\Models\StoreTiming;
use Carbon\Carbon;
class MakeDateAndTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:default-date-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time_schedules = StoreTiming::where('store_open', '09:00 AM')->where('store_close', '07:00 PM')
        ->update(['store_open' => Carbon::now()->format('Y-m-d 09:00'), 'store_close' => Carbon::now()->format('Y-m-d 19:00')]);
    }
}
