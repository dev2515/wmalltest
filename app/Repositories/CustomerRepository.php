<?php

namespace App\Repositories;
use App\User;
use App\Http\Resources\User\UserDataCollection;
use App\Http\Resources\User\Customer as CustomerResource;
use App\Http\Resources\User\CustomerCollection;
use App\Models\Brand;
use App\Http\Resources\Product\BrandCollection;
use App\Models\Product;
use App\Http\Resources\Product\ProductListCollection;
use App\Http\Resources\Stores\FollowList;
use App\Models\Store;
use App\Http\Resources\Stores\StoreListCollection;
use App\Http\Resources\User\CustomerAddress;
use App\Http\Resources\User\CustomerAddressCollection;
use App\Http\Resources\User\CustomerAddressListCollection;
use App\Models\Category;

use App\Models\Order;
use App\Models\OrderStore;
use Illuminate\Support\Facades\Validator;

use App\Interfaces\CustomerInterface;
use App\Models\AddressType;
use App\Models\StoreFollowing;
use App\Models\StoreRating;
use App\Models\UserAddress;
use App\Models\WishList;

class CustomerRepository implements CustomerInterface
{
    public function list($req)
    {
        if ($req->has('store_id')) {

        }
    }

    public function show($req)
    {

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listOfAllCustomer($req)
    {
        if(auth()->user()->type_info == 'customer') {
            return res(__('user.not_allowed'), null, 400);
        }
        if(auth()->user()->type_info == 'vendor' || auth()->user()->type_info == 'vendor staff') {
            $user = User::where('type', 0)->paginate($req->limit);
            $data = new CustomerCollection($user); 
            return res('Success', $data, 200);
        }
        else {
            $user = User::where('type', 0)->paginate($req->limit);
            $data = new UserDataCollection($user); 
            return res('Success', $data, 200);
        }
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandList($req)
    {
        if (!$req->type) return res(__('brand.type_required'), null, 412);

        $limit = 10;
        if($req->limit) $limit = $req->limit;

        if($req->type === 'premium') $brands = Brand::where('is_premium', 1)
        ->where('name', 'LIKE', '%' . $req->key . '%')
            ->where('is_approved', 1)->orderBy('created_at', 'DESC')->paginate($limit);
        
        
        if($req->type === 'regular') $brands = Brand::where('is_premium', null)
        ->where('name', 'LIKE', '%' . $req->key . '%')
            ->where('is_approved', 1)->orderBy('created_at', 'DESC')->paginate($limit);
        

        $data = new BrandCollection($brands); 
        return res('Success', $data, 200);
    }

    public function brandListPremium($req)
    {
        $limit = 10;
        if($req->limit) $limit = $req->limit;

        if($req->type === 'premium') $brands = Brand::where('is_premium', 1)
        ->where('name', 'LIKE', '%' . $req->key . '%')
            ->where('is_approved', 1)->orderBy('created_at', 'DESC')->paginate($limit);

        $data = new BrandCollection($brands); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandListByName($req)
    {
        $brands = Brand::where('name', 'like', '%' . $req->key . '%')->where('status', 1)->paginate($req->limit);

        $data = new BrandCollection($brands); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandSort($req)
    {
        $brands = Brand::orderByRaw('LENGTH(name)', 'ASC')->orderBy('name', 'ASC')->paginate($req->limit);

        $data = new BrandCollection($brands); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function brandSearch($req)
    {
        $brands = Brand::where('name', 'like', '%' . $req->key . '%')->where('status', 1)->paginate($req->limit);

        $data = new BrandCollection($brands); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalBrands()
    {
        $brands = Brand::count('id');
        return res('success', $brands, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productList($req)
    {
        $products = Product::where('status', 1)->orderBy('created_at', 'desc')->paginate($req->limit);

        $data = new ProductListCollection($products); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productSearch($req)
    {
        $products = Product::where('name', 'like', '%' . $req->key . '%')->where('status', 1)->paginate($req->limit);

        $data = new ProductListCollection($products); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productSort($req)
    {
        $products = Product::orderByRaw('LENGTH(name)', 'ASC')->orderBy('name', 'ASC')->paginate($req->limit);

        $data = new ProductListCollection($products); 
        return res('Success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalProducts()
    {
        $products = Product::count('id');
        return res('success', $products, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function productListBytags($req)
    {
        if(!$req->tags) return res('tags is required', null, 400);

        if(!$req->limit) return res('limit is required', null, 400);

        $product = Product::where('tags', 'like', '%' . $req->tags . '%')->paginate($req->limit);
        $product = new ProductListCollection($product);

        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function restaurantList($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $product = Store::where('business_type_id', 3)->orderBy('name', 'ASC')->paginate($req->limit);
       // $product = new StoreListCollection($product);

        return res('success', $product, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function categoryList()
    {
        $list = Category::where('parent_id', 0)->where('status', 1)->orderBy('name', 'ASC')->get();
        $list->makeHidden(['created_at', 'updated_at', 'status', 'parent_id']);

        return res('Success', $list);
    }
    
    /**
     * @param $req
     * @return JsonResponse
     */
    public function addAddress($req)
    {
        $validator = Validator::make($req->all(), [
            'street'        => 'required',
            'is_current'    => 'required',
            'latitude'      => 'required',
            'longitude'     => 'required'
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $address = UserAddress::where('user_id', auth()->id())->where('street', $req->street)->where('latitude', $req->latitude)->where('longitude', $req->longitude)->where('status', 1)->first();
        if($address) return res(__('address.already'), null, 409);
        
        if($req->is_current == 1) {
            $is_current = UserAddress::where('user_id', auth()->id())->where('is_current', 1)->where('status', 1)->update(['is_current' => 0]);
        }

        $address_type_id = decode($req->type_hashid, 'uuid');
        $address_type    = AddressType::where('id', $address_type_id)->first();
        if(!$address_type) return res(__('address.type.not_found'), null, 404);

        $req->request->add(['user_id' => auth()->id(), 'address_type_id' => $address_type_id]);
        $add = UserAddress::create($req->all());
        
        return res('success', null, 200);

    }

    public function updateAddress($req)
    {
        if(!$req->address_hashid) return res(__('address.identifier'), null, 412);

        $address_id = decode($req->address_hashid, 'uuid');

        $address = UserAddress::where('id', $address_id)->first();
        if(!$address) return res(__('address.not_found'), null, 404);

        if($req->is_current == 1) {
            $is_current = UserAddress::where('user_id', auth()->id())->where('is_current', 1)->where('status', 1)->update(['is_current' => 0]);
        }
        
        if ($req->street != null) $address->street = $req->street;
        if ($req->state != null) $address->state = $req->state;
        if ($req->city != null) $address->city = $req->city;
        if ($req->latitude != null) $address->latitude = $req->latitude;
        if ($req->longitude != null) $address->longitude = $req->longitude;

        if (isset($req->is_current)) $address->is_current = $req->is_current;

        $address->save();

        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function deleteAddress($req)
    {
        if(!$req->address_hashid) return res(__('address.identifier'), null, 412);

        $address_id = decode($req->address_hashid, 'uuid');

        $address = UserAddress::where('id', $address_id)->where('status', 1)->first();
        if(!$address) return res(__('address.not_found'), null, 404);

        $address->status = 0;
        $address->save();

        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addresses()
    {
        $addresses = UserAddress::where('user_id', auth()->id())->where('status', 1)->get();
        $data = new CustomerAddressListCollection($addresses);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function showAddress($req)
    {
        $permissions = ['customer', 'agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('auth.not_authorize'), null, 401);
        }

        $validator  = Validator::make($req->all(), ['address_hashid' => 'required|string',]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $address_id = decode($req->address_hashid, 'uuid');
        $address    = UserAddress::where('id', $address_id)->where('status', 1)->first();
        if(!$address) return res(__('address.not_found'), null, 404);

        $data       = new CustomerAddress($address);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function customerAccount($req)
    {
        if (auth()->user()->type_info != 'customer') {
            return res(__('user.not_allowed'), null, 400);
        }
        
        if(!$req->customer_hashid) return res('hashid is required', null, 400);

        $id = decode($req->customer_hashid, 'uuid');

        $customer = User::where('id', $id)->where('type', 0)->first();
        if(!$customer) return res(__('account.not_found'), null, 400);
        $customer = new CustomerResource($customer);

        return res('success', $customer, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function orderList($req)
    {
        if(!$req->customer_hashid) return res('hashid is required', null, 400);

        $id = decode($req->customer_hashid, 'uuid');

        $ss = Order::where('customer_id', $id)->where('status', 1)->paginate($req->limit)->items();
        if(!$ss) return 'Order not found';

        return res('success', $ss, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function orderShow($req)
    {
        if(!$req->order_hashid) return res('hashid is required', null, 400);

        $id = decode($req->order_hashid, 'uuid');

        $ss = Order::where('id', $id)->where('status', 1)->first();
        if(!$ss) return 'Order not found';

        return res('success', $ss, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function orderStore($req)
    {
        //if(!$req->order_hashid) return res('hashid is required', null, 400);

        $id = decode($req->order_hashid, 'uuid');

        $order = Order::where('id', $req->id)->where('status', 1)->first();

        $os = OrderStore::where('store_id', $order->id)->get();
        if(!$os) return 'Order not found';

        return res('success', $os, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeFollowing($req)
    {
        $limit = 10;
        if($req->limit) $limit = $req->limit;

        $store_ids = StoreFollowing::where('user_id', auth()->id())->where('is_followed', 1)->pluck('store_id');

        $stores = Store::whereIn('id', $store_ids)->where('name', 'LIKE', '%' . $req->key . '%')->paginate($limit);

        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    public function storefollowed($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        // if(!$req->key) return res('Key is required', null, 401);

        $store = StoreFollowing::with('user', 'store')
            ->where('user_id', auth()->user()->id)->get();
        
        $store = FollowList::collection($store);

        return res('success', $store);
    }

    public function isFollow($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        if(!$req->store_hash_id) return res('Store hashid is required', null, 401);
        if(!$req->is_followed) return res('Is_followed hashid is required', null, 401);
        
        $store_id = decode($req->store_hash_id, 'uuid');
        $follow = StoreFollowing::updateOrCreate(
        [
            'store_id' => $store_id,
            'user_id' => auth()->user()->id,
        ],
        [
            'user_id' => auth()->user()->id,
            'store_id' => $store_id,
            'is_followed' => $req->is_followed,
            'status' => 1,
        ]);
        
        if($req->is_followed == 1) return res('Followed');
        if($req->is_followed == 2) return res('Unfollowed');
    }
    
    /**
     * @param $req
     * @return JsonResponse
     */
    public function addStoreRating($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        if(!$req->store_hash_id) return res('Store hashid is required', null, 401);
        if(!$req->rating) return res('Rating is required', null, 401);

        $store_id = decode($req->store_hash_id, 'uuid');

        $rating = StoreRating::updateOrCreate(
        [
            'store_id' => $store_id,
            'user_id' => auth()->user()->id,
        ],
        [
            'user_id' => auth()->user()->id,
            'store_id' => $store_id,
            'rating' => $req->rating,
            'content' => $req->content,
            'status' => 1,
        ]);

        return res('success', $rating);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function removeRating($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        if(!$req->store_rating_hash_id) return res('Store rating hashid is required', null, 401);
        if(!$req->store_hash_id) return res('Store rating hashid is required', null, 401);
        
        $store_id = decode($req->store_hash_id, 'uuid');
        $rating_id = decode($req->store_hash_id, 'uuid');

        $rating = StoreRating::where('id', $rating_id)->where('user_id', auth()->user()->id)->where('store_id', $store_id)->where('status', 1)->first();

        $rating->status = 0;
        return res('success');
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addToWishList($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        if(!$req->product_hashid) return res(__('product.identifier'), null, 401);

        $product_id = decode($req->product_hashid, 'uuid');
        $check      = WishList::where('user_id', auth()->id())->where('product_id', $product_id)->where('status', 1)->first();

        if(!$check){
            $wishlist               = new WishList();
            $wishlist->user_id      = auth()->id();
            $wishlist->product_id   = $product_id;
            $wishlist->save();

            return res('success', null, 200);
        }

        return res('Already added', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function wishLists($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $pagination = 10;
        if($req->pagination) $pagination = $req->pagination;

        $product_ids = WishList::where('user_id', auth()->id())->pluck('product_id');
        $products = Product::whereIn('id', $product_ids)->paginate($pagination);

        $data = new ProductListCollection($products);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addressTypes()
    {
        $types = AddressType::get();
        return res('success', $types, 200);
    }
}
