<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeFoodImageToRestaurantFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_foods', 'food_image')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->longText('food_image')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_foods', function (Blueprint $table) {
            //
        });
    }
}
