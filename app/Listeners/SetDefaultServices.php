<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\RestaurantService;
use App\Models\BusinessType;
use App\Models\RestaurantServicePivot;

class SetDefaultServices
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data;
        $business_type = BusinessType::where('name', 'restaurant')->first();

        if($store->business_type_id == $business_type->id){
            $restaurant_services = RestaurantService::get();
            foreach($restaurant_services as $restaurant_service){
                $add_default = new RestaurantServicePivot;
                $add_default->store_id = $store->id;
                $add_default->restaurant_service_id = $restaurant_service->id;
                $add_default->save();
            }
        }
    }
}
