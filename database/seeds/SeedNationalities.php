<?php

use Illuminate\Database\Seeder;
use App\Models\Nationality;

class SeedNationalities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Nationality::count();
        if($count == 0){
            $path = public_path('data/nationalities.json');
            $json = file_get_contents($path);
            $json = json_decode($json);

            foreach ($json->nationalities as $nationality){
                Nationality::firstOrCreate(['name' => $nationality]);
            }
        }
    }
}
