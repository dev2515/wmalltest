<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnOrderListIdInRestaurantOrderFood extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_order_food', 'restaurant_id')){
            Schema::table('restaurant_order_food', function (Blueprint $table) {
                $table->renameColumn('restaurant_id','order_list_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_order_food', 'order_list_id')){
            Schema::table('restaurant_order_food', function (Blueprint $table) {
                $table->renameColumn('order_list_id', 'restaurant_id');
            });
        }
    }
}
