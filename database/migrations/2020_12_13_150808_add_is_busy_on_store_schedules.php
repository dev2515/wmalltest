<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsBusyOnStoreSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_schedules', 'is_busy')){
            Schema::table('store_schedules', function (Blueprint $table) {
                $table->boolean('is_busy')->default(0)->after('is_opened');
            });
        }

        if(!Schema::hasColumn('store_schedules', 'is_busy_duration')){
            Schema::table('store_schedules', function (Blueprint $table) {
                $table->string('is_busy_duration')->nullable()->after('is_busy');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_schedules', 'is_busy')){
            Schema::table('store_schedules', function (Blueprint $table) {
                $table->dropColumn('is_busy');
            });
        }

        if(Schema::hasColumn('store_schedules', 'is_busy_duration')){
            Schema::table('store_schedules', function (Blueprint $table) {
                $table->dropColumn('is_busy_duration');
            });
        }
    }
}
