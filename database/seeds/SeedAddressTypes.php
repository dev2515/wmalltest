<?php

use App\Models\AddressType;
use Illuminate\Database\Seeder;

class SeedAddressTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = AddressType::count();
        $types = ['house', 'office', 'appartment'];

        if($count == 0){
            foreach ($types as $type){
                AddressType::firstOrCreate(['name' => $type, 'status' => 1]);
            }
        }
    }
}
