<?php

namespace App\Http\Controllers\API\Blogger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\BloggerVideoInterface;

class BloggerVideoController extends Controller
{

    private $req;
    private $bloggervideo;

    public function __construct(Request $req, BloggerVideoInterface $bloggervideo)
    {
        $this->req = $req;
        $this->bloggervideo = $bloggervideo;
    }
    public function getVideos()
    {
        return $this->bloggervideo->video_list($this->req);
    }

    public function storeVideo()
    {
        return $this->bloggervideo->save_video($this->req);

    }

    public function deleteVideo($id)
    {
        return $this->bloggervideo->delete_video($this->req,$id);
    }
}
