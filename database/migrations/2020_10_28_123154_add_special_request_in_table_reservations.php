<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecialRequestInTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('table_reservations', 'special_request')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->string('special_request')->nullable();
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'is_dine_in')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('is_dine_in', 'order_type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('table_reservations','special_request')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->dropColumn('special_request');
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'is_dine_in')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('order_type', 'is_dine_in');
            });
        }
    }
}
