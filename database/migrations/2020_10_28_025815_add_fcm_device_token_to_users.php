<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFcmDeviceTokenToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('users', 'fcm_device_token')){
            Schema::table('users', function (Blueprint $table) {
                $table->longText('fcm_device_token')->nullable()->after('type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('users', 'fcm_device_token')){
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('fcm_device_token');
            });
        }
    }
}
