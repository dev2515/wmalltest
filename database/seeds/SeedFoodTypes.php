<?php

use Illuminate\Database\Seeder;
use App\Models\FoodType;

class SeedFoodTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = FoodType::count();
        $types = ['Main Course', 'Drinks', 'Desserts'];
        if($count == 0){
            foreach ($types as $type){
                FoodType::firstOrCreate(['name' => $type, 'status' => 1]);
            }
        }
    }
}
