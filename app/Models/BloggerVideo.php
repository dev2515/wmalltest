<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BloggerVideo extends Model
{
    public function owner()
    {
        return $this->belongsTo(User::class,'id');
    }
}
