<?php

namespace App\Interfaces;

interface CheckoutInterface
{
    public function list($req);

    public function placeOrder($req);
}
