<?php

namespace App\Http\Resources\RestaurantManager;

use Illuminate\Http\Resources\Json\JsonResource;

class FoodVariationSelection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'            => $this->hashid,
            'name'              => $this->name,
            'price'             => centToPrice($this->amount),
            'is_added_price'    => $this->is_added_price
        ];
    }
}
