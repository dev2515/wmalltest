<?php

namespace App\Http\Resources\Restaurant;

use App\Model\StoreBranch;
use App\Models\StoreTiming;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class RestaurantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $dt = Carbon::parse($this->updated_at);
        return[
            'full_cover' =>                     asset($this->cover()->full),
            'thumbnail_cover' =>                asset($this->cover()->thumbnail),
            'hash_id' =>                        $this->hash_id,
            'Restaurant' =>                     $this->name,
            'Email' =>                          $this->store_email,
            'Store_Information' =>              $this->store_information,
            'Contact' =>                        $this->phone,
            'City' =>                           $this->city,
            'full' =>                           asset($this->photo()->full),
            'thumbnail' =>                      asset($this->photo()->thumbnail),
            'timestamp' =>                      $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'timings' =>                        $this->is_opened, 
            'ratings' =>                        $this->ratings,
            'number_of_followers' =>            $this->followers,
            'branches' =>                       $this->branches(),
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/stores/' . $this->hash_id . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hash_id . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->hash_id . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->hash_id . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function cover()
    {      
        $full = public_path() . '/images/stores/' . $this->hash_id . '/cover-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hash_id . '/cover-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->hash_id . '/cover-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->hash_id . '/cover-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function branches()
    {
        $s = StoreBranch::select('id','store_id','contact_number','street','state','city','other_address_info','contact_email','is_approved'
        ,'is_main')
        ->where('store_id', $this->id)->where('is_approved', 1)->first();        
        
        if($s) $s->makeHidden(['id','store_id']); 

        if($s->is_main == 1) {return 'Main Branch';}
        else{return $s;}
    }
}
