<?php

namespace App\Models;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = 'restaurants';
    protected $fillable = ['name', 'user_id', 'requested_by', 'process_by', 'business_type_id', 'email', 'phone_number',
    'information','profile_photo', 'cover_photo', 'street', 'city', 'state', 'country', 'zip', 'other_address_info',
    'latitude', 'longitude',
    ];
    
    protected $append =['hash_id', 'totalRate',];

    public function scopeCloseTo($query, $lat, $lon, $radius, $limit)
    {
        $haversine = "(6371 * acos(cos(radians($lat)) 
            * cos(radians(latitude)) 
            * cos(radians(longitude) 
            - radians($lon)) 
            + sin(radians($lat)) 
            * sin(radians(latitude))))";
        return $query
            ->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])->orderBy('distance')->limit($limit)->get();
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\RestaurantReview::class, 'restaurant_id');
    }

    public function menu()
    {
        return $this->hasMany(\App\Models\Menu::class,'restaurant_id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Restaurant::class, 'restaurant_id');
    }

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function foods()
    {
        return $this->hasMany(\App\Models\RestaurantFood::class, 'restaurant_id');
    }

    public function food_reviews()
    {
        return $this->hasMany(\App\Models\FoodReview::class, 'restaurant_id');
    }

    public function tables()
    {
        return $this->hasMany(\App\Models\RestaurantTable::class, 'restaurant_id');
    }

    public function reservation()
    {
        return $this->hasMany(\App\Models\TableReservation::class, 'restaurant_id');
    }

    public function carts()
    {
        return $this->hasMany(\App\Models\RestaurantCart::class, 'restaurant_id');
    }

    public function items()
    {
        return $this->hasMany(\App\Models\RestaurantCartItem::class, 'restaurant_id');
    }

    public function order_lists()
    {
        return $this->hasMany(\App\Models\RestaurantOrderList::class, 'restaurant_id');
    }

    public function category()
    {
        return $this->hasMany(\App\Models\FoodCategory::class, 'restaurant_id');
    }
}
