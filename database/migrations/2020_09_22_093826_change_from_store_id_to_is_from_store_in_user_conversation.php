<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFromStoreIdToIsFromStoreInUserConversation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('user_conversations', 'from_store_id')){
            Schema::table('user_conversations', function (Blueprint $table) {
                $table->renameColumn('from_store_id', 'is_from_store');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_conversations', 'is_from_store')){
            Schema::table('user_conversations', function (Blueprint $table) {
                $table->renameColumn('is_from_store', 'from_store_id');
            });
        }
    }
}
