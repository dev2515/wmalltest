<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RestaurantReview extends Model
{
    protected $table = 'restaurant_reviews';
    protected $fillable = ['user_id', 'restaurant_id', 'rate', 'review', 'flag', 'comment'];
    protected $appends = ['hash_id'];

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($this->id, 'uuid');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'restaurant_id');
    }
}
