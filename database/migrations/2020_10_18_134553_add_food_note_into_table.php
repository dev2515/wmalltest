<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFoodNoteIntoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_cart_items','request')){   
            Schema::table('restaurant_cart_items', function (Blueprint $table) {
                $table->string('request')->nullable();
            });
        }

        if(!Schema::hasColumn('restaurant_order_food','request')){   
            Schema::table('restaurant_order_food', function (Blueprint $table) {
                $table->string('request')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_cart_items', 'request')){
            Schema::table('restaurant_cart_items', function (Blueprint $table) {
                $table->dropColumn('request');
            });
        }

        if(Schema::hasColumn('restaurant_order_food', 'request')){
            Schema::table('restaurant_order_food', function (Blueprint $table) {
                $table->dropColumn('request');
            });
        }
    }
}
