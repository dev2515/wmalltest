<?php

namespace App\Repositories;

use App\Http\Resources\Restaurant\FavoriteResource;
use App\Http\Resources\Restaurant\FoodCategoryResource;
use App\Http\Resources\Restaurant\FoodRestaurant;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Restaurant\FoodSearch;
use App\Http\Resources\Restaurant\RestaurantResource;
use App\Http\Resources\Restaurant\FoodShow;
use App\Http\Resources\Restaurant\FoodType;
use App\Http\Resources\Restaurant\ImagesResource;
use App\Http\Resources\Restaurant\MealResource;
use App\Interfaces\RestaurantInterface;
use App\Models\FoodCategory;
use App\Models\FoodFavorite;
use App\Models\StoreChangeField;
use App\Models\RestaurantFood as ModelsRestaurantFood;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Models\RestaurantFoodCategoryPivot;
use App\Models\Store;
use App\Models\Meal;
use App\Models\RestaurantImage;
use App\Models\UserAddress;
use App\Models\BusinessType;
use App\Http\Resources\Restaurant\Information;
use App\Http\Resources\Restaurant\RestaurantSearch;
use App\Http\Resources\Stores\StoreList;
use stdClass;
use App\Models\FoodType as FoodTypeModel;
use App\Models\FoodTypePivot;
use App\Http\Resources\RestaurantManager\FoodListCollection;
use App\Models\StoreFollowing;

class RestaurantRepository implements RestaurantInterface
{
    //***************************************************************************/
    /*                   RESTAURANT LIST                                        */
    //***************************************************************************/

    public function testlistbyrate($req)
    {
        $r = DB::table('stores')->leftJoin('reviews', 'stores.id', '=', 'reviews.type_id')
            ->select('stores.id','stores.name','stores.business_type_id','stores.store_email', 'stores.phone', 'stores.city', DB::raw('AVG(reviews.rate) as avg_rate'))
            ->where('stores.business_type_id', 3)
            ->where('stores.is_approved', 1)
            //->where('stores.id', 'DESC')
            ->orderByDesc('avg_rate')
            ->groupBy('stores.id')
            ->get();
        
        $r = RestaurantResource::collection($r);
        return res('success', $r);
    }

    public function listByRate($req)
    {
        $restaurant = Store::select('id','name','business_type_id', 'profile_photo_img', 'cover_photo_img', 'store_email', 'phone', 'city')
        ->where('business_type_id', 3)
        ->where('is_approved', 1)
        ->orderBy('id', 'DESC')
        ->get();
        
        $restaurant = collect($restaurant)->sortByDesc('ratings.average')->take(10);
        $restaurant = RestaurantResource::collection($restaurant);

        return res('success', $restaurant);
    }

    public function listByFollowers($req)
    {
        $restaurant = Store::select('id','name','business_type_id', 'profile_photo_img', 'cover_photo_img', 'store_email', 'phone', 'city')
        ->where('business_type_id', 3)
        ->where('is_approved', 1)
        ->orderBy('id', 'DESC')
        ->get();
        
        $restaurant = collect($restaurant)->sortByDesc('followers')->take(10);
        $restaurant = RestaurantResource::collection($restaurant);

        return res('success', $restaurant);
    }

    public function listByRestaurant($req)
    {
        $restaurant = Store::select('id','name','business_type_id', 'profile_photo_img', 'cover_photo_img', 'store_email', 'phone', 'city')
        ->where('business_type_id', 3)
        ->where('is_approved', 1)
        ->inRandomOrder()
        ->orderBy('id', 'DESC')
        ->get();
        
        $restaurant = collect($restaurant)->sortByDesc('count')->take(10);
        $restaurant = RestaurantResource::collection($restaurant);

        return res('success', $restaurant);
    }

    public function listByAllRestaurant($req)
    {
        $business_type  = BusinessType::where('name', 'restaurant')->first();

        $selected_store_ids = [];

        $stores = Store::where('business_type_id', $business_type->id)->where('is_main_branch', 1)->where('is_active', 1)->where('is_approved', 1)->pluck('id');

        foreach ($stores as $store_id){
            $total_food = FoodTypePivot::where('store_id', $store_id)->where('status', 1)->count();
            if($total_food > 0){
                array_push($selected_store_ids, $store_id);
            }
        }

        $restaurants = Store::whereIn('id', $selected_store_ids)->where('business_type_id', $business_type->id)->where('is_active', 1)->where('is_approved', 1)->orderBy(DB::raw('RAND(1)'))->paginate($req->limit);
        $data = RestaurantResource::collection($restaurants);
        
        return res('success', $data, 200);
    }

    public function nearme($req)
    {
        //$ip ='37.208.190.18'; //for test only
        $ip = \Request::ip();
        $location = geoip()->getLocation($ip);
        //return res($ip);
        $lat = $location->lat;
        $log = $location->lon;
        
        $nearme = Store::closeTo($lat, $log, 5, 10);
        $collector = RestaurantResource::collection($nearme);
        
        return res('success', $collector);
    }
    
    //***************************************************************************/
    /*                   FOOD ADD & UPDATE                                      */
    //***************************************************************************/
    public function addFood($req)
    {
        $permissions = ['vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid' => 'required',
            'name' => 'required',
            'type' => 'required',
            'price' => 'required',
            'description' => 'required',
        ], [
            'name.unique' => 'The Brand Name has already been taken',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $store_id = decode($req->store_hashid, 'uuid');
        
        $store = Store::where('id', $store_id)->where('is_approved', 1)->first();
        if(!$store) return res(__('store.not_found'), null, 404);
        if($store->businessType->name != 'restaurant') return res(__('restaurant.not_restaurant'), null, 400);

        $food = new ModelsRestaurantFood;
        $food->restaurant_id = $store->id;
        $food->name = $req->name;
        $food->type = $req->type;
        $food->price = $req->price;
        $food->description = $req->description;
        $food->category_id = $req->category_id;
        $food->is_available = 1;
        $food->food_image = $req->food_image;
        $food->status = 1;
        $food->save();

        $this->restaurantPerCategory(['restaurant_id' => $food->restaurant_id, 'food_category_id' => $food->category_id]);
        
        if($req->food_image){
            $food->food_image = $req->food_image;

            $full = public_path() . '/images/food/' . $food->hashid . '-full.png';
            $thumbnail = public_path() . '/images/food/' . $food->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/food/';

            $image = Image::make($food->food_image);
            $image->save($local_path . encode($food->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($food->id, 'uuid') . '-thumbnail.png');
        }

        return res('success', null, 200);
    }

    public function updateFood($req)
    {
        $permissions = ['agent', 'vendor', 'vendor staff', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'food_hashid' => 'required',
        ]);

        if($validator->fails())
        {   
            return res('failed', $validator->errors(), 402);
        }
        
        $food_id = decode($req->food_hashid, 'uuid');

        $meal_id = decode($req->meal_hashid, 'uuid');
        $meal = Meal::where('id', $meal_id)->where('status', 1)->first();

        $food = ModelsRestaurantFood::where('id', $food_id)->first();

        $new = new StoreChangeField;
        $new->type = 'edit food';
        $new->data = $req->except('food_image');
        $new->base64 = $req->food_image;
        $new->request_by = auth()->id();
        $new->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new->process_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;

        if($req->name) $food->name = $req->name;
        if($req->price) $food->price = $req->price;
        if($req->description) $food->description = $req->description;
        if($meal) $food->meal = $meal->name;
        $food->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $food->process_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $food->request_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $food->status = auth()->user()->type_info == 'administrator' ? 1 : 0;
        

         if($req->food_image){
            $food->food_image = $req->food_image;

            $full = public_path() . '/images/food/' . $food->hashid . '-full.png';
            $thumbnail = public_path() . '/images/food/' . $food->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/food/';

            $image = Image::make($food->food_image);
            $image->save($local_path . encode($food->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($food->id, 'uuid') . '-thumbnail.png');
        }

        $food->save();
        $new->restaurant_foods_id = $food->id;
        $new->store_id = $food->restaurant_id;
        $new->save();
        return res('success');
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function foodByCategory($req)
    {
        if (!$req->has('restaurant_hashid')) {
            return res(__('validation.identifier'), null, 400);
        }

        $restaurant_hashid = decode($req->restaurant_hashid, 'uuid');

        $cat = FoodCategory::where('name', $req->category)
        ->where('status', 1)->first();
        if(!$cat) return res(__('category.not_found'), null, 400);

        if($req->status === 'inactive') $foods = ModelsRestaurantFood::where('restaurant_id', $restaurant_hashid)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('category_id', $cat->id)
        ->where('status', 1)
        ->paginate($req->limit);

        if($req->status === 'inactive') $foods = ModelsRestaurantFood::where('restaurant_id', $restaurant_hashid)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('category_id', $cat->id)
        ->where('status', 0)
        ->paginate($req->limit);

        if(!$foods) return res(__('food.not_found'), null, 400);
        
        $collector = FoodRestaurant::collection($foods);

        return res('success', $collector, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function foodTypes($req)
    {
        if (!$req->store_hashid) return res(__('store.not_found'), null, 400);

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->where('is_approved', 1)->first();

        if(!$store) return res(__('store.not_found'), null, 404);
        if($store->businessType->name != 'restaurant') return res(__('restaurant.not_restaurant'), null, 400);

        return res('success', $store->foodCategories, 200);
    }

    public function ListByCategory($req)
    {
        $v = Validator::make($req->all(),[
            'restaurant_hashid' => 'required',
            'category_hashid' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $store_id = decode($req->restaurant_hashid, 'uuid');
        $category_id = decode($req->category_hashid, 'uuid');

        $food_list_by_category = DB::table('food_types')
                ->join('food_type_pivot', 'food_types.id', '=', 'food_type_pivot.food_type_id')
                ->select('food_types.id','food_types.name','food_type_pivot.food_type_id','food_type_pivot.store_id')
                ->where('food_types.id', $category_id)
                ->where('food_type_pivot.food_type_id', $category_id)
                ->where('food_type_pivot.store_id', $store_id)
                ->first();
    
        if($food_list_by_category) $food_list_by_category = new FoodCategoryResource($food_list_by_category);
        
        return res('success', $food_list_by_category);
    }

    public function ListByImageCategory($req)
    {
        if (!$req->restaurant_hashid) return res(__('restaurant.not_found'), null, 400);
        if (!$req->category_hashid) return res(__('category.not_found'), null, 400);

        $store_id = decode($req->restaurant_hashid, 'uuid');
        $category_id = decode($req->category_hashid, 'uuid');

        if($req->category_hashid == 'All')
        {
            $key = FoodCategory::where('restaurant_id', $store_id)->where('status', 1)->get();
        }
        else
        {
            $key = FoodCategory::where('restaurant_id', $store_id)
            ->where('id', $category_id)
            ->where('status', 1)
            ->get();
        }

        $key = FoodCategoryResource::collection($key);
        
        return res('success', $key);
    }
   

    /***************************************[CATEGORY LIST END]*************************************************/

    public function addFavorite($req)
    {
        $permissions = ['customer'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'food_id' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $food_hash_id = decode($req->food_id, 'uuid');    
       
        $restaurant_id = ModelsRestaurantFood::where('id', $food_hash_id)->where('status', 1)->first()->restaurant_id;
        
         $fave = FoodFavorite::create([
            'food_id' => $food_hash_id,
            'restaurant_id' => $restaurant_id,
            'user_id' => auth()->user()->id,
            'status' => 1,
         ]);

         $fave->save();

         return res('success');
    }

    public function favoriteList($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $fave = FoodFavorite::with('food')->where('user_id', auth()->user()->id)->where('status', 1)->get();
        $fave = FavoriteResource::collection($fave);

        return res('success', $fave);
    }

    public function restaurantFavorite($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor and vendor staff can do this process', null, 401);
        }

        $restaurant_hash_id = decode($req->restaurant_hash_id, 'uuid');

        $fave = FoodFavorite::where('restaurant_id', $restaurant_hash_id)->where('status', 1)->get();
        $fave = FavoriteResource::collection($fave);

        return res('success', $fave);
    }

    public function removeFavorite($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'favorite_hash_id' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $favorite_id = decode($req->favorite_hash_id, 'uuid');

        $fave = FoodFavorite::where('id', $favorite_id)->where('status', 1)->first();
        $fave->status = 0;
        $fave->save();

        return res('success');
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listSearchStatus($req)
    {   
        if(!$req->restaurant_hashid) return res(__('validation.identifier'), null, 400);

        $store_id   = decode($req->restaurant_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        if($store->is_main_branch == 0){
            $store_id = $store->main_store_id;
            $req->request->add(['store_id' => $store->id]);
        }

        if ($req->status == 'all') $foods = ModelsRestaurantFood::where('restaurant_id', $store_id)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('status', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);
        
        if ($req->status == 'approved') $foods = ModelsRestaurantFood::where('restaurant_id', $store_id)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('status', 1)
        ->where('is_approved', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit); 

        if ($req->status == 'pending') $foods = ModelsRestaurantFood::where('restaurant_id', $store_id)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('status', 1)
        ->where('is_approved', 0)->where('is_declined', 0)
        ->orderBy('created_at', 'desc')->paginate($req->limit); 

        if ($req->status == 'declined') $foods = ModelsRestaurantFood::where('restaurant_id', $store_id)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('status', 1)
        ->where('is_declined', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit); 
        
        if(!$foods) return res(__('food.not_found'), null, 400);

        $collector = FoodRestaurant::collection($foods);
        return res('success', $collector, 200);
    }
    
    /**
     * @param $req
     * @return JsonResponse
     */
    public function showFood($req)
    {
        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
            'food_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        $store = Store::where('id', $restaurant_id)->where('business_type_id', 3)->first();
        if(!$store) return res(__('restaurant.not_found'), null, 400);

        $food_id = decode($req->food_hash_id, 'uuid');

        $food = ModelsRestaurantFood::where('id', $food_id)->where('restaurant_id', $restaurant_id)->first();
        if(!$food) return res(__('food.not_found'), null, 400);
        
        $food_details = ModelsRestaurantFood::where('id', $food_id)->where('restaurant_id', $restaurant_id)->get();

        $food_details = FoodShow::collection($food_details);

        return res('success', $food_details, 200);
    }
    
    /**
     * @param $req
     * @return JsonResponse
     */
    public function foodChangeStatus($req)
    {
        if (!$req->food_hashid) return res(__('food.not_found'), null, 400);

        $food_id = decode($req->food_hashid, 'uuid');
        $food = ModelsRestaurantFood::where('id', $food_id)->first();

        if(!$food) return res(__('food.not_found'), null, 400);

        if($req->is_available === 'available') $food->is_available = 1;
        if($req->is_available === 'unavailable')$food->is_available = 0;

        $food->save();

        return res('success');
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalFood($req)
    {
        if (!$req->restaurant_hashid) return res(__('food.not_found'), null, 400);

        $store_id = decode($req->restaurant_hashid, 'uuid');
        
        $count = ModelsRestaurantFood::where('restaurant_id', $store_id)->count();
        if(!$count) return res(__('restaurant.not_found'), null, 400);

        return res('success', $count, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalCategory($req)
    {
        if (!$req->restaurant_hashid) return res(__('food.not_found'), null, 400);

        $store_id = decode($req->restaurant_hashid, 'uuid');

        $category = decode($req->category_hashid, 'uuid');

        $cat = FoodCategory::where('id', $category)->first();
        if(!$cat) return res(__('category.not_found'), null, 400);
        
        $count = ModelsRestaurantFood::where('restaurant_id', $store_id)
        ->where('category_id', $cat->id)->count();
        if(!$count) return res(__('restaurant.not_found'), null, 400);

        return res('success', $count, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function myFoodType($req)
    {
        $category = decode($req->food_type_hashid, 'uuid');

        $cat = RestaurantFoodCategoryPivot::where('id', $category)
        ->first();
        if(!$cat) return res(__('food type.not_found'), null, 400);

        $data = new FoodType($cat);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function foodTypeList($req)
    {
        if (!$req->restaurant_hashid) return res(__('food.not_found'), null, 400);

        $store_id = decode($req->restaurant_hashid, 'uuid');

        $cat = RestaurantFoodCategoryPivot::where('restaurant_id', $store_id)
        ->where('food_category_name', 'like', '%' . $req->key . '%')
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if(!$cat) return res(__('category.not_found'), null, 400);

        $data = FoodType::collection($cat);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalFoodType($req)
    {
        if (!$req->restaurant_hashid) return res(__('food.not_found'), null, 400);

        $store_id = decode($req->restaurant_hashid, 'uuid');
        
        $count = RestaurantFoodCategoryPivot::where('restaurant_id', $store_id)->count();
        if(!$count) return res(__('restaurant.not_found'), null, 400);

        return res('success', $count, 200);
    }
 
    public function foodlist($req)
    {
        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $key = ModelsRestaurantFood::where('restaurant_id', $restaurant_id)
            ->where('status', 1)
                ->where('is_approved', 1)->get();

        $collector = FoodRestaurant::collection($key);
        return res('success', $collector);
    }

    public function search_food($req)
    {
        $validatore = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($validatore->fails()) return res('failed', $validatore->errors(), 402);
        
        $store_id   = decode($req->restaurant_hash_id, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);
        
        if($store->is_main_branch == 0){
            $foods = ModelsRestaurantFood::where('restaurant_id', $store->main_store_id)->where('name', 'LIKE', '%' . $req->food_name_key . '%')->where('status', 1)->orderBy('name', 'asc')->paginate($req->limit);
            $req->request->add(['store_id' => $store_id]);
            $collector = FoodRestaurant::collection($foods);
            return res('success', $collector);
        }

        $foods = ModelsRestaurantFood::where('restaurant_id', $store_id)->where('name', 'LIKE', '%' . $req->food_name_key . '%')->where('status', 1)->orderBy('name', 'asc')->paginate($req->limit);
        $collector = FoodRestaurant::collection($foods);
        return res('success', $collector);
    }

    public function foodCategoryList()
    {
        $category = FoodCategory::get();
        return res('success', $category);
    }

    public function adminFoodChangeStatus($req)
    {
        $permissions = ['administrator'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin can do this process', null, 401);
        }

        $food_id = decode($req->food_hashid, 'uuid');
        $food = ModelsRestaurantFood::where('id', $food_id)
        ->first();

        if($req->is_approved === 'approved'){
            $food->is_approved = 1;
            $food->is_declined = 0;
            $food->process_by = auth()->id();
            $food->status = 1;
            $food->save();
        }

        if($req->is_approved === 'declined'){
            $food->is_approved = 0;
            $food->is_declined = 1;
            $food->process_by = auth()->id();
            $food->status = 0;
            $food->save();
        }
        return res('success');
    }

    public function listByMeals($req)
    {
        $restaurant_id = decode($req->restaurant_hashid, 'uuid');
        
         $food = ModelsRestaurantFood::where('meal', 'like', '%' . $req->meals . '%')
        ->where('restaurant_id', $restaurant_id)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        $foods = FoodShow::collection($food);
        
        return res('success', $foods);
    }

    public function mealsList()
    {

        $meals = Meal::all();

        $meals = MealResource::collection($meals);
        
        return res('success', $meals);
    }

    public function listOfFoodByMyFoodType($req)
    {
        $food_type_id = decode($req->food_type_hashid, 'uuid');
        $restaurant_id = decode($req->restaurant_hashid, 'uuid');
        
        $food_type = RestaurantFoodCategoryPivot::where('id', $food_type_id)->first();
        
        $food_category = FoodCategory::where('name', $food_type->food_category_name)->first();

        
        $food = ModelsRestaurantFood::where('restaurant_id', $restaurant_id)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('category_id', $food_category->id)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status == 'approved'){
            $food = ModelsRestaurantFood::where('restaurant_id', $restaurant_id)
            ->where('name', 'like', '%' . $req->key . '%')
            ->where('category_id', $food_category->id)
            ->where('is_approved', 1)
            ->orderBy('created_at', 'desc')
            ->paginate($req->limit); 
        }

        if($req->status == 'pending') 
        {
            $food = ModelsRestaurantFood::where('restaurant_id', $restaurant_id)
            ->where('name', 'like', '%' . $req->key . '%')
            ->where('category_id', $food_category->id)
            ->where(['is_approved' => 0, 'is_declined' => 0])
            ->orderBy('created_at', 'desc')
            ->paginate($req->limit); 
        }

        if($req->status == 'declined')
        {
            $food = ModelsRestaurantFood::where('restaurant_id', $restaurant_id)
            ->where('name', 'like', '%' . $req->key . '%')
            ->where('category_id', $food_category->id)
            ->where('is_declined', 1)
            ->orderBy('created_at', 'desc')
            ->paginate($req->limit); 
        } 

        $foods = FoodShow::collection($food);
        
        return res('success', $foods);
    }

    public function totalOfFoodByMyFoodType($req)
    {
        $food_type_id = decode($req->food_type_hashid, 'uuid');
        $restaurant_id = decode($req->restaurant_hashid, 'uuid');
        
        $food_type = RestaurantFoodCategoryPivot::where('id', $food_type_id)->first();
        $food_category = FoodCategory::where('name', $food_type->food_category_name)->first();

        $count = ModelsRestaurantFood::where('restaurant_id', $restaurant_id)
        ->where('category_id', $food_category->id)
        ->count();
        
        return res('success', $count);
    }

    public function adminChangeName($req)
    {
        if(!auth()->user()->type_info === 'administrator') 
        return res('Only admin can do this process', null, 401);

        $category = decode($req->category_hashid, 'uuid');
        
        $food_category = FoodCategory::where('id', $category)->first();
        if(!$food_category) return res(__('category.not_found'), null, 400);

        if($req->name) $food_category->name = $req->name;
        $food_category->save();
        
        return res('success');
    }

    public function restaurantCoordinates($req)
    {
        if (!$req->restaurant_hashid) return res(__('restaurant.not_found'), null, 400);

        $restaurant_id = decode($req->restaurant_hashid, 'uuid');

        $restaurant = Store::select('id', 'name','business_type_id', 'latitude', 'longitude')
        ->where('id', $restaurant_id)->where('is_approved', 1)->first();
        $restaurant->makeHidden(['status_info', 'ratings', 'id', 'chat_performance', 'ship_out_time', 'cancellation_rate', 'product_count', 'categories','banners','total_staff', 'timings',
        'followers', 'total_foods']);
        
        return res('success', $restaurant);
    }

    public function addRestaurantImage($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
            'base64image' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        $ImgCount = RestaurantImage::where('restaurant_id', $restaurant_id)->where('status', 1)->count();
        
        if($ImgCount >= 10) return res('failed, you cannot add more than 10 iamges');

        $restIMG = RestaurantImage::create([
            'restaurant_id' => $restaurant_id,
            'restaurant_image' => $req->base64image,
        ]);

        $restIMG->save();

        $full = public_path() . '/images/restaurant_img/' . $restIMG->hashid . '-full.png';
        $thumbnail = public_path() . '/images/restaurant_img/' . $restIMG->hashid . '-thumbnail.png';

        if(file_exists($full) && file_exists($thumbnail)) {
            File::delete($full);
            File::delete($thumbnail);
        }

        $local_path = public_path() . '/images/restaurant_img/';
        File::makeDirectory($local_path, 0777, true, true);

        $image = Image::make($restIMG->restaurant_image);
        $image->save($local_path . encode($restIMG->id, 'uuid') . '-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($local_path . encode($restIMG->id, 'uuid') . '-thumbnail.png');

        return res('success');
    }

    public function updateRestaurantImage($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'image_hash_id' => 'required',
            'restaurant_hash_id' =>'required',
            'restaurant_image' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        $image_hash_id = decode($req->image_hash_id, 'uuid');

        $restaurant = RestaurantImage::where('id', $image_hash_id)->where('restaurant_id', $restaurant_id)->first();

        if($req->restaurant_image){
            $restaurant->restaurant_image = $req->restaurant_image;

            $full = public_path() . '/images/restaurant_img/' . $restaurant->hashid . '-full.png';
            $thumbnail = public_path() . '/images/restaurant_img/' . $restaurant->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/restaurant_img/';

            $image = Image::make($restaurant->restaurant_image);
            $image->save($local_path . encode($restaurant->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($restaurant->id, 'uuid') . '-thumbnail.png');
        }

        $restaurant->save();

        return res('success');
    }

    public function RestaurantImgList($req)
    {
        if (!$req->restaurant_hash_id) return res(__('restaurant.not_found'), null, 400);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $restaurant = RestaurantImage::select('id','restaurant_id','restaurant_image','status')
        ->where('restaurant_id', $restaurant_id)->where('status', 1)->get();

        $restaurant->makeHidden(['restaurant_image']);
        
        $restaurant = ImagesResource::collection($restaurant);
        
        return res('success', $restaurant);
    }

    public function RestaurantShow($req)
    {
        if (!$req->restaurant_hash_id) return res(__('restaurant.not_found'), null, 400);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $business_type_id = BusinessType::where('name', 'restaurant')->first();
        if(!$business_type_id) return res('Wmall restaurant services not found', null, 404);

        $restaurant = Store::where('id', $restaurant_id)->where('business_type_id', 3)->first();
        if(!$restaurant) return res(__('restaurant.not_found'), null, 404);

        $data = new Information($restaurant);

        return res('success', $data);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addMultipleFood($req)
    {
        $permissions = ['vendor', 'vendor staff', 'administrator', 'agent'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        //$arr = json_decode($req->foods, true); // ------------- comment for testing--------
        $arr = $req->foods; // ------------- uncomment for testing--------

        if (count($arr) > 0) {
            foreach ($arr as $value) {

                $category_id = decode($value['category_hashid'], 'uuid');
                $category = FoodCategory::where('id', $category_id)->where('status', 1)->first();
                if(!$category) return res(__('category.not_found'), null, 404);
                
                $meal_id = decode($value['meal_hashid'], 'uuid');
                $meal = Meal::where('id', $meal_id)->where('status', 1)->first();
                if(!$meal) return res(__('meal.not_found'), null, 404);
                
                $store_id = decode($value['store_hashid'], 'uuid');
                $store = Store::where('id', $store_id)->first();
                if(!$store) return res(__('store.not_found'), null, 404);
                if($store->businessType->name != 'restaurant') return res(__('restaurant.not_restaurant'), null, 400);
                
                $ifExist = ModelsRestaurantFood::where(['restaurant_id' => $store->id, 'name' => $value['name']])->first();
                if($ifExist) return res(__('food already exist'), null, 404);

                $new = new StoreChangeField;
                $new->type = 'add food';
                $new->data = $value;
                $new->request_by = auth()->id();
                $new->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;
                $new->process_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;

                $foods = new ModelsRestaurantFood;
                $foods->restaurant_id = $store->id;
                $foods->name = $value['name'];
                $foods->price = $value['price'];
                $foods->meal = $meal->name;
                if($value['description']) $foods->description = $value['description'];
                if(!$value['description']) $foods->description = "";
                $foods->food_image = "";
                $foods->category_id = $category->id;
                $foods->is_available = 1;
                $foods->status = 1;
                $foods->is_approved = 1;
                $foods->is_declined = 0;
                $foods->process_by = auth()->id();
                $foods->request_by = auth()->id();
                $foods->status = 1;
                $foods->save();
                $new->restaurant_foods_id = $foods->id;
                $new->store_id = $foods->restaurant_id;
                $new->save();

                $this->restaurantPerCategory1(['restaurant_id' => $foods->restaurant_id, 'food_category_id' => $foods->category_id]);
            }
        }

        return res('success', null, 200);
    }

    public function restaurantPerCategory1($data)
    {
        $categort = FoodCategory::where('id', $data['food_category_id'])->where('status', 1)->first();

        $ifExists = RestaurantFoodCategoryPivot::where('restaurant_id', $data['restaurant_id'])
        ->where('food_category_name', $categort->name)
            ->where('status', 1)->first();

        if($ifExists) return false;
        
        $food_type = new RestaurantFoodCategoryPivot;
        $food_type->restaurant_id = $data['restaurant_id'];
        $food_type->food_category_name = $categort->name;
        $food_type->food_type_image = "";
        $food_type->save();
    }

    public function addLocation($req)
    {
        $validator = Validator::make($req->all(), [
            'address'   => 'required',
            'latitude'  => 'required',
            'longitude' => 'required',
            'place_id'  => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $check = UserAddress::where('user_id', auth()->id())->where('place_id', $req->place_id)->first();
        $addresses = explode(',',$req->address);

        $count = UserAddress::where('user_id', auth()->id())->count();
        if(!$check){
            $add                = new UserAddress();
            $add->user_id       = auth()->id();
            $add->street        = $addresses[0];
            $add->city          = $addresses[1];
            $add->state         = end($addresses);
            $add->latitude      = $req->latitude;
            $add->longitude     = $req->longitude;
            $add->place_id      = $req->place_id;
            $add->is_current    = $count == 0 ? 1 : 0;
            $add->save();

            return res('success', null, 200);
        }

        return res('This address is already added', null, 200);
    }

    public function editOrderDuration($req)
    {
        $validator = Validator::make($req->all(), [
            'store_hashid'   => 'required',
        ]);
        
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        $duration = new stdClass;
        if($req->minimum) {
            $duration->minimum = (int) $req->minimum;
            $duration->maximum = $store->order_duration['maximum'];

            $store->order_duration = $duration;
        }
        if($req->maximum) {
            $duration->minimum = $store->order_duration['minimum'];
            $duration->maximum = (int) $req->maximum; 

            $store->order_duration = $duration;
        }

        $store->save();

        return res('success', null, 201);
    }

    public function searchMonster($req)
    {
        $v = Validator::make($req->all(),[
            'key' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $s = Store::select('id','name', 'business_type_id')->where('name','LIKE','%'.$req->key.'%')->where('business_type_id', 3)->orderBy('name', 'asc')->get();

        $f = ModelsRestaurantFood::select('id','name','restaurant_id')->where('name','LIKE','%'.$req->key.'%')->orderBy('name', 'asc')->get();

        if($s)$s->makeHidden(['id','business_type_id','status_info','ratings', 'chat_performance','ship_out_time','cancellation_rate','product_count','categories','banners'
        ,'total_staff','timings','followers','total_foods']);

        if($f)$f->makeHidden(['id','reviews']);
        
        $s = RestaurantSearch::collection($s);
        $f = FoodSearch::collection($f);

        $collect = array(['Restaurant' => $s, 'Food' => $f]);

        return res('success', $collect);
    }

    public function search_store($req)
    {
        $v = Validator::make($req->all(),[
            'key' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $s = Store::where('name','LIKE','%'.$req->key.'%')
        ->where('business_type_id', 3)->limit($req->limit)->orderBy('name', 'asc')->paginate($req->limit);

        $s = RestaurantResource::collection($s);

        return res('success', $s);
    }

    public function search_store_food($req)
    {
        $v = Validator::make($req->all(),[
            'key' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $s = ModelsRestaurantFood::where('name','LIKE','%'.$req->key.'%')->limit($req->limit)->orderBy('name', 'asc')->paginate($req->limit);

        $s = new FoodListCollection($s);

        return res('success', $s);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listOfFoodType()
    {
        $types = FoodTypeModel::get();
        return res('success', $types, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeListOfFoodType($req):object
    {
        $validator = Validator::make($req->all(),[
            'store_hashid' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);
        
        $array = [];
        $types = FoodTypeModel::get();
        foreach ($types as $type){
            $total = FoodTypePivot::where('food_type_id', $type->id)->where('store_id', $store_id)->where('status', 1)->count();
            array_push($array, [
                'total'     => $total,
                'hashid'    => $type->hashid,
                'name'      => $type->name,
            ]);
        }
        return res('success', $array, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeListwithFollow($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $r = Store::where('business_type_id', 3)->get();
        $r = StoreList::collection($r);

        return res('success', $r, 200);
    }

     /**
     * @param $req
     * @return JsonResponse
     */
    public function menuList($req):object
    {
        $validator = Validator::make($req->all(),[
            'store_hashid' => 'required',
        ]);

        if($validator->fails()) {
            return res('failed', $validator->errors(), 402);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $list = [];
        $food_types = FoodTypeModel::get();
        foreach ($food_types as $food_type){
            $food_ids = FoodTypePivot::where('store_id', $store->id)->where('food_type_id', $food_type->id)->where('status', 1)->pluck('food_id');
            $foods = ModelsRestaurantFood::whereIn('id', $food_ids)->get();

            $obj = [
                'food_type_hashid'  => $food_type->hashid,
                'food_type_name'    => $food_type->name,
                'foods' => new FoodListCollection($foods)
            ];
            
            if(sizeof($foods) >= 1){
                array_push($list, $obj);
            }
        }

        return res('success', $list, 200);
    }

    public function restaurant_details($req)
    {
        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required'
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $r = decode($req->restaurant_hash_id, 'uuid');
        $restaurant = Store::where('id', $r)->where('business_type_id', 3)->where('is_approved', 1)->first();

        $restaurant = new RestaurantResource($restaurant);
        return res('success', $restaurant);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function isFollowed($req):object
    {
        $validatior = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($validatior->fails()) return res('failed', $validatior->errors(), 402);
        
        $restaurant_id  = decode($req->restaurant_hash_id, 'uuid');
        $restaurant     = Store::where('id', $restaurant_id)->first();
        if(!$restaurant){
            return res(__('restaurant.not_found'), null, 404);
        } 

        $store_following = StoreFollowing::where('store_id', $restaurant_id)->where('user_id', auth()->id())->where('is_followed', 1)->first();
        if(!$store_following){
            return res('success', 0, 200);
        }

        return res('success', 1, 200);
    }
}