<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
class CoverPhotoImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data;

        $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';

        if($store->cover_photo_img){
            $image = Image::make($store->cover_photo_img);
            $image->save($store_path . 'cover-photo-full.png');
            $image->resize(150, 150);
            $image->save($store_path . 'cover-photo-thumbnail.png');
        }

    }
}
