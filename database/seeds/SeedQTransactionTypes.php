<?php

use App\Models\QRTransactionType;
use Illuminate\Database\Seeder;

class SeedQTransactionTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['product', 'customer', 'restaurant', 'parcel', 'marketplace', 'hotel', 'activity'];

        foreach ($names as $name) {
            QRTransactionType::firstOrCreate(['name' => $name]);
        }
    }
}
