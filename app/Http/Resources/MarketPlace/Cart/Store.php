<?php

namespace App\Http\Resources\MarketPlace\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Store as StoreModel;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cart_hashid'           => $this->hashid,
            'store_hashid'          => $this->getStore()->hashid,
            'transaction_number'    => $this->transaction_number,
            'store_name'            => $this->getStore()->name,
            'product'               => new StoreItemCollection($this->items),
        ];
    }

    public function getStore()
    {
        $store = StoreModel::where('id', $this->store_id)->first();
        return $store;
    }
}
