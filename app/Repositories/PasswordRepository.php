<?php

namespace App\Repositories;

use App\User;
use App\Interfaces\PasswordInterface;
use Illuminate\Support\Facades\Validator;

class PasswordRepository implements PasswordInterface
{
    public function requestEmail($req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email', $req->email)->where('status', 1)->first();
        if (!$user) {
            return res(__('auth.email_not_found'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        $user->password_token = generateEmailToken($user);
        $user->save();

        // TODO: Send email for link

        return res(__('auth.email_password_token', ['email' => $req->email]));
    }

    public function change($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required',
            'password' => 'required|min:6|alpha_num|confirmed'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $user = User::where('password_token', $req->token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }
        $user->password_token = null;
        $user->password = bcrypt($req->password);
        $user->save();

        return res('Success');
    }

    public function requestMobile($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true)
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile', $req->mobile)->where('status', 1)->where('iso', session('iso'))->first();
        if (!$user) {
            return res(__('auth.mobile_not_found'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        $user->password_token = generateMobileToken();
        $user->save();

        // TODO: Send text for link

        return res(__('auth.mobile_password_token', ['mobile' => getMobilePrefix(session('iso')) . $req->mobile]));
    }
}
