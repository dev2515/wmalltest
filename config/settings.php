<?php

return [
    'driver_delivery_preferences' => [
        'marketplace' => env('SETTINGS_DRIVER_DELIVERY_MARKETPLACE', 1),
        'restaurant' => env('SETTINGS_DRIVER_DELIVERY_RESTAURANT', 1),
        'hotel' => env('SETTINGS_DRIVER_DELIVERY_HOTEL', 1),
        'activity' => env('SETTINGS_DRIVER_DELIVERY_ACTIVITY', 1),
    ],
];
