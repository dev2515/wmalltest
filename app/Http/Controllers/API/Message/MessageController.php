<?php

namespace App\Http\Controllers\API\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\MessageInterface;

class MessageController extends Controller
{
    protected $req;
    protected $message;

    public function  __construct(Request $req, MessageInterface $message)
    {
        $this->req = $req;
        $this->message = $message;
    }

    public function userSendMessage()
    {
        return $this->message->userSendMessage($this->req);
    }

    public function userConversationList()
    {
        return $this->message->userConversationList($this->req);
    }

    public function userMessageList()
    {
        return $this->message->userMessageList($this->req);
    }

    public function storeMessageList()
    {
        return $this->message->storeMessageList($this->req);
    }

    public function storeSendMessage()
    {
        return $this->message->storeSendMessage($this->req);
    }

    public function storeConversationList()
    {
        return $this->message->storeConversationList($this->req);
    }

    public function sendEmail()
    {
        return $this->message->sendEmail($this->req);
    }
}
