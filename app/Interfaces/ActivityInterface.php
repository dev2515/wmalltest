<?php

namespace App\Interfaces;

interface ActivityInterface
{
    public function list($req);

    public function add($req);

    public function update($req);

    public function addImage($req);

    public function updateImage($req);
    
    public function typeList($req);

    public function addType($req);

    public function updateType($req);
}