<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodReview extends Model
{
    protected $table = 'food_reviews';
    protected $fillable = ['user_id', 'restaurant_id', 'food_id', 'rate', 'review', 'flag', 'comment'];
    protected $append = ['hash_id','restaurant_id'];

     public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Restaurant::class, 'restaurant_id');
    }

    public function food()
    {
        return $this->belongsTo(\App\Models\RestaurantFood::class, 'food_id');
    }
}
