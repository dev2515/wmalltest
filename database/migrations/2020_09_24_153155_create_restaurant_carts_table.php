<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('restaurant_carts')){
            Schema::create('restaurant_carts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('user_id')->unsigned();
                $table->integer('restaurant_id')->unsigned();
                $table->double('voucher_value')->nullable()->default(null);
                $table->string('voucher_type')->nullable()->default(null);
                $table->json('voucher_condition')->nullable()->default(null);
                $table->integer('on_checkout')->default(0);
                $table->integer('is_ordered')->default(0);
                $table->integer('is_deleted')->default(0);
                $table->unsignedBigInteger('table_id')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_carts');
    }
}
