<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\VendorImagesEvent' => [
            'App\Listeners\ProfilePhotoImgListener',
            'App\Listeners\CoverPhotoImgListener',
            'App\Listeners\SyncDaysToStoreListener',
            'App\Listeners\SetDefaultTimePerDayListener',
            'App\Listeners\SetDefaultServices',
            'App\Listeners\SetFoodAvailablesListener',
        ],
        'App\Events\AddFoodEvent' => [
            'App\Listeners\AddStoreRequestListener',
            'App\Listeners\AddFoodImageListener',
            'App\Listeners\AddFoodBranchInventoryListener',
        ],
        'App\Events\EditFoodEvent' => [
            'App\Listeners\EditFoodStoreRequestListener',
        ],
        'App\Events\AddDriver' => [
            'App\Listeners\AddDriverVehicleListener',
            'App\Listeners\AddDriverLicenseListener',
            'App\Listeners\AddDriverActivityListener',
            'App\Listeners\AddDriverSettingListener',
            'App\Listeners\AddDriverProfilePicListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
