<?php

namespace App\Interfaces;

interface ShippingInterface
{
    public function getProviders($req);

    public function categories($req);
}
