<?php

namespace App\Models;

use App\Models\State;
use App\Models\ShippingRate;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    public function shipping_rate()
    {
        return $this->belongsTo(ShippingRate::class, 'shipping_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state', 'name');
    }
}
