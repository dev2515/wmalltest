<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreChangeFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('store_change_fields')){
            Schema::create('store_change_fields', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('store_id');
                $table->string('type');
                $table->longText('value');
                $table->integer('is_approved')->default(0);
                $table->integer('is_declined')->default(0);
                $table->integer('process_by')->default(0);
                $table->smallInteger('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_change_fields');
    }
}
