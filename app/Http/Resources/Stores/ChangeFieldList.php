<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class ChangeFieldList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'            => $this->hashid,
            'type'              => ucwords($this->type),
            'requested_name'    => $this->requestBy(),
            'store_name'        => $this->store ? $this->store->name : null,
            'status'            => $this->status(),
            'created_at'        => $this->created_at,
        ];
    }

    public function requestBy()
    {
        if($this->store){
            if($this->request_by == 0){
                $user = User::where('id', $this->store->user_id)->first();
                if(!$user) return null;

                return $user->name;
            }
            
            $user = User::where('id', $this->request_by)->first();
            if(!$user) return null;
            
            return $user->name;
        }

        $user = User::where('id', $this->request_by)->first();
        if(!$user) return null;
        
        return $user->name;
    }

    public function status()
    {
        if($this->is_approved === 1) return 'approved';

        if($this->is_declined === 1) return 'declined';

        if($this->is_approved === 0 && $this->is_declined === 0) return 'pending';

    }
}
