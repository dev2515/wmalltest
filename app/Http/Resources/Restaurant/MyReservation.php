<?php

namespace App\Http\Resources\Restaurant;

use App\Models\Restaurant;
use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use App\Models\RestaurantFood;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\RestaurantTable;
use App\Models\Store;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class MyReservation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $order = 'no';
        if(!$this->order()->reservation_id){$order = 'No';}
        else{$order= 'Yes';}

        $status = 'Open';
        if($this->status == 1){$status = 'Open';}
        elseif($this->status == 2){$status = 'accepted';}
        elseif($this->status == 1){$status = 'Cancelled';}
        elseif($this->status == 0){$status = 'Expired';}
        else{$status= 'Done';}

        return [
            'reservation_hash_id'=>             $this->hash_id,
            'restaurant' =>                     $this->store(),
            'full_cover' =>                     asset($this->cover()->full),
            'thumbnail_cover' =>                asset($this->cover()->thumbnail),
            'full' =>                           asset($this->photo()->full),
            'thumbnail' =>                      asset($this->photo()->thumbnail),
            'table_no' =>                       $this->table_number,
            'time_in' =>                        date('H:i', $this->arrive_time),
            'date' =>                           date('Y-m-d', $this->arrive_date),
            'number_of_guest' =>                strval($this->guest),
            'with_order' =>                     $order,
            'status' =>                         $status,
            'my_pre_order' =>                   $this->order_food_list(),
            'total_amount' =>                   $this->order()->sub_total !== null ? $this->text : '0.00',
            'my_cart' =>                        $this->mycart(),
            'qr_code' =>                        $this->qrcode,
            'qrcode_status' =>                  $this->qrcode_status,
            'special_request' =>                $this->special_request,
        ];
    }

    public function store()
    {
        $store = Store::select('id', 'name', 'store_information','phone','latitude', 'longitude')->where('id', $this->restaurant_id)->first();
        if($store != null) $store->makeHidden(['id', 'status_info', 'ratings', 'chat_performance', 'cancellation_rate', 'product_count', 'categories', 'banners', 'total_staff', 'timings', 'followers',
        'total_foods', 'ship_out_time']);
        return $store;
    }

    public function photo()
    {      
        $full = public_path() . '/images/stores/' . $this->store()->hashid . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->store()->hashid . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->store()->hashid . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->store()->hashid . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function cover()
    {      
        $full = public_path() . '/images/stores/' . $this->store()->hashid . '/cover-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->store()->hashid . '/cover-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->store()->hashid . '/cover-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->store()->hashid . '/cover-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function order()
    {
        $order = RestaurantOrder::select('id', 'reservation_id','order_type', 'sub_total')
        ->where('reservation_id', $this->id)->where('order_type', 1)->first();
        return $order;
    }

    public function order_food_list()
    {
        $order = RestaurantOrderList::select('id', 'order_id', 'order_type')->where('order_id', $this->order()->id)->where('order_type', 'Dine In')->first()->id;
        if($order != null){
            $order_food_list =  RestaurantOrderFood::select('*')->where('order_list_id', $order)->get();
            if($order_food_list != null) $order_food_list->makeHidden(['id','order_list_id','food_id','commission','is_rate','created_at','updated_at']);
        }

        if(!$order) $order_food_list = [];

        return $order_food_list;
    }

    public function mycart()
    {
        $rc = RestaurantCart::select('id','user_id','restaurant_id','transaction_number','reservation_id','created_at')
                ->where('on_checkout',0)->where('is_ordered',0)->where('reservation_id', $this->id)->first();               
        if($rc) $rc->makeHidden(['id','user_id','restaurant_id','reservation_id','restaurant_info','foods_info.on_checkout']);

        if(!$rc) $rc = null;

        return $rc;
    }
}
