<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Models\SponsorQidImage;
class SponsorQidImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data->model;

        $sponsor_qid_images = json_decode($event->data->sponsor_qid_images, true);

        foreach ($sponsor_qid_images as $image){
            $new = new SponsorQidImage;
            $new->store_id = $store->id;
            $new->base64 = $image;
            $new->save();

            $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';
        
            $path = $store_path . 'sponsor-qid-images/';

            if(!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/stores/' . encode($store->id, 'uuid') . '/sponsor-qid-images', $mode = 0777, true, true);
            }

            $image = Image::make($new->base64);
            $image->save($path . $new->id .'-sponsor-qid-full.png');
            $image->resize(150, 150);
            $image->save($path . $new->id .'-sponsor-qid-thumbnail.png');
        }

        
    }
}
