<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use App\Models\RestaurantCartItemVariation;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'customer' =>                       $this->user->name,
            'table_no' =>                       $this->table_id,
            'reservation_hash_id' =>            encode($this->reservation_id,'uuid'),    
            'cart_hash_id' =>                   $this->hash_id,
            'cart_id' =>                        $this->id,
            'restaurant' =>                     $this->restaurant_info, 
            'food' =>                           $this->foods_info,
            'total_info' =>                     $this->totalinfo(),
        ];
    }

    public function totalinfo()
    {
        $select_all = false;
        $cart_count = RestaurantCart::where('user_id', auth()->user()->id)->where('is_deleted', 0)->count();
        $on_checkout_count = RestaurantCart::where('user_id', auth()->user()->id)->where('is_deleted', 0)->where('on_checkout', 0)
        ->where('is_ordered', 0)->count();
        
        if ($on_checkout_count >= $cart_count) {
            $select_all = true;
        }
        if($cart_count and $on_checkout_count)
        {
            $total = $this->calculateTotalAmount();
            $collect = collect($total);
        }else{$collect = null;}
        

        return $collect;
    }

    public function calculateTotalAmount() 
    {
        $total = 0.00;
        $total_count = 0;
        $user_id = auth()->user()->id;
        $cart_ids = RestaurantCart::where('id', $this->id)->where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $items = RestaurantCartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->get();
        
        foreach ($items as $item) {
    
           // return res($v_item)->send();
            $total += $item->totalamount;
            $total_count += $total_count + 1;
        }

        $t = collect(['total' => twoDecimalPlaces($total), 'total_count' => $total_count]);
        return $t;
    }
}