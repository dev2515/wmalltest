<?php

namespace App\Listeners;

use App\Events\AddDriver;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DriverVehicle;

class AddDriverVehicleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddDriver  $event
     * @return void
     */
    public function handle(AddDriver $event)
    {
        $info = auth()->user()->type_info;
        $data = $event->data;

        $vehicle                        = new DriverVehicle;
        $vehicle->user_id               = $data['user']['id'];
        $vehicle->requested_by          = auth()->id();
        $vehicle->processed_by          = $info == 'administrator' ? auth()->id() : 0;
        $vehicle->owner                 = 'N/A';
        $vehicle->nationality           = 'N/A';
        $vehicle->registration_status   = 'not-submitted';
        $vehicle->registration_date     = 'N/A';
        $vehicle->expiration_date       = $data['vehicle_expiration_date'];
        $vehicle->plate_number          = $data['plate_number'];
        $vehicle->make                  = 'N/A';
        $vehicle->model                 = 'N/A';
        $vehicle->color                 = 'N/A';
        $vehicle->transmission          = 'N/A';
        $vehicle->year_model            = 0;
        $vehicle->is_approved           = $info == 'administrator' ? 1 : 0;
        $vehicle->save();
    }
}
