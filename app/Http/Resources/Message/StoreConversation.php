<?php

namespace App\Http\Resources\Message;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserConversation;
use App\User;
use stdClass;
class StoreConversation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'    =>      $this->hashid,
            'time'      =>      $this->time(),
            'user'     =>      $this->user(),
            'is_seen'   =>      $this->isNew() > 0 ? 0 : 1,
            'new_message_total'   =>      $this->isNew(),
        ];
    }

    public function time()
    {
        $user_conversation = UserConversation::where('conversation_id', $this->id)->where('created_at', 'DESC')->first();
        if(!$user_conversation) return $this->created_at;

        return $user_conversation->created_at;
    }

    public function user()
    {
        $user = User::where('id', $this->user_id)->first();
        if(!$user) return null;

        $data = new stdClass;
        $data->hashid = $user->hash_id;
        $data->name = $user->name;

        return $data;
    }

    public function isNew()
    {
        $count = UserConversation::where('conversation_id', $this->id)->where('is_from_user', 1)->where('is_new', 1)->where('deleted_user', '!=', 1)->count();
        return $count;
    }
}
