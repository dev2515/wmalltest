<?php

namespace App\Interfaces;

interface StoreInterface
{
    public function find($req);

    public function details($req);
    
    public function listOfdays($req);

    public function showTimeSchedule($req);

    public function addTimeShedule($req);

    public function openOrClose($req);

    public function editTimeSchedule($req);

    public function deleteTimeSchedule($req);

    public function addAdvertisement($req);

    public function storeAdvertisementList($req);

    public function editAdvertisement($req);

    public function deleteAdvertisement($req);

    public function durationList();

    public function changeFieldTypes();
}