<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use App\Models\Product;
use App\Models\Meal;
use App\Models\Store;
use App\Models\FoodCategory;
use App\Models\RestaurantFood;
use App\Http\Resources\Images\ProductImageCollection;

use Carbon\Carbon;
use App\User;
use App\Models\DriverLicense;
use App\Models\DriverVehicle;
class StoreChangeField extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'        => $this->hashid,
            'type'          => ucwords($this->type),
            'data'          => $this->dynamicReturn(),
            'status'        => $this->status(),
            'created_at'    => $this->created_at,
            'store'         => $this->store_id ? $this->store() : null,
            'user'          => $this->user_id ? $this->user() : null,
            'request_by'    => $this->requestBy()

        ];
    }

    public function dynamicReturn()
    {
        switch ($this->type) {
            case 'informations' :
                return $this->data;
            break;
            case 'description' :
                $data = new stdClass;
                $data->description = $this->data;

                return $data;
            break;
            case 'locations' :
                return $this->data;
            break;
            case 'add branches' :
                return $this->data;
            break;
            case 'add brand' :
                return $this->data;
            break;
            case 'edit food' :
            $data = [
                'name'          => $this->data->name,
                'price'         => centToPrice($this->data->price),
                'description'   => $this->data->description,
                'store'         => $this->getStoreName(),
                'full'          => $this->foodPhoto('edit')->full,
                'thumbnail'     => $this->foodPhoto('edit')->thumbnail
            ];
                return $data;
            break;
            case 'add food' :
                $food = RestaurantFood::where('id', $this->restaurant_foods_id)->first();
                if(!$food) return __('food.not_found');

                $data = [
                    'name'          => $food->name,
                    'price'         => centToPrice($food->price),
                    'description'   => $food->description,
                    'store'         => $this->getStoreName(),
                    'full'          => $this->foodPhoto()->full,
                    'thumbnail'     => $this->foodPhoto()->thumbnail
                ];
              
                return $data;
            break;
            case 'add product' :
                $data                       = new stdClass;
                $data->price                = $this->data->price = (int) $this->data->price;
                $data->store_id             = $this->data->store_id = $this->data->store_id;
                $data->name                 = $this->data->name = $this->data->name;
                $data->on_sale              = $this->data->on_sale = (int) $this->data->on_sale;
                $data->sale_price           = $this->data->sale_price = (int) $this->data->sale_price;
                $data->short_description    = $this->data->short_description = $this->data->short_description;
                
                $data->images = new ProductImageCollection($this->productImages());  

                return $data;
            break;
            case 'user details' :
                return $this->data;
            break;
            case 'user profile photo' :
                return $this->profilePhoto();
            break;
            case 'profile photo' :
                return $this->profilePhoto();
            break;
            case 'cover photo' :
                return  $this->coverPhoto();
            break;
            case 'legal document images' :
                $data                   = new stdClass;
                $data->document_type    = $this->document_name ? $this->document_name : null;
                $data->images           = new StoreRequestDocumentImgCollection($this->documentRequestImages);

                return $data;
            break;
            case 'legal document PDF' :
                return $this->fileName();
            break;
            case 'driver-licenses' :

                $license = DriverLicense::where('user_id', $this->user_id)->first();
                if(!$license) return null;

                $data = new stdClass;

                $full = public_path() . '/images/driver-licenses/' . encode($this->user_id, 'uuid') . '/full.png';

                if(file_exists($full)){
                    $full = '/images/driver-licenses/' . encode($this->user_id, 'uuid') . '/full.png';
                } else { $full = null; }

                $thumbnail = public_path() . '/images/driver-licenses/' . encode($this->user_id, 'uuid') . '/thumbnail.png';

                if(file_exists($thumbnail)){
                    $thumbnail = '/images/driver-licenses/' . encode($this->user_id, 'uuid') . '/thumbnail.png';
                } else { $thumbnail = null; }

                $data->file_type    = $license->file_type;
                $data->full         = $license->file_type == 'img' ? asset($full) : null;
                $data->thumbnail    = $license->file_type == 'img' ? asset($thumbnail) : null;
                $data->file_name    = $license->file_type == 'pdf' ? $license->file_name : null;

                return $data;
                
            break;

            case 'vehicle-registrations' :
                $vehicle    = DriverVehicle::where('user_id', $this->user_id)->first();
                if(!$vehicle) return null;

                $data = new stdClass;

                $full = public_path() . '/images/vehicle-registrations/' . encode($this->user_id, 'uuid') . '/full.png';

                if(file_exists($full)){
                    $full = '/images/vehicle-registrations/' . encode($this->user_id, 'uuid') . '/full.png';
                } else { $full = null; }

                $thumbnail = public_path() . '/images/vehicle-registrations/' . encode($this->user_id, 'uuid') . '/thumbnail.png';

                if(file_exists($thumbnail)){
                    $thumbnail = '/images/vehicle-registrations/' . encode($this->user_id, 'uuid') . '/thumbnail.png';
                } else { $thumbnail = null; }

                $data->file_type    = $vehicle->file_type;
                $data->full         = $vehicle->file_type == 'img' ? asset($full) : null;
                $data->thumbnail    = $vehicle->file_type == 'img' ? asset($thumbnail) : null;
                $data->file_name    = $vehicle->file_type == 'pdf' ? $vehicle->registration_file_name : null;

                return $data;

            default :
                return res('Invalid type', null, 400);
            break;
        }
    }

    public function profilePhoto()
    {
       $full = public_path() . '/images/store-requests/' . $this->hashid . '/profile-photo-full.png';

       if(file_exists($full)){
            $full = '/images/store-requests/' . $this->hashid . '/profile-photo-full.png';
        } else { $full = null; }

        $thumbnail = public_path() . '/images/store-requests/' . $this->hashid . '/profile-photo-thumbnail.png';

        if(file_exists($thumbnail)){
            $thumbnail = '/images/store-requests/' . $this->hashid . '/profile-photo-thumbnail.png';
        } else { $thumbnail = null; }


        $data               = new stdClass;
        $data->full         = asset($full);
        $data->thumbnail    = asset($thumbnail);

        return $data;
    }

    public function coverPhoto(Type $var = null)
    {
        $full = public_path() . '/images/store-requests/' . $this->hashid . '/cover-photo-full.png';

       if(file_exists($full)){
            $full = '/images/store-requests/' . $this->hashid . '/cover-photo-full.png';
            } else { $full = null; }

        $thumbnail = public_path() . '/images/store-requests/' . $this->hashid . '/cover-photo-thumbnail.png';

        if(file_exists($thumbnail)){
            $thumbnail = '/images/store-requests/' . $this->hashid . '/cover-photo-thumbnail.png';
            } else { $thumbnail = null; }


        $data               = new stdClass;
        $data->full         = asset($full);
        $data->thumbnail    = asset($thumbnail);

        return $data;
    }

    public function fileName()
    {
        if(!$this->store) return null;

        $data = new stdClass;
        $data->document_type = $this->data->document_name;
        if ($this->data->document_name === 'signature')                 $data->file_name = $this->store->signature_file_name;
        if ($this->data->document_name === 'computer-card')             $data->file_name = $this->store->cc_file_name;
        if ($this->data->document_name === 'commercial-registration')   $data->file_name = $this->store->cr_file_name;
        if ($this->data->document_name === 'commercial-permit')         $data->file_name = $this->store->cp_file_name;
        if ($this->data->document_name === 'sponsor-qid')               $data->file_name = $this->store->sq_file_name;

        
        return $data;
    }

    public function store()
    {
        $dt = Carbon::parse($this->updated_at);
        
        if(!$this->store) return null;

        $full = public_path().'/images/stores/' . $this->store->hashid . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->store->hashid . '/profile-photo-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . $this->store->hashid . '/profile-photo-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . $this->store->hashid . '/profile-photo-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;

        $data->hash_id = $this->store->hashid;
        $data->name = $this->store->name;

        return $data;
    }

    public function requestBy()
    {
        if($this->store){
            if($this->request_by == 0){
                $user = User::where('id', $this->store->user_id)->first();
                if(!$user) return null;

                $data                   = new stdClass;
                $data->name             = $user->name;
                $data->role_type        = $user->type_info;
                $data->contact_number   = $user->mobile;
                $data->email            = $user->email;
                
                return $data;
            }
            
            $user = User::where('id', $this->request_by)->first();
            if(!$user) return null;
            
            $data                   = new stdClass;
            $data->name             = $user->name;
            $data->role_type        = $user->type_info;
            $data->contact_number   = $user->mobile;
            $data->email            = $user->email;
            
            return $data;
        }

        $user = User::where('id', $this->request_by)->first();
        if(!$user) return null;
        
        $data                   = new stdClass;
        $data->name             = $user->name;
        $data->role_type        = $user->type_info;
        $data->contact_number   = $user->mobile;
        $data->email            = $user->email;
        
        return $data;

        
    }

    public function user()
    {
        $user = User::where('id', $this->user_id)->first();
        if(!$user) return null;

        $data                   = new stdClass;
        $data->name             = $user->name;
        $data->role_type        = $user->type_info;
        $data->contact_number   = $user->mobile;
        $data->email            = $user->email;

        return $data;
    }

    public function status()
    {
        if($this->is_approved === 1) return 'approved';

        if($this->is_declined === 1) return 'declined';

        if($this->is_approved === 0 && $this->is_declined === 0) return 'pending';

    }

    public function productImages()
    {
        $product = Product::where('id', $this->product_id)->first();

        return $product->images;
    }

    public function categoryName()
    {
        $category_id    = decode($this->data->category_hashid, 'uuid');
        $category       = FoodCategory::where('id', $category_id)->first();

        return $category;
    }

    public function getStoreName():string
    {
        $store      = Store::where('id', $this->store_id)->first();
        if(!$store) return __('store.not_found');

        return $store->name;
    }

    public function foodPhoto($type = 'add'):object
    {   
        if($type == 'edit'){

            $full = public_path() . '/images/store-requests/' . encode($this->id, 'uuid') . '/full.png';
            if(file_exists($full)){
                $full = '/images/store-requests/' . encode($this->id, 'uuid') . '/full.png';
            } else { $full = null;}

            $thumb = public_path() . '/images/store-requests/' . encode($this->id, 'uuid') . '/thumbnail.png';

            if(file_exists($thumb)){
                $thumb = '/images/store-requests/' . encode($this->id, 'uuid') . '/thumbnail.png';
            } else { $thumb = null;}

            $data               = new stdClass;
            $data->full         = asset($full);
            $data->thumbnail    = asset($thumb);

            return $data;
        }

        $food = RestaurantFood::where('id', $this->restaurant_foods_id)->first();
        if (!$food) return __('food.not_found');

        $full = public_path() . '/images/food/' . $food->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $food->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $food->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $food->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data               = new stdClass;
        $data->full         = asset($full);
        $data->thumbnail    = asset($thumb);

        return $data;
    }

    public function meal()
    {
        $meal_id    = decode($this->data->meal_hashid, 'uuid');
        $meal       = Meal::where('id', $meal_id)->where('status', 1)->first();

        return $meal;
    }
}
