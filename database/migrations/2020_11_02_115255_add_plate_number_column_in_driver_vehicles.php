<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlateNumberColumnInDriverVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('driver_vehicles', 'plate_number')) {
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->string('plate_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('driver_vehicles', 'plate_number')) {
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->dropColumn('plate_number');
            });
        }
    }
}
