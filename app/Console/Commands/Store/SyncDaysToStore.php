<?php

namespace App\Console\Commands\Store;

use Illuminate\Console\Command;
use App\Models\Store;
use App\Models\Day;
use App\Models\StoreSchedule;

class SyncDaysToStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:days-to-store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will sync the days to store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = Store::get();

        $bar = $this->output->createProgressBar(count($stores));
        $bar->start();

        $days = Day::get();

        foreach($stores as $store){
                foreach($days as $day){
                    $check = StoreSchedule::where('store_id', $store->id)->where('day_id', $day->id)->first();
                    if(!$check){
                        $add_schedule           = new StoreSchedule();
                        $add_schedule->store_id = $store->id;
                        $add_schedule->day_id   = $day->id;
                        $add_schedule->save();
                    }
                }
            $bar->advance();
        }
        $bar->finish();
    }
}
