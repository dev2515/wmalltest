<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsAddedToVariationTypeSelections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('variation_type_selections', 'is_added_price')){
            Schema::table('variation_type_selections', function (Blueprint $table) {
                $table->boolean('is_added_price')->default(1)->after('price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('variation_type_selections', 'is_added_price')){
            Schema::table('variation_type_selections', function (Blueprint $table) {
                $table->dropColumn('is_added_price');
            });
        }
    }
}
