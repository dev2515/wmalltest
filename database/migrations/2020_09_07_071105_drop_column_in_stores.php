<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('stores', 'commercial_permit')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_permit');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_registration');
            });
        }

        if(Schema::hasColumn('stores', 'eid')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('eid');
            });
        }

        if(Schema::hasColumn('stores', 'qid')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('qid');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
