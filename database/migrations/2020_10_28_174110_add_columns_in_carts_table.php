<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('carts', 'is_ordered')){
            Schema::table('carts', function (Blueprint $table) {
                $table->integer('is_ordered')->default(0)->after('voucher_condition');
            });
        }

        if(!Schema::hasColumn('carts', 'reservation_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->unsignedBigInteger('reservation_id')->nullable()->after('is_deleted');
            });
        }

        if(!Schema::hasColumn('carts', 'order_type')){
            Schema::table('carts', function (Blueprint $table) {
                $table->integer('order_type')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('carts', 'is_ordered')){
            Schema::table('carts', function (Blueprint $table) {
                $table->dropColumn('is_ordered');
            });
        }

        if(Schema::hasColumn('carts', 'reservation_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->dropColumn('reservation_id');
            });
        }

        if(Schema::hasColumn('carts', 'order_type')){
            Schema::table('carts', function (Blueprint $table) {
                $table->dropColumn('order_type');
            });
        }
    }
}
