<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContactNameToStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'contact_name')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('contact_name')->nullable()->after('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('store_branches', 'contact_name')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('contact_name');
            });
        }
    }
}
