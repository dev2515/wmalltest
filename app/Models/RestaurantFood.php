<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantFood extends Model
{
    protected $table = 'restaurant_foods';
    protected $fillable = ['id','name'];
    protected $appends = ['hash_id', 'reviews','variation', 'is_have_variation'];

    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getReviewsAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $count = FoodReview::where('food_id', $id)->where('status', 1)->count();
        $sum = FoodReview::where('food_id', $id)->where('status', 1)->sum('rate');
        return [
            'count' => $count,
            'average' => $count > 0 ? ($sum / $count) : 0
        ];
    }

    public function getVariationAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $variation = FoodVariationSelection::where('food_id', $id)->get();

        return $variation;
    }

    public function getIsHaveVariationAttribute()
    {
        $variation = FoodVariationSelection::where('food_id', $this->id)->where('status', 1)->count();
        if($variation > 0) return 1;
        return 0;
    }

    public function getFoodTypeAttribute()
    {
        
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\FoodCategory::class, 'category_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Store::class, 'restaurant_id');
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\FoodReview::class, 'food_id');
    }

    public function menulist()
    {
        return $this->hasMany(\App\Models\RestaurantMenuList::class, 'food_id');
    }

    public function cart_item()
    {
        return $this->hasMany(\App\Models\RestaurantCartItem::class,'food_id');
    }

    public function favorite()
    {
        return $this->hasMany(\App\Models\FoodFavorite::class, 'food_id');
    }

    public function dineInOrder()
    {
        return $this->hasMany(\App\Models\RestaurantDineInOrder::class, 'food_id');
    }
}
