<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDaysOfReturnToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('products', 'days_of_return')){
            Schema::table('products', function (Blueprint $table) {
                $table->integer('days_of_return')->default(0)->after('sale_date_to');
            });
        }

        if(!Schema::hasColumn('products', 'years_of_warranty')){
            Schema::table('products', function (Blueprint $table) {
                $table->integer('years_of_warranty')->default(0)->after('days_of_return');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('products', 'days_of_return')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('days_of_return');
            });
        }

        if(Schema::hasColumn('products', 'years_of_warranty')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('years_of_warranty');
            });
        }
    }
}
