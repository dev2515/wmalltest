<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveQrcodeImageFromTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('table_reservations','qrcode_image')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->dropColumn('qrcode_image');
             
            });
        }

        if(Schema::hasColumn('restaurant_orders','qrcode_image')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('qrcode_image');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('table_reservations','qrcode_image')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->longText('qrcode_image');
             
            });
        }

        if(!Schema::hasColumn('restaurant_orders','qrcode_image')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->longText('qrcode_image');
            });
        }
    }
}
