<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class QRPointer extends Model
{
    protected $appends = ['hash_id'];

    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactionType()
    {
        return $this->belongsTo(QRTransactionType::class, 'transaction_type_id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Cart::class, 'transaction_number', 'transaction_number');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'assigned_driver_id');
    }

    public function packageType()
    {
        return $this->belongsTo(PackageType::class, 'package_type_id');
    }
}
