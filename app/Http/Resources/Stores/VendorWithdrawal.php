<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorWithdrawal extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'            => $this->hashid,
            'type'              => $this->type,
            'amount'            => $this->amount,
            'company_name'      => $this->company_name,
            $this->mergeWhen($request->type === 'show', [
                'receiver_name'     => $this->receiver_name,
                'bank_name'         => $this->bank_name,
                'account_number'    => $this->account_number,
                'iban'              => $this->iban,
                'qid_number'        => $this->qid_number,
            ]),
            'status'            => $this->status(),
            'created_at'        => $this->created_at
        ];
    }

    /**
     * Transform the resource into an array.
     *
     * @return string
     */
    public function status():string
    {
        if($this->is_approved) return 'approved';
        if($this->is_declined) return 'declined';

        return 'pending';
    }
}
