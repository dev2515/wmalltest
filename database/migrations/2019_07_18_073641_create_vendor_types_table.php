<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vendor_types')) {
            Schema::create('vendor_types', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('slug');
                $table->string('name');
                $table->text('short_description');
                $table->text('long_description');
                $table->text('details')->nullable()->default(null);
                $table->integer('status')->default(1);
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_types');
    }
}
