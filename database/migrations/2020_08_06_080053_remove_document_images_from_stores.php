<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDocumentImagesFromStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('stores', 'computer_card_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('computer_card_img');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_registration_img');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_permit_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_permit_img');
            });
        }

        if(Schema::hasColumn('stores', 'sponsor_qid_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('sponsor_qid_img');
            });
        }

        if(Schema::hasColumn('stores', 'signature_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_img');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
