<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class SeedDefaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where('type', 9)->first();
        $agent = User::where('type', 7)->first();

        if(!$admin) {
            User::firstOrCreate(['name' => 'admin', 'email' => 'admin@email.com', 
            'email_verified_at' => Carbon::now(), 'password' => bcrypt('password'), 'verified_email' => 1, 'status' => 1, 'iso' => 'qat', 'type' => 9]);
        }

        if(!$agent){
            User::firstOrCreate(['name' => 'agentx44', 'email' => 'agentx44@email.com', 
            'email_verified_at' => Carbon::now(), 'password' => bcrypt('password'), 'verified_email' => 1, 'status' => 1, 'iso' => 'qat', 'type' => 7]);
        }
    }
}
