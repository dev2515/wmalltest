<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionNumberInRestaurantCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_carts', 'transaction_number')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->unsignedBigInteger('transaction_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_carts', 'transaction_number')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->dropColumn('transaction_number');
            });
        }
    }
}
