<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $table = 'social_medias';
    
    protected $fillable = [
        'user_id',
        'name',
        'social_type',
        'social_id',
        'status',
    ];
}
