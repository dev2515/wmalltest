<?php

namespace App\Repositories;

use App\Interfaces\AdminInterface;
use Illuminate\Support\Facades\Validator;
use App\Models\StoreChangeField;
use App\Models\Store;
use App\Http\Resources\Stores\StoreCollection;
use App\Http\Resources\Stores\StoreListCollection;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Stores\StoreChangeFieldCollection;
use App\User;
use App\Models\RestaurantFood;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Models\Product;
use App\Model\StoreBranch;
use App\Models\VendorNotification;
use App\Models\StoreRequestImages as StoreRequestImage;
use App\Models\ComputerCardImage;
use App\Models\CommercialRegistrationImage;
use App\Models\CommercialPermitImage;
use App\Http\Resources\Stores\StoreChangeField as StoreChangeFieldResource;
use App\Http\Resources\Stores\ChangeFieldListCollection;
use App\Models\Brand;
use Carbon\Carbon;
use stdClass;
use App\Interfaces\RestaurantDashboardInterface;
class AdminRepository implements AdminInterface
{

    private $restaurant_dashboard;

    public function __construct(RestaurantDashboardInterface $restaurant_dashboard)
    {
        $this->restaurant_dashboard = $restaurant_dashboard;
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function changeStatus($req)
    {
        
        if(auth()->user()->type_info != 'administrator'){
            return res(__('user.not_allowed'), null, 400);
        }
    
        $validator = Validator::make($req->all(), [
            'status' => 'required',
            'hashid' => 'required'
        ]);
        
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $scf_id = decode($req->hashid, 'uuid');
        
        $scf = StoreChangeField::where('id', $scf_id)->where('is_approved', 0)->where('is_declined', 0)->first();
    
        if(!$scf) return res('Request not found', null, 404);

        if($scf->type == 'locations' || $scf->type == 'informations'){
            if(!$scf->data) return res('Data must have value', null, 404);
        }

        $store = null;
        $message = $req->message;
        if($scf->store_id != null){
            $store = Store::where('id', $scf->store_id)->first();
            if(!$store) return res(__('store.not_found'), null, 404);

            if($store->is_declined == 1) $store->is_declined = 0;
        }
        
        
        switch ($scf->type) {
        case 'informations' : // Informations ---

            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }

            $vendor = User::where('id', $store->user_id)->first();
            if(!$vendor) return res(__('Vendor not found'), null, 404);

            $scf->is_approved = 1;
            $scf->process_by = auth()->id();

            if($scf->data->store_name != null) $store->name = $scf->data->store_name;
            if($scf->data->store_phone_number != null) $store->phone = $scf->data->store_phone_number;
            if($scf->data->store_email != null) $store->store_email = $scf->data->store_email;
            if($scf->data->vendor_name != null) $vendor->name = $scf->data->vendor_name;
            if($scf->data->vendor_email != null) $vendor->email = $scf->data->vendor_email;

            if($scf->data->vendor_mobile != null) {
                $mobile = User::where('mobile', $scf->data->vendor_mobile)->first();
                if($mobile) {
                    return res('The vendor mobile number has already been taken', null, 400);
                }
                $vendor->mobile = $scf->data->vendor_mobile;
            }

            $password = explode("@", $scf->data->vendor_email);
            if (strlen($password[0]) < 6) $password = $password[0].generateMobileToken();

            $vendor->password = bcrypt($password[0]);

            $vendor->save();
            $store->save();
            $scf->save();

            $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, 'Store information request has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 200);
        break;
        case 'description' :
            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }
            $store->store_information = $scf->data;
            $scf->is_approved = 1;
            $scf->process_by = auth()->id();

            $scf->save();
            $store->save();

            $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, 'Store description request has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 201);

        break;
            
        case 'locations' : // Locations ---

            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }

            $scf->is_approved = 1;
            $scf->process_by = auth()->id();

            if($scf->data->street != null) $store->street = $scf->data->street;
            if($scf->data->latitude != null) $store->latitude = $scf->data->latitude;
            if($scf->data->longitude != null) $store->longitude = $scf->data->longitude;
            if($scf->data->state != null) $store->state = $scf->data->state;
            if($scf->data->city != null) $store->city = $scf->data->city;
            if($scf->data->other_address_info != null) $store->other_address_info = $scf->data->other_address_info;

            $store->save();
            $scf->save();
            $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, 'Store location request has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'profile photo' : // Profile photo ---

            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved = 1;
            $scf->process_by = auth()->id();

            $store->profile_photo_img = $scf->base64;
            
            $scf->save();
            $store->save();

            $store_parent_folder = public_path() . '/images/stores/' . $store->hashid;
            if(!file_exists($store_parent_folder)){
                File::makeDirectory(base_path() . '/public/images/stores/' . $store->hashid, $mode = 0777, true, true);
            }

            $fullPath = public_path() . '/images/stores/' . $store->hashid . '/profile-photo-full.png';
            $thumbnailPath = public_path() . '/images/stores/' . $store->hashid . '/profile-photo-thumbnail.png';
            
            // @AES Delete existing
            if (File::exists($thumbnailPath) && File::exists($fullPath)) {
                File::delete($thumbnailPath);
                File::delete($fullPath);
            }

            $store_path = public_path() . '/images/stores/' . $store->hashid . '/';

            $image = Image::make($store->profile_photo_img);
            $image->save($store_path . 'profile-photo-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($store_path . 'profile-photo-thumbnail.png');

            $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, 'Store profile photo request has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'cover photo' : // Cover photo ---

            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved = 1;
            $scf->process_by = auth()->id();

            $store->cover_photo_img = $scf->base64;
            
            $scf->save();
            $store->save();

            $store_parent_folder = public_path() . '/images/stores/' . $store->hashid;
            if(!file_exists($store_parent_folder)){
                File::makeDirectory(base_path() . '/public/images/stores/' . $store->hashid, $mode = 0777, true, true);
            }

            $fullPath = public_path() . '/images/stores/' . $store->hashid . '/cover-photo-full.png';
            $thumbnailPath = public_path() . '/images/stores/' . $store->hashid . '/cover-photo-thumbnail.png';
            
            // @AES Delete existing
            if (File::exists($thumbnailPath) && File::exists($fullPath)) {
                File::delete($thumbnailPath);
                File::delete($fullPath);
            }

            $store_path = public_path() . '/images/stores/' . $store->hashid . '/';

            $image = Image::make($store->cover_photo_img);
            $image->save($store_path . 'cover-photo-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($store_path . 'cover-photo-thumbnail.png');

            $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, 'Store cover photo request has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'add branches' :
            
            $new_branch = StoreBranch::where('id', $scf->branch_id)->first();
            if($req->status === 'declined'){
                $scf->is_declined = 1;
                $new_branch->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, $message, 'Add branch request has been decline', 'declined');
                activityLog('Declined branches request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved = 1;
            
            $new_branch->is_approved = 1;
            $new_branch->process_by = auth()->id();
            $new_branch->status = 1;
            

            $scf->process_by = auth()->id();
            $scf->save();
            $new_branch->save();

            $this->notify($scf->type, $scf->id,$scf->request_by, $store->user_id, 'Store add branches request has been approved', 'approve');
            activityLog('Approved branches request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'add brand' :

            $brand = Brand::where('id', $scf->brand_id)->first();
            if(!$brand) return res(__('product.brand_not_found'), null, 404);

            if($req->status === 'declined'){
                $scf->is_declined = 1;
                $brand->is_declined = 1;
                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $brand->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Add brand request has been decline', 'declined');
                activityLog('Declined brand request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved = 1;
            $brand->is_approved = 1;
            $brand->request_by = $scf->request_by;
            $brand->process_by = auth()->id();
            $brand->status = 1;
            $brand->save();
            

            $scf->process_by = auth()->id();
            $scf->save();

            $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Add brand request has been approved', 'approve');
            activityLog('Approved brand request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'add food' :

            $food = RestaurantFood::where('id', $scf->restaurant_foods_id)->first();
            if(!$food) return res(__('food_not_found'), null, 404);

            if($req->status === 'declined'){
                $scf->is_declined = 1;
                $food->is_declined = 1;
                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $food->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Add food request has been decline', 'declined');
                activityLog('Declined food request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved = 1;
            $food->is_approved = 1;
            $food->request_by = $scf->request_by;
            $food->process_by = auth()->id();
            $food->status = 1;
            $food->save();
            

            $scf->process_by = auth()->id();
            $scf->save();

            $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Add food request has been approved', 'approve');
            activityLog('Approved food request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'edit food' :

            $food = RestaurantFood::where('id', $scf->restaurant_foods_id)->first();
            if(!$food) return res(__('food_not_found'), null, 404);

            if($req->status === 'declined'){
                $scf->is_declined = 1;
                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Edit food request has been decline', 'declined');
                activityLog('Declined food request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved   =  1;
            $scf->process_by = auth()->id();

            if($scf->data->name != null){
                $food->name  = $scf->data->name;
            }

            if($scf->data->price != null){
                $food->price  = $scf->data->price;
            }

            if($scf->data->description != null){
                $food->description  = $scf->data->description;
            }

            if($scf->base64 != null){
                $food->food_image  = $scf->base64;

                $path_full  = public_path() . '/images/food/' . encode($food->id, 'uuid') . '-full.png';
                $path_thumb = public_path() . '/images/food/' . encode($food->id, 'uuid') . '-thumbnail.png';

                if(file_exists($path_full) && file_exists($path_thumb)){
                    File::delete($path_full);
                    File::delete($path_thumb);
                }

                $children_path = public_path() . '/images/food/';

                $image  = Image::make($food->food_image);
                $image->save($children_path . encode($food->id, 'uuid') . '-full.png');
                $image->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                }); //@AES resizing
                $image->save($children_path . encode($food->id, 'uuid') . '-thumbnail.png');
            }

            $food->save();
            $scf->save();

            $this->notify($scf->type, $scf->id, $scf->request_by, $message, 'Edit food request has been approved', 'approve');
            activityLog('Approved edit food request number ' . $scf->id);
            return res('success', null, 200);

        break;

        case 'add product' :

            $product = Product::where('id', $scf->product_id)->first();
            if(!$product) return res(__('product_not_found'), null, 404);

            if($req->status === 'declined'){
                $scf->is_declined = 1;
                $product->is_declined = 1;
                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $product->save();
                $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Add product request has been decline', 'declined');
                activityLog('Declined product request number ' . $scf->id);
                return res('success', null, 200);
            }
            
            $scf->is_approved = 1;
            $product->is_approved = 1;
            $product->request_by = $scf->request_by;
            $product->process_by = auth()->id();
            $product->status = 1;
            $product->save();
            

            $scf->process_by = auth()->id();
            $scf->save();

            $this->notify($scf->type, $scf->id,$scf->request_by, $message, 'Add product request has been approved', 'approve');
            activityLog('Approved product request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'user profile photo' :

            $user = User::where('id', $scf->user_id)->first();
            if(!$user) return res(__('user.not_found'), null, 404);


            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id, $scf->request_by, $user->id, $message, 'declined');
                activityLog('Declined user request number ' . $scf->id);
                return res('success', null, 200);
            }

            $user->profile_photo = $scf->base64;
            $scf->is_approved = 1;

            $user->save();
            $scf->save();

            $path = public_path() . '/images/users/' . $user->hash_id . '/';
            if(!file_exists($path)){
                File::makeDirectory(base_path() . '/public/images/users/' . $user->hash_id, $mode = 0777, true, true);

                $image = Image::make($user->profile_photo);
                $image->save($path . 'full.png');
                $image->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                }); //@AES resizing
                $image->save($path . 'thumbnail.png');
            }

            $fullPath = public_path() . '/images/users/' . $user->hash_id . '/full.png';
            $thumbnailPath = public_path() . '/images/users/' . $user->hash_id . '/thumbnail.png';
            
            // @AES Delete existing
            if (File::exists($thumbnailPath) && File::exists($fullPath)) {
                File::delete($thumbnailPath);
                File::delete($fullPath);
            }

            $image = Image::make($user->profile_photo);
            $image->save($path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path . 'thumbnail.png');

            $this->notify($scf->type, $scf->id, $scf->request_by, $user->id, 'Your profile photo changes has been approve, please refresh the page to see the changes', 'approve');
            activityLog('Approved user request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'update brand photo' :

            $brand = Brand::where('id', $scf->brand_id)->first();
            if(!$brand) return res(__('user.not_found'), null, 404);

            if($req->name) $brand->name = $req->name;
            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('brand.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id, $scf->request_by, $brand->id, $message, 'declined');
                activityLog('Declined brand request number ' . $scf->id);
                return res('success', null, 200);
            }

            if($scf->data->name != null) $brand->name = $scf->data->name;
            if($scf->base64 != null) $brand->profile_base64 = $scf->base64;
            $scf->is_approved = 1;

            $brand->save();
            $scf->save();

            $path = public_path() . '/images/brands/' . $brand->hash_id . '/';
            if(!file_exists($path)){
                File::makeDirectory(base_path() . '/public/images/brands/' . $brand->hash_id, $mode = 0777, true, true);

                $image = Image::make($brand->profile_base64);
                $image->save($path . 'full.png');
                $image->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                }); //@AES resizing
                $image->save($path . 'thumbnail.png');
            }

            $fullPath = public_path() . '/images/brands/' . $brand->hash_id . '/full.png';
            $thumbnailPath = public_path() . '/images/brands/' . $brand->hash_id . '/thumbnail.png';
            
            // @AES Delete existing
            if (File::exists($thumbnailPath) && File::exists($fullPath)) {
                File::delete($thumbnailPath);
                File::delete($fullPath);
            }

            $image = Image::make($brand->profile_base64);
            $image->save($path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path . 'thumbnail.png');

            $this->notify($scf->type, $scf->id, $scf->request_by, $brand->id, 'Your brand photo changes has been approve, please refresh the page to see the changes', 'approve');
            activityLog('Approved brand request number ' . $scf->id);
            return res('success', null, 200);
        break;

        case 'legal document PDF' :
            $user = User::where('id', $scf->user_id)->first();
            if(!$user) return res(__('user.not_found'), null, 404);
            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                if($scf->data->document_name == 'computer-card') $store->computer_card_status = 'declined';
                if($scf->data->document_name == 'commercial-registration') $store->commercial_registration_status = 'declined';
                if($scf->data->document_name == 'commercial-permit') $store->commercial_permit_status = 'declined';
                if($scf->data->document_name == 'sponsor-qid') $store->sponsor_qid_status = 'declined';
                if($scf->data->document_name == 'signature') $store->signature_status = 'declined';

                $scf->save();
                $store->save();
                $this->notify($scf->type, $scf->id, $scf->request_by, $user->id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }

            if($scf->data->document_name == 'computer-card') $store->computer_card_status = 'approved';
            if($scf->data->document_name == 'commercial-registration') $store->commercial_registration_status = 'approved';
            if($scf->data->document_name == 'commercial-permit') $store->commercial_permit_status = 'approved';
            if($scf->data->document_name == 'sponsor-qid') $store->sponsor_qid_status = 'approved';
            if($scf->data->document_name == 'signature') $store->signature_status = 'approved';
            

            $scf->is_approved = 1;
            $scf->save();
            $store->save();

            $this->notify($scf->type, $scf->id, $scf->request_by, $store->user_id, 'Your ' . $scf->data->document_name . ' legal documents has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 200);
        break;
        
        case 'legal document images' :
            $user = User::where('id', $scf->user_id)->first();
            if(!$user) return res(__('user.not_found'), null, 404);

            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                if($scf->data->image_name == 'computer-card') $store->computer_card_status = 'declined';
                if($scf->data->image_name == 'commercial-registration') $store->commercial_registration_status = 'declined';
                if($scf->data->image_name == 'commercial-permit') $store->commercial_permit_status = 'declined';
                if($scf->data->image_name == 'sponsor-qid') $store->sponsor_qid_status = 'declined';
                if($scf->data->image_name == 'signature') $store->signature_status = 'declined';

                $scf->save();
                $store->save();
                $this->notify($scf->type, $scf->id, $scf->request_by, $user->id, $message, 'declined');
                activityLog('Declined store request number ' . $scf->id);
                return res('success', null, 200);
            }

            if($scf->data->image_name == 'computer-card') $store->computer_card_status = 'approved';
            if($scf->data->image_name == 'commercial-registration') $store->commercial_registration_status = 'approved';
            if($scf->data->image_name == 'commercial-permit') $store->commercial_permit_status = 'approved';
            if($scf->data->image_name == 'sponsor-qid') $store->sponsor_qid_status = 'approved';
            if($scf->data->image_name == 'signature') $store->signature_status = 'approved';
            
            $scf->is_approved = 1;
            $scf->save();
            $store->save();
            $this->notify($scf->type, $scf->id, $scf->request_by, $store->user_id, 'Your ' . $scf->data->image_name . ' legal documents has been approved', 'approve');
            activityLog('Approved store request number ' . $scf->id);
            return res('success', null, 200);
        break;
        
        case 'user details' :
            $user = User::where('id', $scf->user_id)->first();
            if(!$user) return res(__('user.not_found'), null, 404);


            if($req->status === 'declined'){
                $scf->is_declined = 1;

                if($message == null) return res(__('user.declined_message'), null, 401);

                $scf->save();
                $this->notify($scf->type, $scf->id, $scf->request_by, $user->id, $message, 'declined');
                activityLog('Declined user request number ' . $scf->id);
                return res('success', null, 200);
            }

            if ($scf->data->name != null ) $user->name = $scf->data->name;
            if ($scf->data->mobile_no != null ) $user->mobile = $scf->data->mobile_no;
            if ($scf->data->vendor_email != null ) $user->email = $scf->data->vendor_email;

            $scf->is_approved = 1;
            $scf->save();
            $user->save();

            $this->notify($scf->type, $scf->id, $scf->request_by, $user->id, 'Your details changes has been approve', 'approve');
            activityLog('Approved user request number ' . $scf->id);
            return res('success', null, 200);
        break;

        default :
            return res('Invalid status', null, 401);
        break;
        }

    }

    public function createDynamicPath($store_hashid, $data, $base64, $id)
    {
        $path = public_path() . '/images/stores/' . $store_hashid . '/' . $data->image_name . '-images';
        if(!file_exists($path)){
            File::makeDirectory(base_path() . '/public/images/stores/' . $store_hashid . '/' . $data->image_name . '-images', $mode = 0777, true, true);
        }

        $rooth_path = $path . '/';

        $image = Image::make($base64);
        $image->save($rooth_path . $id . '-' . $data->image_name . '-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($rooth_path . $id . '-' . $data->image_name . '-thumbnail.png');
    }

    public function storeChangeFields($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $scfs = StoreChangeField::where('is_approved', 0)->where('is_declined', 0)->orderBy('created_at', 'desc')->paginate($req->limit);

        $data = new ChangeFieldListCollection($scfs);
        return res('success', $data, 200);
    }

    public function notify($change_field_type, $change_field_id, $request_by, $vendor_id, $message, $status_type)
    {
       $user_ids = [$request_by, $vendor_id];

       foreach($user_ids as $user_id){
            if($user_id != 0){
                $notify = new VendorNotification;
                $notify->notify_to_user_id = $user_id;
                $notify->message = $message;
                $notify->change_field_id = $change_field_id;
                $notify->status_type = $status_type;
                $notify->save();
            }
       }
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalRequest()
    {
        $count = StoreChangeField::count('id');
        return res('succes', $count, 200);
    }

     /**
     * @param $req
     * @return JsonResponse
     */
    public function status($req)
    {
        $validator = Validator::make($req->all(), [
            'status'    => 'required|string',
            'title'     => 'required|string',
            'limit'     => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        switch($req->status){

            case 'approved' :
            $scfs = StoreChangeField::where('type', $req->title)->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            if($req->title == 'all'){
                $scfs = StoreChangeField::where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            }
            break;

            case 'declined' :
            $scfs = StoreChangeField::where('type', $req->title)->where('is_declined', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            if($req->title == 'all'){
                $scfs = StoreChangeField::where('is_declined', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            }
            break;

            case 'pending' :
            $scfs = StoreChangeField::where('type', $req->title)->where('is_declined', 0)->where('is_approved', 0)->orderBy('created_at', 'desc')->paginate($req->limit);
            if($req->title == 'all'){
                $scfs = StoreChangeField::where('is_declined', 0)->where('is_approved', 0)->orderBy('created_at', 'desc')->paginate($req->limit);
            }
            break;

            case 'all' :
            $scfs = StoreChangeField::where('type', $req->title)->orderBy('created_at', 'desc')->paginate($req->limit);
            if($req->title == 'all'){
                $scfs = StoreChangeField::orderBy('created_at', 'desc')->paginate($req->limit);
            }
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }

        $data = new ChangeFieldListCollection($scfs);
        
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function showRequest($req)
    {
        if(!$req->hashid) return res(__('validation.identifier'), null, 400);

        $id = decode($req->hashid, 'uuid');

        $scf = StoreChangeField::where('id', $id)->first();
        if(!$scf) return res(__('store.request_not_found'), null, 404);

        $data = new StoreChangeFieldResource($scf);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function overAll()
    {
        $vendor = Store::where('business_type_id', 1)->count();

        $hotel = Store::where('business_type_id', 2)->count();

        $restaurant = Store::where('business_type_id', 3)->count();

        $activities = Store::where('business_type_id', 4)->count();

        $data = new stdClass;
        $data->marketplace = $vendor;
        $data->hotel = $hotel;
        $data->restaurant = $restaurant;
        $data->activities = $activities;

        return res('succes', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function marketplaceList($req)
    {
        if(!$req->status) return res(__('status.request_not_found'), null, 404);

        if($req->status === 'all') $stores = Store::where('business_type_id', 1)
        ->where('name', 'like', '%' . $req->key . '%')
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'approved') $stores = Store::where('business_type_id', 1)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'pending') $stores = Store::where('business_type_id', 1)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 0)
        ->where('is_declined', 0)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'declined') $stores = Store::where('business_type_id', 1)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_declined', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);
        
        if(!$stores) return res(__('store.request_not_found'), null, 404);

        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function hotelList($req)
    {
        if(!$req->status) return res(__('status.request_not_found'), null, 404);

        if($req->status === 'all') $stores = Store::where('business_type_id', 2)
        ->where('name', 'like', '%' . $req->key . '%')
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'approved') $stores = Store::where('business_type_id', 2)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'pending') $stores = Store::where('business_type_id', 2)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 0)
        ->where('is_declined', 0)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'declined') $stores = Store::where('business_type_id', 2)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_declined', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);
        
        if(!$stores) return res(__('store.request_not_found'), null, 404);

        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function restaurantList($req)
    {
        if(!$req->status) return res(__('status.request_not_found'), null, 404);

        if($req->status === 'all') $stores = Store::where('business_type_id', 3)
        ->where('name', 'like', '%' . $req->key . '%')
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'approved') $stores = Store::where('business_type_id', 3)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'pending') $stores = Store::where('business_type_id', 3)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 0)
        ->where('is_declined', 0)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'declined') $stores = Store::where('business_type_id', 3)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_declined', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);
        
        if(!$stores) return res(__('store.request_not_found'), null, 404);

        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function activitiesList($req)
    {
        if(!$req->status) return res(__('status.request_not_found'), null, 404);

        if($req->status === 'all') $stores = Store::where('business_type_id', 4)
        ->where('name', 'like', '%' . $req->key . '%')
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'approved') $stores = Store::where('business_type_id', 4)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'pending') $stores = Store::where('business_type_id', 4)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_approved', 0)
        ->where('is_declined', 0)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);

        if($req->status === 'declined') $stores = Store::where('business_type_id', 4)
        ->where('name', 'like', '%' . $req->key . '%')
        ->where('is_declined', 1)
        ->orderBy('created_at', 'desc')
        ->paginate($req->limit);
        
        if(!$stores) return res(__('store.request_not_found'), null, 404);

        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    public function approvedAllAddFood()
    {
        return res('Method not Available');
    }

    public function approvedAllEditFood()
    {
        return res('Method not Available');
    }

    public function dashboardRestaurant($req)
    {
        $permissions = ['administrator', 'agent', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Dashboard is for admin only', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid'  => 'required|string',
        ]);
        
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }
    
        return res('success', [
            'order_received'    => $this->restaurant_dashboard->getOrderReceived($store_id),
            'total_customer'    => $this->restaurant_dashboard->getTotalCustomer($store_id),
            'total_delivered'   => $this->restaurant_dashboard->getTotalDelivery($store_id),
            'total_revenue'     => 0,
            'total_earning'     => 0
        ], 200);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function modifyActiveStatus($req):object
    {
        $permissions = ['administrator', 'agent'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid'  => 'required|string',
            'is_active'     => 'required|boolean'
        ]);
        
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store){
            return res(__('store.not_found'), null, 404);
        }

        $store->is_active = $req->is_active;
        $store->save();
        
        return res('success', null, 200);
    }
}    
