<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RestaurantInterface;

class RestaurantController extends Controller
{
    protected $req;
    protected $restaurant;

    public function __construct(Request $req, RestaurantInterface $restaurant)
    {
        $this->req = $req;
        $this->restaurant = $restaurant;
    }

    public function nearme()
    {
        return $this->restaurant->nearme($this->req);
    }

    public function listByRate()
    {
        return $this->restaurant->listByRate($this->req);
    }

    public function listByFollowers()
    {
        return $this->restaurant->listByFollowers($this->req);
    }

    public function foodlist()
    {
        return $this->restaurant->foodlist($this->req);
    }

    public function addFood()
    {
        return $this->restaurant->addFood($this->req);
    }

    public function updateFood()
    {
        return $this->restaurant->updateFood($this->req);
    }

    public function foodByCategory()
    {
        return $this->restaurant->foodByCategory($this->req);
    }
    
    public function addFavorite()
    {
        return $this->restaurant->addFavorite($this->req);
    }

    public function favoriteList()
    {
        return $this->restaurant->favoriteList($this->req);
    }

    public function restaurantFavorite()
    {
        return $this->restaurant->restaurantFavorite($this->req);
    }

    public function removeFavorite()
    {
        return $this->restaurant->removeFavorite($this->req);
    }

    public function foodTypes()
    {
        return $this->restaurant->foodTypes($this->req);
    }

    public function listSearchStatus()
    {
        return $this->restaurant->listSearchStatus($this->req);
    }

    public function showFood()
    {
        return $this->restaurant->showFood($this->req);

    }
        
    public function listByRestaurant()
    {
        return $this->restaurant->listByRestaurant($this->req);
    }
        
    public function foodChangeStatus()
    {
        return $this->restaurant->foodChangeStatus($this->req);
    }
        
    public function totalFood()
    {
        return $this->restaurant->totalFood($this->req);
    }
        
    public function totalCategory()
    {
        return $this->restaurant->totalCategory($this->req);
    }
        
    public function myFoodType()
    {
        return $this->restaurant->myFoodType($this->req);
    }
        
    public function foodTypeList()
    {
        return $this->restaurant->foodTypeList($this->req);
    }
        
    public function totalFoodType()
    {
        return $this->restaurant->totalFoodType($this->req);
    }
    public function ListByCategory()
    {
        return $this->restaurant->ListByCategory($this->req);
    }

    public function foodCategoryList()
    {
        return $this->restaurant->foodCategoryList();
    }

    public function adminFoodChangeStatus()
    {
        return $this->restaurant->adminFoodChangeStatus($this->req);
    }

    public function listByMeals()
    {
        return $this->restaurant->listByMeals($this->req);
    }

    public function mealsList()
    {
        return $this->restaurant->mealsList();
    }   

    public function listOfFoodByMyFoodType()
    {
        return $this->restaurant->listOfFoodByMyFoodType($this->req);
    }   

    public function totalOfFoodByMyFoodType()
    {
        return $this->restaurant->totalOfFoodByMyFoodType($this->req);
    }   

    public function adminChangeName()
    {
        return $this->restaurant->adminChangeName($this->req);
    }
    
    public function restaurantCoordinates()
    {
        return $this->restaurant->restaurantCoordinates($this->req);
    }

    public function addRestaurantImage()
    {
        return $this->restaurant->addRestaurantImage($this->req);
    }

    public function updateRestaurantImage()
    {
        return $this->restaurant->updateRestaurantImage($this->req);
    }

    public function RestaurantImgList()
    {
        return $this->restaurant->RestaurantImgList($this->req);
    }

    public function RestaurantShow()
    {
        return $this->restaurant->RestaurantShow($this->req);
    }

    public function addMultipleFood()
    {
        return $this->restaurant->addMultipleFood($this->req);
    }

    public function addLocation()
    {
        return $this->restaurant->addLocation($this->req);
    }

    public function listByAllRestaurant()
    {
        return $this->restaurant->listByAllRestaurant($this->req);
    }

    public function editOrderDuration()
    {
        return $this->restaurant->editOrderDuration($this->req);
    }

    public function searchMonster()
    {
        return $this->restaurant->searchMonster($this->req);
    }

    public function listOfFoodType()
    {
        return $this->restaurant->listOfFoodType();
    }

    public function storeListwithFollow()
    {
        return $this->restaurant->storeListwithFollow($this->req);
    }

    public function menuList()
    {
        return $this->restaurant->menuList($this->req);
    }

    public function restaurant_details()
    {
        return $this->restaurant->restaurant_details($this->req);
    }

    public function search_food()
    {
        return $this->restaurant->search_food($this->req);
    }

    public function isFollowed()
    {
        return $this->restaurant->isFollowed($this->req);
    }

    public function search_store()
    {
        return $this->restaurant->search_store($this->req);
    }

    public function search_store_food()
    {
        return $this->restaurant->search_store_food($this->req);
    }

    public function storeListOfFoodType()
    {
        return $this->restaurant->storeListOfFoodType($this->req);
    }

    public function testlistbyrate()
    {
        return $this->restaurant->testlistbyrate($this->req);
    }
}
