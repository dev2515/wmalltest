<?php

namespace App\Models;

use App\Model\StoreBranch;
use App\Models\State;
use App\Models\Product;
use App\Models\RestaurantFood;
use App\Models\StoreBanner;
use App\Models\StoreRating;
use Illuminate\Database\Eloquent\Model;
use App\User;
use stdClass;
use Carbon\Carbon;
class Store extends Model
{
    protected $appends = ['hashid', 'main_store_id', 'status_info', 'ratings', 'chat_performance', 'ship_out_time', 'cancellation_rate', 'product_count', 'categories', 'banners', 'total_staff','timings', 'followers', 'total_foods', 'is_opened'];
    
    protected $casts = ['store_branches' => 'array', 'order_duration' => 'array'];
    //==================Function===============
    public function scopeCloseTo($query, $lat, $lon, $radius, $limit)
    {
        $haversine = "(6371 * acos(cos(radians($lat)) 
            * cos(radians(latitude)) 
            * cos(radians(longitude) 
            - radians($lon)) 
            + sin(radians($lat)) 
            * sin(radians(latitude))))";


        return $query
            ->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])
            ->where('is_active', 1)
            ->orderBy('distance')->limit($limit)->get();
    }
    //====END OF FUNCTIONS

    // -------------------------------APPENDS ----------------------------------
    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getMainStoreIdAttribute()
    {
        $store_branch = StoreBranch::where('store_child_id', $this->id)->first();
        if(!$store_branch) return null;

        return $store_branch->store_id;
    }

    public function getStatusInfoAttribute()
    {
        $status = isset($this->attributes['is_approved']) ? $this->attributes['is_approved'] : 0;
        $ban = isset($this->attributes['ban']) ? $this->attributes['ban'] : 0;
        $is_declined = isset($this->attributes['is_declined']) ? $this->attributes['is_declined'] : 0;
        if ($ban == 1) {
            return 'banned';
        }
        if ($is_declined == 1) {
            return 'declined';
        }
        if ($status === 0) {
            return 'pending';
        } elseif ($status === 1) {
            return 'approved';
        }
    }

    public function getRatingsAttribute()
    {
        $average = 0.0;
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $count = Review::where('type_id', $id)->where('status', 1)->count();
        $average = $average + Review::where('category', 'Store')->where('type_id', $id)->where('status', 1)->pluck('rate')->avg();

        $collect = array('count' => $count, 'average' => $average);
        return $collect;
    }

    public function getStateInfoAttribute()
    {
        return State::where('name', $this->attributes['state'])->first();
    }

    public function getChatPerformanceAttribute()
    {
        // TODO: Chat Performance
        return  0;
    }

    public function getShipOutTimeAttribute()
    {
        // TODO: Ship out time
        return  [
            'rate' => 'Fast',
            'days' => 2
        ];
    }

    public function getCancellationRateAttribute()
    {
        // TODO: Cancellation rate
        return  0;
    }

    public function getTotalStaffAttribute()
    {
        return $this->staffs()->count();
    }

    public function getProductCountAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return Product::where('store_id', $id)->where('status', 1)->count();
    }

    public function getCategoriesAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $categories = null;

        $category_ids = Product::where('store_id', $id)->pluck('category_ids');
        $category_ids = array_collapse($category_ids);
        $category_ids = array_unique($category_ids);
        $categories = Category::where('parent_id', 0)->where('status', 1)->whereIn('id', $category_ids)->get();
        $categories->makeHidden(['parent_id', 'created_at', 'updated_at']);

        return $categories;
    }

    public function getBannersAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $banners = StoreBanner::where('store_id', $id)->get();
        return $banners->pluck('base64');
    }

    public function getTimingsAttribute()
    {
        $id = decode($this->hash_id, 'uuid');
        $timings = StoreTiming::select('id','store_schedule_id','name','store_open','store_close')->where('store_schedule_id', $id)->where('status', 1)->get();
        $timings ->makeHidden(['id', 'store_schedule_id']);
        
        return $timings;
    }

    public function getFollowersAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $followers = StoreFollowing::where('store_id',  $id)->where('is_followed', 1)->count('is_followed');
        return $followers;
    }

    public function getTotalFoodsAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $foods = RestaurantFood::where('restaurant_id',  $id)->count();
        return $foods;
    }

    public function getIsOpenedAttribute():object
    {
        $now = Carbon::now(); // Get current time now.
        
        $schedule = StoreSchedule::where('store_id', $this->id)->where('day_id', $now->dayOfWeekIso)->first();
        if(!$schedule) return __('schedule.not_found');

        if(!$schedule->is_opened){
            $data           = new stdClass;
            $data->now      = $now->format('H:i'); // What time is now for debugging purpose only.
            $data->status   = 'Close';  // Open.
            $data->next     = null; // get the next time schedule for close.

            return $data;
        }

        $time_schedules = StoreTiming::where('store_schedule_id', $schedule->id)->where('status', 1)->get(); // get all schedules of a store

        $time_constant = 'Close'; // Store schedule status.
        $executed = false; //identifier.

        $open_schedules = []; // array.

        $data = new stdClass; // object.
        foreach ($time_schedules as $time_schedule){

            //For open condition.
            if($now->format('H:i') >= Carbon::parse($time_schedule->store_open)->format('H:i') && $now->format('H:i') <= Carbon::parse($time_schedule->store_close)->format('H:i')){
                $time_constant = 'Open'; // Change status from close to open.

                $data->now = $now->format('H:i'); // What time is now for debugging purpose only.
                $data->time_remaining = $schedule->is_busy == 1 ? Carbon::parse($schedule->is_busy_duration)->minute - $now->minute : 0;
                $data->status = $schedule->is_busy == 1 ? 'Busy' : $time_constant;  // Open.
                $data->next = 'Close at: '. Carbon::parse($time_schedule->store_close)->format('H:i'); // get the next time schedule for close.

                $executed = true; // executed if condtition is successfully triggired.
                break; // break the loop.
            }
            //----
            
            //For close condition.
            if($now->format('H:i') <= Carbon::parse($time_schedule->store_open)->format('H:i')){
                $data->now = $now->format('H:i');
                $data->status = $time_constant; // Close.
                $data->next = 'Open at: '. Carbon::parse($time_schedule->store_open)->format('H:i'); // get the nex time schedule for open.

                $executed = true; // executed if condtition is successfully triggired.
                break; // break the loop.
            }

            array_push($open_schedules, Carbon::parse($time_schedule->store_open)->format('H:i')); // e sulod niya ang store_open na data diari na array.
        }

        // if the condition inside loop is not perform this will be executed. Time reset.
        if($executed == false){
            $data->now = $now->format('H:i');
            $data->status = $time_constant; // close
            $data->next = 'Open at: '. min($open_schedules); // Get the minimum store schedule for next open.

            return $data;
        }
        
        return $data;
    }
    //--------------------------------------------------------------------


    //---------------------------------RELATIONSHIPS ---------------------------------------------------


    // --ONE TO MANY
    public function products()
    {
        return $this->hasMany(Product::class, 'store_id');
    }

    public function commercialPermitImages()
    {
        return $this->hasMany(CommercialPermitImage::class, 'store_id')->where('status', 1);
    }

    public function commercialRegistrationImages()
    {
        return $this->hasMany(CommercialRegistrationImage::class, 'store_id')->where('status', 1);
    }

    public function computerCardImages()
    {
        return $this->hasMany(ComputerCardImage::class, 'store_id')->where('status', 1);
    }

    public function signatureImages()
    {
        return $this->hasMany(SignatureImage::class, 'store_id')->where('status', 1);
    }

    public function sponsorQidImages()
    {
        return $this->hasMany(SponsorQidImage::class, 'store_id')->where('status', 1);
    }

    public function branches()
    {
        return $this->hasMany(StoreBranche::class, 'store_id');
    }

    public function orders()
    {
        return $this->hasMany(\App\Models\OrderStore::class, 'store_id');
    }

    public function timings()
    {
        return $this->hasMany(\App\Models\StoreTiming::class, 'store_schedule_id');
    }

    public function followers()
    {
        return $this->hasMany(StoreFollowing::class, 'store_id');
    }

    public function restaurantreview()
    {
        return $this->hasMany(RestaurantReview::class, 'restaurant_id');
    }

    public function foods()
    {
        return $this->hasMany(RestaurantFood::class, 'restaurant_id');
    }
    //--


    // -- BELONGS TO ONE
    public function theme()
    {
        return $this->belongsTo(StoreTheme::class, 'theme_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }
    // --
    
    
    // -- MANY TO MANY
    public function staffs()
    {
        return $this->belongsToMany(User::class, 'store_staffs');
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'store_likes');
    }

    public function follows()
    {
        return $this->belongsToMany(User::class, 'store_followings');
    }

    public function foodCategories()
    {
        return $this->belongsToMany(FoodCategory::class, 'restaurant_food_category_pivot', 'restaurant_id', 'food_category_id');
    }

    public function schedules()
    {
        return $this->belongsToMany(Day::class, 'store_schedules', 'store_id', 'day_id')->withPivot('id', 'is_opened');
    }
    
    public function restaurantServices()
    {
        return $this->belongsToMany(RestaurantService::class, 'restaurant_service_pivots', 'store_id', 'restaurant_service_id')->withPivot('is_active');
    }
    //--

    //--------------------------------------------------------------------
}
