<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveredInToUserAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('user_addresses', ['delivered_in', 'notes'])){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->string('delivered_in')->default('home')->after('longitude');
                $table->longText('notes')->nullabe()->after('delivered_in');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumns('user_addresses', ['delivered_in', 'notes'])){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->dropColumn('delivered_in');
                $table->dropColumn('notes');
            });
        }
    }
}
