<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileNameToDriverLicenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_licenses', 'file_name')){
            Schema::table('driver_licenses', function (Blueprint $table) {
                $table->string('file_name')->nullable()->after('image');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_licenses', 'file_name')){
            Schema::table('driver_licenses', function (Blueprint $table) {
                $table->dropColumn('file_name');
            });
        }
    }
}
