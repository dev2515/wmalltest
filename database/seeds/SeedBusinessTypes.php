<?php

use Illuminate\Database\Seeder;
use App\Models\BusinessType;
use Illuminate\Support\Facades\DB;

class SeedBusinessTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_types')->delete();

        $count = BusinessType::count();
        if($count == 0){
            BusinessType::firstOrCreate(['id' => 1, 'name' => 'marketplace']);
            BusinessType::firstOrCreate(['id' => 2, 'name' => 'hotel']);
            BusinessType::firstOrCreate(['id' => 3, 'name' => 'restaurant']);
            BusinessType::firstOrCreate(['id' => 4, 'name' => 'activities']);
            BusinessType::firstOrCreate(['id' => 5, 'name' => 'boutique']);
        }
    }
}
