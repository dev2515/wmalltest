<?php

namespace App\Models;

use App\Http\Resources\Restaurant\CartResource;
use App\Model\StoreBranch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RestaurantCart extends Model
{
    protected $table = 'restaurant_carts';
    protected $fillable = ['user_id','is_dine_in'];
    protected $appends = ['hash_id','restaurant_info','foods_info','total_count','total_amount'];

    public function foods()
    {
        return $this->hasMany(RestaurantCartItem::class, 'cart_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Restaurant::class, 'restaurant_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    
    public function getRestaurantInfoAttribute()
    {
        $id = isset($this->attributes['restaurant_id']) ? $this->attributes['restaurant_id'] : 0;

        // $r = DB::table('store')->select()
        
        $restaurant = Store::select('id','business_type_id','is_approved', 'name','latitude','longitude')
        ->where('id', $id)
        ->where('business_type_id', 3)
        ->where('is_approved', 1)
        ->first();
        
        if ($restaurant) {
            $restaurant->makeHidden(['id','status_info','business_type_id', 'is_approved','ratings','ship_out_time','chat_performance','cancellation_rate', 'product_count','categories','banners','total_staff','timings','followers']);
        }

        return $restaurant;
    }

    public function getFoodsInfoAttribute()
    {        
        $f = DB::table('restaurant_cart_items')->join('restaurant_foods', 'restaurant_cart_items.food_id', '=', 'restaurant_foods.id')
            ->select('restaurant_foods.id','restaurant_foods.name','restaurant_cart_items.quantity','restaurant_cart_items.quantity','restaurant_foods.description',DB::raw('restaurant_foods.price * restaurant_cart_items.quantity as sub_total'))
            ->where('restaurant_cart_items.cart_id', $this->id)
            ->where('quantity', '!=', 0)->where('is_deleted', 0)->where('on_checkout', 0)->where('is_ordered', 0)
            ->get();
        
        $f = collect([$f['id']]); 

        $foods = RestaurantCartItem::select('id','cart_id','quantity','on_checkout', 'is_ordered','food_id','food_status')
        ->where('cart_id', $this->id)->where('quantity', '!=', 0)->where('is_deleted', 0)->where('on_checkout', 0)->where('is_ordered', 0)->get();
        if($foods) $foods->makeHidden(['id','cart_id', 'food_id', 'hash_id']);

        return $foods;
    }

    public function getTotalCountAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $count = RestaurantCartItem::where('cart_id', $id)->count();
        return $count;
    }

    public function getTotalAmountAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $item = RestaurantCartItem::where('cart_id', $id)->get();
        $amount = collect($item)->sum('totalamount');
        return centToPrice($amount);
    }
}
