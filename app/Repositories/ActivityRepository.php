<?php

namespace App\Repositories;

use App\Interfaces\ActivityInterface;
use App\Models\Activity;
use App\Models\ActivityImage;
use App\Models\ActivityReview;
use App\Models\ActivityType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ActivityRepository implements ActivityInterface
{
    public function list($req)
    {
        $activity = Activity::where('status', 1)
        ->get();

        return res('success', $activity);
    }

    public function add($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'vendor_id' => 'required',
            'description' => 'required',
            'price' => 'required',
            'pickup_time' => 'required',
            'pickup_date' => 'required',
            'pickup_address' => 'required',
            'drop_address' => 'required',
            'pickup_lat' => 'required',
            'pickup_lng' => 'required',
            'drop_lat' => 'required',
            'drop_lng' => 'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 400);
        }

        DB::transaction(function() use ($req){
            
            $vendor_id = decode($req->vendor_id, 'uuid');

            $activity = Activity::create([
                'vendor_id' => $vendor_id,
                'description' => $req->description,
                'price' => $req->price,
                'pickup_time' => $req->pickup_time,
                'pickup_date' => $req->pickup_date,
                'pickup_address' => $req->pickup_address,
                'drop_address' => $req->drop_address,
                'pickup_lat' => $req->pickup_lat,
                'pickup_lng' => $req->pickup_lng,
                'drop_lat' => $req->drop_lat,
                'drop_lng' => $req->drop_lng,
            ]);

            $activity -> save();

            return res('success', $activity->hash_id)->send();
        });        
    }

    public function update($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $activity_id = decode($req->activity_id, 'uuid');

        $activity = Activity::where('id', $activity_id)
        ->where('status', 1)
        ->first();

        if($req->description) $activity->description = $req->description;
        if($req->price) $activity->price = $req->price;
        if($req->pickup_time) $activity->pickup_time = $req->pickup_time;
        if($req->pickup_date) $activity->pickup_date = $req->pickup_date;
        if($req->pickup_address) $activity->pickup_address = $req->pickup_address;
        if($req->drop_address) $activity->drop_address = $req->drop_address;
        if($req->pickup_lat) $activity->pickup_lat = $req->pickup_lat;
        if($req->pickup_lng) $activity->pickup_lng = $req->pickup_lng;
        if($req->drop_lat) $activity->drop_lat = $req->drop_lat;
        if($req->drop_lng) $activity->drop_lng = $req->drop_lng;

        $activity->save();

        return res('success', $activity->id);
    }

    public function addImage($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        DB::transaction(function() use ($req){
            
            $activity = decode($req->activity, 'uuid');

            $imahe = ActivityImage::create([
                'activity_id' => $activity,
                'filename' => $req->filename,
                'activity_image' => $req->activity_image,
            ]);

            $imahe->save();

            $path = public_path() . '/images/activity';
    
            if(!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/activity', $mode = 0777, true, true);
            }
    
            $local_path = public_path() . '/images/activity/';
    
            $image = Image::make($imahe->activity_image);
            $image->save($local_path . encode($imahe->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($imahe->id, 'uuid') . '-thumbnail.png');

            return res('success', $imahe->hash_id)->send();
        });     
    }

    public function updateImage($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $image_id = decode($req->image_id, 'uuid');

        $image_id = ActivityImage::where('id', $image_id)
        ->where('status', 1)
        ->first();

        if($req->filename) $image_id->filename = $req->filename;

        if($req->activity_image) 
        {
            $image_id->activity_image = $req->activity_image;
    
            $full = public_path() . '/images/activity/' . $image_id->hashid . '-full.png';
            $thumbnail = public_path() . '/images/activity/' . $image_id->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/activity/';

            $image = Image::make($image_id->activity_image);
            $image->save($local_path . encode($image_id->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($image_id->id, 'uuid') . '-thumbnail.png');
        }

        $image_id->save();

        return res('success', $image_id->id);
    }

    public function typeList($req)
    {

    }

    public function addType($req)
    {
        $permissions = ['administrator', 'admin staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'type' => 'required',
            'activity_image' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $type = ActivityType::create([
            'type' => $req->type,
            'description' => $req->description,
            'activity_image' => $req->activity_image,
        ]);

        $type->save();

        $path = public_path() . '/images/activity_type';
    
            if(!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/activity_type', $mode = 0777, true, true);
            }
    
            $local_path = public_path() . '/images/activity_type/';
    
            $image = Image::make($type->activity_image);
            $image->save($local_path . encode($type->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($type->id, 'uuid') . '-thumbnail.png');

        return res('success');
    }

    public function updateType($req)
    {
        $permissions = ['administrator', 'admin staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $type_id = decode($req->type_id, 'uuid');
        $type = ActivityType::find('id', $type_id);

        if($req->type) $type->type = $req->type;
        if($req->description) $type->description = $req->description;
        if($req->status) $type->status = $req->status;

        if($req->activity_image)
        {
            $type->activity_image = $req->activity_image;

            $full = public_path() . '/images/activity_type/' . $type->hashid . '-full.png';
            $thumbnail = public_path() . '/images/activity_type/' . $type->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/activity_type/';

            $image = Image::make($type->activity_image);
            $image->save($local_path . encode($type->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($type->id, 'uuid') . '-thumbnail.png');
        }

        $type->save();

        return res('success');
    }

    public function reviewlist($req)
    {
        if($req->filter == 'all') $review = ActivityReview::where('status', 1)->get();
        elseif($req->rate) $review = ActivityReview::where('status', 1)->orderBy('rating')->get();

        return res('success', $review);
    }

    public function createOrUpdateReview($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'activity_id' => 'required',
            'rating' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $review = ActivityReview::updateOrCreate([
            'activity_id' => $req->activity_id,
            'user_id' => auth()->user()->id,
        ],
        [
            'activity_id' => $req->activity_id,
            'user_id' => auth()->user()->id,
            'rating' => $req->rating,
            'review' => $req->review,
            'status' => 1,
        ]);

        return res('success', $review);
    }

    public function deleteReview($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }
        
        $validator = Validator::make($req->all(),[
            'activity_id' => 'required',
            'rating' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $review = ActivityReview::where('review_id', $req->review_id)->where('status', 1)->first();
        $review->status = 0;
        $review->save();

        return res('success');  
    }
}