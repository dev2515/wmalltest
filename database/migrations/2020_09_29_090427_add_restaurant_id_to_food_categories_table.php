<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRestaurantIdToFoodCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('food_categories', 'restaurant_id')){
            Schema::table('food_categories', function (Blueprint $table) {
                $table->unsignedBigInteger('restaurant_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('food_categories', 'restaurant_id')){
            Schema::table('food_categories', function (Blueprint $table) {
                $table->dropColumn('restaurant_id');
            });
        }
    }
}
