<?php

use Illuminate\Database\Seeder;
use App\Models\FoodCategory;


class SeedFoodCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = FoodCategory::count();

        $names = ['Main Course', 'Appetizer', 'Dessert', 'Side Dish', 'Sauce', 'Beverage', 'Salad', 'Soup', 'Snack'];
        if($count === 0){
            foreach ($names as $name){
                FoodCategory::firstOrCreate(['name' => $name, 'category_image' => 1, 'restaurant_id' => 1]);
            }
        }
    }
}
