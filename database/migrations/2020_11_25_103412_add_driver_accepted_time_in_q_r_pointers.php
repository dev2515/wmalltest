<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverAcceptedTimeInQRPointers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('q_r_pointers', 'driver_accepted_time')){
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->dateTime('driver_accepted_time')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('q_r_pointers', 'driver_accepted_time')){
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->dropColumn('driver_accepted_time');
            });
        }
    }
}
