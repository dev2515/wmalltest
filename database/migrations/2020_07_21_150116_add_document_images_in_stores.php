<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentImagesInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'computer_card_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('computer_card_img')->nullable()->after('commercial_permit');
            });
        }

        if(!Schema::hasColumn('stores', 'commercial_registration_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('commercial_registration_img')->nullable()->after('computer_card_img');
            });
        }

        if(!Schema::hasColumn('stores', 'computer_permit_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('computer_permit_img')->nullable()->after('commercial_registration_img');
            });
        }

        if(!Schema::hasColumn('stores', 'sponsor_qid_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('sponsor_qid_img')->nullable()->after('computer_permit_img');
            });
        }

        if(!Schema::hasColumn('stores', 'signature_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('signature_img')->nullable()->after('sponsor_qid_img');
            });
        }

        if(!Schema::hasColumn('stores', 'profile_photo_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('profile_photo_img')->nullable()->after('name');
            });
        }

        if(!Schema::hasColumn('stores', 'cover_photo_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('cover_photo_img')->nullable()->after('profile_photo_img');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'computer_card_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('computer_card_img');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_registration_img');
            });
        }

        if(Schema::hasColumn('stores', 'computer_permit_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('computer_permit_img');
            });
        }

        if(Schema::hasColumn('stores', 'sponsor_qid_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('sponsor_qid_img');
            });
        }

        if(Schema::hasColumn('stores', 'signature_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_img');
            });
        }

        if(Schema::hasColumn('stores', 'profile_photo_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('profile_photo_img');
            });
        }

        if(Schema::hasColumn('stores', 'cover_photo_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('cover_photo_img');
            });
        }
    }
}
