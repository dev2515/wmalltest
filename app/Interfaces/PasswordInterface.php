<?php

namespace App\Interfaces;

interface PasswordInterface
{
    public function requestEmail($req);

    public function change($req);

    public function requestMobile($req);
}
