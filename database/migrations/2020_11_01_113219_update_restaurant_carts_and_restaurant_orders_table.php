<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRestaurantCartsAndRestaurantOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_orders','qrcode')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('qrcode');
            });
        }

        if(Schema::hasColumn('restaurant_carts','reservation_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('reservation_id','table_id');
            });
        }

        if(!Schema::hasColumn('restaurant_orders','reservation_id')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->unsignedBigInteger('reservation_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('restaurant_orders','qrcode')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->longText('qrcode');
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'table_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('table_id', 'reservation_id');
            });
        }

        if(Schema::hasColumn('restaurant_orders','reservation_id')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('reservation_id');
            });
        }
    }
}
