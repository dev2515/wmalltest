<?php

namespace App\Http\Controllers\API\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ProductInterface;

class ProductController extends Controller
{
    private $req;
    private $product;

    public function __construct(Request $req, ProductInterface $product)
    {
        $this->req = $req;
        $this->product = $product;
    }

    public function list()
    {
        return $this->product->list($this->req);
    }

    public function show()
    {
        return $this->product->show($this->req);
    }

    public function add()
    {
        return $this->product->add($this->req);
    }

    public function update()
    {
        return $this->product->update($this->req);
    }

    public function uploadImage()
    {
        return $this->product->uploadImage($this->req);
    }

    public function uploadImages()
    {
        return $this->product->uploadImages($this->req);
    }

    public function totalProducts()
    {
        return $this->product->totalProducts($this->req);
    }
     public function add_image_product()
     {
         return $this->product->add_image_product($this->req);
     }
}
