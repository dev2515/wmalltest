<?php

namespace App\Http\Controllers\API\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CustomerInterface;

class CustomerController extends Controller
{
    protected $req;
    protected $customer;

    public function __construct(Request $req, CustomerInterface $customer)
    {
        $this->req = $req;
        $this->customer = $customer;
    }

    public function list()
    {
        return $this->customer->list($this->req);
    }

    public function show()
    {
        return $this->customer->show($this->req);
    }

    public function listOfAllCustomer()
    {
        return $this->customer->listOfAllCustomer($this->req);
    }

    public function brandList()
    {
        return $this->customer->brandList($this->req);
    }

    public function brandListPremium()
    {
        return $this->customer->brandListPremium($this->req);
    }

    public function brandListByName()
    {
        return $this->customer->brandListByName($this->req);
    }

    public function brandSort()
    {
        return $this->customer->brandSort($this->req);
    }

    public function totalBrands()
    {
        return $this->customer->totalBrands();
    }

    public function brandSearch()
    {
        return $this->customer->brandSearch($this->req);
    }

    public function productList()
    {
        return $this->customer->productList($this->req);
    }

    public function productSearch()
    {
        return $this->customer->productSearch($this->req);
    }

    public function productSort()
    {
        return $this->customer->productSort($this->req);
    }

    public function totalProducts()
    {
        return $this->customer->totalProducts();
    }

    public function productListBytags()
    {
        return $this->customer->productListBytags($this->req);
    }

    public function restaurantList()
    {
        return $this->customer->restaurantList($this->req);
    }

    public function categoryList()
    {
        return $this->customer->categoryList();
    }

    public function customerAccount()
    {
        return $this->customer->customerAccount($this->req);
    }

    public function orderList()
    {
        return $this->customer->orderList($this->req);
    }

    public function orderShow()
    {
        return $this->customer->orderShow($this->req);
    }
    
    public function addAddress()
    {
        return $this->customer->addAddress($this->req);
    }

    public function updateAddress()
    {
        return $this->customer->updateAddress($this->req);
    }

    public function addresses()
    {
        return $this->customer->addresses();
    }

    public function showAddress()
    {
        return $this->customer->showAddress($this->req);
    }

    public function deleteAddress()
    {
        return $this->customer->deleteAddress($this->req);
    }

    public function orderStore()
    {
        return $this->customer->orderStore($this->req);
    }

    public function storeFollowing()
    {
        return $this->customer->storeFollowing($this->req);
    }

    public function isFollow()
    {
        return $this->customer->isFollow($this->req);
    }

    public function storefollowed()
    {
        return $this->customer->storefollowed($this->req);
    }

    public function addStoreRating()
    {
        return $this->customer->addStoreRating($this->req);
    }
    
    public function removeRating()
    {
        return $this->customer->removeRating($this->req);
    }

    public function addToWishList()
    {
        return $this->customer->addToWishList($this->req);
    }

    public function wishLists()
    {
        return $this->customer->wishLists($this->req);
    }

    public function addressTypes()
    {
        return $this->customer->addressTypes();
    }
}
