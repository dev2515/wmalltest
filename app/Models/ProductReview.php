<?php

namespace App\Models;

use App\Models\ProductReviewImage;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $appends = ['images'];

    public function getImagesAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $images = ProductReviewImage::where('product_review_id', $id)->get();
        $images = $images->makeHidden(['created_at', 'updated_at']);
        return $images;
    }
}
