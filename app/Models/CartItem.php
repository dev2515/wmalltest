<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $appends = ['product_info'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getProductInfoAttribute()
    {
        $product = Product::find($this->attributes['product_id']);
        if ($product) {
            $product->makeHidden(['store_id', 'created_at', 'updated_at', 'store_info']);
        }
        return $product;
    }
}
