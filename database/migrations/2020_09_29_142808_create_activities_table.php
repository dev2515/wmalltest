<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('activities')){
            Schema::create('activities', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('vendor_id');
                $table->string('name');
                $table->unsignedBigInteger('type_id');
                $table->longText('description');
                $table->double('price');
                $table->dateTime('pickup_time');
                $table->dateTime('pickup_date');
                $table->string('pickup_address');
                $table->string('drop_address');
                $table->longText('pickup_lat');
                $table->longText('pickup_lng');
                $table->longText('drop_lat');
                $table->longText('drop_lng');
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
