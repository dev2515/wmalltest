<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use stdClass;
class DriverInfoList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hashid'            => $this->user->hashid,
            'name'              => $this->user->name,
            'order_reference'   => null,
            'total_delivery'    => 0,
            'declined_clients'  => 0,
            'total_income'      => centToPrice($this->total_income),
            'status'            => $this->status->name,
            'timestamp'         =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'full'              => asset($this->profilePhoto()->full),
            'thumbnail'         => asset($this->profilePhoto()->thumbnail),
        ];
    }

    public function profilePhoto():object
    {
        $full = public_path() . '/images/users/' . $this->user->hashid . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . $this->user->hashid . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . $this->user->hashid . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . $this->user->hashid . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }
}
