<?php

namespace App\Http\Controllers\API\Activity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ActivityInterface;

class ActivityController extends Controller
{
    protected $req;
    protected $activity;

    public function __construct(Request $req, ActivityInterface $activity)
    {
        $this->req = $req;
        $this->activity = $activity;
    }

    public function list()
    {
        return $this->activity->list($this->req);
    }

    public function add()
    {
        return $this->activity->add($this->req);
    }

    public function update()
    {
        return $this->activity->update($this->req);
    }

    public function addImage()
    {
        return $this->activity->addImage($this->req);
    }

    public function updateImage()
    {
        return $this->activity->updateImage($this->req);
    }

    public function typeList()
    {
        return $this->activity->typeList($this->req);
    }

    public function addType()
    {
        return $this->activity->typeList($this->req);
    }

    public function updateType()
    {
        return $this->activity->updateType($this->req);
    }
}
