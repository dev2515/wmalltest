<?php

namespace App\Repositories;

use App\Http\Resources\Restaurant\MyReservation;
use App\Http\Resources\Restaurant\QRcodeFood;
use App\Http\Resources\Restaurant\ReservationCheckout;
use App\Http\Resources\Restaurant\ReservationResource;
use App\Http\Resources\Restaurant\ScanCartResource;
use App\Interfaces\TableReservationInterface;
use App\Model\StoreBranch;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\TableReservation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class TableReservationRepository implements TableReservationInterface
{
    public function list($req)
    {
       
    }

    public function myreservationhistory($req)
    {
        $dateFrom = strtotime($req->dateFrom);
        $dateTo = strtotime($req->dateTo);

        if($req->restaurant)
        {
            $id = decode($req->restaurant_id, 'uuid');
            $reserve = TableReservation::where('user_id', auth()->user()->id)
            ->whereBetween('arrive_date', [$dateFrom, $dateTo])
            ->where('restaurant_id', $req->$id)
            ->orderBy('created_at')
            ->get();

            return res('success', $reserve);
        }

        $id = decode($req->restaurant_id, 'uuid');
        $reserve = TableReservation::where('user_id', auth()->user()->id)
        ->whereBetween('arrive_date', [$dateFrom, $dateTo])
        ->orderBy('created_at')
        ->get();

        $collection = ReservationResource::collection($reserve);

        return res('success', $collection);
    }

    public function myqrcode($req)
    {
        $validator = Validator::make($req->all(),[
            'qrcode' =>     'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }
        
        $reserved = TableReservation::where('qrcode', $req->qrcode)
        ->where('user_id', auth()->user()->id)
        ->get();

        $collection = ReservationResource::collection($reserved);

        return res('success', $collection);
    }

    public function scanQRcode($req)
    {
        $v = Validator::make($req->all(),[
            'qrcode' =>                 'required',
            'restaurant_hash_id' =>     'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $reserved = TableReservation::where('qrcode', $req->qrcode)
        ->first();

        if(!$reserved) return res('failed...qrcode not exist.');

        $reserved->qrcode_status = 1;
        $reserved->assigned_waiter_id = auth()->user()->id;
        $reserved->save();  

        $reserved = TableReservation::where('id', $reserved->id)
        ->where('restaurant_id', $restaurant_id)
        ->get();

        if(!$reserved) return res('failed...qrcode not exist.');
        
        $reserved = ReservationResource::collection($reserved);

        return res('successfully scanned', $reserved);
    }

    public function scan_cart_detail($req)
    {
        $validator = Validator::make($req->all(),[
            'reservation_hash_id' =>     'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $reservation_id = decode($req->reservation_hash_id, 'uuid');
 

        $reserved = TableReservation::where('id', $reservation_id)
         ->pluck('id');
        
        $order = RestaurantOrder::select('id','reservation_id', 'cart_id')->where('reservation_id', $reserved)->first();
        $cart = RestaurantCart::where('id', $order->cart_id)->get();
        $cart = ScanCartResource::collection($cart);

        return res('success', $cart);
    }

    public function restaurantQRcode($req)
    {
        $validator = Validator::make($req->all(),[
            'qrcode' =>     'required',
            'restaurant_hash_id' =>     'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $reserved = TableReservation::where('qrcode', $req->qrcode)
        ->where('restaurant_id', $restaurant_id)
        ->get();

        $collection = ReservationResource::collection($reserved);

        return res('success', $collection);
    }

    public function listbydatereserve($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'restaurant_id' =>      'required',
            'dateFrom' =>           'required',
            'dateTo' =>             'required',
            'status' =>             'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $id = decode($req->restaurant_id, 'uuid');
        $dateFrom = $req->dateFrom.' 00:00:00';
        $dateTo = $req->dateTo.' 23:59:59';

        if($req->status == 'All')
        {
            return res('here');
            $reservation = TableReservation::where('restaurant_id', $id)
            ->whereBetween('created_at', [$dateFrom, $dateTo]) //if [1] open [2] reserved [3] complete [4] cancelled [5] expired
            ->get();
        }
        else
        {
            $reservation = TableReservation::where('restaurant_id', $id)
            ->whereBetween('created_at', [$dateFrom, $dateTo])
            ->where('status', $req->status) //if [1] open [2] reserve [3] cancelled [expired]
            ->get();
        }

        $collection = ReservationResource::collection($reservation);
        return res('success', $collection);
    }

    public function listbydate($req)
    {   
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'restaurant_id' =>      'required',
            'dateFrom' =>           'required',
            'dateTo' =>             'required',
            'status' =>             'required',
        ]);
        
        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $id = decode($req->restaurant_id, 'uuid');
        $dateFrom = strtotime($req->dateFrom);
        $dateTo = strtotime($req->dateTo);
        
        if($req->status == 'All')
        {
            $reservation = TableReservation::with('user','restaurant')
            ->where('restaurant_id', $id)
            ->whereBetween('arrive_date', [$dateFrom, $dateTo])
            ->get();
        }
        else
        {
            $reservation = TableReservation::with('user','restaurant')
            ->where('restaurant_id', $id)
            ->whereBetween('arrive_date', [$dateFrom, $dateTo])
            ->where('status', $req->status) //if [1] open [2] reserve [3] cancelled [expired]
            ->get();
        }
        
        $collection = ReservationResource::collection($reservation);

        return res('success', $collection);
    }

    public function add($req)
    {
        $validator = Validator::make($req->all(),[
            'restaurant_id' =>      'required',
            'arrive_time' =>        'required',
            'arrive_date' =>        'required',
            'guest' =>              'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $id = decode($req->restaurant_id, 'uuid');
        
        $time = strtotime($req->arrive_time);
        $date = strtotime($req->arrive_date);       

        $exist = TableReservation::where('table_number', $req->table_number)
        ->where('restaurant_id', $id)
        ->where('arrive_time', $time)
        ->where('arrive_date', $date)//means reserved
        ->first();

        if($exist != null){
            if($exist->arrive_time == $time && $exist->arrive_time == $date)
            {
                if($exist->user_id == auth()->user()->id && $exist->table_number == $req->table_number)
                {
                    return res('you already reserved this table on the same date and time')->send();
                }
                return res('this table is already reserved')->send();
            }
            return res('this table is already reserved');
        }       

        $reserve_number = encode(Carbon::now()->timestamp, 'reservation-number');

        $c = TableReservation::where('reservation_number', $reserve_number)->first();

        if(!$c) $reserve_number = encode(Carbon::now()->timestamp, 'reservation-number');

        $reserve =                          new TableReservation;
        $reserve->restaurant_id =           $id;
        $reserve->user_id =                 auth()->user()->id;
        $reserve->table_number =            $req->table_number;
        $reserve->note =                    $req->note;
        $reserve->arrive_time =             $time;
        $reserve->arrive_date =             $date;
        $reserve->order =                   1;
        $reserve->guest =                   $req->guest;
        $reserve->qrcode =                  Str::random(32);
        $reserve->reservation_number =      $reserve_number;
        $reserve->save();
        
        return res('success', 'qrcode: '.$reserve->qrcode);
    }

    public function update($req)
    {   
        $validator = Validator::make($req->all(),[
            'reservation_hash_id' => 'required',
        ]);

        if($validator->fails())
        {
            return res('success', $validator->errors(), 402);
        }

        $id = decode($req->reservation_hash_id, 'uuid');

        $time = strtotime($req->arrive_time);
        $date = strtotime($req->arrive_date);

        $reserve = TableReservation::where('id', $id)
        ->where('user_id', auth()->user()->id)
        ->where('status', 1) // can update if status oper or reserve
        ->first();

        if($req->arrive_time || $req->arrive_date)
        {
            $check = TableReservation::where('arrive_time', $time)
            ->where('arrive_date', $date)
            ->first();

            if($check) return res('reservation invalid', null, 402);

            $reserve->arrive_time = $time;
            $reserve->arrive_date = $date;
        }

        if($req->table_number)
        {
            $check = TableReservation::where('arrive_time', $time)
            ->where('arrive_date', $date)
            ->first();

            if($check->exists()) return res('reservation invalid', null, 402);

            $reserve->table_number = $req->table_number;
        }

        if($req->note) $reserve->note = $req->note;
        if($req->guest) $reserve->guest = $req->guest;
        

        $reserve -> save();

        return res('success');
    }

    public function cancel($req)
    {
        $permissions = ['customer','vendor', 'vendor staff'];

        if(!in_array(auth()->user()->type_info, $permissions)) return res('You are not allowed to do this operation', null, 401); 
        
        $v = Validator::make($req->all(),[
            'reservation_hash_id' => 'required',
        ]);

        if($v->fails())
        {
            return res('success', $v->errors(), 402);
        }

        $r = decode($req->reservation_hash_id, 'uuid');

        $reserve = TableReservation::where('id', $r)
        ->where('status', 1)
        ->first();

        $reserve->status = 0;
        $reserve->save();

        return res('reservation cancelled');
    }

    public function acceptreservation($req)
    {
        $validator = Validator::make($req->all(),[
            'reservation_id' =>     'required',
            'restaurant_id' =>      'required',
        ]);

        if($validator->fails())
        {
            return res('success', $validator->errors(), 402);
        }

        $reservation_id =   decode($req->reservation_id, 'uuid');
        $restaurant_id =    decode($req->restaurant_id, 'uuid');

        $reserve = TableReservation::where('id', $reservation_id)
        ->where('restaurant_id', $restaurant_id)
        ->where('status', 1)
        ->first();

        $reserve->status = 2;
        $reserve->save();

        return res('reservation accepted');
    }

    public function myreservation($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $myReservationList = TableReservation::with('user')->where('user_id', auth()->user()->id)->where('status', 1)->orderBy('created_at','DESC')->get();
        $myReservationList = MyReservation::collection($myReservationList);
        
        return res('success', $myReservationList);
    }

    public function scannedQRcode($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $r = decode($req->restaurant_hash_id, 'uuid');
        $scannedlist = TableReservation::select('id','reservation_number','restaurant_id','table_number','user_id')
            ->where('restaurant_id', $r)->where('qrcode_status', 1)->orderBy('updated_at', 'DESC')->get();
        
        if($scannedlist != null)  $scannedlist->makeHidden(['id','restaurant_id','table_number', 'user_id']);

        return res('success', $scannedlist);
    }

    public function reservation_info($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'reservation_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $r = decode($req->reservation_hash_id, 'uuid');

        $rt = TableReservation::where('id', $r)->first()->id;
        if(!$rt) return res('no order exisit.');

        if($rt) $ro = RestaurantOrder::where('reservation_id', $rt)->first()->id;
        
        if($ro) $rol = RestaurantOrderList::where('order_id', $ro)->first()->id;

        if($rol) $rof = RestaurantOrderFood::where('order_list_id', $rol)->get();
        
        $rof = QRcodeFood::collection($rof);

        return res('success',$rof);
    }

    public function restaurant_qrcode_list($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $qrlist = TableReservation::select('id', 'restaurant_id', 'qrcode', 'user_id', 'table_number')->where('restaurant_id', $restaurant_id)->get();
        $qrlist->makeHidden(['id', 'restaurant_id']);

        return res('success', $qrlist);
    }

    public function assignTable($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'reservation_hash_id' => 'required',
            'table_number' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $reservation_id = decode($req->reservation_hash_id, 'uuid');

        $reservation = TableReservation::find($reservation_id);
        if($reservation)
        {
            $reservation -> table_number = $req->table_number;
            $reservation->save();
            return res('success');
        }
        else
        {
            return res('no reservation found');
        }
    }

    public function reservation_checkout_details($req)
    {
        $permissions = ['customer','vendor', 'vendor staff'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'reservation_hash_id' => 'required',
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $reservation_id = decode($req->reservation_hash_id, 'uuid');
        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $s = StoreBranch::select('id','store_id','is_main')->where('store_id', $restaurant_id)->first();
        if($s->is_main == 0) $restaurant_id = $s->id;

        $reservation = TableReservation::where('id', $reservation_id)
                        ->where('restaurant_id', $restaurant_id)->first();

        $reservation = new ReservationCheckout($reservation);

        return res('success', $reservation);
    }

    public function my_reservation_list($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $myReservationList = TableReservation::select('*')->where('user_id', auth()->user()->id)->where('status', 1)->orderBy('created_at','DESC')->get();
        if($myReservationList) $myReservationList->makeHidden(['id','restaurant_id','user_id','table_number','order','status','updated_at','qrcode_status','special_request','reservation_number','assigned_waiter_id']);

        return res('success', $myReservationList);
    }
}