<?php

namespace App\Repositories;

use App\User;
use App\Models\OauthAccessToken;
use App\Interfaces\LoginInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\SocialMedia;
use Braintree_Customer;
class LoginRepository implements LoginInterface
{

    /**
     * @param $req
     * @return JsonResponse
     */
    public function email($req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email', $req->email)->where('status', 1)->first();
        if (!$user) {
            return res(__('auth.email_not_found'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        if (!Hash::check($req->password, $user->password)) {
            return res(__('auth.invalid_password'), null, 400);
        }

        $token = $this->generateToken($user);

        return res('Success', $token);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agent($req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email', $req->email)->where('status', 1)->first();

        if (!$user) return res(__('auth.email_not_found'), null, 400);

        if($user->type != 7) return res(__('auth.not_allowed'), null, 400);

        if ($user->ban === 1) return res(__('auth.banned'), null, 400);

        if (!Hash::check($req->password, $user->password)) return res(__('auth.invalid_password'), null, 400);


        $token = $this->generateToken($user);

        return res('Success', $token);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function vendor($req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email', $req->email)->where('status', 1)->first();

        if (!$user) return res(__('auth.email_not_found'), null, 400);

        if($user->type != 2 && $user->type != 1) return res(__('auth.not_allowed'), null, 400);

        if ($user->ban === 1) return res(__('auth.banned'), null, 400);

        if (!Hash::check($req->password, $user->password)) return res(__('auth.invalid_password'), null, 400);


        $token = $this->generateToken($user);

        return res('Success', $token);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function driver($req){
        $validator = Validator::make($req->all(), [
            'mobile' => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile', $req->mobile)->where('status', 1)->first();

        if (!$user) return res(__('auth.mobile_not_found'), null, 400);

        if($user->type != 4) return res(__('auth.not_allowed'), null, 400);

        if ($user->ban === 1) return res(__('auth.banned'), null, 400);

        $token = $this->generateToken($user);

        return res('Success', $token);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function mobile($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true),
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile', $req->mobile)->where('iso', session('iso'))->where('status', 1)->first();
        if (!$user) {
            return res(__('auth.mobile_not_found'), null, 400);
        }
        if ($user->ban === 1) {
            return res(__('auth.banned'), null, 400);
        }
        if (!Hash::check($req->password, $user->password)) {
            return res(__('auth.invalid_password'), null, 400);
        }

        $token = $this->generateToken($user);

        return res('Success', $token);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function mobileOtp($req)
    {
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true),
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile', $req->mobile)->where('iso', session('iso'))->where('status', 1)->first();

        if (!$user) return res(__('auth.mobile_not_found'), null, 400);
        if ($user->ban === 1) return res(__('auth.banned'), null, 400);

        $token = $this->generateToken($user);

        return res('Success', $token);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function generateToken(User $user, $sm_id = null)
    {
        $origin = session('origin');
        OauthAccessToken::where('user_id', $user->id)->where('name', $origin)->delete();
        $data = [
            'token' => $user->createToken($origin)->accessToken,
            'user' => [
                'user_id' => $user->id,
                'hashid' => encode($user->id, 'uuid'),
                'social_id' => $sm_id ? $sm_id : null,
                'email' => $user->email,
                'mobile_prefix' => $user->mobile_prefix,
                'mobile' => $user->mobile,
                'name' => $user->name,
                'type_info' => $user->type_info,
                'gender' => $user->gender,
                'bdate' => $user->bdate,
            ]
        ];
        // NOTE: Also update on API routes
        return $data;
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function facebook($req)
    {
        $validator = Validator::make($req->all(), [
            'social_id' => 'required',
            'name' => 'required',
            'base64' => 'required'
        ]);
        if($validator->fails()) return res('Failed', $validator->errors(), 412);

        $social_media = SocialMedia::where('social_id', $req->social_id)->where('status', 1)->first();
        if(!$social_media){
            $token = $this->saveSocialMedia($req, 'facebook', $req->base64);
            return res('success', $token, 201);
        }

        $user = User::where('id', $social_media->user_id)->first();
        $token = $this->generateToken($user, $req->social_id);

        return res('success', $token, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function twitter($req)
    {
        $validator = Validator::make($req->all(), [
            'social_id' => 'required',
            'name' => 'required',
            'base64' => 'required'
        ]);
        if($validator->fails()) return res('Failed', $validator->errors(), 412);

        $social_media = SocialMedia::where('social_id', $req->social_id)->where('status', 1)->first();
        if(!$social_media){
            $token = $this->saveSocialMedia($req, 'twitter', $req->base64);
            return res('success', $token, 201);
        }

        $user = User::where('id', $social_media->user_id)->first();
        $token = $this->generateToken($user, $req->social_id);

        return res('success', $token, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function instagram($req)
    {
        $validator = Validator::make($req->all(), [
            'social_id' => 'required',
            'name' => 'required',
            'base64' => 'required'
        ]);
        if($validator->fails()) return res('Failed', $validator->errors(), 412);

        $social_media = SocialMedia::where('social_id', $req->social_id)->where('status', 1)->first();
        if(!$social_media){
            $token = $this->saveSocialMedia($req, 'instagram', $req->base64);
            return res('success', $token, 201);
        }

        $user = User::where('id', $social_media->user_id)->first();
        $token = $this->generateToken($user, $req->social_id);

        return res('success', $token, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function apple($req)
    {
        $validator = Validator::make($req->all(), [
            'social_id' => 'required',
            'name' => 'required',
            'base64' => 'required'
        ]);
        if($validator->fails()) return res('Failed', $validator->errors(), 412);

        $social_media = SocialMedia::where('social_id', $req->social_id)->where('status', 1)->first();
        if(!$social_media){
            $token = $this->saveSocialMedia($req, 'apple', $req->base64);
            return res('success', $token, 201);
        }

        $user = User::where('id', $social_media->user_id)->first();
        $token = $this->generateToken($user, $req->social_id);

        return res('success', $token, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function google($req)
    {
        $validator = Validator::make($req->all(), [
            'social_id' => 'required',
            'name' => 'required',
            'base64' => 'required'
        ]);
        if($validator->fails()) return res('Failed', $validator->errors(), 412);

        $social_media = SocialMedia::where('social_id', $req->social_id)->where('status', 1)->first();
        if(!$social_media){
            $token = $this->saveSocialMedia($req, 'google', $req->base64);
            return res('success', $token, 201);
        }

        $user = User::where('id', $social_media->user_id)->first();
        $token = $this->generateToken($user, $req->social_id);

        return res('success', $token, 200);
    }

    public function saveSocialMedia($req, String $social_type, String $base64)
    {
        $user = new User;
        $user->name = $req->name;
        $user->profile_photo = $base64;
        $user->password = '';
        $user->iso = session('iso', getIso());
        $user->status = 1;
        $user->save();

        $create_user_SM = new SocialMedia;
        $create_user_SM->user_id = $user->id;
        $create_user_SM->name = $req->name;
        $create_user_SM->social_type = $social_type;
        $create_user_SM->social_id = $req->social_id;
        $create_user_SM->save();

        // Uncomment if you want to test.
        // if you want to test please setup your braintree credential on .env files
        // if($user){
        //     $names = explode(" ", $user->name);
        //     $add_to_braintree = braintree()->customer()->create([
        //         'id'        => $user->id,
        //         'firstName' => $names[0],
        //         'lastName'  => end($names),
        //         'phone'     => $user->mobile ? $user->mobile : null,
        //     ]);
        // }

        (new ImageRepository())->createUserProfilePhoto($user->id, $user->profile_photo);
        $token = $this->generateToken($user, $req->social_id);
        return $token;
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function logout($req)
    {
        $origin = session('origin');
        OauthAccessToken::where('user_id', auth()->id())->where('name', $origin)->delete();
        return res('Success');
    }
}
