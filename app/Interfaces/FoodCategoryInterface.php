<?php

namespace App\Interfaces;

interface FoodCategoryInterface
{
    public function addMyFoodType($req);

    public function updateMyFoodType($req);
}