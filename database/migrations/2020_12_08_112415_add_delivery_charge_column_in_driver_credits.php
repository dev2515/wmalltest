<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryChargeColumnInDriverCredits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_credits', 'delivery_charge')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->integer('delivery_charge');
            });
        }

        if(Schema::hasColumn('driver_credits', 'income')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->renameColumn('income','commision');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('driver_credits', 'delivery_charge')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->integer('delivery_charge');
            });
        }

        if(Schema::hasColumn('driver_credits', 'commision')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->renameColumn('commision','income');
            });
        }
    }
}
