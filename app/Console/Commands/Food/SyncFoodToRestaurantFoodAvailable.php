<?php

namespace App\Console\Commands\Food;

use App\Model\StoreBranch;
use App\Models\RestaurantFood;
use App\Models\RestaurantFoodAvailable;
use App\Models\Store;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncFoodToRestaurantFoodAvailable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:food-inventory-to-old-branches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync food to restaurant food available';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        $main_stores = Store::where('is_main_branch', 1)->get();
        $bar = $this->output->createProgressBar(count($main_stores));
        $bar->start();
        foreach ($main_stores as $main_store){
            $branch_ids = StoreBranch::where('store_id', $main_store->id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            foreach ($branch_ids as $branch_id){
                $foods = RestaurantFood::where('restaurant_id', $main_store->id)->get();
                foreach ($foods as $food){
                    $check = RestaurantFoodAvailable::where('store_id', $branch_id)->where('food_id', $food->id)->first();
                    if(!$check){
                        $add            = new RestaurantFoodAvailable();
                        $add->store_id  = $branch_id;
                        $add->food_id   = $food->id;
                        $add->save();
                    }
                }
            }
            $bar->advance();
        }
        DB::commit();
        $bar->finish();
    }
}
