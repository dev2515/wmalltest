<?php

namespace App\Interfaces;

interface PaymentMethodInterface
{
    public function generateToken();

    public function register($req);

    public function checkout($req);

    public function savedCardInfo($req);
}
