<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">  
                <!-- <form method="POST" action="https://demopaymentapi.qpayi.com/api/gateway/v1.0" name="frmTransaction" id="1">
                    <input name="action" value="capture"/>
                    <input name="gatewayId" value="014146718"/>
                    <input name="secretKey" value="2-nT7NVDXyhiN/TS"/>
                    <input name="referenceId" value="1"/>
                    <input name="amount" value="23"/>
                    <input name="currency" value="QAR"/>
                    <input name="mode" value="test"/>
                    <input style="width:500px" name="Sample product" value="Product 1"/>
                    <input style="width:500px" name="returnUrl" value="https://wmall.qa/landing"/>
                    <input name="name" value="Earvin Michael Galimba"/>
                    <input style="width:500px" name="address" value="Flat No.8, Bldg No.12، Doha, Qatar"/>
                    <input name="city" value="Doha"/>
                    <input name="state" value="Quatar"/>
                    <input name="country" value="QA"/>
                    <input name="phone" value="44442504"/>
                    <input name="email" value="argiekimch@gmail.com"/>
                    <input value="Submit" type="submit"/>
                </form> -->
                <form method="post" action="https://demopaymentapi.qpayi.com/api/gateway/v1.0" name="frmTransaction" id="1" >
                    <input name="action" value="capture" />
                    <input name="gatewayId" value="014146718" />
                    <input name="secretKey" value="2-nT7NVDXyhiN/TS" />
                    <input name="referenceId" value="1" />
                    <input name="amount" value="1" />
                    <input name="currency" value="QAR" />
                    <input name="mode" value="TEST" />(TEST or LIVE)
                    <input style="width:500px" name="description" value="demo Transaction" />
                    <input style="width:500px" name="returnUrl" value="https://wmall.qa/" />
                    <input name="name" value="tester" />
                    <input name="address" value="testing" />
                    <input name="city" value="Doha" />
                    <input name="state" value="Doha" />
                    <input name="country" value="QR" />
                    <input name="phone" value="33000000" />
                    <input name="email" value="argiekimch@gmail.com" />
                    <input value="Submit" type="submit"/>
                </form>
                <!-- <form method="POST"action="https://demopaymentapi.qpayi.com/api/gateway/v1.0" name="frmTransaction">
                    <input name="action" type="hidden" value="Status" />
                    <input name="transactionId" type="hidden" value="XXXX" />
                    <input name="secretKey" type="hidden" value="014146718" />
                    <input name="gatewayId" type="hidden" value="2-nT7NVDXyhiN/TS"/>
                    <input name="submit" value="Submit" type="submit">
                </form > -->
            </div>
        </div>
    </body>
</html>
