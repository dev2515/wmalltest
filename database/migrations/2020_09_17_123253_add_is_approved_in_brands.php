<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsApprovedInBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('brands', 'is_approved')){
            Schema::table('brands', function (Blueprint $table) {
               $table->boolean('is_approved')->default(0)->after('profile_base64');
            });
        }

        if(!Schema::hasColumn('brands', 'is_declined')){
            Schema::table('brands', function (Blueprint $table) {
                $table->boolean('is_declined')->default(0)->after('is_approved');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('brands', 'is_approved')){
            Schema::table('brands', function (Blueprint $table) {
               $table->boolean('is_approved');
            });
        }

        if(Schema::hasColumn('brands', 'is_declined')){
            Schema::table('brands', function (Blueprint $table) {
                $table->boolean('is_declined');
            });
        }
    }
}
