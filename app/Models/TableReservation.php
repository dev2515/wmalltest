<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TableReservation extends Model
{
    protected $table = 'table_reservations';
    protected $appends = ['hash_id','customer'];
    //protected $fillable = ['restaurant_id','user_id', 'note', 'arrive_time', 'arrive_date', 'order', 'guest', 'qrcode'];

    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }
    
    public function getCustomerAttribute()
    {
        $id = isset($this->attributes['user_id']) ? $this->attributes['user_id'] : 0;
        $user = User::select('id', 'name')->where('id', $id)->first();
        return $user->name;
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }


    public function order()
    {
        return $this->hasMany(TableReservation::class, 'reservation_id');
    }
}
