<?php

namespace App\Http\Resources\restaurant;

use App\Models\QRPointer;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\Store;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class MyOrderInfo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'order_hash_id'                 => $this->hash_id,
            'order_number'                  => decode($this->order_number, 'order-number'),
            'order_type_info'               => $this->order_type_info,
            'payment_type'                  => $this->paymethod->name,
            'qrcode'                        => $this->getqrcode()->hash,
            'store_name'                    => $this->store()->name,
            'store_mobile'                  => $this->store()->phone,
            'customer_address'              => $this->shipping_address,
            'customer_mobile'               => $this->customer->mobile,
            'order_list'                    => $this->orderlist(),
            'sub_total'                     => $this->sub_total,
            'shipping_rate'                 => $this->shipping(),
            'total'                         => twodecimalplaces($this->sub_total + $this->shipping()),
            'driver_name'                   => $this->driver()->name,
            'driver_number'                 => $this->driver()->mobile,
        ];
    }

    public function getqrcode()
    {
        $c = RestaurantCart::where('id', $this->cart_id)->first();
        $qr = QRPointer::where('transaction_number', $c->transaction_number)->first();
        
        return $qr;
    }

    public function store()
    {
        $rol = RestaurantOrderList::where('order_id', $this->id)->first();
        $s = Store::where('id', $rol->restaurant_id)->first();

        return $s;
    }

    public function driver()
    {
        $d = User::where('id', $this->getqrcode()->qrcode->assigned_driver_id)->first();
        return $d;
    }

    public function orderlist()
    {
        $rol = RestaurantOrderList::where('order_id', $this->id)->first();
        $rof = RestaurantOrderFood::where('order_list_id', $rol->id)->get();
        if($rof) $rof->makeHidden(['id','order_list_id','commission','is_rate','status','created_at','updated_at']);

        return $rof;
    }

    public function customer()
    {
        $u = User::where('id', $this->customer_id)->first();
        return $u;
    }

    public function shipping()
    {
        $rate = $this->shipping_rate;
        if($this->order_type_info != 'Deliver') $rate = '0.00';
        return $rate;
    }
}
