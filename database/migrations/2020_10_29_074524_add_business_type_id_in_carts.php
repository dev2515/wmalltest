<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessTypeIdInCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_carts', 'business_type_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->unsignedBigInteger('business_type_id')->default(0);
            });
        }

        if(!Schema::hasColumn('carts', 'business_type_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->unsignedBigInteger('business_type_id')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('restaurant_carts', 'business_type_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->dropColumn('business_type_id');
            });
        }

        if(!Schema::hasColumn('carts', 'business_type_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->dropColumn('business_type_id');
            });
        }
    }
}
