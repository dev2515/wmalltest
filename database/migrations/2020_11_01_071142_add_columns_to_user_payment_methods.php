<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserPaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('user_payment_methods', ['card_number', 'name_on_card', 'expiration', 'cvv'])){
            Schema::table('user_payment_methods', function (Blueprint $table) {
                $table->integer('card_number')->after('user_id')->nullable();
                $table->string('name_on_card')->after('card_number')->nullable();
                $table->string('expiration')->after('name_on_card')->nullable();
                $table->string('cvv')->after('expiration')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('user_payment_methods', ['card_number', 'name_on_card', 'expiration', 'cvv'])){
            Schema::table('user_payment_methods', function (Blueprint $table) {
                $table->dropColumn('card_number');
                $table->dropColumn('name_on_card');
                $table->dropColumn('expiration');
                $table->dropColumn('cvv');
            });
        }
    }
}
