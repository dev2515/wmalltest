<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdInReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('reviews','user_id')){
            Schema::table('reviews', function (Blueprint $table) {
                $table->unsignedBigInteger('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('reviews', 'user_id')){
            Schema::table('reviews', function (Blueprint $table) {
                $table->dropColumn('user_id');
            });
        }
    }
}
