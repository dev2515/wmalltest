<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class DriverPreference extends Model
{
    protected $fillable = ['driver_id', 'business_type_id'];

    protected $appends = ['hashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
    
    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function type()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }
}
