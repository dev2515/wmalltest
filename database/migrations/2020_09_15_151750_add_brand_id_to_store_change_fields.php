<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBrandIdToStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        if(!Schema::hasColumn('store_change_fields', 'brand_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->integer('brand_id')->nullable()->after('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'brand_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('brand_id');
            });
        }
    }
}
