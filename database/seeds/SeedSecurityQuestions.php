<?php

use Illuminate\Database\Seeder;
use App\Models\SecurityQuestion;

class SeedSecurityQuestions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SecurityQuestion::firstOrCreate(['question' => 'What was the house number and street name you lived in as a child?']);
        SecurityQuestion::firstOrCreate(['question' => 'What were the last four digits of your childhood telephone number?']);
        SecurityQuestion::firstOrCreate(['question' => 'What primary school did you attend?']);
        SecurityQuestion::firstOrCreate(['question' => 'In what town or city was your first full time job?']);
        SecurityQuestion::firstOrCreate(['question' => 'In what town or city did you meet your spouse/partner?']);
        SecurityQuestion::firstOrCreate(['question' => 'What is the middle name of your oldest child?']);
        SecurityQuestion::firstOrCreate(['question' => 'What are the last five digits of your driver\'s licence number?']);
        SecurityQuestion::firstOrCreate(['question' => 'What is your grandmother\'s (on your mother\'s side) maiden name?']);
        SecurityQuestion::firstOrCreate(['question' => 'What is your spouse or partner\'s mother\'s maiden name?']);
        SecurityQuestion::firstOrCreate(['question' => 'In what town or city did your mother and father meet?']);
        SecurityQuestion::firstOrCreate(['question' => 'What is your first pet\'s name?']);
    }
}
