<?php

namespace App\Interfaces;

interface QRCodeInterface
{
    public function scan($req);

    public function bind($req);
}