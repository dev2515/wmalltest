<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercialPermitImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('commercial_permit_images')){
            Schema::create('commercial_permit_images', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('store_id');
                $table->longText('base64');
                $table->string('file_name')->default('');
                $table->smallInteger('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercial_permit_images');
    }
}
