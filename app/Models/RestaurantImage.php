<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantImage extends Model
{
    protected $table = 'restaurant_images';
    protected $fillable = ['restaurant_id', 'restaurant_image'];
    protected $appends = ['hash_id','store_name',];

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }
}
