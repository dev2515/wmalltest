<?php

namespace App\Interfaces;

interface InventoryInterface
{
    public function add($req);

    public function minus($req);

    public function list($req);
}
