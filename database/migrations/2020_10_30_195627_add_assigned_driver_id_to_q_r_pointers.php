<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssignedDriverIdToQRPointers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('q_r_pointers', 'assigned_driver_id')) {
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->integer('assigned_driver_id')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('q_r_pointers', function (Blueprint $table) {
            //
        });
    }
}
