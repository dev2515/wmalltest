<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use App\Http\Resources\User\Vendor;
use Carbon\Carbon;
class MyStore extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hashid' => $this->hashid,
            'store_name' => $this->name,
            'business_type' => $this->businessType ? $this->businessType->name : null,
            'store_information' => $this->store_information,
            'store_phone_number' => $this->phone,
            'store_email' => $this->paypal_email,
            'street' => $this->street,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'state' => $this->state,
            'city' => $this->city,
            'other_address_info' => $this->other_address_info,
            'vendor' => new Vendor($this->owner),
            'status_info' => $this->status_info,
            'ratings' => $this->ratings,
            'themes' => $this->theme(),
            'total_staff' => $this->total_staff,
            'total_product' => $this->products->count(),
            'timestamp' =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'profile_photo_full' => asset($this->local('/profile-photo')->full),
            'profile_photo_thumbnail' => asset($this->local('/profile-photo')->thumbnail),
            'cover_photo_full' => $this->local('/cover-photo')->full ? asset($this->local('/cover-photo')->full) : null,
            'cover_photo_thumbnail' => $this->local('/cover-photo')->thumbnail ? asset($this->local('/cover-photo')->thumbnail) : null,
        ];
    }

    public function local($type)
    {
        $full = public_path().'/images/stores/' . $this->hashid . $type .'-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hashid . $type .'-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . $this->hashid . $type .'-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . $this->hashid . $type .'-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }

    public function theme()
    {
        $data = new stdClass;
        $data->id = $this->theme->id;
        $data->name = $this->theme->name;
        $data->color1 = $this->theme->codes[0];
        $data->color2 = $this->theme->codes[1];
        $data->color3 = $this->theme->codes[2];
        $data->color4 = $this->theme->codes[3];

        return $data;
    }
}
