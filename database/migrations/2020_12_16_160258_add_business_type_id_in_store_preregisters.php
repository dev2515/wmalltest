<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessTypeIdInStorePreregisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_preregisters','business_type_id')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->string('business_type_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_preregisters','business_type_id')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->dropColumn('business_type_id');
            });
        }
    }
}
