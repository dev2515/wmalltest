<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverReview extends Model
{
    protected $table = 'driver_reviews';
    protected $fillable = ['user_id', 'driver_id', 'rate', 'review','status'];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
