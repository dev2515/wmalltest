<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInStorePreregisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('store_preregisters',['latitude','longitude','other_address_info'])){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->string('latitude');
                $table->string('longitude');
                $table->string('other_address_info');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('store_preregisters',['latitude','longitude','other_address_info'])){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->dropColumn('latitude');
                $table->dropColumn('longitude');
                $table->dropColumn('other_address_info');
            });
        }
    }
}
