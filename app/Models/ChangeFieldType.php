<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangeFieldType extends Model
{
    protected $hidden = ['id', 'status', 'created_at', 'updated_at'];
}
