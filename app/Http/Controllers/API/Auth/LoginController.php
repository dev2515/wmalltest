<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Interfaces\LoginInterface;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    protected $req;
    protected $login;

    public function __construct(Request $req, LoginInterface $login)
    {
        $this->req = $req;
        $this->login = $login;
    }

    public function email()
    {
        return $this->login->email($this->req);
    }

    public function mobile()
    {
        return $this->login->mobile($this->req);
    }

    public function mobileOtp()
    {
        return $this->login->mobileOtp($this->req);
    }

    public function logout()
    {
        return $this->login->logout($this->req);
    }

    public function agent()
    {
        return $this->login->agent($this->req);
    }

    public function vendor()
    {
        return $this->login->vendor($this->req);
    }

    public function driver()
    {
        return $this->login->driver($this->req);
    }

    public function facebook()
    {
        return $this->login->facebook($this->req);
    }

    public function twitter()
    {
        return $this->login->twitter($this->req);
    }

    public function instagram()
    {
        return $this->login->instagram($this->req);
    }

    public function apple()
    {
        return $this->login->apple($this->req);
    }

    public function google()
    {
        return $this->login->google($this->req);
    }
}
