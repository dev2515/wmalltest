<?php

namespace App\Interfaces;

interface ProductDetailInterface
{
    public function create_product($req);

    public function get_product($req);
}