<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressTypeIdToUserAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('user_addresses', 'address_type_id')){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->integer('address_type_id')->default(1)->after('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_addresses', 'address_type_id')){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->dropColumn('address_type_id');
            });
        }
    }
}
