<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTableIdInRestaurantCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_carts', 'table_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('table_id', 'reservation_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_carts', 'table_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('reservation_id', 'table_id');
            });
        }
    }
}
