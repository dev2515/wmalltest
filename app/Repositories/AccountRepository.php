<?php

namespace App\Repositories;

use App\User;
use App\Mail\EmailToken;
use App\Models\SecurityQuestion;
use App\Repositories\SMSRepository;
use App\Interfaces\AccountInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Repositories\LoginRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\User\UserData;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\DB;
use App\Models\StoreChangeField;
use App\Http\Resources\User\UserDataCollection;
use App\Http\Resources\User\AgentCollection;

class AccountRepository implements AccountInterface
{
    public function refreshToken($req)
    {
        $user = User::find(auth()->user()->id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        $token = (new LoginRepository)->generateToken($user);

        return res('Refreshed', $token);
    }

    public function update($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'name'      => 'required',
            'gender'    => 'required|in:male,female',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
        $validator = Validator::make($req->all(), [
            'bdate' => 'required',
        ]);
        if ($validator->fails()) {
            return res(__('user.required_bdate'), null, 400);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        $user->name     = $req->name;
        $user->gender   = $req->gender;
        $user->bdate    = $req->bdate;
        $user->save();

        return res('Success', $user);
    }

    public function changePassword($req)
    {
        $validator = Validator::make($req->all(), [
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user_id = auth()->id();

        $user = User::find($user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if (!Hash::check($req->current_password, $user->password)) {
            return res(__('user.invalid_current_password'), null, 400);
        }
        $user->password = bcrypt($req->password);
        $user->save();

        return res('Success');
    }

    public function checkPassword($req)
    {
        if (!$req->has('user_hash_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user_id = decode($req->user_hash_id, 'uuid');

        $user = User::find($user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if (!Hash::check($req->password, $user->password)) {
            return res(__('user.invalid_current_password'), null, 400);
        }
        return res('Success');
    }

    public function requestMobileChange($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'mobile' => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true),
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if ($user->iso === session('iso') && $user->mobile === $req->mobile) {
            return res(__('user.same_mobile'), null, 400);
        }
        $other = User::where('mobile', $req->mobile)->where('id', '<>', $req->user_id)->count();
        if ($other > 0) {
            return res(__('user.existing_mobile'), null, 400);
        }
        $user->temp_mobile  = $req->mobile;
        $user->mobile_token = generateMobileToken();
        $user->save();

        (new SMSRepository())->sendSecurityCode($user);

        return res(__('auth.verify_mobile', ['mobile' => getMobilePrefix(session('iso')) . $user->temp_mobile]));
    }

    public function confirmMobileChange($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('mobile_token', $req->token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }

        $user->mobile_token = null;
        $user->mobile       = $user->temp_mobile;
        $user->iso          = session('iso');
        $user->temp_mobile  = null;
        $user->save();

        return res('Success', [
            'mobile' => $user->mobile,
            'mobile_prefix' => getMobilePrefix($user->iso)
        ]);
    }

    public function requestEmailChange($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res(__('user.not_found'), null, 400);
        }
        if ($user->email === $req->email) {
            return res(__('user.same_email'), null, 400);
        }
        $other = User::where('email', $req->email)->where('id', '<>' ,$req->user_id)->count();
        if ($other > 0) {
            return res(__('user.existing_email'), null, 400);
        }
        $user->temp_email   = $req->email;
        $user->email_token  = generateEmailToken();
        $user->save();

        Mail::to($user->temp_email)->bcc(config('mail.admin.address'))->send(new EmailToken($user));

        return res(__('auth.verify_email', ['email' => $user->temp_email]));
    }

    public function confirmEmailChange($req)
    {
        $validator = Validator::make($req->all(), [
            'token' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email_token', $req->token)->first();
        if (!$user) {
            return res(__('auth.invalid_token'), null, 400);
        }

        $user->email_token  = null;
        $user->email        = $user->temp_email;
        $user->temp_email   = null;
        $user->save();

        return res('Success', [
            'email' => $user->email
        ]);
    }

    public function getSecurityQuestions($req)
    {
        $list = SecurityQuestion::where('status', 1)->pluck('question');

        return res('Success', $list);
    }

    public function checkInfo($req)
    {
        if (!$req->has('user_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $user = User::find($req->user_id);
        if (!$user) {
            return res('Unable to find user', null, 400);
        }
        $user->makeHidden(['store_info', 'email_verified_at', 'type', 'temp_email', 'temp_mobile']);

        return res('Success', $user);
    }

    public function userProfile($req)
    {
        $user_id = decode($req->hash_id, 'uuid');

        $user = User::where('id', $user_id)->first();

        if(!$user) return res('User not found', null, 404);

        $data = new UserData($user);

        return res('success', $data, 200);
    }

    public function createAgent($req)
    {
        DB::beginTransaction();
        if(auth()->user()->type_info != 'administrator'){
            return res('Only admin can create agent', null, 401);
            DB::rollback();
        }

        $validator = Validator::make($req->all(), [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => 'required|email',
            'profile_photo' => 'required',
            'password'      => 'required|min:6|alpha_num|confirmed'
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
            DB::rollback();
        }

        $user = User::where('email', $req->email)->first();
        if($user){
            return res('The email has already been taken', null, 400);
        }
        
        $user                   = new User;
        $user->name             = $req->first_name . ' ' . $req->last_name;
        $user->profile_photo    = $req->profile_photo;
        $user->email            = $req->email;
        $user->password         = bcrypt($req->password);
        $user->mobile           = $req->mobile ? $req->mobile : '';
        $user->iso              = session('iso', getIso());
        $user->type             = 7;
        $user->status           = 1;
        $user->save();

        if($user){
            $path = public_path() . '/images/users';
            if (!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/users', $mode = 0777, true, true);
            }
            File::makeDirectory(base_path() . '/public/images/users/' . encode($user->id, 'uuid'), $mode = 0777, true, true);

            $root_path = public_path() . '/images/users/' . encode($user->id, 'uuid') . '/';

            $image = Image::make($user->profile_photo);
            $image->save($root_path . 'full.png');
            $image->resize(150, 150);
            $image->save($root_path . 'thumbnail.png');
        }
        
        DB::commit();
        return res('success', $user->hash_id, 201);
    }

    public function createAdminStaff($req)
    {
        DB::beginTransaction();
        if(auth()->user()->type_info != 'administrator'){
            return res('Only admin can create admin staff', null, 401);
            DB::rollback();
        }

        $validator = Validator::make($req->all(), [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => 'required|email',
            'profile_photo' => 'required',
            'password'      => 'required|min:6|alpha_num|confirmed'
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
            DB::rollback();
        }

        $user = User::where('email', $req->email)->first();
        if($user){
            return res('The email has already been taken', null, 400);
        }
        
        $user                   = new User;
        $user->name             = $req->first_name . ' ' . $req->last_name;
        $user->profile_photo    = $req->profile_photo;
        $user->email            = $req->email;
        $user->password         = bcrypt($req->password);
        $user->mobile           = $req->mobile ? $req->mobile : '';
        $user->iso              = session('iso', getIso());
        $user->type             = 8;
        $user->status           = 1;
        $user->save();

        if($user){
            $path = public_path() . '/images/users';
            if (!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/users', $mode = 0777, true, true);
            }
            File::makeDirectory(base_path() . '/public/images/users/' . encode($user->id, 'uuid'), $mode = 0777, true, true);

            $root_path = public_path() . '/images/users/' . encode($user->id, 'uuid') . '/';

            $image = Image::make($user->profile_photo);
            $image->save($root_path . 'full.png');
            $image->resize(150, 150);
            $image->save($root_path . 'thumbnail.png');
        }
        
        DB::commit();
        return res('success', $user->hash_id, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function myProfile()
    {
        $user = User::where('id', auth()->id())->where('status', 1)->first();

        if(!$user) return res('User not found', null, 404);

        $data = new UserData($user);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function userChangeDetails($req)
    {   
        if(!$req->user_hashid) return res('user_hashid is required', null, 404);

        $user_id = decode($req->user_hashid, 'uuid');

        $user = User::where('id', $user_id)->where('status', 1)->first();
        if(!$user) return res(__('user.not_found'), null, 404);

        if($req->name != null)          $user->name     = $req->name;
        if($req->mobile_no != null)     $user->mobile   = $req->mobile_no;
        if($req->vendor_email != null)  $user->email    = $req->vendor_email;

        $user->save();
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentList($req)
    {
        $agents = User::where('type', 7)->where('name', 'LIKE', '%' . $req->key . '%')->orderBy('created_at', 'desc')->paginate($req->limit);
        if ($req->status == 'inactive') $agents     = User::where('status', 0)->where('name', 'LIKE', '%' . $req->key . '%')->where('type', 7)->orderBy('created_at', 'desc')->paginate($req->limit);
        if ($req->status == 'active')   $agents     = User::where('status', 1)->where('name', 'LIKE', '%' . $req->key . '%')->where('type', 7)->orderBy('created_at', 'desc')->paginate($req->limit);
        
        $data = new AgentCollection($agents);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentChangeStatus($req)
    {
        $permissions = ['administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)) return res('Only admin can do this process', null, 401);

        $validator = Validator::make($req->all(), [
            'user_hashid'   => 'required',
            'status'        => 'required',
        ]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);


        $id = decode($req->user_hashid, 'uuid');
        $agent = User::where('id', $id)->first();

        if ($req->status === 'active'){ $agent->status = 1;} 
        else { $agent->status = 0;}


        $agent->save();
        return res('success', null, 200);
    }

    public function agentCount()
    {
        $total = User::where('type', 7)->where('status', 1)->count();
        return res('success', $total, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function staffChangeStatus($req)
    {
        $permissions = ['administrator', 'agent', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('auth.not_authorize'), null, 401);
        }

        $validator = Validator::make($req->all(), [
            'user_hashid'   => 'required',
            'status'        => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user_id = decode($req->user_hashid, 'uuid');
        $user = User::where('id', $user_id)->where('type', 1)->first();
        if(!$user) return res('User not found', null, 404);

        if ($req->status === 'active'){ 
            $user->status = 1;
        } 
        else { 
            $user->status = 0;
        }

        $user->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addFcmToken($req)
    {
        $validator = Validator::make($req->all(), [
            'user_hashid'   => 'required',
            'fcm_token'     => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $user_id    = decode($req->user_hashid, 'uuid');

        $user       = User::where('id', $user_id)->first();
        if(!$user) return res(__('user.not_found'), null, 404);
        
        $user->fcm_device_token     = $req->fcm_token;
        $user->save();

        return res('success', null, 201);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function getFcmToken($req)
    {
        $validator = Validator::make($req->all(), [
            'user_hashid' => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $user_id    = decode($req->user_hashid, 'uuid');

        $user       = User::where('id', $user_id)->first();
        if(!$user) return res(__('user.not_found'), null, 404);

        if(!$user->fcm_device_token) return res(__('user.fcm_not_added'), null, 404);

        return res('success', $user->fcm_device_token, 200);
    }
}
