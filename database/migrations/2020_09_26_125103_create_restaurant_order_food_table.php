<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantOrderFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('restaurant_order_food')) {
            Schema::create('restaurant_order_food', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('order_list_id');
                $table->unsignedBigInteger('food_id');
                $table->string('name');
                $table->double('price');
                $table->integer('quantity');
                $table->double('commission');
                $table->integer('is_rate');
                $table->integer('status')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_order_food');
    }
}
