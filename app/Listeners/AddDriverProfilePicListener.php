<?php

namespace App\Listeners;

use App\Events\AddDriver;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\ImageRepository;

class AddDriverProfilePicListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddDriver  $event
     * @return void
     */
    public function handle(AddDriver $event)
    {
        $data = $event->data;

        if($data['profile_photo'] != null){
            (new ImageRepository())->createUserProfilePhoto($data['user']['id'], $data['user']['profile_photo']);
        }
    }
}
