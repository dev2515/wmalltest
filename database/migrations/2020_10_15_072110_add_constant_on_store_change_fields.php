<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstantOnStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_change_fields', 'constant')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->json('constant')->nullable()->after('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'constant')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('constant');
            });
        }
    }
}
