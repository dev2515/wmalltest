<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsExpirationDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('ads_expiration_dates')){
            Schema::create('ads_expiration_dates', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('duration_name');
                $table->string('duration_type');
                $table->integer('duration_value');
                $table->boolean('status');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads_expiration_dates');
    }
}
