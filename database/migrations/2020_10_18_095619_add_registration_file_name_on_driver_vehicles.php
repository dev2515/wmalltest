<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegistrationFileNameOnDriverVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_vehicles', 'registration_file_name')){
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->string('registration_file_name')->nullable()->after('registration_image');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('driver_vehicles', 'registration_file_name')){
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->dropColumn('registration_file_name');
            });
        }
    }
}
