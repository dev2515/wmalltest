<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsOpenedToStoreTimings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_timings', 'is_opened')){
            Schema::table('store_timings', function (Blueprint $table) {
                $table->boolean('is_opened')->default(1)->after('store_close');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_timings', 'is_opened')){
            Schema::table('store_timings', function (Blueprint $table) {
                $table->dropColumn('is_opened');
            });
        }
    }
}
