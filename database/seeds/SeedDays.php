<?php

use Illuminate\Database\Seeder;
use App\Models\Day;

class SeedDays extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Day::count();
        if($count == 0){
            $days = ['Monday', 'Tuesday', 'Wednesday', 'Thurstday', 'Friday', 'Saturday', 'Sunday'];
            foreach($days as $day){
                Day::firstOrCreate(['name' => $day]);
            }
        }
    }
}
