<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use Carbon\Carbon;
class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);
        
        return [
            'hashid'        => $this->hashid,
            'owner'         => $this->owner,
            'nationality'   => $this->nationality,
            'model'         => $this->model,
            'transmission'  => $this->transmission,
            'status'        => $this->status(),
            'timestamp'     =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            
        ];
    }

    public function status()
    {
        if($this->is_approved === 1) return 'approved';
        if($this->is_declined === 1) return 'declined';
        if($this->is_approved === 0 && $this->is_declined === 0) return 'pending';
    }
}
