<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestInCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('cart_items', 'request')){
            Schema::table('cart_items', function (Blueprint $table) {
                $table->string('request')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('cart_items', 'request')){
            Schema::table('cart_items', function (Blueprint $table) {
                $table->dropColumn('request');
            });
        }
    }
}
