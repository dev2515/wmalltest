<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdsExpirationDate extends Model
{
    protected $table = 'ads_expiration_dates';

    protected $appends = ['hashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
