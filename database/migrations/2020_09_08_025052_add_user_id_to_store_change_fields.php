<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_change_fields', 'user_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->integer('user_id')->nullable()->after('store_id');
            });
        }

        if(Schema::hasColumn('store_change_fields', 'store_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->integer('store_id')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'user_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('user_id');
            });
        }
    }
}
