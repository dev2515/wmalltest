<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    protected $appends = ['hashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function foods()
    {
        return $this->hasMany(\App\Models\RestaurantFood::class, 'meal');
    }
}

