<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherInfoToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'other_address_info')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('other_address_info')->nullable()->after('state');
            });
        }

        if(Schema::hasColumn('stores', 'address')){
            Schema::table('stores', function (Blueprint $table) {
                $table->renameColumn('address', 'street');
            });
        }

        if(Schema::hasColumn('stores', 'computer_permit_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->renameColumn('computer_permit_img', 'commercial_permit_img');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'other_address_info')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('other_address_info');
            });
        }

        if(Schema::hasColumn('stores', 'street')){
            Schema::table('stores', function (Blueprint $table) {
                $table->renameColumn('street', 'address');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_permit_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_permit_img', 'computer_permit_img');
            });
        }
    }
}
