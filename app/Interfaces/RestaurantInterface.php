<?php 

namespace App\Interfaces;

interface RestaurantInterface
{

    public function nearme($req);

    public function listByRate($req);

    public function listByFollowers($req);

    public function foodlist($req);

    public function addFood($req);

    public function updateFood($req);

    public function foodByCategory($req);

    public function addFavorite($req);

    public function favoriteList($req);

    public function restaurantFavorite($req);

    public function removeFavorite($req);

    public function foodTypes($req);

    public function listSearchStatus($req);

    public function showFood($req);
    
    public function listByRestaurant($req);
    
    public function foodChangeStatus($req);
    
    public function totalFood($req);
    
    public function totalCategory($req);
    
    public function myFoodType($req);
    
    public function foodTypeList($req);
    
    public function totalFoodType($req);

    public function ListByCategory($req);

    public function foodCategoryList();

    public function adminFoodChangeStatus($req);

    public function listByMeals($req);

    public function mealsList();

    public function listOfFoodByMyFoodType($req);

    public function totalOfFoodByMyFoodType($req);

    public function adminChangeName($req);

    public function restaurantCoordinates($req);

    public function addRestaurantImage($req);

    public function updateRestaurantImage($req);

    public function RestaurantImgList($req);

    public function RestaurantShow($req);

    public function addMultipleFood($req);

    public function addLocation($req);

    public function listByAllRestaurant($req);

    public function editOrderDuration($req);

    public function searchMonster($req);

    public function listOfFoodType();

    public function storeListwithFollow($req);

    public function menuList($req);

    public function restaurant_details($req);

    public function search_food($req);

    public function isFollowed($req);

    public function search_store($req);

    public function search_store_food($req);

    public function storeListOfFoodType($req);

    public function testlistbyrate($req);
}