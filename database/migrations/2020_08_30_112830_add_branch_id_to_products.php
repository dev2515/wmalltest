<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchIdToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('products', 'branch_ids')){
            Schema::table('products', function (Blueprint $table) {
                $table->json('branch_ids')->nullable()->after('product_type_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('products', 'branch_ids')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('branch_ids');
            });
        }
    }
}
