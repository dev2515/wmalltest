<?php

use App\Models\State;
use Illuminate\Database\Seeder;

class SeedStatesCities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iso = 'qat';
        $path = public_path('/data') . '/qat-states.json';
        $json = file_get_contents($path);
        $json = json_decode($json);
        foreach ($json as $key => $value) {
            $state = State::firstOrCreate(['name' => $key, 'iso' => $iso]);
            if (count($value) > 0) {
                foreach ($value as $city) {
                    State::firstOrCreate(['state_id' => $state->id, 'name' => $city, 'iso' => $iso]);
                }
            }
        }
    }
}
