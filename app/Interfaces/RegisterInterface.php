<?php

namespace App\Interfaces;

interface RegisterInterface
{
    public function email($req);

    public function verifyEmail($req);

    public function sendEmailVerificationToken($req, $email = null);

    public function mobile($req);

    public function verifyMobile($req);

    public function verifyMobileOtp($req);

    public function sendMobileVerificationToken($req, $mobile = null);

    public function checkEmailAvailability($req);

    public function checkMobileAvailability($req);

    public function checkMobile($req);

    public function getNationalities($req);

    public function blogger_registration($req);
}
