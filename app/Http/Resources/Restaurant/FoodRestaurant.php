<?php

namespace App\Http\Resources\Restaurant;
use App\User;
use App\FoodCategory;
use App\Models\RestaurantFoodAvailable;
use App\Models\StoreChangeField;
use stdClass;
use Illuminate\Http\Resources\Json\JsonResource;

class FoodRestaurant extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'        => $this->hashid,
            'is_edited'     => $this->isEditing(),
            'restaurant'    => new RestaurantResource($this->restaurant),
            'name'          => $this->name,
            'price'         => centToPrice($this->price),
            'description'   => $this->description,
            'status'        => $this->status(),
            'food_type'     => $this->category->name,
            'is_available'  => $this->available($request->store_id),
            'full'          => $this->photo()->full,
            'thumbnail'     => $this->photo()->thumbnail,
            'reviews'       => $this->reviews,
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function status()
    {
        if($this->is_approved === 1) return 'approved';
        if($this->is_declined === 1) return 'declined';
        
        return 'pending';
    }

    public function addedBy()
    {
        $user = User::where('id', $this->request_by)->first();
        if(!$user) return null;
            
        return $user->name;
    }

    public function available($store_id = null)
    {
        if($store_id){
            $food_available = RestaurantFoodAvailable::where('store_id', $store_id)->where('food_id', $this->id)->first();
            if(!$food_available) return __('food.food_not_bind');
            if($food_available->is_available === 1) return 'available';
            return 'unavailable';
        }

        if($this->is_available === 1) return 'available';
        return 'unavailable';
    }

    public function isEditing()
    {
        $request = StoreChangeField::where('restaurant_foods_id', $this->id)->where('is_approved', 0)->where('is_declined', 0)->count();
        if($request > 0) return 1;

        return 0;
    }
}
