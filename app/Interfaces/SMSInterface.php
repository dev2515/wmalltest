<?php

namespace App\Interfaces;

interface SMSInterface
{
    public function sendRegistrationCode($user);

    public function sendSecurityCode($user);
}
