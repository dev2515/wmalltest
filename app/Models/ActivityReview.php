<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityReview extends Model
{
    protected $table = 'activity_reviews';
    protected $fillable = ['activity_id','user_id','rating', 'review'];

    public function activity()
    {
        return $this->belongsTo(\App\Models\Activity::class, 'activity_id');
    }
}
