<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // One to many relationship on user table (attribute -> type_info == driver)
        if(!Schema::hasTable('driver_vehicles')){
            Schema::create('driver_vehicles', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('requested_by');
                $table->unsignedBigInteger('processed_by');
                $table->string('owner')->nullable();
                $table->string('qid')->nullable();
                $table->longText('registration_image')->nullable();
                $table->string('nationality');
                $table->string('registration_date');
                $table->string('expiration_date');
                $table->string('make');
                $table->string('model');
                $table->string('transmission')->nullable();
                $table->string('color');
                $table->integer('year_model');
                $table->boolean('is_approved')->default(0);
                $table->boolean('is_declined')->default(0);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_vehicles');
    }
}
