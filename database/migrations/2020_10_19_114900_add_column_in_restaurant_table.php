<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_tables','angle')){
            Schema::table('restaurant_tables', function (Blueprint $table) {
                $table->integer('angle')->default(45);
            });
        }

        if(!Schema::hasColumn('restaurant_tables', 'is_selected')){
            Schema::table('restaurant_tables', function (Blueprint $table) {
                $table->boolean('is_selected')->default(0);
            });
        }
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_tables', 'angle')){
            Schema::table('restaurant_tables', function (Blueprint $table) {
                $table->dropColumn('angle');
            });
        }

        if(Schema::hasColumn('restaurant_tables', 'is_selected')){
            Schema::table('restaurant_tables', function (Blueprint $table) {
                $table->dropColumn('is_selected');
            });
        }
    }
}
