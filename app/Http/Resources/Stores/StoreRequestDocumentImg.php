<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
class StoreRequestDocumentImg extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'full' => $this->image()->full,
            'thumbnail' => $this->image()->thumbnail,
        ];
    }

    public function image(Type $var = null)
    {
        $full = public_path() . '/images/store-requests/' . encode($this->store_change_field_id, 'uuid') . '/' . $this->id . '-' . $this->document_type . '-full.png';
        
        if(file_exists($full)){
            $full = '/images/store-requests/' . encode($this->store_change_field_id, 'uuid') . '/' . $this->id . '-' . $this->document_type . '-full.png';
        } else { $full = null; }

        $thumbnail = public_path() . '/images/store-requests/' . encode($this->store_change_field_id, 'uuid') . '/' . $this->id . '-' . $this->document_type . '-thumbnail.png';
        
        if(file_exists($thumbnail)){
            $thumbnail = '/images/store-requests/' . encode($this->store_change_field_id, 'uuid') . '/' . $this->id . '-' . $this->document_type . '-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumbnail);

        return $data;
    }
}
