<?php

namespace App\Http\Resources\Restaurant;

use App\Model\StoreBranch;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\Store;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ConfirmOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'order_hash_id' => $this->hash_id,
            'delivery_address' => $this->shipping_address,
            'latitude' => strval($this->shipping_address_latitude),
            'longitude' => strval($this->shipping_address_longitude),
            'user' => $this->user(),
            'store' => $this->store(),
            'contact_number' => $this->user()->mobile,
            'landline' =>   $this->user()->landline,
            'contactless' => 'none',
            'food_items' => $this->order_list_item(),
            'order_number' => strval(decode($this->order_number, 'order-number')),
            'total_amount' => twoDecimalPlaces($this->sub_total + $this->shipping_rate),
            'payment_method' => $this->paymethod->name,
        ];
    }

    public function user()
    {
        $u = User::select('id','name','email','mobile')
        ->where('id', $this->customer_id)->first();

        if($u) $u->makeHidden(['id','status_info','mobile_prefix','type_info','store_info','default_billing_address','default_shipping_address','driver_license_status','vehicle_registration_status']);
        return $u;
    }

    public function order_list_item()
    {
        $l = RestaurantOrderList::where('order_id', $this->id)->first();
        $f = RestaurantOrderFood::select('id','order_list_id','food_id','name','price','quantity')->where('order_list_id', $l->id)->get();
        if($f) $f->makeHidden(['id','order_list_id','food_id','food_info']);

        return $f;
    }

    public function store()
    {
        $order_list = RestaurantOrderList::where('order_id', $this->id)->first();
        
        $storebranch = Store::select('id','name','street','state','city','other_address_info','phone','store_email','latitude','longitude')
        ->where('id', $order_list->restaurant_id)->first();

        if($storebranch) $storebranch->makeHidden(['id']);
        
        return $storebranch;
    }    
}
