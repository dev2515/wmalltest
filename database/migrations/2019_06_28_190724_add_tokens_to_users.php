<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokensToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'email_token')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('email_token')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('users', 'mobile_token')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('mobile_token')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('users', 'password_token')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('password_token')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
