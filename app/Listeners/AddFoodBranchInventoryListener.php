<?php

namespace App\Listeners;

use App\Events\AddFoodEvent;
use App\Model\StoreBranch;
use App\Models\RestaurantFoodAvailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddFoodBranchInventoryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddFoodEvent  $event
     * @return void
     */
    public function handle(AddFoodEvent $event)
    {
        $food = $event->data->model;
        $branch_ids = StoreBranch::where('store_id', $food->restaurant_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
        foreach ($branch_ids as $branch_id){
            $check = RestaurantFoodAvailable::where('store_id', $branch_id)->where('food_id', $food->id)->first();
            if(!$check){
                $add_availability               = new RestaurantFoodAvailable();
                $add_availability->store_id     = $branch_id;
                $add_availability->food_id      = $food->id;
                $add_availability->is_available = 1;
                $add_availability->save();
            }
        }
    }
}
