<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('driver_preferences')) {
            Schema::create('driver_preferences', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('driver_id')->unsigned();
                $table->integer('business_type_id')->unsigned();
                $table->integer('status')->default(1);
                $table->timestamps();
            });     
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_preferences');
    }
}
