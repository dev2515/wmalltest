<?php

namespace App\Models;

use App\Http\Resources\Restaurant\TableResource;
use Illuminate\Database\Eloquent\Model;

class RestaurantFloor extends Model
{
    protected $table = 'restaurant_floors';
    protected $fillable = ['restaurant_id', 'name', 'floor_bg'];
    protected $appends = ['hash_id', 'table_info'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function getTableInfoAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $tables = RestaurantTable::where('floor', $id)->where('status', 1)->get();

        $tables = TableResource::collection($tables);
        return $tables;
    }
}
