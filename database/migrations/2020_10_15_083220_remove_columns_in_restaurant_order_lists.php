<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsInRestaurantOrderLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_order_lists', function (Blueprint $table) {
            $table->dropColumn('shipping_rate_info');
            $table->dropColumn('is_rated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_order_lists', function (Blueprint $table) {
            $table->integer('shipping_rate_info');
            $table->integer('is_rated');
        });
    }
}
