<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQidNumberQidExpirationToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('users', ['qid_number', 'qid_expiration', 'nationality'])){
            Schema::table('users', function (Blueprint $table) {
                $table->string('nationality')->nullable()->after('type');
                $table->string('qid_number')->nullable()->after('nationality');
                $table->string('qid_expiration')->nullable()->after('type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('users', ['qid_number', 'qid_expiration', 'nationality'])){
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('nationality');
                $table->dropColumn('qid_number');
                $table->dropColumn('qid_expiration');
            });
        }
    }
}
