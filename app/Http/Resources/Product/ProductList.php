<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Stores\StoreList;
use App\Models\ProductReview;
use App\Models\Inventory;
use Carbon\Carbon;
use stdClass;
class ProductList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hash_id' => $this->hashid,
            'name' => $this->name,
            'short_description' => $this->short_description,
            'on_sale' => $this->is_on_sale,
            'sale_price' => $this->sale_price,
            'stock_status' => $this->getStockStatus(),
            'initial_quantity' => $this->quantity(),
            'rating' => $this->rating_info,
            'Reviews' => $this->totalReview(),
            'timestamp' =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'product_price' => $this->price,
            'image_full' => asset($this->local()->full),
            'image_thumbnail' => asset($this->local()->thumbnail)
        ];
    }

    public function local()
    {
        $full = public_path().'/images/products/' . encode($this->id, 'uuid') . '/' . encode($this->images[0]->id, 'uuid') .'-full.png';
        if(file_exists($full)){
            $full = '/images/products/' . encode($this->id, 'uuid') . '/' . encode($this->images[0]->id, 'uuid') .'-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/products/' . encode($this->id, 'uuid') . '/' . encode($this->images[0]->id, 'uuid') .'-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/products/' . encode($this->id, 'uuid') . '/' . encode($this->images[0]->id, 'uuid') .'-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }

    public function totalReview(){

        $id = decode($this->hashid, 'uuid');
        $review = ProductReview::where('product_id', $id)->where('status', 1)->count();

        return $review;
    }

    public function quantity(){

        $id = decode($this->hashid, 'uuid');
        $review = Inventory::where('product_id', $id)->where('status', 1)->first();

        return $review->quantity;
    }

    public function getStockStatus()
    {
        $inventory = Inventory::where('product_id', $this->id)->where('status', 1)->first();
        if(!$inventory) return false;

        if($inventory->quantity >= 1) return 'in_stock';

        return 'out_of_stock';
    }
}
