<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('restaurants')){
            Schema::create('restaurants', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('user_id');
                $table->integer('requested_by')->nullable();
                $table->integer('process_by')->nullable();
                $table->integer('business_type_id')->nullable();
                $table->string('name');
                $table->string('email');
                $table->string('phone_number');
                $table->string('information')->nullable();
                $table->longText('profile_photo')->nullable();
                $table->longText('cover_photo')->nullable();
                $table->string('street')->nullable();
                $table->string('city')->nullable();
                $table->string('state')->nullable();
                $table->string('country')->nullable();
                $table->string('zip')->nullable();
                $table->string('other_address_info')->nullable();
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();
                $table->boolean('is_approved')->default(0);
                $table->boolean('is_declined')->default(0);
                $table->boolean('is_ban')->default(0);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
