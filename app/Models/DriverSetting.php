<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverSetting extends Model
{
    protected $fillable = ['user_id','notification_is_activated'];
}
