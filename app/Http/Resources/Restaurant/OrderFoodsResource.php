<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class OrderFoodsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'food_order_id' =>          $this->hash_id,
            'name' =>                   $this->name,
            'price' =>                  centToPrice($this->price),
            'quantity' =>               $this->quantity,
        ];
    }

    
}
