<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class RestaurantSearch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'restaurant' =>                 $this->name,
            'hash_id' =>                    $this->hash_id,
            'full' =>                       asset($this->photo()->full),
            'thumbnail' =>                  asset($this->photo()->thumbnail), 
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/stores/' . $this->hash_id . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hash_id . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->hash_id . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->hash_id . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
