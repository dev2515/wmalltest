<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantServicePivot extends Model
{
    protected $table = 'restaurant_service_pivots';
}
