<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model
{
    protected $appends = ['hashid'];
    protected $hidden = ['id', 'created_at', 'updated_at', 'status'];
    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
