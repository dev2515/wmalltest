<?php

namespace App\Http\Resources\Restaurant;

use App\Models\PaymentMethod;
use App\Models\QRPointer;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class DeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return[
            'order_hash_id'                 => encode($this->id,'uuid'),
            'order_number'                  => decode($this->order_number,'order-number'),
            'order_type'                    => 'Delivery',
            'order_status'                  => $this->order_status(),
            'sub_total'                     => centToPrice($this->sub_total),
            'shipping_rate'                 => centToPrice($this->shipping_rate),
            'total'                         => twoDecimalPlaces(centToPrice($this->sub_total) + centToPrice($this->shipping_rate)),
            'customer_hash_id'              => encode($this->customer()->id,'uuid'),
            'customer'                      => $this->customer()->name,
            'customer_mobile'               => $this->customer()->mobile,
            'payment_method'                => $this->paymentmethod()->name,
            'order_date'                    => Carbon::parse($this->created_at)->format('Y-m-d'),
            'order_time'                    => Carbon::parse($this->created_at)->format('g:i A'),
            'delivery_guy'                  => $this->driver()->name,
            'vehicle_plate'                 => $this->vehicle()->plate_number,
            'qrcode'                        => $this->qrcode(),
            'order_list'                    => $this->order_list(),
        ];
    }

    public function order_status()
    {
        switch ($this->status) {
            case 9 :
            return $status = 'Order Taken';
            break;

            case 8 :
            return $status = 'Preparing';
            break;

            case 7 :
            return $status = 'Delivery guy arrived';
            break;

            case 6 :
            return $status = 'Completed';
            break;

            case 5 :
            return $status = 'Delivered';
            break;

            case 4 :
            return $status = 'Delivering';
            break;

            case 3 :
            return $status = 'Ready To Deliver';
            break;

            case 2 :
            return $status = 'Preparing';
            break;

            case 1 :
            return $status = 'Pending';
            break;

            case 0 :
            return $status = 'Cancelled';
            break;

            default :
            return $status = 'Pending';
            break;
        }
    }

    public function customer()
    {
        $c = User::select('id', 'name', 'mobile')->where('id', $this->customer_id)->first();
        return $c;
    }

    public function paymentmethod()
    {
        $pm = PaymentMethod::select('id', 'name')->where('id', $this->payment_method_id)->first();
        return $pm;
    }

    public function driver()
    {
        $c = DB::table('restaurant_carts')->select('id', 'transaction_number')->where('id', $this->cart_id)->first();
        $p = DB::table('q_r_pointers')->select('id', 'transaction_number', 'assigned_driver_id')->where('transaction_number' , $c->transaction_number)->first();
        $d = DB::table('users')->select('id','name','mobile')->where('id', $p->assigned_driver_id)->first();
        return $d;
    }

    public function vehicle()
    {
        $vp = DB::table('driver_vehicles')->select('user_id','plate_number')->where('user_id', $this->driver()->assinged_driver_id)->first();
        return $vp;
    }

    public function order_list()
    {
        $rol = DB::table('restaurant_order_lists')->select('id','order_id')->where('order_id', $this->id)->first();
        $fl = RestaurantOrderFood::select('id','order_list_id','food_id','name','price','quantity','request')->where('order_list_id', $rol->id)->get();
        if($fl) $fl->makeHidden(['id','order_list_id','food_id']);
        return $fl;
    }

    public function qrcode()
    {
        $c = DB::table('restaurant_carts')->select('id', 'transaction_number')->where('id', $this->cart_id)->first();
        $p = DB::table('q_r_pointers')->select('id', 'hash', 'transaction_number', 'assigned_driver_id')->where('transaction_number' , $c->transaction_number)->first();
        return $p->hash;
    }
}
 