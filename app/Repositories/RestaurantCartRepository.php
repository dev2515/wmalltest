<?php

namespace App\Repositories;

use App\Http\Resources\Restaurant\CartResource;
use App\Interfaces\QRCodeInterface;
use App\Interfaces\RestaurantCartInterface;
use App\Model\StoreBranch;
use App\Models\FoodVariationSelection;
use App\Models\QRPointer;
use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use App\Models\RestaurantCartItemVariation;
use App\Models\RestaurantFood;
use App\Models\Store;
use App\Models\TableReservation;
use App\Models\VariationTypeSelection;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RestaurantCartRepository implements RestaurantCartInterface
{    
    protected $qrcode;
    public function __construct(QRCodeInterface $qrcode)
    {
        $this->qrcode = $qrcode;
    }

    public function list($req)
    {
        $permissions = ['vendor', 'vendor staff', 'customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed'. $v->errors(), 402);

        $id = decode($req->restaurant_hash_id, 'uuid');
        
        $list = RestaurantCart::where('restaurant_id', $id)->where('is_deleted', 0)->get();
        $list = CartResource::collection($list);

        return res('Success', $list);
    }

    public function mycartlist($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }
        
        $list = RestaurantCart::where('user_id', auth()->user()->id)
        ->where('on_checkout', 0)
        ->where('is_ordered', 0)
        ->where('is_deleted', 0)->first();

        if($list) $list = new CartResource($list);
        
        return res('Success', $list);
    }

    public function open_cart($req)
    {
        $permissions = ['customer','vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
            'cart_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $restaurant = decode($req->restaurant_hash_id, 'uuid');
        $reservation_id = decode($req->reservation_hash_id, 'uuid');
        
        $cart_item = RestaurantCart::where('restaurant_id', $restaurant)
                        ->where('reservation_id', $reservation_id)
                            ->first();
    
        $cart_item = new CartResource($cart_item);

        return res('success', $cart_item);
    }

    public function calculateTotalAmount() {
        $total = 0;
        $total_count = 0;
        $np = 0.0;
        $user_id = auth()->user()->id;
        $cart_ids = RestaurantCart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $items = RestaurantCartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->get();

        foreach ($items as $item) {
            $var = FoodVariationSelection::where('food_id', $item->id)->first();
            if($var) $np = centToPrice($var->price);

            $total += (centToPrice($item->food->price) + $np) * $item->quantity;
            $total_count += $total_count + 1;
        }

        $t = array(['total' => $total, 'total_count' => $total_count]);
        return $t;
    }

    public function add($req)
    {
        DB::beginTransaction();
        try{
            $permissions = ['customer','vendor', 'vendor staff'];

            if (!in_array(auth()->user()->type_info, $permissions)) {
                return res(__('user.not_allowed'), null, 400);
            }

            $validator = Validator::make($req->all(), [
                'food_id' => 'required',
                'quantity' => 'required',
                'order_type' => 'required',
            ]);

            if ($validator->fails()) return res('Failed', $validator->errors(), 412); DB::rollBack();
            
            $reservation_id = 0;
            $special_request = '';
            $has_booking = 0;

            if($req->reservation_hash_id != null || $req->reservation_hash_id != 0) $reservation_id = decode($req->reservation_hash_id, 'uuid');            

            if($req->quantity <= 0) return res('invalid quantity', $req->quantity, 412); DB::rollBack();
            if($req->special_request) $special_request = $req->special_request;

            $food_id = decode($req->food_id, 'uuid');   
            $food = RestaurantFood::where('id', $food_id)->first();
            if (!$food) return res(__('food.not_found'), null, 400);
        
            $restaurant = Store::where('id', $food->restaurant_id)->where('business_type_id', 3)
                ->where('is_approved', 1)->first();
            
            if (!$restaurant) return res(__('restaurant.not_found'), null, 400);

            if($req->has_booking == 1 && $req->order_type != 1) return res('failed...booking is active but the order type is not dine in');
            
            if($req->has_booking) $has_booking = 1;

            $cart = $this->fillCart($restaurant, $req->order_type, $has_booking, $reservation_id);

            $this->addCartItem($cart, $food, $req->quantity, $special_request);

            DB::commit();

            return res('Food Added to Cart', 'Cart Id: '.$cart->hash_id);

        } catch (\Throwable $e){
            DB::rollback();
            throw $e;
        } 
    }

    private function fillCart(Store $Restaurant, $order_type, $has_booking, $reservation_id)
    {
        $user_id = auth()->user()->id;
        $restaurant_id = $Restaurant->id;
        $s = StoreBranch::select('id','store_id', 'is_main')->where('store_id', $restaurant_id)->first();
        if($s->is_main == 0) $restaurant_id = $s->id;

        if(auth()->user()->type_info == 'vendor' || auth()->user()->type_info == 'vendor staff')
        {
            $reservation = TableReservation::where('id', $reservation_id)->first();
            $user_id = $reservation->user_id;
            $restaurant_id = $reservation->restaurant_id;
        }
        
        $cart = RestaurantCart::where('user_id', $user_id)
                    ->where('restaurant_id', $restaurant_id)
                        ->where('is_ordered', 0)->where('reservation_id', 0)
                            ->where('is_deleted', 0)->first();
        
        if (!$cart) {
            $tr =  encode(strtotime(Carbon::now()->format('YmdHis')), 'transaction_hash');

            $cart =                         new RestaurantCart;
            $cart->user_id =                $user_id;
            $cart->restaurant_id =          $Restaurant->id;
            $cart->order_type =             $order_type;
            $cart->has_booking =            $has_booking;
            $cart->business_type_id =       3;
            $cart->reservation_id =         $reservation_id;
            $cart->transaction_number =     decode($tr, 'transaction_hash');
        }
        else{
            if($reservation != 0)
            {
                $cart = RestaurantCart::where('user_id', $user_id)->where('restaurant_id', $restaurant_id)->where('is_ordered', 0)->where('is_deleted', 0)->first();  
                $cart->reservation_id = $reservation_id;
            }
        }      
        
        $cart->save();

        if($order_type != 1 && $reservation_id != 0){
            $req = collect(['qr_transaction_type' => 'restaurant', 'restaurant_hashid' => encode($Restaurant->id,'uuid'), 'transaction_number' => $cart->transaction_number]);
            $this->qrcode->bind($req);
        }
        return $cart;
    }

    private function addCartItem(RestaurantCart $cart, RestaurantFood $food, $quantity, String $request)
    {
        $item = RestaurantCartItem::where('cart_id', $cart->id)->where('food_id', $food->id)
            ->where('is_deleted', 0)->where('is_ordered', 0)->first();

        if (!$item) {
            $item =                         new RestaurantCartItem;
            $item->cart_id =                $cart->id;
            $item->food_id =                $food->id;
            $item->quantity =               $quantity;
            $item->request =                $request;
        }else{
            $item->quantity =               $item->quantity + $quantity;
        }

        $item->save();
        return $item;
    }

    public function count($req)
    {
        $user_id = auth()->user()->id;
        $cart_ids = RestaurantCart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $item_count = RestaurantCartItem::whereIn('cart_id', $cart_ids)
            ->where('is_deleted', 0)->where('is_ordered', 0)->sum('quantity');
        $item_count = (int)$item_count;

        return res('Success', $item_count);
    }

    public function clear($req)
    {
        $user_id = auth()->user()->id;
        $cart_ids = RestaurantCart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');

        RestaurantCart::whereIn('id', $cart_ids)->update(['is_deleted' => 1]);
        RestaurantCartItem::whereIn('cart_id', $cart_ids)->update(['is_deleted' => 1]);

        return res('Cart Cleared');
    }

    public function updateQuantity($req)
    {
        $v = Validator::make($req->all(),[
            'cart_hash_id' => 'required',
            'food_hash_id' => 'required',
            'quantity' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $cart_id = decode($req->cart_hash_id, 'uuid');
        $food_id = decode($req->food_hash_id, 'uuid');

        $id  = RestaurantCart::where('id', $cart_id)
        ->where('on_checkout', 0)->where('is_ordered', 0)->where('is_deleted', 0)->pluck('id');
        
        if(!$id) return res('failed! No cart found.');
        
        $item = RestaurantCartItem::where('cart_id', $id)->where('food_id', $food_id)
                ->where('is_ordered', 0)->where('is_deleted', 0)
                ->first();
        
        if(!$item) {
            return res(__('cart_item.not_found'), null, 400);
        }

        $food = RestaurantFood::find($food_id);
        if (!$food) {
            return res(__('food.not_found'), null, 400);
        }

        $qty = $req->quantity;

        $item->quantity = $qty;
        $item->save();

        $total = $this->calculateTotalAmount();

        $i = RestaurantCartItem::where('cart_id', $cart_id)->where('is_ordered', 0)->where('is_deleted', 0)->count();
        if($i === 0)
        {
            RestaurantCart::where('id', $cart_id)->update(['is_deleted' => 1]);
        }

        return res('Success', [
            'qty' => (int)$qty,
            'total' => $total
        ]);
    }

    public function toggleCheckout($req)
    {
        if (!$req->has('cart_no')) {
            return res(__('cart_no.identifier'), null, 400);
        }

        if (!$req->has('switch')) {
            return res('Switch is required', null, 400);
        }
        $user_id = auth()->user()->id;
        $cart_no = decode($req->cart_no, 'uuid');

        $cart_id  = RestaurantCart::where('id', $cart_no)->where('is_deleted', 0)->pluck('id');

        if ($req->cart_id == 0) {
            RestaurantCart::where('user_id', $user_id)->where('is_deleted', 0)->update(['on_checkout' => $req->switch]);
            $cart_ids = RestaurantCart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
            RestaurantCartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => $req->switch]);
        } else {
            $cart = RestaurantCart::where('id', $cart_id)->where('is_deleted', 0)->first();
            if (!$cart) {
                return res(__('validation.not_found'), null, 400);
            }
            if ($req->food_id == 0) {
                RestaurantCartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => $req->switch]);
                RestaurantCart::where('id', $cart_id)->update(['on_checkout' => $req->switch]);
            } else {
                RestaurantCartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => $req->switch]);
            }
        }   

        $total_item_count = RestaurantCartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->count();
        $checkout_item_count = RestaurantCartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->where('on_checkout', 1)->count();
        if ($checkout_item_count >= $total_item_count) {
            RestaurantCart::where('id', $cart_id)->update(['on_checkout' => 1]);
        } else {
            RestaurantCart::where('id', $cart_id)->update(['on_checkout' => 0]);
        }

        return res('Success');
    }

    public function remove($req)
    {
        if (!$req->has('cart_no')) {
            return res(__('cart.identifier'), null, 400);
        }
        if (!$req->has('food_id')) {
            return res(__('food.identifier'), null, 400);
        }

        $cart_id = decode($req->cart_no, 'uuid');
        $food_id = decode($req->food_id, 'uuid');

       // $cart_id  = RestaurantCart::where('id', $cart_no)->where('is_deleted', 0)->pluck('id');
        $r = RestaurantCartItem::where('cart_id', $cart_id)->where('food_id', $food_id)->where('is_ordered', 0)->where('is_deleted', 0)->first();
        //return res($food_id);
        RestaurantCartItem::where('cart_id', $cart_id)->where('food_id', $food_id)->where('is_ordered', 0)->where('is_deleted', 0)->update(['is_deleted' => 1]);
        $item_count = RestaurantCartItem::where('cart_id', $cart_id)->where('is_ordered', 0)->where('is_deleted', 0)->count();
        if ($item_count === 0) {
            RestaurantCart::where('id', $cart_id)->update(['is_deleted' => 1]);
        }

        return res('success');
    }

    public function checkout($req)
    {   
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_no' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $cart_no    = decode($req->cart_hash_id, 'uuid');
        $user_id    = auth()->user()->id;
        
        $list       = RestaurantCart::where('user_id', $user_id)->where('id', $cart_no)->where('is_deleted', 0)->get();
        if($list == null) return res('cart not exist');
        
        $list->makeHidden(['is_deleted', 'created_at', 'updated_at']);
        $list       = CartResource::collection($list);

        $list = $list->filter(function ($item) {
            return count($item->foods_info) > 0;
        });

        return res('Success', $list);
    }

    public function cartTotal($req)
    {
        $total = 0;
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_id' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $cart_id = decode($req->cart_id, 'uuid');
        
        $items = RestaurantCartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->get();
        
        foreach ($items as $item){
            $total += centToPrice($item->food->price) * $item->quantity;
        }

        return res('success', $total);
    }

    public function addTableReservation($req, $restaurant)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'table_id' =>           'required',
            'time_arrive' =>        'required',
            'date_arrive' =>        'required',
            'guest' =>              'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402)->send();
        }

        $table_id = decode($req->table_id, 'uuid');
        
        $time = strtotime($req->date_arrive);
        $date = strtotime($req->date_arrive);
        
        $available = Store::where('id', $restaurant)
        ->where('is_approved', 1) //available
        ->where('business_type_id', 3)
        ->count();

        if($available <= 0) return res('no table available', $restaurant)->send();

        $exist = TableReservation::where('table_id', $table_id)
        ->where('restaurant_id', $restaurant)
        ->where('arrive_time', $time)
        ->where('arrive_date', $date)
        ->where('status', 2) //means reserve
        ->first();

        if($exist != null){
            
            if($exist->arrive_time == $time && $exist->arrive_time == $date)
            {
                if($exist->user_id == auth()->user()->id && $exist->table_id == $req->table_id)
                {
                    return res('you already reserved this table on the same date and time')->send();
                }
                return res('this table is already reserved')->send();
            }
            return res('this table is already reserved');
        }

        $tr = TableReservation::where('restaurant_id', $restaurant)->where('user_id', auth()->user()->id)->where('table_id', $table_id)->where('status', 1)->first();
        if(!$tr)
        {
            $reserve =                      new TableReservation;
            $reserve->restaurant_id =       $restaurant;
            $reserve->user_id =             auth()->user()->id;
            $reserve->table_id =            $table_id;
            $reserve->note =                $req->note;
            $reserve->arrive_time =         $time;
            $reserve->arrive_date =         $date;
            $reserve->order =               1;
            $reserve->guest =               $req->guest;
            $reserve->qrcode =              Str::random(32);
            $reserve->save();

            return res('Table reservation success.', 'reservation code: '.$reserve->qrcode)->send();
        }
    }

    public function CartConfirmation($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_hash_id' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $cart_id = decode($req->cart_hash_id, 'uuid');
        $customer_cart =  RestaurantCart::where('id', $cart_id)->where('user_id', auth()->user()->id)->where('is_completed', 0)->first();

        $customer_cart->is_completed = 1;
        $customer_cart->save();

        return res('order confirmed by the customer');
    }

    public function reopenCart($req)
    {
        $permissions = ['vendor','vendor staff', 'customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_hast_id' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $cart_id = decode($req->cart_hast_id, 'uuid');
        
        $cart = RestaurantCart::where('id', $cart_id)->first();

        $c = RestaurantCart::where('user_id', $cart->user_id)
            ->where('on_checkout', 0)->where('is_ordered', 0)
                ->where('is_deleted', 0)->count();
        
        if($c > 0) return res('failed...there is an open cart.');

        if(!$cart) return res('cart not found');

        $cart -> on_checkout =          0;
        $cart -> is_ordered =           0;
        $cart -> save();

        return res('cart is successfully re-open');
    }

    public function addOrderInExistingCart($req)
    {
        $permissions = ['customer', 'vendor staff', 'vendor'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_hast_id' => 'required',
            'food_hash_id' => 'required',
            'quantity' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $cart_id = decode($req->cart_hast_id, 'uuid');
        $food_id = decode($req->food_hash_id, 'uuid');

        $cart = RestaurantCart::where('id', $cart_id)->first();
        if(!$cart) return res('cart not exist');

        $food = RestaurantFood::where('id', $food_id)->first();
        if(!$food) return res('cart not exist');

        $this->addCartItem($cart, $food, $req->quantity, $req->special_request);
        
        return res('successfully added!');
    }

    public function getQRCode($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_hash_id' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $cart_id = decode($req->cart_hash_id, 'uuid');

        $cart = RestaurantCart::where('id', $cart_id)->first();
        $qrcode = QRPointer::select('*')->where('transaction_number', $cart->transaction_number)->first();
        $qrcode->makeHidden(['id','transaction_type_id','business_type_id','transaction_status','receiver_hash', 'status','created_at','updated_at','product_id','user_id','business_link','assigned_driver_id']);
        
        return res('success', $qrcode);
    }

    public function addtoCart($req)
    {       
        DB::beginTransaction();
        try{ 
            $permissions = ['customer','vendor', 'vendor staff'];

            if (!in_array(auth()->user()->type_info, $permissions)) {
                return res(__('user.not_allowed'), null, 400);
            }

            $validator = Validator::make($req->all(),[
                'food_id' => 'required',
                'quantity' => 'required|min:1',
                'order_type' => 'required',
            ]);

            if($validator->fails()) return res('Failed', $validator->errors(), 402);
            
            $food_id = decode($req->food_id, 'uuid');
            $reservation_id = 0;
            $special_request = '';
            $has_booking = 0;
            
            if($req->reservation_hash_id != null || $req->reservation_hash_id != 0) $reservation_id = decode($req->reservation_hash_id, 'uuid');
         
            if($req->special_request) $special_request = $req->special_request;
               
            $food = RestaurantFood::where('id', $food_id)->first();
            if(!$food) return res(__('food.not_found'), null, 400);

            $restaurant = Store::where('id', $food->restaurant_id)->where('business_type_id', 3)->where('is_approved', 1)->first();            
            if (!$restaurant) return res(__('restaurant.not_found'), null, 400);
            
            if($req->has_booking == 1 && $req->order_type != 1) return res('failed...booking is active but the order type is not dine in');
            
            if($req->has_booking) $has_booking = 1;

           // $restaurant_id = $this->get_customer_currnet_location($restaurant);

            $cart = $this->fillMyCart($restaurant, $req->order_type, $has_booking, $reservation_id);
            
            $this->addMyCartItem($cart, $food, $req->quantity, $special_request, $req->variations);

            DB::commit();

            return res('Food Added to Cart', $cart->hash_id);

        } catch (\Throwable $e){
            DB::rollback();
            throw $e;
        } 
    }

    private function fillMyCart(Store $Restaurant, $order_type, $has_booking, $reservation_id)
    {   
        $user_id = auth()->user()->id;
        $r = $Restaurant->id;
        if(auth()->user()->type_info == 'vendor' || auth()->user()->type_info == 'vendor staff')
        {
            $reservation = TableReservation::where('id', $reservation_id)->first();
            $user_id = $reservation->user_id;
            $r = $reservation->restaurant_id;
        }

        if($reservation_id == null) $reservation_id = 0;
        $cart = RestaurantCart::where('user_id', $user_id)
                    ->where('restaurant_id', $r)
                    ->where('on_checkout', 0)
                    ->where('is_ordered', 0)->where('reservation_id', $reservation_id)
                    ->where('is_deleted', 0)->first();

        if (!$cart) {
            $tr = encode(strtotime(Carbon::now()->format('YmdHis')), 'transaction_hash');
        
            $cart =                         new RestaurantCart;
            $cart->user_id =                $user_id;
            $cart->restaurant_id =          $r;
            $cart->order_type =             $order_type;
            $cart->has_booking =            $has_booking;
            $cart->business_type_id =       3;
            $cart->reservation_id =         $reservation_id;
            $cart->transaction_number =     decode($tr, 'transaction_hash');
        }else{
            if($reservation != 0)
            {
                $cart = RestaurantCart::where('user_id', $user_id)->where('restaurant_id', $r)->where('is_ordered', 0)->where('is_deleted', 0)->first();  
                $cart->reservation_id = $reservation_id;
            }
        }

        $cart->save();
        if($order_type != 1 && $reservation_id < 1)
        {
            $req = collect(['qr_transaction_type' => 'restaurant', 'restaurant_hashid' => encode($r,'uuid'), 'transaction_number' => $cart->transaction_number]);
            $this->qrcode->bind($req);
        }

        return $cart;
    }

    private function addMyCartItem(RestaurantCart $cart, RestaurantFood $food, $quantity, String $request, $variations)
    {
        if(!$variations)
        {           
            $item = RestaurantCartItem::where('cart_id', $cart->id)->where('food_id', $food->id)
            ->where('is_deleted', 0)->where('is_ordered', 0)->first();
                 
            if(!$item) {
                
                $item =                         new RestaurantCartItem;
                $item->cart_id =                $cart->id;
                $item->food_id =                $food->id;
                $item->quantity =               $quantity;
                $item->request =                $request;
            } else {
                $item->quantity =               $item->quantity + $quantity;
            }
            
            $item->save();
        }
        else
        {
            foreach($variations as $v)
            {
                $r = decode($v['variation_item_id'],'uuid');
                $item = RestaurantCartItem::where('cart_id', $cart->id)->where('food_id', $food->id)
                ->where('is_deleted', 0)->where('is_ordered', 0)->first();
                     
                $item_v = RestaurantCartItemVariation::where('variation_id', $r)->first();
                $item_t = RestaurantCartItemVariation::where('variation_id', $item_v->variation_id)->where('cart_item_id', $item->cart_item_id)->first();
            
                if(!$item && !$item_t) {
                    $item =                         new RestaurantCartItem;
                    $item->cart_id =                $cart->id;
                    $item->food_id =                $food->id;
                    $item->quantity =               $quantity;
                    $item->request =                $request;
                } else {
                    $item->quantity =               $item->quantity + $quantity;
                }
                
                $item->save();
                $this->addMyCartItemVariation($item->id, $item->food_id, $r);  
            }
        }
        return $item;
    }

    public function addMyCartItemVariation($cart_item_id, $food_id, $req)
    {    
        $np = 0.0;
        $var = VariationTypeSelection::where('id', $req)->first();
        $fv  = FoodVariationSelection::where('id', $var->food_variation_id)->where('food_id', $food_id)->first();
        if(!$fv) return res('failed',' no food with variation found.');

        if($var->is_added_price == 1) {$np = $var->price;}
        if($var->is_added_price == 0) {$np = -$var->price;}

        $vp = RestaurantCartItemVariation::where('cart_item_id', $cart_item_id)->where('variation_id', $req)->first();            
        if(!$vp) {
            $vp =                        new RestaurantCartItemVariation;
            $vp->cart_item_id =          $cart_item_id;
            $vp->variation_id =          $req;
            $vp->amount =                priceToCent($np); 
        }else{
            $vp->amount =                priceToCent($np);
        }

        $vp->save();
    }

    public function get_customer_currnet_location($req)
    {
        $v = Validator::make($req->all(),[
            'radius' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);
        
        $radius = $req->radius;
        $lat = $req->latitude;
        $log = $req->longitude; 

        $haversine = "(6371 * acos(cos(radians($lat)) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) 
        - radians($log)) 
        + sin(radians($lat)) 
        * sin(radians(latitude))))";

        $r = Store::select('id','name','latitude','longitude') //pick the columns you want here.
        ->where('business_type_id', 3)
        ->selectRaw("{$haversine} AS distance")
        ->whereRaw("{$haversine} < ?", [$radius])->orderBy('distance')->limit(1)->get();

        if($r) $r->makeHidden(['id','main_store_id','status_info','chat_performance','cancellation_rate','product_count','categories',
        'banners','total_staff','']);

        return res('success', $r);
    }
}

