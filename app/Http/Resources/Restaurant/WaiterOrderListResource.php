<?php

namespace App\Http\Resources\Restaurant;

use App\Models\PaymentMethod;
use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use App\Models\RestaurantFood;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\RestaurantTable;
use App\Models\TableReservation;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class WaiterOrderListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'reservation_hash_id' =>            $this->hash_id,
            'name' =>                           $this->customer()->name,
            'mobile' =>                         $this->customer()->mobile,
            'table_no' =>                       $this->table_number,
            'time' =>                           date('H:i', $this->arrive_time),
            'date' =>                           date('Y-m-d', $this->arrive_date),
            'payment_method' =>                 $this->order()->paymethod->name,
            'order' =>                          $this->order(),
            'pre_ordered' =>                    $this->order_items(),
            'whats_in_my_cart' =>               $this->mycart(),
        ];
    }

    public function customer()
    {
        $cx = User::where('id', $this->user_id)->first();
        return $cx;
    }

    public function paymethod()
    {
        $paymethod = PaymentMethod::where('id', $this->order()->payment_method_id)->where('status', 1)->first();
        return $paymethod;
    }

    public function order()
    {
        $order = RestaurantOrder::select('id','order_number','sub_total','note','status','order_type','payment_method_id','cart_id')->where('reservation_id', $this->id)->where('order_type', 1)->first();
        if($order != null) $order->makeHidden(['id','status','order_type','user_name','paymethod','cart_id']);
        return $order;
    }

    public function order_items()
    {
        $item_order_list = RestaurantOrderList::where('order_id', $this->order()->id)->first()->id;
        $order_items = RestaurantOrderFood::where('order_list_id', $item_order_list)->get();
        if($order_items) $order_items->makeHidden(['id','order_list_id','food_id','commission','is_rate'
        ,'status','created_at','updated_at']);

        return $order_items;
    }

    public function mycart()
    {
        $rc = RestaurantCart::select('id','user_id','restaurant_id','transaction_number','reservation_id','created_at')
                ->where('on_checkout',0)->where('is_ordered',0)->where('is_deleted', 0)->where('reservation_id', $this->id)->get();               
        if($rc) $rc->makeHidden(['id','user_id','restaurant_id','reservation_id','restaurant_info','foods_info.on_checkout']);

        return $rc;
    }
}
