<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use App\Models\SignatureImage;

use Illuminate\Support\Facades\File;
class SignatureImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data->model;

        $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';

        $image = Image::make($store->signature_img);
        $image->save($store_path . 'signature-full.png');
        $image->resize(150, 150);
        $image->save($store_path . 'signature-thumbnail.png');
    }
}
