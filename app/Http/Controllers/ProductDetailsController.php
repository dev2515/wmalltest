<?php

namespace App\Http\Controllers;

use App\Interfaces\ProductDetailInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductDetailsController extends Controller
{
    private $req;
    private $productdetail;

    public function __construct(Request $req, ProductDetailInterface $productdetail)
    {
        $this->req = $req;
        $this->productdetail = $productdetail;
    }
    
    public function addProduct()
    {
        return $this->productdetail->create_product($this->req);
    }

   
}
