<?php

namespace App\Interfaces;

interface CartInterface
{
    public function list($req);

    public function add($req);

    public function count($req);

    public function clear($req);

    public function addQuantity($req);

    public function subtractQuantity($req);

    public function toggleCheckout($req);

    public function remove($req);

    // Market Place Lists

    public function mk_cart_list($req);

    // Restaurant Lists

    public function restaurant_cart_list($req);
}
