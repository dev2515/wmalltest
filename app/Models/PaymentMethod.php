<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public function restaurant_orders()
    {
        return $this->hasMany(\App\Models\RestaurantOrder::class, 'payment_method_id');
    }
}
