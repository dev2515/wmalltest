<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Interfaces\CMSInterface;
use App\Interfaces\BankInterface;
use App\Interfaces\ImageInterface;
use App\Interfaces\OrderInterface;
use App\Interfaces\VendorInterface;
use App\Http\Controllers\Controller;
use App\Interfaces\AccountInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\CategoryInterface;
use App\Interfaces\RegisterInterface;
use App\Interfaces\ShippingInterface;
use App\Interfaces\AdminInterface;
use App\Interfaces\StoreInterface;

class OpenController extends Controller
{
    protected $req;
    protected $vendor;
    protected $account;
    protected $product;
    protected $category;
    protected $bank;
    protected $cms;
    protected $register;
    protected $shipping;
    protected $image;
    protected $order;
    protected $admin;
    protected $store;

    public function __construct(Request $req, VendorInterface $vendor, AccountInterface $account, ProductInterface $product, CategoryInterface $category, BankInterface $bank, CMSInterface $cms, RegisterInterface $register, ShippingInterface $shipping, ImageInterface $image, OrderInterface $order, AdminInterface $admin, StoreInterface $store)
    {
        $this->req      = $req;
        $this->vendor   = $vendor;
        $this->account  = $account;
        $this->product  = $product;
        $this->category = $category;
        $this->bank     = $bank;
        $this->cms      = $cms;
        $this->register = $register;
        $this->shipping = $shipping;
        $this->image    = $image;
        $this->order    = $order;
        $this->admin    = $admin;
        $this->store    = $store;
    }

    public function vendorTypes()
    {
        return $this->vendor->getTypes($this->req);
    }

    public function storeListFilter()
    {
        return $this->vendor->storeListFilter($this->req);
    }

    public function storeAdvertisementList()
    {
        return $this->store->storeAdvertisementList($this->req);
    }

    public function showStoreOpenApi()
    {
        return $this->vendor->showStoreOpenApi($this->req);
    }

    public function securityQuestions()
    {
        return $this->account->getSecurityQuestions($this->req);
    }

    public function productTypes()
    {
        return $this->product->types($this->req);
    }

    public function categories()
    {
        return $this->category->list($this->req);
    }

    public function banks()
    {
        return $this->bank->list($this->req);
    }

    public function getVendorTermsAndConditions()
    {
        return $this->cms->getContent($this->req, 'vendor-terms-and-conditions');
    }

    public function getAboutUs()
    {
        return $this->cms->getContent($this->req, 'about-us');
    }

    public function getBuyerProtection()
    {
        return $this->cms->getContent($this->req, 'buyer-protection');
    }

    public function checkEmailAvailability()
    {
        return $this->register->checkEmailAvailability($this->req);
    }

    public function checkMobileAvailability()
    {
        return $this->register->checkMobileAvailability($this->req);
    }

    public function getNationalities()
    {
        return $this->register->getNationalities($this->req);
    }

    public function getShippingProviders()
    {
        return $this->shipping->getProviders($this->req);
    }

    public function getStockStatus()
    {
        return $this->product->stockStatus($this->req);
    }

    public function getPrivacyPolicy()
    {
        return $this->cms->getContent($this->req, 'privacy-policy');
    }

    public function getProductPhotos()
    {
        return $this->product->getProductPhotos($this->req);
    }

    public function getBannerNames()
    {
        return $this->image->bannerNames($this->req);
    }

    public function getPaymentMethods()
    {
        return $this->order->paymentMethods($this->req);
    }

    public function getStoreChangeFields()
    {
        return $this->admin->storeChangeFields($this->req);
    }

    public function convertCurrency()
    {
        return $this->bank->convertCurrency($this->req);
    }
}
