<?php

namespace App\Http\Resources\MarketPlace\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Product;
use stdClass;
class StoreItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'            => $this->getProduct() ? $this->getProduct()->hashid : 'This product is deleted',
            'name'              => $this->getProduct() ? $this->getProduct()->name : 'This product is deleted',
            'price'             => $this->getProduct() ? $this->getProduct()->price : 'This product is deleted',
            'quantity'          => $this->quantity,
            'photo_full'        => $this->productImage()->full,
            'photo_thumbnail'   => $this->productImage()->thumbnail,
        ];
    }

    public function getProduct()
    {
        $product = Product::where('id', $this->product_id)->first();
        if(!$product) return null;

        return $product;
    }

    public function productImage()
    {
        $product = Product::where('id', $this->product_id)->first();
        if(!$product) return null;
        
        $full = public_path().'/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-full.png';
        if(file_exists($full)){
            $full = '/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumbnail);

        return $data;
    }
}
