<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use App\Models\CommercialPermitImage;

use Illuminate\Support\Facades\File;
class ComputerPermitImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data->model;

        $commercial_permit_images = json_decode($event->data->commercial_permit_images, true);

        foreach ($commercial_permit_images as $image){
            $new = new CommercialPermitImage;
            $new->store_id = $store->id;
            $new->base64 = $image;
            $new->save();

            $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';
        
            $path = $store_path . 'commercial-permit-images/';

            if(!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/stores/' . encode($store->id, 'uuid') . '/commercial-permit-images', $mode = 0777, true, true);
            }

            $image = Image::make($new->base64);
            $image->save($path . $new->id .'-commercial-permit-full.png');
            $image->resize(150, 150);
            $image->save($path . $new->id .'-commercial-permit-thumbnail.png');
        }

    }
}
