<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\StoreSchedule;
use App\Models\StoreTiming;
use Carbon\Carbon;
class SetDefaultTimePerDayListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data;
        $store_schedule_ids = StoreSchedule::where('store_id', $store->id)->pluck('id');
            foreach ($store_schedule_ids as $id){
                $check = StoreTiming::where('store_schedule_id', $id)->where('store_open', Carbon::now()->format('Y-m-d 09:00'))->where('store_close', Carbon::now()->format('Y-m-d 19:00'))->first();
                if(!$check){
                    $add_time = new StoreTiming();
                    $add_time->store_schedule_id = $id;
                    $add_time->store_open   = Carbon::now()->format('Y-m-d 09:00');
                    $add_time->store_close  = Carbon::now()->format('Y-m-d 19:00');
                    $add_time->save();
                }
            }
    }
}
