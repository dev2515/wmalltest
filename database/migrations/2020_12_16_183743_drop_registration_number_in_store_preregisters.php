<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRegistrationNumberInStorePreregisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('store_preregisters','registration-number')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->dropColumn('registration-number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_preregisters','registration-number')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->string('registration-number');
            });
        }
    }
}
