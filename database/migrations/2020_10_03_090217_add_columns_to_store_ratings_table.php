<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToStoreRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_ratings', 'flag')){
            Schema::table('store_ratings', function (Blueprint $table) {
                $table->integer('flag')->nullable();
            });
        }

        if(!Schema::hasColumn('store_ratings', 'comment')){
            Schema::table('store_ratings', function (Blueprint $table) {
                $table->integer('comment')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_ratings', 'flag')){
            Schema::table('store_ratings', function (Blueprint $table) {
                $table->dropColumn('flag');
            });
        }

        if(Schema::hasColumn('store_ratings', 'comment')){
            Schema::table('store_ratings', function (Blueprint $table) {
                $table->dropColumn('comment');
            });
        }
    }
}
