<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StoreBranch extends Model
{
    protected $appends = ['hashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
