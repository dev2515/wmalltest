<?php

namespace App\Interfaces;

interface TableReservationInterface
{
    public function list($req);

    public function myreservationhistory($req); 

    public function myqrcode($req);

    public function restaurantQRcode($req);

    public function listbydatereserve($req);

    public function listbydate($req);

    public function add($req);

    public function update($req);

    public function cancel($req);

    public function acceptreservation($req);

    public function myreservation($req);

    public function scanQRcode($req);

    public function scannedQRcode($req);

    public function scan_cart_detail($req);

    public function reservation_info($req);

    public function restaurant_qrcode_list($req);

    public function assignTable($req);

    public function reservation_checkout_details($req);

    public function my_reservation_list($req);
}