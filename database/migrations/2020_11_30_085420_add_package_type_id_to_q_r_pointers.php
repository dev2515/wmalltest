<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackageTypeIdToQRPointers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('q_r_pointers', 'package_type_id')){
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->integer('package_type_id')->default(1)->after('business_type_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('q_r_pointers', 'package_type_id')){
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->dropColumn('package_type_id');
            });
        }
    }
}
