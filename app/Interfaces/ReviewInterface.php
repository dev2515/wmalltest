<?php

namespace App\Interfaces;

interface ReviewInterface{

    public function store_review_list($req);

    public function store_review_add($req);

    public function store_review_remove($req);

    public function my_review_and_rating($req);
}