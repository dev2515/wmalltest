<?php

namespace App\Listeners;

use App\Events\AddFoodEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\StoreChangeField;

class AddStoreRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddFoodEvent  $event
     * @return void
     */
    public function handle(AddFoodEvent $event)
    {
        $food = $event->data->model;
        $store_request                      = new StoreChangeField();
        $store_request->store_id            = $food->restaurant_id;
        $store_request->restaurant_foods_id = $food->id;
        $store_request->type                = 'add food';
        $store_request->request_by          = auth()->id();   
        $store_request->process_by          = auth()->user()->type_info === 'administrator' ? auth()->id() : 0;
        $store_request->is_approved         = auth()->user()->type_info === 'administrator' ? 1 : 0;
        $store_request->save();
    }
}
