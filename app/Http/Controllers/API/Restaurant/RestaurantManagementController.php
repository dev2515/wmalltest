<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RestaurantManagementInterface;

class RestaurantManagementController extends Controller
{
    protected $req;
    protected $restaurant;

    public function __construct(Request $req, RestaurantManagementInterface $management)
    {
        $this->req = $req;
        $this->management = $management;
    }

    /*****************************************************************************************
     * 
     *                  MANAGER CONTROLLER
     * 
     *****************************************************************************************/

    public function dine_in_order_list()
    {
        return $this->management->dine_in_order_list($this->req);
    }

    public function manager_order_list()
    {
       return $this->management->manager_order_list($this->req);
    }

     /*****************************************************************************************
     * 
     *                  WAITER CONTROLLER
     * 
     *****************************************************************************************/

     public function dine_in_order_list_waiter()
     {
        return $this->management->dine_in_order_list_waiter($this->req);
     }

     public function waiter_confirmation_order()
     {
        return $this->management->waiter_confirmation_order($this->req);
     }

     public function complete_order()
     {
        return $this->management->complete_order($this->req);
     }

     public function updateFoodStatus()
     {
        return $this->management->updateFoodStatus($this->req);
     }

     public function waiter_confirm_order()
     {
        return $this->management->waiter_confirm_order($this->req);
     }
     
}
