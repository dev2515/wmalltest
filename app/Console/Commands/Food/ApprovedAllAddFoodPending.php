<?php

namespace App\Console\Commands\Food;

use Illuminate\Console\Command;
use App\Models\RestaurantFood;
use App\Models\StoreChangeField;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
class ApprovedAllAddFoodPending extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'approved:food {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        $password = 'paoten';

        $password_confirm = $this->secret('What is the password?');

        $val  = Carbon::now();
        $date = Carbon::parse($val);

        $now = (int)$date->format('ymdhis');

        if($password == $password_confirm){
            if ($this->confirm('Buckup database and execute this command')) {

                $path = base_path() . '/database/buckups';
                if(!file_exists($path)){
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
                $cmd = 'mysqldump -h ' . config('database.connections.mysql.host') . ' -u ' . config('database.connections.mysql.username') . ' -p ' . config('database.connections.mysql.password') . ' '. config('database.connections.mysql.database') .' > database/buckups/' . $now .'-buckup.sql';
                exec($cmd);
                $this->info('Database successfully buck-up');
                if($type == 'add') {
                    $get_store_ids  = RestaurantFood::where('is_approved', 0)->where('is_declined', 0)->pluck('restaurant_id');
                    $store_request  = StoreChangeField::whereIn('store_id', $get_store_ids)->where('type', 'add food')->update(['is_approved' => 1]);
                    $foods          = RestaurantFood::where('is_approved', 0)->where('is_declined', 0)->update(['is_approved' => 1]);
                    $this->info('Successfuly approved');
                }else if ($type == 'edit') {
                    $this->info('Undergoing Maintenance');
                }
            }else {
                if($type == 'add') {
                    $get_store_ids  = RestaurantFood::where('is_approved', 0)->where('is_declined', 0)->pluck('restaurant_id');
                    $store_request  = StoreChangeField::whereIn('store_id', $get_store_ids)->where('type', 'add food')->update(['is_approved' => 1]);
                    $foods          = RestaurantFood::where('is_approved', 0)->where('is_declined', 0)->update(['is_approved' => 1]);
                    $this->info('Successfuly approved');
                }else if ($type == 'edit') {
                    $this->info('Undergoing Maintenance');
                }
            }
        }else {
            $this->info('Invalid password');
        }
    }
}
