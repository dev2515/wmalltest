<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

use App\Models\Store;
use App\Models\StoreStaff;
use Carbon\Carbon;
class UserData extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'type'              => $this->type_info,
            'business_type'     => $this->type_info == 'vendor staff' ? $this->businessType() : null,
            'hashid'            => $this->hash_id,
            'name'              => $this->name,
            'email'             => $this->email,
            'mobile_number'     => $this->mobile,
            'joined_date'       => $this->created_at,
            'status'            => $this->status ? 'active' : 'inactive',
            'timestamp'         => $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'address'           => new CustomerAddress($this->current_address),
            'full'              => asset($this->profilePhoto()->full),
            'thumbnail'         => asset($this->profilePhoto()->thumbnail),
        ];
    }

    public function profilePhoto()
    {
        $full = public_path() . '/images/users/' . $this->hash_id . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . $this->hash_id . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . $this->hash_id . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . $this->hash_id . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }

    public function businessType()
    {
        $staff = StoreStaff::where('user_id', auth()->id())->where('status', 1)->first();
        if(!$staff) return null;

        $store = Store::where('id', $staff->store_id)->first();
        if(!$store) return null;

        $data = new stdClass;
        $data->hashid = $store->hashid;
        $data->type = $store->businessType ? $store->businessType->name : null;

        return $data;
    }

}
