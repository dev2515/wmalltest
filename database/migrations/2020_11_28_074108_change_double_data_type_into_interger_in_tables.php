<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDoubleDataTypeIntoIntergerInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumns('restaurant_order_food', ['price','commission'])){
            Schema::table('restaurant_order_food', function (Blueprint $table) {
                $table->integer('price')->change();
                $table->integer('commission')->change();
            });
        }

        if(Schema::hasColumn('restaurant_order_lists', 'sub_total')){
            Schema::table('restaurant_order_lists', function (Blueprint $table) {
                $table->integer('sub_total')->change();
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'voucher_value')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->integer('voucher_value')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_items', 'voucher_value')){
            Schema::table('restaurant_cart_items', function (Blueprint $table) {
                $table->integer('voucher_value')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_item_variations', 'np')){
            Schema::table('restaurant_cart_item_variations', function (Blueprint $table) {
                $table->integer('np')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_item_variations', 'np')){
            Schema::table('restaurant_cart_item_variations', function (Blueprint $table) {
                $table->integer('np')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_item_variations', 'np')){
            Schema::table('restaurant_cart_item_variations', function (Blueprint $table) {
                $table->renameColumn('np', 'amount');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('restaurant_order_food', ['price','commission'])){
            Schema::table('restaurant_order_food', function (Blueprint $table) {
                $table->double('price')->change();
                $table->double('commission')->change();
            });
        }

        if(Schema::hasColumn('restaurant_order_lists', 'sub_total')){
            Schema::table('restaurant_order_lists', function (Blueprint $table) {
                $table->double('sub_total')->change();
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'voucher_value')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->double('voucher_value')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_items', 'voucher_value')){
            Schema::table('restaurant_cart_items', function (Blueprint $table) {
                $table->double('voucher_value')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_item_variations', 'np')){
            Schema::table('restaurant_cart_item_variations', function (Blueprint $table) {
                $table->double('np')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_item_variations', 'np')){
            Schema::table('restaurant_cart_item_variations', function (Blueprint $table) {
                $table->double('np')->change();
            });
        }

        if(Schema::hasColumn('restaurant_cart_item_variations', 'amount')){
            Schema::table('restaurant_cart_item_variations', function (Blueprint $table) {
                $table->renameColumn('amount', 'np');
            });
        }
    }
}
