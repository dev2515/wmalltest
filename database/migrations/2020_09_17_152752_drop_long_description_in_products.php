<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropLongDescriptionInProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('products', 'long_description')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('long_description');
            });
        }

        if(!Schema::hasColumn('products', 'brand_id')){
            Schema::table('products', function (Blueprint $table) {
                $table->integer('brand_id')->after('store_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('products', 'brand_id')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('brand_id');
            });
        }
    }
}
