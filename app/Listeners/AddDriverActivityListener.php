<?php

namespace App\Listeners;

use App\Events\AddDriver;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DriverActivity;

class AddDriverActivityListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddDriver  $event
     * @return void
     */
    public function handle(AddDriver $event)
    {
        $data = $event->data;

        $activity = new DriverActivity;
        $activity->user_id = $data['user']['id'];
        $activity->save();
    }
}
