<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessTypeIdToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'business_type_id')){
            Schema::table('stores', function (Blueprint $table) {
                $table->integer('business_type_id')->after('theme_id')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'business_type_id')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('business_type_id');
            });
        }
    }
}
