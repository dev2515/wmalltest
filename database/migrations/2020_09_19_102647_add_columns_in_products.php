<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasColumn('products', 'is_approved')){
            Schema::table('products', function (Blueprint $table) {
                $table->boolean('is_approved')->nullable()->after('delivery_option');
            });
        }

        if(!Schema::hasColumn('products', 'is_declined')){
            Schema::table('products', function (Blueprint $table) {
                $table->boolean('is_declined')->nullable()->after('is_approved');
            });
        }

        if(!Schema::hasColumn('products', 'process_by')){
            Schema::table('products', function (Blueprint $table) {
                $table->integer('process_by')->nullable()->after('is_declined');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('products', 'is_approved')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('is_approved');
            });
        }

        if(Schema::hasColumn('products', 'is_declined')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('is_declined');
            });
        }

        if(Schema::hasColumn('products', 'process_by')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('process_by');
            });
        }
    }
}
