<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('commands')){
            Schema::create('commands', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->boolean('is_multiple_called')->default(0);
                $table->boolean('is_called')->default(0);
                $table->string('executed_at');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commands');
    }
}
