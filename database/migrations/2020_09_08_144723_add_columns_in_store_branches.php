<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'street')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->text('street')->nullable()->after('name');
            });
        }

        if(!Schema::hasColumn('store_branches', 'state')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('state')->nullable()->after('street');
            });
        }

        if(!Schema::hasColumn('store_branches', 'city')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('city')->nullable()->after('state');
            });
        }

        if(!Schema::hasColumn('store_branches', 'other_info')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('other_info')->nullable()->after('city');
            });
        }

        if(!Schema::hasColumn('store_branches', 'contact')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('contact')->nullable()->after('other_info');
            });
        }

        if(!Schema::hasColumn('store_branches', 'email')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('email')->nullable()->after('contact');
            });
        }

        if(!Schema::hasColumn('store_branches', 'mobile')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('mobile')->nullable()->after('email');
            });
        }

        if(!Schema::hasColumn('store_branches', 'latitude')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('latitude')->nullable()->after('mobile');
            });
        }

        if(!Schema::hasColumn('store_branches', 'longitude')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('longitude')->nullable()->after('latitude');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_branches', 'street')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('street');
            });
        }

        if(Schema::hasColumn('store_branches', 'state')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('state');
            });
        }

        if(Schema::hasColumn('store_branches', 'city')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('city');
            });
        }

        if(Schema::hasColumn('store_branches', 'other_info')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('other_info');
            });
        }

        if(Schema::hasColumn('store_branches', 'contact')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('contact');
            });
        }

        if(Schema::hasColumn('store_branches', 'email')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('email');
            });
        }

        if(Schema::hasColumn('store_branches', 'mobile')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('mobile');
            });
        }

        if(Schema::hasColumn('store_branches', 'latitude')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('latitude');
            });
        }

        if(Schema::hasColumn('store_branches', 'longitude')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('longitude');
            });
        }
    }
}
