<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class ScanOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'customer_name' => $this->customer()->name,
            'full' => asset($this->profilePhoto()->full),
            'thumbnail' => asset($this->profilePhoto()->thumbnail),
            'order_number' => decode($this->order_number,'order-number'),
            'order_hash_id' => $this->hash_id,
            'total' => $this->sub_total,
            'payment_method' => $this->paymethod->name,
            'order_list' => $this->order_list(),

        ];
    }

    public function customer()
    {
        $u = User::select('id','name','profile_photo')->where('id', $this->customer_id)->first();
        return $u;
    }

    public function profilePhoto()
    {
        $full = public_path() . '/images/users/' . $this->customer()->hash_id . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . $this->customer()->hash_id . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . $this->customer()->hash_id . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . $this->customer()->hash_id . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }

    public function order_list()
    {
        $l = RestaurantOrderList::where('order_id', $this->id)->first();
        $f = RestaurantOrderFood::select('id','order_list_id','food_id','name','price','quantity','request')
        ->where('order_list_id', $l->id)->first();
        
        if($f) $f->makeHidden(['id','order_list_id','food_id']);
        return $f;
    }
}
