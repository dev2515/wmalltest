<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    protected $hidden = ['id', 'created_at', 'updated_at'];
    protected $appends = ['hashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
