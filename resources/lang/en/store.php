<?php

return [
    'not_found'         => 'Store not found',
    'identifier'        => 'Store hashid is required',
    'path_not_found'    => 'Store path not found',
    'request_already'   => 'You already have a request, Please wait to approve or decline your request before you can request another',
    'request_not_found' => 'This request not found',
    'advertise_not_found' => 'Advertisement infor not found',
    'main_branch'   => [
        'not_found'       => 'Main branch not found',
        'not_main_branch' => 'This store is not a main branch',
    ],
    'sub_branch'    => [
        'not_found'       => 'Sub branch information not found',
        'not_authorized'  => 'This branch is main only sub branch can process only',
        'not_equal'       => 'Sub branch parent id is not equal to'  
    ],
    'withdrawal'            => [
        'already_exists'  => 'You have already requested a withdrawal',
        'not_found'       => 'Withdrawal request information not found'
    ],
    'not_owner'     => 'You are not owner of this store.'
];