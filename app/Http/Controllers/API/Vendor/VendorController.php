<?php

namespace App\Http\Controllers\API\Vendor;

use Illuminate\Http\Request;
use App\Interfaces\VendorInterface;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{
    private $req;
    private $vendor;

    public function __construct(Request $req, VendorInterface $vendor)
    {
        $this->req = $req;
        $this->vendor = $vendor;
    }

    public function register()
    {
        return $this->vendor->register($this->req);
    }

    public function profile()
    {
        return $this->vendor->profile($this->req);
    }

    public function getSecurityQuestions()
    {
        return $this->vendor->getSecurityQuestions($this->req);
    }

    public function updateStore()
    {
        return $this->vendor->updateStore($this->req);
    }

    public function updateBank()
    {
        return $this->vendor->updateBank($this->req);
    }

    public function updateSecurity()
    {
        return $this->vendor->updateSecurity($this->req);
    }

    public function list()
    {
        return $this->vendor->list($this->req);
    }

    public function changeStatus()
    {
        return $this->vendor->changeStatus($this->req);
    }

    public function updateBanners()
    {
        return $this->vendor->updateBanners($this->req);
    }

    public function allStatusList()
    {
        return $this->vendor->allStatusList($this->req);
    }

    public function showStore()
    {
        return $this->vendor->showStore($this->req);
    }

    public function addStaff()
    {
        return $this->vendor->addStaff($this->req);
    }

    public function showStaff()
    {
        return $this->vendor->showStaff($this->req);
    }

    public function listStaff()
    {
        return $this->vendor->listStaff($this->req);
    }

    public function searchStaff()
    {
        return $this->vendor->searchStaff($this->req);
    }

    public function agentNewVendor()
    {
        return $this->vendor->agentNewVendor($this->req);
    }

    public function myStore()
    {
        return $this->vendor->myStore();
    }

    public function agentPrime()
    {
        return $this->vendor->agentPrime($this->req);
    }

    public function agentOracle()
    {
        return $this->vendor->agentOracle($this->req);
    }

    public function agentSailore()
    {
        return $this->vendor->agentSailore($this->req);
    }

    public function agentOverAll()
    {
        return $this->vendor->agentOverAll();
    }

    public function searchStore()
    {
        return $this->vendor->searchStore($this->req);
    }

    public function searchByStoreStatus()
    {
        return $this->vendor->searchByStoreStatus($this->req);
    }

    public function searchSailor()
    {
        return $this->vendor->searchSailor($this->req);
    }

    public function searchOracle()
    {
        return $this->vendor->searchOracle($this->req);
    }

    public function searchPrime()
    {
        return $this->vendor->searchPrime($this->req);
    }

    public function searchNew()
    {
        return $this->vendor->searchNew($this->req);
    }

    public function themeList()
    {
        return $this->vendor->themeList($this->req);
    }

    public function changeTheme()
    {
        return $this->vendor->changeTheme($this->req);
    }

    public function changeInformations()
    {
       return $this->vendor->changeInformations($this->req);
    }

    public function changeDescription()
    {
       return $this->vendor->changeDescription($this->req);
    }

    public function storeBranches()
    {
        return $this->vendor->storeBranches($this->req);
    }

    public function pendingStaffList()
    {
        return $this->vendor->pendingStaffList();
    }

    public function documents()
    {
        return $this->vendor->documents($this->req);
    }

    public function addDocumentImages()
    {
        return $this->vendor->addDocumentImages($this->req);
    }

    public function changeLocations()
    {
        return $this->vendor->changeLocations($this->req);
    }

    public function changeProfilePhoto()
    {
        return $this->vendor->changeProfilePhoto($this->req);
    }

    public function changeCoverPhoto()
    {
        return $this->vendor->changeCoverPhoto($this->req);
    }

    public function addBranches()
    {
        return $this->vendor->addBranches($this->req);
    }

    public function notificationList()
    {
        return $this->vendor->notificationList();
    }

    public function agentChangeProfilePhoto()
    {
        return $this->vendor->agentChangeProfilePhoto($this->req);
    }

    public function vendorStores()
    {
        return $this->vendor->vendorStores($this->req);
    }

    public function searchOwnStores()
    {
       return $this->vendor->searchOwnStores($this->req);
    }

    public function notificationCount()
    {
        return $this->vendor->notificationCount();
    }

    public function searchPendingBranch()
    {
        return $this->vendor->searchPendingBranch($this->req);
    }

    public function searchApproveBranch()
    {
        return $this->vendor->searchApproveBranch($this->req);
    }

    public function agentList()
    {
        return $this->vendor->agentList();
    }

    public function allVendorApproved()
    {
        return $this->vendor->allVendorApproved();
    }

    public function searchApprovedVendor()
    {
        return $this->vendor->searchApprovedVendor($this->req);
    }

    public function businessTypes()
    {
        return $this->vendor->businessTypes();
    }

    public function addFood()
    {
        return $this->vendor->addFood($this->req);
    }

    public function branchList()
    {
        return $this->vendor->branchList($this->req);
    }

    public function totalBranch()
    {
        return $this->vendor->totalBranch($this->req);
    }

    public function totalStaff()
    {
        return $this->vendor->totalStaff($this->req);
    }

    public function branchShow()
    {
        return $this->vendor->branchShow($this->req);
    }

    public function branchChangesStatus()
    {
        return $this->vendor->branchChangesStatus($this->req);
    }

    public function editBranch()
    {
        return $this->vendor->editBranch($this->req);
    }

    public function customerLikeOrUnlikeStore()
    {
        return $this->vendor->customerLikeOrUnlikeStore($this->req);
    }

    public function customerfolloweOrUnfollowStore()
    {
        return $this->vendor->customerfolloweOrUnfollowStore($this->req);
    }

    public function store_registration()
    {
        return $this->vendor->store_registration($this->req);
    }

    public function preregistration_list()
    {
        return $this->vendor->preregistration_list($this->req);
    }

    public function get_business_type()
    {
        return $this->vendor->get_business_type($this->req);
    }

    public function get_state()
    {
        return $this->vendor->get_state($this->req);
    }

    public function get_city()
    {
        return $this->vendor->get_city($this->req);
    }

    public function requestWithdrawal()
    {
        return $this->vendor->requestWithdrawal($this->req);
    }

    public function withDrawalList()
    {
        return $this->vendor->withDrawalList($this->req);
    }

    public function showWithDrawal()
    {
        return $this->vendor->showWithDrawal($this->req);
    }

    public function withdrawalChangeStatus()
    {
        return $this->vendor->withdrawalChangeStatus($this->req);

    }
}
