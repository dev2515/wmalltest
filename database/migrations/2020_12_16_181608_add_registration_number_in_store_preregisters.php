<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegistrationNumberInStorePreregisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_preregisters','registration_number')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->string('registration_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_preregisters','registration_number')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->dropColumn('registration_number');
            });
        }
    }
}
