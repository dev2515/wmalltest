<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $appends = ['hashid'];


    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
