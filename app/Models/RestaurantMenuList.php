<?php

namespace App\Models;

use App\Http\Resources\Restaurant\MenuListResource;
use Illuminate\Database\Eloquent\Model;

class RestaurantMenuList extends Model
{
    protected $appends = ['hash_id','foods'];

    protected $table = 'restaurant_menu_lists';
    protected $fillable = ['menu_id', 'food_id'];

    public function menu()
    {
        return $this->belongsTo(\App\Models\RestaurantMenu::class, 'menu_id');
    }

    public function foods()
    {
        return $this->belongsTo(\App\Models\RestaurantFood::class,'food_id');
    }
    
    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    
    public function getFoodsAttribute()
    {
        // $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $id = isset($this->attributes['food_id']) ? $this->attributes['food_id'] : 0;
        $food = RestaurantFood::select('id','name', 'meal', 'price', 'description', 'food_image','category_id')->where('id', $id)
        ->with('category')
        ->where('is_approved', 1)->where('status', 1)
        ->get();

        $food = MenuListResource::collection($food);

        return $food;
    }
}
