<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Day;
use App\Models\StoreSchedule;

class SyncDaysToStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data;

        $days = Day::get();

        foreach ($days as $day){
            $check = StoreSchedule::where('store_id', $store->id)->where('day_id', $day->id)->first();
            if(!$check){
                $add_schedule           = new StoreSchedule();
                $add_schedule->store_id = $store->id;
                $add_schedule->day_id   = $day->id;
                $add_schedule->save();
            }
        }
    }
}
