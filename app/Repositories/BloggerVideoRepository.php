<?php

namespace App\Repositories;

use App\Http\Resources\Blogger\BloggerVideoResource;
use App\Interfaces\BloggerVideoInterface;
use App\Models\BloggerVideo;
use App\User;

/*

Used Default userid - 1
Because Authentication Not working 

*/

class BloggerVideoRepository implements BloggerVideoInterface
{

    public function save_video($req)
    {
        $validator = validator()->make($req->all(),[
            'video_title' => 'required',
            'video_desc'  => 'required',
            'video_thumb' => 'required',
            'video'       => 'required'
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $thumb = $req->file('video_thumb');
        $thumbName = $this->generateRandomString(20).'.'. $thumb->getClientOriginalExtension();
        $thumb->move(public_path('images/blogger'),$thumbName);
        
        $vid = $req->file('video');
        $vidName = $this->generateRandomString(20).'.'. $vid->getClientOriginalExtension();
        $vid->move(public_path('videos/blogger'),$vidName);

        $video = new BloggerVideo();

        $video->video_title = $req->video_title;
        $video->video_description = $req->video_desc;
        $video->video_thumbnail = $thumbName;
        $video->video_url = $vidName;
      
        $user = User::find('1');

        $user->blogger_videos()->save($video);


        return res('successfully created video',null,201);


    }

    public function video_list($req)
    {
        $user = User::find('1');
        $videos = $user->blogger_videos;

        return res('success', BloggerVideoResource::collection($videos),200);
    }

    public function delete_video($req,$id)
    {
        $user = User::find('1');

        if($user->blogger_videos->find($id))
        {
            $user->blogger_videos->find($id)->delete();
            return res('successfully deleted video',null,200);
        }

        return res('Video Not Found',null,404);
    }

    private function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyz', ceil($length/strlen($x)) )),1,$length);
    }
}