<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RestaurantOrder extends Model
{
    protected $table = 'restaurant_orders';
    protected $fillable = 'user_id';
    protected $appends = ['hash_id','sub_total', 'status_info', 'order_type_info', 'user_name','paymethod','cart_hash_id','shipping_rate'];

    protected $casts = [
        'billing_address_coordinates' => 'array',
        'shipping_address_coordinates' => 'array',
    ];

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getCartHashIdAttribute()
    {
        $id = isset($this->attributes['cart_id']) ? $this->attributes['cart_id'] : 0;
        return encode($id, 'uuid');
    }

    public function getStatusInfoAttribute()
    {
        switch ($this->status) {
            case 9 :
            return $status = 'Order Taken';
            break;

            case 8 :
            return $status = 'Preparing';
            break;

            case 7 :
            return $status = 'Delivery guy arrived';
            break;

            case 6 :
            return $status = 'Completed';
            break;

            case 5 :
            return $status = 'Delivered';
            break;

            case 4 :
            return $status = 'Delivering';
            break;

            case 3 :
            return $status = 'Ready To Deliver';
            break;

            case 2 :
            return $status = 'Preparing';
            break;

            case 1 :
            return $status = 'Pending';
            break;

            case 0 :
            return $status = 'Cancelled';
            break;

            default :
            return $status = 'Pending';
            break;
        }
    }

    public function getOrderTypeInfoAttribute()
    {
        switch ($this->order_type) {
            case 3 :
            return $status = 'Take Away';
            break;

            case 2 :
            return $status = 'Deliver';
            break;

            case 1 :
            return $status = 'Dine In';
            break;
        }
    }

    public function getUserNameAttribute()
    {
        $id = isset($this->attributes['customer_id']) ? $this->attributes['customer_id'] : 0;
        $user = User::select('id','name')
        ->where('id', $id)
        ->first();

        return $user;
    }
    
    public function getPayMethodAttribute()
    {
        $id = isset($this->attributes['payment_method_id']) ? $this->attributes['payment_method_id'] : 0;
        $paymethod = PaymentMethod::where('id', $id)->first();
        return $paymethod;
    }

    public function lists()
    {
        return $this->hasMany(\App\Models\RestaurantOrderList::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'customer_id');
    }

    public function pay_method()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class, 'payment_method_id');
    }

    public function reservation()
    {
        return $this->belongsTo(TableReservation::class, 'reservation_id');
    }

    public function getSubTotalAttribute()
    {
        $st = isset($this->attributes['sub_total']) ? $this->attributes['sub_total'] : 0;
        return centToPrice($st);
    }

    public function getShippingRateAttribute()
    {
        return centToPrice($this->attributes['shipping_rate']);
    }
}
