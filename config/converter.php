<?php

return [
    'api_key' => env('CONVERTER_API_KEY', ''),
    'host' => env('CONVERTER_HOST', '')
];
