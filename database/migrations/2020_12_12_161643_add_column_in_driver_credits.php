<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInDriverCredits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_credits', 'is_notify')) {
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->boolean('is_notify')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_credits', 'is_notify')) {
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->dropColumn('is_notify');
            });
        }
    }
}
