<?php

namespace App\Models;

use App\Http\Resources\Restaurant\CartFoodResource;
use Illuminate\Database\Eloquent\Model;

class RestaurantCartItem extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'restaurant_cart_items';
    protected $appends = ['hash_id','food_info','foodvariation','totalamount'];

    public function food()
    {
        return $this->belongsTo(RestaurantFood::class, 'food_id');
    }

    public function cart()
    {
        return $this->belongsTo(\App\Models\RestaurantCartItem::class, 'cart_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Restaurant::class, 'restaurant_id');
    }

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getFoodInfoAttribute()
    {
        $food = RestaurantFood::where('id', $this->food_id)->first();
        $collect = new CartFoodResource($food);
        return $collect;
    }

    public function getFoodVariationAttribute()
    {
        $variation = RestaurantCartItemVariation::select('id','cart_item_id','variation_id','amount')
        ->where('cart_item_id', $this->id)->get();

        if($variation) $variation->makeHidden(['id','cart_item_id','variation_id']);

        return $variation;
    }

    public function getTotalAmountAttribute()
    {
        $var = 0;
        $id = isset($this->attributes['food_id']) ? $this->attributes['food_id'] : 0;
        $quantity = isset($this->attributes['quantity']) ? $this->attributes['quantity'] : 0;
        
        $food = RestaurantFood::select('id','price')->where('id', $id)->first();
        
        $variation = RestaurantCartItemVariation::where('cart_item_id', $this->id)->get();
        if($variation)
        {
            foreach ($variation as $item) {
                $var += $item->amount;
            }
        }

        $amount = $food->price + $var;
        $amount = $amount * $quantity;

        return centToPrice($amount);
    }
}
