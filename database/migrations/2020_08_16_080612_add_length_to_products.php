<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLengthToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('products', 'length')){
            Schema::table('products', function (Blueprint $table) {
                $table->double('length')->after('dimensions');
            });
        }

        if(!Schema::hasColumn('products', 'width')){
            Schema::table('products', function (Blueprint $table) {
                $table->double('width')->after('length');
            });
        }

        if(!Schema::hasColumn('products', 'height')){
            Schema::table('products', function (Blueprint $table) {
                $table->double('height')->after('width');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('products', 'length')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('length');
            });
        }

        if(Schema::hasColumn('products', 'width')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('width');
            });
        }

        if(Schema::hasColumn('products', 'height')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('height');
            });
        }
    }
}
