<?php

namespace App\Repositories;

use App\Http\Resources\MarketPlace\Cart\Store;
use App\Models\Cart;
use App\Models\Order;
use App\Models\CartItem;
use App\Models\Inventory;
use App\Models\OrderStore;
use App\Models\PaymentMethod;
use App\Models\OrderStoreItem;
use App\Models\OrderItemTracking;
use App\Interfaces\OrderInterface;
use App\Models\RestaurantOrder;
use App\Models\Store as ModelsStore;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderRepository implements OrderInterface
{
    public function list($req)
    {

    }

    public function show($req)
    {

    }

    public function paymentMethods($req)
    {
        $methods = PaymentMethod::where('status', 1)->get();
        $methods->makeHidden(['created_at', 'updated_at', 'status']);

        return res('Success', $methods);
    }

    public function place($req) {
        DB::beginTransaction();

        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res(__('user_.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'shipping_rate' => 'required',
            'payment_method_id' => 'required',
            'sub_total' => 'required',
            'selected_shippings' => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $carts = Cart::where('useer_id', auth()->user()->id)->where('on_checkout', 0)->where('is_ordered',0)->where('is_deleted', 0)->first();

        if(!$carts->has_booking) {
            $shipping_address_latitude = $req->shipping_address_latitude;
            $shipping_address_longitude = $req->shipping_address_longitude;
        }

        $submission = true;
        $on = generateOrderNumber();
        while($submission){
            $count = Order::where('order_number', $on)->count();
            if($count > 0) {
                $on = generateOrderNumber();
            } else {
                $submission = false;
            }
        }

        $order                                                              = new Order;
        $order->customer_id                                                 = auth()->user()->id;
        $order->order_number                                                = $on;
        $order->sub_total                                                   = 0;
        $order->shipping_rate                                               = priceToCent($req->shipping_rate);
        $order->payment_method_id                                           = $req->payment_method_id;
        $order->billing_address                                             = json_encode(auth()->user()->default_billing_address);
        $order->shipping_address                                            = $req->shipping_address;
        $order->billing_address_coordinates                                 = json_encode(auth()->user()->default_billing_address->latlng);
        $order->shipping_address_coordinates                                = 'null';
        $order->shipping_address_latitude                                   = $shipping_address_latitude;
        $order->shipping_address_longitude                                  = $shipping_address_longitude;
        $order->order_type                                                  = $carts->order_type;
        $order->note                                                        = $req->add_note;
        $order->reservation_id                                              = 0;
        $order->is_paid                                                     = 0;
        $order->save();

        $this->saveOrderStore($order->id, $req->selected_shippings);

        DB::commit();
        return res('Success', decode($on, 'order-number'), 201);
    }

    private function saveOrderStore(int $order_id, $selected_shippings)
    {
        $carts = Cart::where('user_id', auth()->user()->id)->where('on_checkout', 1)->where('is_deleted', 0)->get();
        foreach ($carts as $value) {
            $sub_total = 0;
            foreach ($value->items as $item) {
                if ($item->product->is_on_sale == 1) {
                    $sub_total += $item->product->sale_price * $item->quantity;
                } else {
                    $sub_total += $item->product->price * $item->quantity;
                }
            }
            
            $os = new OrderStore;
            $os->order_id = $order_id;
            $os->store_id = $value->store_id;
            $os->sub_total = $sub_total;
            $os->shipping_rate_info = isset($selected_shippings[$value->id]) ? json_encode($selected_shippings[$value->id]) : json_encode([]);
            $os->is_rated = 0;
            $os->save();

            $this->saveOrderStoreItem($value, $os->id);
        }
    }

    private function saveOrderStoreItem(Cart $cart, int $order_store_id)
    {
        foreach ($cart->items as $value) {
            $comm_settings = settings('commission');
            $osi = new OrderStoreItem;
            $osi->order_store_id = $order_store_id;
            $osi->product_id = $value->product_id;
            $osi->name = $value->product->name;
            $osi->price = $value->product->is_on_sale == 1 ? $value->product->sale_price : $value->product->price;
            $osi->quantity = $value->quantity;
            if ($comm_settings->mode == 'percentage') {
                $osi->commission = ($osi->price * $osi->quantity) * ($comm_settings->value / 100);
            } else {
                $osi->commission = $comm_settings->value;
            }
            $osi->is_rated = 0;
            $osi->save();

            $oit = new OrderItemTracking;
            $oit->order_store_item_id = $osi->id;
            $oit->datail = 'Order Created';
            $oit->save();

            $inv = new Inventory;
            $inv->product_id = $osi->product_id;
            $inv->actor_id = auth()->user()->id;
            $inv->quantity = $osi->quantity * -1;
            $inv->remarks = 'Ordered';
            $inv->save();

            $item = CartItem::find($value->id);
            $item->is_ordered = 1;
            $item->save();
        }
    }

    public function orderHistory($req)
    {
        $permissions = ['customer', 'administrator', 'admin staff', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed here', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'dateFrom' =>'required',
            'dateTo' =>'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
    
        $dateFrom = $req->dateFrom.'00:00:00';        
        $dateTo = $req->dateTo.'00:00:00';

        if(auth()->user()->type_info == 'customer')
        {
            $history = Order::with('user','order_store')->where('customer_id', auth()->user()->id)->whereBetween('created_at', [$dateFrom, $dateTo])
            ->where('status', $req->status)->get();
        }

        elseif(auth()->user()->type_info == 'administrator' || auth()->user()->type_info == 'admin staff')
        {
            $history = OrderStore::with('order','order_items','user','store')->whereBetween('created_at', [$dateFrom, $dateTo])->where('status', $req->status)->get();
        }

        elseif(auth()->user()->type_info == 'vendor' || auth()->user()->type_info == 'vendor staff')
        {
            $store_id = ModelsStore::where('id', auth()->user()->id)->where('status', 1)->pluck('id');
            $history = OrderStore::with('order','order_items','user','store')
            ->where('store_id', $store_id)->whereBetween('created_at', [$dateFrom,$dateTo])->get();
        }

        return res('success', $history);
    }

    public function trackOrder($req)
    {
        $permissions = ['customer', 'administrator', 'admin staff', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed here', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'order_number' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        if(auth()->user()->type_info == 'customer')
        {
            $order = Order::where('customer_id', auth()->user()->id)->orwhere('order_number', $req->order_number)->get();
        }

        elseif(auth()->user()->type_info == 'administrator' || auth()->user()->type_info == 'admin staff')
        {
            if($req->order_number) $order = Order::with('user','order_store')->where('order_number', $req->order_number)->get();
            elseif($req->status) $order = Order::with('user','order_store')->where('status', $req->status)->get();
        }

        elseif(auth()->user()->type_info == 'vendor' || auth()->user()->type_info == 'vendor staff')
        {
            if($req->order_number)
            {
                $order_id = Order::where('order_number', $req->order_number)->pluck('id');
                $order_store_id = OrderStore::where('order_id', $order_id)->pluck('order_id');
                $order = Order::with('user','order_store')
                ->where('order_id', $order_store_id)->get();
            }
            elseif($req->store_id)
            {
                $order = OrderStore::with('order','order_items','user','store')->where('store_id', $req->store_id)->get();
            }
        }

        return res('success', $order);
    }
}
