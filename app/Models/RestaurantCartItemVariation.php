<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantCartItemVariation extends Model
{
    protected $appends = ['hash_id','variation_type_selected'];

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getVariationTypeSelectedAttribute()
    {
        $id = isset($this->attributes['variation_id']) ? $this->attributes['variation_id'] : 0;
        $v = VariationTypeSelection::select('id','food_variation_id','name','price')->where('id', $id)->first();
        if($v) $v->makeHidden(['id','food_variation_id','food_variation']);

        return $v;
    }

}
