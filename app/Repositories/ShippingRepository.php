<?php

namespace App\Repositories;

use App\Models\Shipping;
use App\Models\ShippingCategory;
use App\Interfaces\ShippingInterface;

class ShippingRepository implements ShippingInterface
{
    public function getProviders($req)
    {
        $list = Shipping::where('status', 1)->get();
        $list->makeHidden(['status', 'created_at', 'updated_at']);

        return res('Success', $list);
    }

    public function categories($req)
    {
        $list = ShippingCategory::all();
        $list->makeHidden(['created_at', 'updated_at']);
        
        return res('Success', $list);
    }
}
