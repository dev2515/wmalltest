<?php

namespace App\Http\Resources\RestaurantManager;

use Illuminate\Http\Resources\Json\JsonResource;

class FoodVariation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'                    => $this->hashid,
            'name'                      => $this->name,
            'is_required'               => $this->is_required,
            'is_multiple'               => $this->is_multiple,
            'selection_max_number'      => $this->selection_max_number,
            'variation_type_selections' => new FoodVariationSelectionCollection($this->variationTypeSelections)
        ];
    }
}
