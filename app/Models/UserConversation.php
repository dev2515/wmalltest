<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserConversation extends Model
{
    protected $table  = 'user_conversations';
    protected $append = 'hashid';
    
    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
