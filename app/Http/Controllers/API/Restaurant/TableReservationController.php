<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\TableReservationInterface;

class TableReservationController extends Controller
{
    protected $req;
    protected $reserve;

    public function __construct(Request $req, TableReservationInterface $reserve)
    {
        $this->req = $req;
        $this->reserve = $reserve;
    }

    public function list()
    {
        return $this->reserve->list($this->req);
    }

    public function myreservationhistory()
    {
        return $this->reserve->myreservationhistory($this->req);
    }

    public function myqrcode()
    {
        return $this->reserve->myqrcode($this->req);
    }

    public function restaurantQRcode()
    {
        return $this->reserve->restaurantQRcode($this->req);
    }

    public function listbydatereserve()
    {
        return $this->reserve->listbydatereserve($this->req);
    }

    public function listbydate()
    {
        return $this->reserve->listbydate($this->req);
    }

    public function add()
    {
        return $this->reserve->add($this->req);
    }

    public function update()
    {
        return $this->reserve->update($this->req);
    }

    public function cancel()
    {
        return $this->reserve->cancel($this->req);
    }

    public function acceptreservation()
    {
        return $this->reserve->acceptreservation($this->req);
    }

    public function myreservation()
    {
        return $this->reserve->myreservation($this->req);
    }

    public function scanQRcode()
    {
        return $this->reserve->scanQRcode($this->req);
    }

    public function scannedQRcode()
    {
        return $this->reserve->scannedQRcode($this->req);
    }

    public function reservation_info()
    {
        return $this->reserve->reservation_info($this->req);
    }

    public function restaurant_qrcode_list()
    {
        return $this->reserve->restaurant_qrcode_list($this->req);
    }

    public function assignTable()
    {
        return $this->reserve->assignTable($this->req);
    }

    public function scan_cart_detail()
    {
        return $this->reserve->scan_cart_detail($this->req);
    }

    public function reservation_checkout_details()
    {
        return $this->reserve->reservation_checkout_details($this->req);
    }

    public function my_reservation_list()
    {
        return $this->reserve->my_reservation_list($this->req);
    }
}
