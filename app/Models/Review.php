<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Review extends Model
{
    protected $table = 'reviews';
    protected $fillable = ['user_id', 'category','type_id','rate', 'content', 'flag','status'];
    protected $appends = ['hash_id','user'];

    public function getHashidAttribute()
    {
       return encode($this->id, 'uuid');
    }

    public function getUserAttribute()
    {
        $u = DB::table('users')->select('name')->where('id', $this->user_id)->first();
        return $u;
    }
}
