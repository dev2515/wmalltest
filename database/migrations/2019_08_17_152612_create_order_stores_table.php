<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_stores')) {
            Schema::create('order_stores', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('order_id')->unsigned();
                $table->integer('store_id')->unsigned();
                $table->double('sub_total');
                $table->json('shipping_rate_info');
                $table->integer('is_rated');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_stores');
    }
}
