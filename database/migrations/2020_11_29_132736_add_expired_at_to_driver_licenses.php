<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class AddExpiredAtToDriverLicenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_licenses', 'expired_at')){
            Schema::table('driver_licenses', function (Blueprint $table) {
                $table->string('expired_at')->default(Carbon::now()->addYear())->after('license_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('driver_licenses', 'expired_at')){
            Schema::table('driver_licenses', function (Blueprint $table) {
                $table->dropColumn('expired_at');
            });
        }
    }
}
