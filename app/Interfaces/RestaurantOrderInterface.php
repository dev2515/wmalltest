<?php

namespace App\Interfaces;

interface RestaurantOrderInterface
{

    public function add($req);

    public function paymentMethods($req);

    public function restaurantOrderList($req);

    public function order_status_update($req);

    public function myOrderlist($req);

    public function listOrderByType($req);

    public function orderQRCode($req);

    public function show($req);

    public function totalOrder($req);

    public function orderFoodDetail($req);

    public function checkout($req);

    public function qrcodeList($req);

    public function updateOrder($req);

    public function add_order_in_reservation($req);

    public function complete_order($req);

    public function reservation_checkout($req);

    public function get_distance_store_to_customer($req);

    public function checkout_details($req);

    public function default_address($req);

    public function confirm_order_details($req);

    public function order_progress($req);

    public function get_pickup_orders($req);

    public function my_order_list($req);

    public function my_order_info($req);

    public function deliver_order_list($req);

    public function pickup_order_list($req);

    public function get_order_using_order_number($req);

    public function total_status_count($req);
}   

