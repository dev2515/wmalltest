<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function getLatlngAttribute($value)
    {
        return json_decode($value, true);
    }
}
