<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class StoreRating extends Model
{
    protected $table = 'store_ratings';
    protected $fillable = ['user_id','store_id', 'rating', 'content', 'flag', 'comment'];
    protected $appends = ['user'];

    public function getUserAttribute()
    {
        $id = isset($this->attributes['store_id']) ? $this->attributes['store_id'] : 0;
        $user = User::select('id','name')->where('id', $id)->first();
        if($user) $user->makeHidden(['id','status_info','mobile_prefix','type_info','store_info','default_billing_address','default_shipping_address'
        ,'driver_license_status','vehicle_registration_status']);

        return $user->name;
    }
}
