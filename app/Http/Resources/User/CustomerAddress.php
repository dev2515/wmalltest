<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerAddress extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'                => $this->hashid,
            'type'                  => $this->type->name,
            'is_current'            => $this->is_current,
            'latitude'              => $this->latitude,
            'longitude'             => $this->longitude,
            'nickname'              => $this->nickname,
            'area'                  => $this->area,
            'street'                => $this->street,
            'house'                 => $this->house,
            'additional_directions' => $this->additional_directions,
            'mobile'                => $this->mobile,
            'landline'              => $this->landline,
            'building'              => $this->building,
            'floor'                 => $this->floor,
            'apartment_number'      => $this->apartment_number,
            'office'                => $this->office
        ];
    }
}
