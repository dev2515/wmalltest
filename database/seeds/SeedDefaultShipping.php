<?php

use App\Models\Shipping;
use Illuminate\Database\Seeder;

class SeedDefaultShipping extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('data/qat-shippings.json');
        $json = file_get_contents($path);
        $json = json_decode($json);

        foreach ($json->shippings as $value) {
            Shipping::firstOrCreate(['name' => $value->name, 'max_rate' => 7, 'iso' => 'qat', 'website' => $value->website, 'number' => $value->number, 'location' => $value->location, 'email' => $value->email]);
        }
    }
}
