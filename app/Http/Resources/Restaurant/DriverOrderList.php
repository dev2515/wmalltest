<?php

namespace App\Http\Resources\Restaurant;

use App\Model\StoreBranch;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class DriverOrderList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'transaction_number'                                => $this->transaction_number,
            'store_name'                                        => $this->restaurant()->name,
            'store_phone'                                       => $this->restaurant()->phone,
            'store_address'                                     => $this->restaurant()->street.', '.$this->restaurant()->city,
            'store_latitude'                                    => $this->restaurant()->latitude,
            'store_longitude'                                   => $this->restaurant()->longitude,
            'customer_hash_id'                                  => $this->customer()->hash_id,
            'customer_name'                                     => $this->customer()->name,
            'customer_phone'                                    => $this->customer()->mobile,
            'customer_address'                                  => $this->order()->shipping_address,
            'customer_latitude'                                 => strlen($this->order()->shipping_address_latitude),
            'customer_longitude'                                => strlen($this->order()->shipping_address_longitude),
            'order_id'                                          => encode($this->order()->id,'uuid'),
            'order_number'                                      => decode($this->order()->order_number, 'order-number'),
            'order_status'                                      => $this->order()->status_info,
            'payment_method'                                    => $this->order()->paymethod->name,
            'order_created'                                     => Carbon::parse($this->order()->created_at)->format('g:i A'),
            'total_amount'                                      => $this->order()->sub_total,
            'qrcode'                                            => $this->hash,
            'driver_name'                                       => $this->driver()->name,
            'driver_mobile'                                     => $this->driver()->mobile,
            'order_items'                                       => $this->order_list(),
        ];
    }

    public function order()
    {
        $c = DB::table('restaurant_carts')
            ->join('restaurant_orders', 'restaurant_carts.id', '=', 'restaurant_orders.cart_id')
            ->select('restaurant_orders.id','restaurant_orders.customer_id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_address','restaurant_orders.shipping_address_latitude','restaurant_orders.shipping_address_longitude','restaurant_orders.payment_method_id','restaurant_orders.created_at','restaurant_orders.status')
            ->where('restaurant_carts.transaction_number', $this->transaction_number)
            ->first();

        return $c;
    }

    public function restaurant()
    {
        $c = DB::table('restaurant_order_lists')->join('stores', 'restaurant_order_lists.restaurant_id', '=', 'stores.id')
            ->select('name','latitude', 'longitude','phone','street','city','state','other_address_info')
            ->where('restaurant_order_lists.order_id', $this->order()->id)
            ->first();
        
        return $c;
    }

    public function order_list()
    {
        $i = DB::table('restaurant_order_lists')->join('restaurant_order_food', 'restaurant_order_lists.id', '=', 'restaurant_order_food.order_list_id')
            ->select('restaurant_order_food.id','restaurant_order_food.name','restaurant_order_food.price','restaurant_order_food.quantity','restaurant_order_food.request')
            ->where('order_id', $this->order()->id)
            ->get();
        
        return $i;
    }

    public function customer()
    {
        $u = User::select('id','name','mobile')->where('id', $this->order()->customer_id)->first();
        if($u) $u->makeHidden(['id','status_info','mobile_prefix','type_info','store_info','default_billing_address','default_shipping_address','driver_license_status','vehicle_registration_status']);
        return $u;
    }

    public function driver()
    {
        $d = DB::table('users')->select('id','name','mobile')->where('id', $this->assigned_driver_id)->first();
        return $d;
    }
}
