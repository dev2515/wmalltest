<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('shipping_rates')) {
            Schema::create('shipping_rates', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('product_id')->unsigned();
                $table->integer('shipping_id')->unsigned();
                $table->integer('shipping_category_id')->unsigned();
                $table->integer('state_id')->unsigned();
                $table->integer('min_days')->default(0);
                $table->integer('max_days')->default(0);
                $table->double('rate');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_rates');
    }
}
