<?php

namespace App\Interfaces;

interface DriverInterface
{
    public function add($req);

    public function edit($req);

    public function list($req);

    public function addDocument($req);

    public function listDocument($req);

    public function adminVehicleApprove($req);

    public function vehicleList($req);

    public function driverAccountApprove($req);

    public function show($req);

    public function vehicleShow($req);

    public function myPreferredDelivery($req);

    public function account($req);

    public function updatePreferredDelivery($req);

    public function deliveryList($req);

    public function myCurrentLocation($req);

    public function nearBy($req);

    public function transactionHistory($req);

    public function driver_order_list($req);

    public function driver_accepted_list($req);

    public function driver_accept_order($req);

    public function driver_update_location($req);

    public function customer_order_recieve($req);
    
    public function driver_confirm_order($req);

    public function driver_scan_order_restaurant($req);

    public function driver_order_history($req);

    public function driver_net_income($req);

    public function driver_notification_settings($req);

    public function driver_current_status($req);

    public function driver_credits_today($req);

    public function delete_unnecessary($req);

    public function drivers_collect_income($req);

    public function driver_notify_collection($req);

    public function get_driver_notify($req);

    public function order_reciept($req);
}
