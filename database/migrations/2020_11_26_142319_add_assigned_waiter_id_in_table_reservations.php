<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssignedWaiterIdInTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('q_r_pointers', 'assigned_waiter_id')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->unsignedBigInteger('assigned_waiter_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('q_r_pointers', 'assigned_waiter_id')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->dropColumn('assigned_waiter_id');
            });
        }
    }
}
