<?php

use Illuminate\Database\Seeder;
use App\Models\PackageType;

class SeedPackageTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = PackageType::count();
        if($count == 0){
            PackageType::firstOrCreate(['name' => 'food']);
        }
    }
}
