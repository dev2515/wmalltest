<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class CartFoodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'hash_id' =>                    $this->hash_id,
            'name' =>                       $this->name,
            'type' =>                       $this->type,
            'price' =>                      centToPrice($this->price),
            'description' =>                $this->description,
            'full' =>                       asset($this->photo()->full),
            'thumbnail' =>                  asset($this->photo()->thumbnail),
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
