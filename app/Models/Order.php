<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
        'billing_address_coordinates' => 'array',
        'shipping_address_coordinates' => 'array',
    ];

    public function order_store()
    {
        return $this->hasOne(\App\Models\OrderStore::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}
