<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryImageInFoodCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('food_categories', 'category_image')){
            Schema::table('food_categories', function (Blueprint $table) {
                $table->longText('category_image')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('food_categories', 'category_image')){
            Schema::table('food_categories', function (Blueprint $table) {
                $table->dropColumn('category_image');
            });
        }
    }
}
