<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use stdClass;
use App\Models\StoreSchedule;
use App\Models\StoreTiming;
class Advertisement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request):array
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hashid'                    => $this->hashid,
            'name'                      => $this->name,
            'email'                     => $this->email,
            'information'               => $this->store_information,
            'schedules'                 => $this->is_opened,
            'ratings'                   => $this->ratings,
            'number_of_followers'       => $this->followers,
            'contact'                   => $this->phone,
            'city'                      => $this->city,
            'status'                    => $this->when(auth()->user() ? auth()->user()->type_info != 'customer' : false, $this->status_info),
            'timestamp'                 => $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'image_full'                => $this->image()->full,
            'image_thumbnail'           => $this->image()->thumbnail,
            'profile_photo_full'        => $this->photo()->full,
            'profile_photo_thumb'       => $this->photo()->thumbnail,
            'cover_photo_full'          => $this->cover()->full,
            'cover_photo_thumb'         => $this->cover()->thumbnail,
        ];
    }

    public function image():object
    {
        $full = public_path().'/images/store-advertisements/' . $this->hashid . '/full.png';
        if(file_exists($full)){
            $full = '/images/store-advertisements/' . $this->hashid . '/full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/store-advertisements/' . $this->hashid . '/thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/store-advertisements/' . $this->hashid . '/thumbnail.png';
        } else { $thumbnail = null; }

        $data               = new stdClass;
        $data->full         = asset($full);
        $data->thumbnail    = asset($thumbnail);

        return $data;
    }

    public function photo():object
    {      
        $full = public_path() . '/images/stores/' . $this->hashid . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hashid . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->hashid . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->hashid . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function cover():object
    {      
        $full = public_path() . '/images/stores/' . $this->hash_id . '/cover-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hash_id . '/cover-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->hash_id . '/cover-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->hash_id . '/cover-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
