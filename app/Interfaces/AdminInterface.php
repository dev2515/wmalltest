<?php 

namespace App\Interfaces;


interface AdminInterface
{
    public function changeStatus($req);

    public function storeChangeFields($req);

    public function totalRequest();

    public function status($req);

    public function showRequest($req);

    public function overAll();

    public function marketplaceList($req);

    public function hotelList($req);

    public function restaurantList($req);

    public function activitiesList($req);

    public function approvedAllAddFood();

    public function approvedAllEditFood();

    public function dashboardRestaurant($req);

    public function modifyActiveStatus($req);
}