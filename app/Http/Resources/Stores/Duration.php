<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;

class Duration extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hash_id'           => $this->hashid,
            'duration_name'     => $this->duration_name,
            'duration_type'     => $this->duration_type,
            'duration_value'    => $this->duration_value,
            'status'            => $this->status,
        ];
    }
}
