<?php

return [
    'not_driver'            => 'This user is not a driver',
    'vehicle_not_found'     => 'Vehicle information not found',
    'vehicle_angles'        => 'Vehicle angles must be 4',
    'license_not_found'     => 'License information not found'
];