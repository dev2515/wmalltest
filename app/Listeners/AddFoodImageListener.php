<?php

namespace App\Listeners;

use App\Events\AddFoodEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
class AddFoodImageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddFoodEvent  $event
     * @return void
     */
    public function handle(AddFoodEvent $event)
    {
        $food = $event->data->model;

        if($food->food_image != null){

            $parent_path = public_path() . '/images/food';
            if(!file_exists($parent_path)){
                File::makeDirectory($parent_path, $mode = 0777, true, true);
            }
            
            $children_path = public_path() . '/images/food/';

            $image  = Image::make($food->food_image);
            $image->save($children_path . encode($food->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($children_path . encode($food->id, 'uuid') . '-thumbnail.png');
        }
    }
}
