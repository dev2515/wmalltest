<?php

namespace App\Interfaces;

interface VendorInterface
{
    public function register($req);

    public function profile($req);

    public function getSecurityQuestions($req);

    public function updateStore($req);

    public function updateBank($req);

    public function updateSecurity($req);

    public function getTypes($req);

    public function list($req);
    
    public function changeStatus($req);

    public function updateBanners($req);

    public function allStatusList($req);

    public function showStore($req);

    public function addStaff($req);

    public function showStaff($req);

    public function listStaff($req);

    public function searchStaff($req);

    public function agentNewVendor($req);

    public function agentPrime($req);

    public function agentOracle($req);

    public function agentSailore($req);

    public function agentOverAll();

    public function myStore();

    public function searchStore($req);

    public function searchByStoreStatus($req);

    public function searchSailor($req);

    public function searchOracle($req);

    public function searchPrime($req);

    public function searchNew($req);

    public function themeList($req);

    public function changeTheme($req);

    public function changeInformations($req);

    public function changeDescription($req);

    public function storeBranches($req);

    public function pendingStaffList();

    public function documents($req);

    public function addDocumentImages($req);

    public function changeLocations($req);

    public function changeProfilePhoto($req);

    public function changeCoverPhoto($req);

    public function addBranches($req);

    public function notificationList();

    public function agentChangeProfilePhoto($req);

    public function vendorStores($req);

    public function searchOwnStores($req);

    public function notificationCount();

    public function searchPendingBranch($req);

    public function searchApproveBranch($req);

    public function agentList();

    public function allVendorApproved();

    public function searchApprovedVendor($req);

    public function businessTypes();

    public function addFood($req);

    public function branchList($req);

    public function totalBranch($req);
    
    public function storeListFilter($req);

    public function totalStaff($req);

    public function branchShow($req);

    public function branchChangesStatus($req);

    public function editBranch($req);

    public function customerLikeOrUnlikeStore($req);

    public function customerfolloweOrUnfollowStore($req);

    public function showStoreOpenApi($req);

    public function store_registration($req);

    public function preregistration_list($req);

    public function get_business_type($req);

    public function get_state($req);

    public function get_city($req);

    public function requestWithdrawal($req);

    public function withDrawalList($req);

    public function showWithDrawal($req);

    public function withdrawalChangeStatus($req);
} 
