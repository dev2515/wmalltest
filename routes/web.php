<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Blogger Product Api Routes
Route::get('/product-detail','ProductDetailsController@addProduct');
Route::post('/product-detail','ProductDetailsController@storeImage');

//Blogger Video Api Routes
Route::group(['namespace' => 'API\Blogger'],function(){
    Route::get('/blogger-video','BloggerVideoController@getVideos');
    Route::post('/blogger-video','BloggerVideoController@storeVideo');
    Route::post('/blogger-video/{id}/delete','BloggerVideoController@deleteVideo');
});


Route::get('/', function () {
    return res('WEB Server V1 is UP');
});
Route::get('/payment', function () {
    return view('payment');
});

Route::get('/qpay', function () {
    return view('qpay');
});

Route::group(['namespace' => 'WEB'], function () {
    Route::group(['namespace' => 'Image'], function () {
        Route::get('avatar/{hashid}/{width?}/{height?}', 'ImageController@avatar');
        Route::get('product/{hashid}/{width?}/{height?}', 'ImageController@product');
        Route::get('banner/{name}/{width?}/{height?}', 'ImageController@banner');
        Route::get('store/{hashid}/{width?}/{height?}', 'ImageController@store');
        Route::get('product-base-64s/{hashid}/{index}/{width?}/{height?}', 'ImageController@productBase64s');
        Route::get('download/{hashid}/{documentType}/{fileName}', 'ImageController@downloadPdf');
    });
});

