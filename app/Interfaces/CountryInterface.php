<?php

namespace App\Interfaces;

interface CountryInterface
{
    public function default($req);

    public function list($req);

    public function states($req);
}
