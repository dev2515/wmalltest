<?php

namespace App\Http\Resources\Images;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
class Signature extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'full' => asset($this->local('/signature-images/' . $this->id . '-signature')->full),
            'thumbnail' => asset($this->local('/signature-images/' . $this->id . '-signature')->thumbnail),
        ];
    }

    public function local($type)
    {
        $full = public_path().'/images/stores/' . encode($this->store_id, 'uuid') . $type .'-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . encode($this->store_id, 'uuid') . $type .'-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . encode($this->store_id, 'uuid') . $type .'-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . encode($this->store_id, 'uuid') . $type .'-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }
}
