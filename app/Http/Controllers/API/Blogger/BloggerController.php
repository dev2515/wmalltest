<?php

namespace App\Http\Controllers\API\Blogger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\BloggerInterface;

class BloggerController extends Controller
{
    private $req;
    private $blogger;

    public function __construct(Request $req, BloggerInterface $blogger)
    {
        $this->req = $req;
        $this->blogger = $blogger;
    }

    public function blogger_list()
    {
        return $this->blogger->blogger_list($this->req);
    }

    public function category_list()
    {
        return $this->blogger->category_list($this->req);
    }

    public function product_list()
    {
        return $this->blogger->product_list($this->req);
    }

    public function add_product()
    {
        return $this->blogger->add_product($this->req);
    }

    public function add_boutique_shop(){
        return $this->blogger->add_boutique_shop($this->req);
    }

    public function add_product_category_image()
    {
        return $this->blogger->add_product_category_image($this->req);
    }
}
