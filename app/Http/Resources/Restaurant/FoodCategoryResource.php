<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use stdClass;

class FoodCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'hash_id' =>                    encode($this->id,'uuid'),
            'category' =>                   $this->name,
            'foods' =>                      $this->food(),
        ];
    }

    public function food()
    {
        $foods = DB::table('restaurant_foods')->select('restaurant_foods.id','restaurant_foods.name','restaurant_foods.price')
        ->join('food_type_pivot', 'restaurant_foods.id', '=', 'food_type_pivot.food_id')
        ->where('food_type_pivot.store_id', $this->store_id)
        ->where('food_type_pivot.food_type_id', $this->id)->get();

       $foods = MenuListResource::collection($foods);

        return $foods;
    }
}
