<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $appends = ['city_info','hash_id','state_hash_id'];

    public function getCityInfoAttribute()
    {
        $id = $this->attributes['id'];
        $cities = $this->where('state_id', $id)->where('iso', session('iso'))->where('status', 1)->orderBy('name', 'ASC')->get();
        
        if($cities) $cities->makeHidden(['id','state_id','iso','status','created_at','updated_at','city_info','state_hash_id']);

        return $cities;
    }

    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function getStateHashidAttribute()
    {
        $id = isset($this->attributes['state_id']) ? $this->attributes['state_id'] : 0;
        return encode($id, 'uuid');
    }
}
