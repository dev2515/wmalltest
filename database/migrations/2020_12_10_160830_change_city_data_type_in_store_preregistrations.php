<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCityDataTypeInStorePreregistrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('store_preregistrations', 'store_preregistrations')){
            Schema::table('store_preregistrations', function (Blueprint $table) {
                $table->unsignedBigInteger('city')->change();
            });
        }

        if(Schema::hasColumn('store_preregistrations', 'state')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->unsignedBigInteger('state')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_preregistrations', 'store_preregistrations')){
            Schema::table('store_preregistrations', function (Blueprint $table) {
                $table->string('city')->change();
            });
        }

        if(Schema::hasColumn('store_preregistrations', 'state')){
            Schema::table('store_preregistrations', function (Blueprint $table) {
                $table->string('state')->change();
            });
        }
    }
}
