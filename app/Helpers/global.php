<?php

use Carbon\Carbon;

function res($msg = '', $data = null, $code = 200)
{
    return response()->json([
        'code' => $code,
        'msg' => $msg,
        'data' => $data
    ]);
}

function getIso($iso_code = null)
{
    if ($iso_code === null) {
        $iso_code = geoip()->getLocation()->iso_code;
    }
    if (strlen($iso_code) > 2) {
        $iso_code = iso2ToIso3($iso_code, true);
    }
    return strtolower(country($iso_code)->getIsoAlpha3());
}

function getMobilePrefix($iso)
{
    if (strlen($iso) > 2) {
        $iso = iso2ToIso3($iso, true);
    }
    return '+' . country($iso)->getCallingCodes()[0];
}

function iso2ToIso3($iso = null, $reverse = false)
{
    if ($iso === null) {
        return null;
    }
    if ($reverse) {
        $data = (new \League\ISO3166\ISO3166)->alpha3($iso);
        return $data['alpha2'];
    } else {
        $data = (new \League\ISO3166\ISO3166)->alpha2($iso);
        return $data['alpha3'];
    }
}

function generateEmailToken()
{
    return encode(now()->timestamp, 'email-token');
}

function generateMobileToken()
{
    return rand(100000, 999999);
}

function generateTransactionNumber()
{
    return rand(1000000000, 9999999999);
}

function stringToArray($str)
{
    $arr = preg_replace("/[^0-9,]/", "", $str);
    $arr = explode(',', $arr);
    return $arr;
}

function castsToArray($str)
{
    $str = str_replace('[\'', '', $str);
    $str = str_replace('["', '', $str);
    $str = str_replace('\', \'', ',', $str);
    $str = str_replace('", "', ',', $str);
    $str = str_replace('\',\'', ',', $str);
    $str = str_replace('","', ',', $str);
    $str = str_replace('\']', '', $str);
    $str = str_replace('"]', '', $str);
    $arr = explode(',', $str);
    return $arr;
}

function encode($id, $connection = 'main')
{
    $config = config('hashids.connections.' . $connection);
    return (new \Hashids\Hashids($config['salt'], $config['length'], $config['alphabet']))->encode($id);
}

function decode($hash, $connection = 'main')
{
    $config = config('hashids.connections.' . $connection);
    $decoded = (new \Hashids\Hashids($config['salt'], $config['length'], $config['alphabet']))->decode($hash);
    if (count($decoded) > 0) {
        return $decoded[0];
    }
    return null;
}

function saveClient()
{
    $user_id = 0;
    if (auth()->id()) {
        $user_id = auth()->id();
    }
    $location = geoip()->getLocation(geoip()->getClientIP());
    $location = (array)$location['attributes'];
    $client = new \App\Models\Client;
    $client->user_id = $user_id;
    $client->selected_current_iso = session('iso');
    $client->app_origin = session('origin');
    $client->uuid = session('uuid');
    $client->manufacturer = session('manufacturer');
    $client->version = session('version');
    $client->model = session('model');
    $client->serial = session('serial');
    $client->platform = session('platform');
    $client->location = json_encode($location);
    $client->uri = request()->getRequestUri();
    $client->save();
}

function deliveryOptions(int $product_id, \App\Models\State $state = null)
{
    $delivery_options = [];
    $scs = \App\Models\ShippingCategory::all();
    foreach ($scs as $sc) {
        $ps = \App\Models\ProductShipping::where('product_id', $product_id)->where('shipping_category_id', $sc->id)->where('status', 1)->first();
        if ($ps && $state) {
            $shipping_rate = \App\Models\ShippingRate::where('shipping_id', $ps->shipping_id)->where('state_from_id', $ps->product->store->getStateInfoAttribute()->id)->where('state_to_id', $state->id)->first();
            if ($shipping_rate) {
                array_push($delivery_options, [
                    'shipping_rate_id' => $shipping_rate->id,
                    'shipping_id' => $shipping_rate->shipping_id,
                    'sc_name' => $sc->name,
                    'min_days' => $shipping_rate->min_days,
                    'max_days' => $shipping_rate->max_days,
                    'rate' => $shipping_rate->rate
                ]);
            }
        }
    }

    return $delivery_options;
}

function settings(string $key, string $scope = 'global')
{
    $setting = \App\Models\Settings::where('scope', $scope)->where('status', 1)->first();
    if (!$setting) {
        return null;
    }
    $setting = json_decode($setting->data);
    return $setting->$key;
}

function conversions(...$pair)
{
    $contents = file_get_contents(config('converter.host') . '/api/v7/convert?q=' . implode(',', $pair) . '&compact=ultra&apiKey=' . config('converter.api_key'));
    $json = json_decode($contents);
    return $json;
}

function recordOnProduct(int $product_id, String $action, int $status)
{
    $pa = new \App\Models\ProductActivity;
    $pa->user_id = auth()->id();
    $pa->product_id = $product_id;
    $pa->action = $action;
    $pa->status = $status;
    $pa->save();
}

function activityLog($activity)
{
    $log = [];
    $log['activity'] = $activity;
    $log['url'] = \Request::fullUrl();
    $log['method'] = \Request::method();
    $log['ip'] = \Request::ip();
    $log['user_id'] = auth()->id();
    $log['status'] = 1;
    
    \App\Models\ManagerActivityLog::create($log);
}

function braintree()
{
    $gateway = new \Braintree\Gateway([
        'environment'       => config('services.braintree.environment'),
        'merchantId'        => config('services.braintree.merchantId'),
        'publicKey'         => config('services.braintree.publicKey'),
        'privateKey'        => config('services.braintree.privateKey'),
    ]);

    return $gateway;
}

function payPalApiContext()
{
    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            config('services.paypal.clientId'),     // ClientID
            config('services.paypal.clientSecret')      // ClientSecret
        )
    );

    return $apiContext;
}

function getHash($req)
{
    $hash = $req->hash;
    if (!$hash) {
        $hash = encode(Carbon::now()->timestamp, 'transaction_hash');
    }
    return $hash;
}

function getExpirationDate($model):string
{
    switch ($model->duration_type){
        case 'day' :
            $now = Carbon::now()->addDays($model->duration_value);
            return $now;
        break;

        case 'week' :
            $now = Carbon::now()->addWeeks($model->duration_value);
            return $now;
        break;

        case 'month' :
            $now = Carbon::now()->addMonths($model->duration_value);
            return $now;
        break;

        default :
            return 'Invalid date format';
        break;
    }
}

function priceToCent($price)
{
    return $price * 100;
}

function centToPrice($cents)
{
    $total = $cents / 100;
    return number_format((float)$total, 2, '.', '');
}

function twoDecimalPlaces($total)
{
    return number_format((float)$total,2,'.','');
}

function generateOrderNumber()
{
    return encode(now()->timestamp, 'order-number');
}

function generateRegistrationNumber()
{
    return encode(Carbon::now()->timestamp, 'registration-number');
}