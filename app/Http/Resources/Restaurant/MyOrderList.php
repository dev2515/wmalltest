<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantOrderList;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class MyOrderList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'order_hash_id'                 => $this->hash_id,
            'full'                          => asset($this->photo()->full),
            'thumbnail'                     => asset($this->photo()->thumbnail),
            'restaurant_name'               => $this->store()->name,
            'shipping_address'              => $this->shipping_address,
            'shipping_latitude'             => strval($this->shipping_address_latitude),
            'shipping_longitude'            => strval($this->shipping_address_longitude),
            'store_address'                 => $this->store()->street.', '.$this->store()->city.', '.$this->store()->state,
            'store_latitude'                => $this->store()->latitude,
            'store_longitude'               => $this->store()->longitude,
            'order_date'                    => Carbon::parse($this->created_at)->format('Y-m-d'),
            'order_time'                    => Carbon::parse($this->created_at)->format('g:i A'),
            'order_status'                  => $this->status_info,
        ];
    }

    public function store()
    {
        $r = RestaurantOrderList::where('order_id', $this->id)->first();
        $s = Store::where('id', $r->restaurant_id)->first();

        return $s;
    }

    public function photo()
    {      
        $full = public_path() . '/images/stores/' . $this->store()->hash_id . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->store()->hash_id . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->store()->hash_id . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->store()->hash_id . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
