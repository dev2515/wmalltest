<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariationTypeSelection extends Model
{
    protected $appends = ['hashid', 'food_variation'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function getFoodVariationAttribute()
    {
        $fv = FoodVariationSelection::where('id', $this->food_variation_id)->first();
        return $fv;
    }
}
