<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerAddressList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'    => $this->hashid,
            'nickname'      => $this->nickname,
            'is_current'    => $this->is_current,
            'type'          => $this->type->name,
        ];
    }
}
