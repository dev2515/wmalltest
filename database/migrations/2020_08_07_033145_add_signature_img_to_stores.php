<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSignatureImgToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'signature_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->longText('signature_img')->nullable()->after('cover_photo_img');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'signature_img')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_img');
            });
        }
    }
}
