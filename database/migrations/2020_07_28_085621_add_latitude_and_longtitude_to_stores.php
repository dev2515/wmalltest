<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatitudeAndLongtitudeToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'latitude')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('latitude')->nullable()->after('zip');
            });
        }

        if(!Schema::hasColumn('stores', 'longtitude')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('longtitude')->nullable()->after('latitude');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'latitude')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('latitude');
            });
        }

        if(Schema::hasColumn('stores', 'longtitude')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('longtitude');
            });
        }
    }
}
