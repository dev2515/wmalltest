<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnAndChangeDatatype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_food_category_pivot', 'food_category_id')){
            Schema::table('restaurant_food_category_pivot', function(Blueprint $table) {
                $table->renameColumn('food_category_id', 'food_category_name');
            });
        }

        if(Schema::hasColumn('restaurant_food_category_pivot', 'food_category_name')){
            Schema::table('restaurant_food_category_pivot', function (Blueprint $table) {
                $table->string('food_category_name')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_food_category_pivot', 'food_category_id')){
            Schema::table('restaurant_food_category_pivot', function(Blueprint $table) {
                $table->dropColumn('food_category_name');
            });
        }
    }
}
