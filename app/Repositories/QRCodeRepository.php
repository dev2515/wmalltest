<?php

namespace App\Repositories;

use App\Http\Resources\MarketPlace\Cart\Store;
use App\Http\Resources\Product\Product;
use App\Http\Resources\User\UserData;
use App\Interfaces\QRCodeInterface;
use App\Models\BusinessType;
use App\Models\Cart;
use App\Models\QRPointer;
use App\Models\QRTransactionType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class QRCodeRepository implements QRCodeInterface
{
    public function scan($req)
    {
        $validator = Validator::make($req->all(), [
            'hash' => 'required',
        ]);

        if($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        if (strlen($req->hash) == 128) {
            $q = QRPointer::where('hash', $req->hash)->where('status', 1)->first();
            if (!$q) return res('Invalid QR Code', null, 400);
        } elseif (strlen($req->hash) == 96) {
            $q = QRPointer::where('receiver_hash', $req->hash)->where('status', 1)->first();
            if (!$q) return res('Invalid QR Code', null, 400);

            // TODO: change the transaction_status to the right status number
            if ($q->transaction_status == 100) {
                // TODO: Check the Resource File (Store) if it is correct
                $data = new Store($q->transaction);
                return res('Success', $data, 200);
            } else {
                return res('Parcel status is not yet to receive', null, 400);
            }
        } else {
            return res('Invalid QRCode', null, 400);
        }

        if ($q->transactionType->name == 'product') {
            if (!$q->product) return res('Product not found', null, 400);

            $data = new Product($q->product);

            return res('Success', $data, 200);
        } elseif ($q->transactionType->name == 'customer') {
            if (!$q->user) return res('User not found', null, 400);

            $data = new UserData($q->user);

            return res('Success', $data, 200);
        } elseif ($q->transaction->name == 'marketplace' || $q->transaction->name == 'restaurant' || $q->transaction->name == 'hotel' || $q->transaction->name == 'activity') {
            if (!$q->business_link) return res('Link not found', null, 400);

            return res('Success', $q->business_link);
            
        } elseif ($q->transaction->name == 'parcel') {
            if (!$q->transaction) return res('Transction not found', null, 400);

            $data = new Store($q->transaction);

            // TODO: Update this statuses
            $status = 'pending';
            switch ($q->transaction_status) {
                case 0:
                    $status = 'pending';
                case 1:
                    $status = 'processing';
                default:
                    $status = 'pending';
            }

            return res($status, $data, 200);
        }
    }

    public function bind($req)
    {
        $validator = Validator::make($req->all(), [
            'qr_transaction_type' => 'required',
        ]);

        if($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $qrtt = QRTransactionType::where('name', $req['qr_transaction_type'])->first();
        if (!$qrtt) return res('Invalid QR Transaction Type', null, 400);

        switch ($req['qr_transaction_type']) {
            case 'product':
                $v = Validator::make($req->all(), [
                    'product_hashid' => 'required',
                ]);
                if($v->fails()) return res('Failed', $v->errors(), 412);

                $pid = decode($req->product_hashid, 'uuid');
                if (!$pid) return res('Invalid Product hashid', null, 400);
                
                $qrp = QRPointer::where('product_id', $pid)->first();
                if ($qrp) return res('Product is already bound', null, 400);
                
                $qrp = new QRPointer();
                $qrp->hash = $this->getHash($req);
                $qrp->transaction_type_id = $qrtt->id;
                $qrp->product_id = $pid;
                $qrp->save();

                return res('Success');
            case 'customer':
                $v = Validator::make($req->all(), [
                    'customer_hashid' => 'required',
                ]);
                if($v->fails()) return res('Failed', $v->errors(), 412);

                $cid = decode($req->customer_hashid, 'uuid');
                if (!$cid) return res('Invalid Customer hashid', null, 400);

                $qrp = QRPointer::where('user_id', $cid)->first();
                if ($qrp) return res('Customer is already bound', null, 400);

                $qrp = new QRPointer();
                $qrp->hash = $this->getHash($req);
                $qrp->transaction_type_id = $qrtt->id;
                $qrp->user_id = $cid;
                $qrp->save();

                return res('Success');
            case 'marketplace':
            case 'restaurant':
                $v = Validator::make($req->all(),[
                    'restaurant_hashid' => 'required',
                ]);
                if($v->fails()) return res('Failed', $v->errors(), 412)->send();


                $rid = decode($req['restaurant_hashid'], 'uuid');
                if(!$rid) return res('Invalid restaurant hashid', null, 400)->send();

                $qrp = QRPointer::where('business_link', $rid)->first();
                if($qrp) return res('Business Link is already bound', null, 400)->send();

                $qrp = new QRPointer();
                $qrp->hash = $this->getHash(['hash' => encode($req['transaction_number'], 'transaction_hash')]);
                $qrp->transaction_type_id = $qrtt->id;
                $qrp->business_type_id = 3;
                $qrp->transaction_number = $req['transaction_number'];
                $qrp->user_id = $rid;
                $qrp->save();

                return res('Success')->send();
            case 'hotel':
            case 'activity':
                $v = Validator::make($req->all(), [
                    'business_link' => 'required',
                ]);
                if($v->fails()) return res('Failed', $v->errors(), 412);

                $qrp = QRPointer::where('business_link', $req->business_link)->first();
                if ($qrp) return res('Business Link is already bound', null, 400);

                $bt = BusinessType::where('name', $req->qr_transaction_type)->first();

                $qrp = new QRPointer();
                $qrp->hash = $this->getHash($req);
                $qrp->transaction_type_id = $qrtt->id;
                $qrp->business_type_id = $bt->id;
                $qrp->business_link = $req->business_link;
                $qrp->save();

                return res('Success');
            case 'parcel':
                $v = Validator::make($req->all(), [
                    'hash' => 'required',
                    'transaction_number' => 'required',
                ]);
                if($v->fails()) return res('Failed', $v->errors(), 412);

                if (strlen($req->hash) != 128) return res('Invalid Hash', null, 400);

                $qrp = QRPointer::where('hash', $req->hash)->first();
                if ($qrp) return res('Hash is already bound', null, 400);

                $c = Cart::where('transaction_number', $req->transaction_number)->where('is_deleted', 0)->first();
                if (!$c) return res('Invalid Transaction Number', null, 400);

                $qrp = new QRPointer();
                $qrp->hash = $this->hash;
                $qrp->transaction_type_id = $qrtt->id;
                $qrp->business_type_id = $c->business_type_id;
                $qrp->transaction_number = $req->transaction_number;
                $qrp->reciever_hash = encode(Carbon::now()->timestamp, 'receiver_hash');
                $qrp->save();

                return res('Success');
            default:
                return res('Invalid QR Transaction Type', null, 400);
        }
    }

    private function getHash($req)
    {
        $hash = $req->hash;
        if (!$hash) {
            $hash = encode(Carbon::now()->timestamp, 'transaction_hash');
        }
        
        return $hash;
    }
}