<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariationTypeSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('variation_type_selections')){
            Schema::create('variation_type_selections', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('food_variation_id');
                $table->string('name');
                $table->double('price')->default(0.00);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variation_type_selections');
    }
}
