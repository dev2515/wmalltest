<?php

namespace App\Models;

use App\Http\Resources\Restaurant\MenuListResource;
use Illuminate\Database\Eloquent\Model;

class RestaurantMenu extends Model
{
    protected $table = 'restaurant_menus';
    protected $fillable = ['restaurant_id', 'qr_code', 'qrcode_image'];
    protected $appends = ['hash_id', 'menulists', 'store'];

    public function lists()
    {
        return $this->hasMany(\App\Models\RestaurantMenuList::class, 'menu_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Restaurant::class, 'restaurant_id');
    }

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function getMenuListsAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $menulist = RestaurantMenuList::where('menu_id', $id)
        ->where('status', 1)
        ->get();
        $menulist->makeHidden(['id','food_id', 'status','created_at', 'updated_at', 'menu_id',]);

        //$menulist = MenuListResource::collection($menulist);
        return $menulist;
    }


    public function getStoreAttribute()
    {
        $rest_id = isset($this->attributes['restaurant_id']) ? $this->attributes['restaurant_id'] : 0;
        $food = Store::select('name', 'business_type_id','is_approved')->where('business_type_id', 3)
        ->where('id', $rest_id)
        ->where('is_approved', 1)
        ->get();

        return $food;
    }
}
