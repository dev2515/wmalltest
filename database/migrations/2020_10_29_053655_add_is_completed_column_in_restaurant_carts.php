<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCompletedColumnInRestaurantCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_carts', 'is_completed')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->boolean('is_completed')->default(0);
            });
        }

        if(!Schema::hasColumn('restaurant_carts', 'status')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->integer('status')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_carts','is_completed')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->dropColumn('is_completed');
            });
        }

        if(Schema::hasColumn('restaurant_carts','status')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }
    }
}
