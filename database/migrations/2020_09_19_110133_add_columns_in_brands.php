<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('brands', 'request_by')){
            Schema::table('brands', function (Blueprint $table) {
                $table->integer('request_by')->nullable()->after('is_declined');
            });
        }

        if(!Schema::hasColumn('brands', 'process_by')){
            Schema::table('brands', function (Blueprint $table) {
                $table->integer('process_by')->nullable()->after('request_by');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('brands', 'request_by')){
            Schema::table('brands', function (Blueprint $table) {
                $table->dropColumn('request_by');
            });
        }

        if(Schema::hasColumn('brands', 'process_by')){
            Schema::table('brands', function (Blueprint $table) {
                $table->dropColumn('process_by');
            });
        }
    }
}
