<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
class ProfilePhotoImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data;

        $path = public_path() . '/images/stores';
        if (!file_exists($path)) {
            File::makeDirectory(base_path() . '/public/images/stores', $mode = 0777, true, true);
        }
        File::makeDirectory(base_path() . '/public/images/stores/' . encode($store->id, 'uuid'), $mode = 0777, true, true);
        
        $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';

        $image = Image::make($store->profile_photo_img);
        $image->save($store_path . 'profile-photo-full.png');
        $image->resize(150, 150);
        $image->save($store_path . 'profile-photo-thumbnail.png');

        $user_parent_dir = public_path() . '/images/users';
        if (!file_exists($user_parent_dir)) {
            File::makeDirectory(base_path() . '/public/images/users', $mode = 0777, true, true);
        }
        File::makeDirectory(base_path() . '/public/images/users/' . encode($store->user_id, 'uuid'), $mode = 0777, true, true);

        $root_path = public_path() . '/images/users/' . encode($store->user_id, 'uuid') . '/';
        
        $image = Image::make($store->profile_photo_img);
        $image->save($root_path . 'full.png');
        $image->resize(150, 150);
        $image->save($root_path . 'thumbnail.png');

    }
}
