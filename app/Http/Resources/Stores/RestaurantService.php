<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantService extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'service_hashid'    => encode($this->id, 'uuid'),
            'name'              => $this->name,
            'is_active'         => $this->pivot->is_active
        ];
    }
}
