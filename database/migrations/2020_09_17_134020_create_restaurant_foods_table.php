<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('restaurant_foods')){
            Schema::create('restaurant_foods', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('restaurant_id');
                $table->string('name');
                $table->string('type');
                $table->double('price');
                $table->boolean('is_available')->default(1);
                $table->string('description');
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_foods');
    }
}
