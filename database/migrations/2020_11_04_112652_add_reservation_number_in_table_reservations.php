<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReservationNumberInTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('table_reservations', 'reservation_number')) {
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->longText('reservation_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('table_reservations', 'reservation_number')) {
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->dropColumn('reservation_number');
            });
        }
    }
}
