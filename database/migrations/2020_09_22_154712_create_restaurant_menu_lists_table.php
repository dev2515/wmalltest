<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantMenuListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('restaurant_menu_lists')){
            Schema::create('restaurant_menu_lists', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('menu_id');
                $table->unsignedBigInteger('food_id');
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_menu_lists');
    }
}
