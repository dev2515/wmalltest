<?php

namespace App\Interfaces;

use App\User;

interface LoginInterface
{
    public function email($req);

    public function mobile($req);

    public function mobileOtp($req);

    public function generateToken(User $user);

    public function logout($req);

    public function agent($req);

    public function vendor($req);

    public function driver($req);

    public function facebook($req);

    public function twitter($req);

    public function instagram($req);

    public function apple($req);

    public function google($req);
}
