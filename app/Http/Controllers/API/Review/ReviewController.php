<?php

namespace App\Http\Controllers\API\Review;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ReviewInterface;

class ReviewController extends Controller
{
    protected $req;
    protected $review;

    public function __construct(Request $req, ReviewInterface $review)
    {
        $this->req = $req;
        $this->review = $review;
    }

    public function store_review_list()
    {
        return $this->review->store_review_list($this->req);
    }

    public function store_review_add()
    {
        return $this->review->store_review_add($this->req);
    }

    public function store_review_remove()
    {
        return $this->review->store_review_remove($this->req);
    }

    public function my_review_and_rating()
    {
        return $this->review->my_review_and_rating($this->req);
    }
}
