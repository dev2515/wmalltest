<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('users', ['latitude', 'longitude', 'iso'])){
            Schema::table('users', function (Blueprint $table) {
                $table->string('latitude')->nullable()->after('iso');
                $table->string('longitude')->nullable()->after('latitude');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('users', ['latitude', 'longitude'])){
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('latitude');
                $table->dropColumn('longitude');
            });
        }
    }
}
