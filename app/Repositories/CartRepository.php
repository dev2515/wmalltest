<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\Store;
use App\Models\Product;
use App\Models\CartItem;
use App\Interfaces\CartInterface;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\MarketPlace\Cart\StoreCollection;
use App\Models\ProductVariationTypeSelection;
use App\Models\RestaurantFood;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CartRepository implements CartInterface
{
    public function list($req)
    {
        $user_id = auth()->id();
        $list = Cart::where('user_id', $user_id)->where('is_deleted', 0)->get();
    
        $data = new StoreCollection($list);
        return res('success', $data, 200);
    }

    public function calculateTotalAmount() {
        $total = 0;
        $user_id = auth()->user()->id;
        $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        
        $items = CartItem::whereIn('cart_id', $cart_ids)
        ->where('is_deleted', 0)
            ->where('on_checkout', 0)
                ->where('is_ordered', 0)->get();

        foreach ($items as $item) {
            if ($item->product->getIsOnSaleAttribute() === 1) {
                $total += $item->product->sale_price * $item->quantity;
            } else {
                $total += $item->product->price * $item->quantity;
            }
        } 
        return $total;
    }

    public function add($req)
    {
        DB::beginTransaction();
        $permissions = ['customer','vendor', 'vendor staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_found'), null, 401);
            DB::rollBack();
        }

        $validator = Validator::make($req->all(), [
            'product_id' => 'required',
            'quantity' => 'required|min:1',
            'business_type' => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412); DB::rollBack();
        
        $store_id = 0;
        $special_request = '';
        $business_type = ['market place', 'restaurant'];

        if(!in_array($req->business_type, $business_type)) return res('Product type not found.', null, 402); DB::rollBack();
        
        $product_id = decode($req->product_id, 'uuid');

        if($req->business_type == 'market place')
        {
            $p = Product::where('id', $product_id)->where('is_deleted', 0)->where('status', 1)->first();
            if(!$p) return res(__('product.not_found'), null, 400); DB::rollBack();
            $store_id = $p->store_id;
        }

        if($req->business_type == 'restaurant')
        {
            $reservation_id = 0;
            
            $p = RestaurantFood::where('id', $product_id)->where('status', 1)->first();

            if(!$p) return res(__('food.not_found'), null, 400); DB::rollBack();

            if(!$req->order_type) return res('order type required', null, 400); DB::rollBack();

            if($req->reservation_id)
            {
                $reservation_id = $req->reservtion_id;
            }

            $store_id = $p->restaurant_id;

            $data = ['order_type' => $req->order_type, 'has_booking' => $req->has_book, 'reservation_id' => $reservation_id];
        }

        if($req->special_request) $special_request = $req->special_request;

        $store = Store::where('id', $store_id)->where('is_approved', 1)->where('ban', 0)->first();
        if(!$store) return res(__('store.not_found'), null, 400); DB::rollBack();

        $cart = $this->fillCart($store, $data);
        $this->addCartItem($cart, $p, $req->quantity, $special_request);

        DB::commit();
        return res('Product Added to Cart');
    }

    private function fillCart(Store $store, $data)
    {
        DB::beginTransaction();
        $user_id = auth()->user()->id;
        $cart = Cart::where('user_id', $user_id)->where('store_id', $store->id)->where('is_deleted', 0)->first();
        if (!$cart) {

            $submission = true;
            $tn = strtotime(Carbon::now()->format('YmdHis'));

            while($submission){
                $count = Cart::where('transaction_number', $tn)->count();
                if($count > 0) {
                    $tn = strtotime(Carbon::now()->format('YmdHis'));
                } else {
                    $submission = false;
                }
            }

            $cart                       = new Cart;
            $cart->transaction_number   = $tn;
            $cart->user_id              = $user_id;
            $cart->store_id             = $store->id;
            $cart->business_type_id     = $store->business_type_id;
            $cart->order_type           = $data['order_type'];
            $cart->has_booking          = $data['has_booking'];
        }
        
        $cart->save();
        DB::commit();
        return $cart;
    }

    private function addCartItem(Cart $cart, $product, $quantity, String $request)
    {
        DB::beginTransaction();
        $item = CartItem::where('cart_id', $cart->id)->where('product_id', $product->id)->where('is_deleted', 0)->where('is_ordered', 0)->first();
        if (!$item) {
            $item               = new CartItem;
            $item->cart_id      = $cart->id;
            $item->product_id   = $product->id;
            $item->quantity     = $quantity;
            $item->request      = $request;
        } else {
            $item->quantity = $item->quantity + 1;
        }

        $item->save();
        DB::commit();
        return $item;
    }

    public function count($req)
    {
        $user_id = auth()->id();
        $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $item_count = CartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->sum('quantity');
        $item_count = (int)$item_count;

        return res('Success', $item_count);
    }

    public function clear($req)
    {
        $user_id = auth()->id();
        $cart_ids = Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');

        Cart::whereIn('id', $cart_ids)->update(['is_deleted' => 1]);
        CartItem::whereIn('cart_id', $cart_ids)->update(['is_deleted' => 1]);

        return res('Cart Cleared');
    }

    public function addQuantity($req)
    {
        $validator = Validator::make($req->all(), [
            'product_hashid' => 'required',
            'cart_hashid' => 'required',
        ]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $cart_id = decode($req->cart_hashid, 'uuid');
        $product_id = decode($req->product_hashid, 'uuid');

        $item = CartItem::where('cart_id', $cart_id)->where('product_id', $product_id)->where('is_deleted', 0)->where('is_ordered', 0)->first();
        if (!$item) return res(__('validation.not_found'), null, 400);
    
        $product = Product::find($product_id);
        if (!$product) return res(__('validation.not_found'), null, 400);
        
        $total_item = $product->inventory_info;
        $total = $this->calculateTotalAmount();

        if (1 > $total_item) return res(__('product.out_of_stock'), $total, 200);
        
        $item->quantity += 1;
        $item->save();

        $total = $this->calculateTotalAmount();
        
        return res('Success', [
            'qty' => (int)$item->quantity,
            'total' => $total
        ], 201);
    }

    public function subtractQuantity($req)
    {
        $validator = Validator::make($req->all(), [
            'product_hashid' => 'required',
            'cart_hashid' => 'required',
        ]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $cart_id = decode($req->cart_hashid, 'uuid');
        $product_id = decode($req->product_hashid, 'uuid');

        $item = CartItem::where('cart_id', $cart_id)->where('product_id', $product_id)->where('is_deleted', 0)->where('is_ordered', 0)->first();
        if (!$item) return res(__('cart.item_not_found'), null, 400);
        
        $total = $this->calculateTotalAmount();
        if($item->quantity === 1) return res(__('cart.subtract_not_apply'), $total, 400);

        $item->quantity -= 1;
        $item->save();

        $total = $this->calculateTotalAmount();
        return res('Success', [
            'qty' => (int)$item->quantity,
            'total' => $total
        ], 201);
    }

    public function toggleCheckout($req)
    {
        $user_id = auth()->user()->id;

        foreach ($req->data as $object){
            $cart_id = decode($object['cart_hashid'], 'uuid');
            $product_ids = [];

            foreach ($object['product_hashid'] as $product_id){
                $product_id = decode($product_id, 'uuid');
                array_push($product_ids, $product_id);
            }

            $check = Cart::where('id', $cart_id)->where('user_id', $user_id)->where('is_deleted', 0)->first();
            if(!$check) return res(__('cart.not_found'), null, 400);

            CartItem::where('cart_id', $cart_id)->whereIn('product_id', $product_ids)->where('is_deleted', 0)->where('is_ordered', 0)->update(['on_checkout' => 1]);
            
            $total_item_count = CartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->count();
            $checkout_item_count = CartItem::where('cart_id', $cart_id)->where('is_deleted', 0)->where('is_ordered', 0)->where('on_checkout', 1)->count();

            ($checkout_item_count >= $total_item_count) ? Cart::where('id', $cart_id)->update(['on_checkout' => 1]) : Cart::where('id', $cart_id)->update(['on_checkout' => 0]);
            
        }
        
        return res('success', null, 201);
    }

    public function remove($req)
    {
        $user_id = auth()->user()->id;
        foreach ($req->data as $object){
            $cart_id = decode($object['cart_hashid'], 'uuid');
            $product_ids = [];

            foreach ($object['product_hashid'] as $product_id){
                $product_id = decode($product_id, 'uuid');
                array_push($product_ids, $product_id);
            }

            $check = Cart::where('id', $cart_id)->where('user_id', $user_id)->where('is_deleted', 0)->first();
            if (!$check) return res(__('cart.not_found'), null, 400);

            CartItem::where('cart_id', $cart_id)->whereIn('product_id', $product_ids)->where('is_ordered', 0)->where('is_deleted', 0)->update(['is_deleted' => 1]);
            
            $item_count = CartItem::where('cart_id', $cart_id)->where('is_ordered', 0)->where('is_deleted', 0)->count();

            if ($item_count === 0) Cart::where('id', $cart_id)->update(['is_deleted' => 1]);
        }
        return res('success', null, 201);
    }

    public function item_variation($variation_hash_id, $req)
    {
        $var_id = decode($variation_hash_id,'uuid');
            
        $variant = ProductVariationTypeSelection::where('id',$var_id)->where('status', 1)->first();

        return $variant;
    }

    // Market Place Lists

    public function mk_cart_list($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions))
        {
            return res(__('user.not_found'), null, 401);
        }

        $cart = Cart::where('user_id', auth()->user()->id)->where('business_type_id', 1)->orderBy('created_at', 'DESC')->get();
        return res('success', $cart, 201);
    }

    // Restaurant

    public function restaurant_cart_list($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions))
        {
            return res(__('user.not_found'), null, 401);
        }

        $cart = Cart::where('user_id', auth()->user()->id)->where('business_type_id', 3)->orderBy('created_at', 'DESC')->get();
        return res('success', $cart, 201);
    }
}
