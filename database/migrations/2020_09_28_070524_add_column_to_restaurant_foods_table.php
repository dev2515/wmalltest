<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToRestaurantFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('restaurant_foods', 'category_id')) {
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->unsignedBigInteger('category_id');
            });
        }

        if (!Schema::hasColumn('restaurant_foods', 'food_image')) {
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->longText('food_image');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('restaurant_foods', 'category_id')) {
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('category_id');
            });
        }

        if (Schema::hasColumn('restaurant_foods', 'food_image')) {
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('food_image');
            });
        }
    }
}
