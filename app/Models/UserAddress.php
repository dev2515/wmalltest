<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = 'user_addresses';
    protected $fillable = ['user_id', 'address_type_id', 'street', 'state', 'city', 'other_address_info', 
    'latitude', 'longitude', 'is_current', 'nickname', 'area', 'house', 'additional_directions', 'mobile', 'landline',
    'building', 'floor', 'apartment_number', 'office'];
    protected $appends = ['hashid'];

    public function getHashidAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function type()
    {
        return $this->belongsTo(AddressType::class, 'address_type_id');
    }
}
