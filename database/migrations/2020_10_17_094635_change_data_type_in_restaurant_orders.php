<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeInRestaurantOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_orders', 'shipping_address_coordinates')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->json('shipping_address_coordinates')->change();
            });
        }

        if(!Schema::hasColumn('restaurant_orders', 'billing_address_coordinates')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->json('billing_address_coordinates')->change(); 
            });
        }
    }

    /**
     * Reverse the migrations.p
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
