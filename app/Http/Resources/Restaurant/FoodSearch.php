<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class FoodSearch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'food' =>                       $this->name,
            'food_hash_id' =>               $this->hash_id,
            'restaurnt_hash_id' =>          encode($this->restaurant_id,'uuid'),
            'full' =>                       $this->photo()->full,
            'thumbnail' =>                  $this->photo()->thumbnail,
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
