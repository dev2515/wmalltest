<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('food_type_pivot')){
            Schema::create('food_type_pivot', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('store_id');
                $table->unsignedBigInteger('food_id');
                $table->unsignedBigInteger('food_type_id');
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_type_pivot');
    }
}
