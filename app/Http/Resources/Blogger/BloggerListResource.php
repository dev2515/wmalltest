<?php

namespace App\Http\Resources\Blogger;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use stdClass;

class BloggerListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'hash_id'                       => $this->hashid,
            'shop_name'                     => $this->name,
            'store_information'             => $this->store_information,
            'blogger_name'                  => $this->blogger()->name,
            'email'                         => $this->store_email,
            'full'                          => asset($this->photo()->full),
            'thumbnail'                     => asset($this->photo()->thumbnail),
            'user_full'                     => asset($this->blogger_photo()->full),
            'user_thumbnail'                => asset($this->blogger_photo()->thumbnail),
            'no_of_products'                => $this->product_count(),
            'store_followers'               => $this->followers,
            'product_categories'            => $this->category(),
        ];
    }

    public function blogger()
    {
        $u = DB::table('users')->select('id','name','email', 'mobile')->where('id', $this->user_id)->first();
        return $u;
    }

    public function photo()
    {      
        $full = public_path() . '/images/stores/' . $this->hash_id . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hash_id . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->hash_id . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->hash_id . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function blogger_photo()
    {      
        $full = public_path() . '/images/users/' . encode($this->blogger()->id, 'uuid') . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . encode($this->blogger()->id, 'uuid') . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . encode($this->blogger()->id, 'uuid') . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . encode($this->blogger()->id, 'uuid') . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }

    public function product_count()
    {
        $nop = Product::where('store_id', $this->id)->where('status', 1)->count();
        $nop;
    }

    public function category()
    {
        $c = DB::table('products')->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->select('categories.name')->where('products.store_id', $this->id)->orderBy('categories.name')->get();

        return $c;
    }
}
