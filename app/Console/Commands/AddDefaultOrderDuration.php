<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Store;
use App\Models\BusinessType;

class AddDefaultOrderDuration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:default-order-duration {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add default order duration in wmall services';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $business_type = BusinessType::where('name', 'restaurant')->first();
        if(!$business_type) return false;

        $type = $this->argument('type');

        switch ($type) {
            case 'restaurant' :
            $std_duration = [
                'minimum' => 15,
                'maximum' => 20,
            ];
            $stores = Store::where('business_type_id', $business_type->id)->get();

            foreach ($stores as $store){
                $store->order_duration = $std_duration;
                $store->save();
            }
            $this->info('Default order duration successfully added');
            break;
        }
        
    }
}
