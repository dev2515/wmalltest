<?php

return [
    'not_found'         => 'Path does not exist',
    'file_not_found'    => 'File not found'
];