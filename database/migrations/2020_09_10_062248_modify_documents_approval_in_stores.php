<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDocumentsApprovalInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('stores', 'computer_card_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('computer_card_approval');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_registration_approval');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_permit_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_permit_approval');
            });
        }

        if(Schema::hasColumn('stores', 'sponsor_qid_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('sponsor_qid_approval');
            });
        }

        if(Schema::hasColumn('stores', 'signature_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_approval');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
