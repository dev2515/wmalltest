<?php
return [
    'identifier'    => 'Address hashid is required',
    'already'       => 'Address is already exists',
    'not_found'     => 'Address information not found',
    'type' => [
        'not_found' => 'Address type not found'
    ]
];