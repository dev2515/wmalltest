<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStore extends Model
{
    public function order()
    {
        return $this->belongsTo(\App\Models\Order::class, 'order_id');
    }

    public function order_items()
    {
        return $this->hasMany(\App\Models\OrderStoreItem::class, 'order_store_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store_id');
    }
}
