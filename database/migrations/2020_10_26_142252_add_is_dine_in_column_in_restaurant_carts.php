<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDineInColumnInRestaurantCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_carts','is_dine_in')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->boolean('is_dine_in')->default(0);
            });
        }

        if(Schema::hasColumn('restaurant_carts','table_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->unsignedBigInteger('table_id')->nullable(true)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_carts','is_dine_in')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->dropColumn('is_dine_in');
            });
        }
    }
}
