<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\VariationTypeSelection;
use App\Models\Command as ModelCommand;
use Carbon\Carbon;
class PriceDoubleToAmountInt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-price-to-amount:variation_type_selections'; //Please do not edit this command.. if you edit this please edit in commands table

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer the price to amount(double to integer)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $command = ModelCommand::where('name', $this->signature)->where('is_called', 1)->first();
        if($command){
            $this->info('This command should call at once only');
            return false;
        }

        $variation_selections = VariationTypeSelection::get();
        $bar = $this->output->createProgressBar(count($variation_selections));
        $bar->start();
        foreach ($variation_selections as $variation_selection){
            $variation_selection->amount = priceToCent($variation_selection->price);
            $variation_selection->save();
            $bar->advance();
        }
        $bar->finish();

        $executed = new ModelCommand();
        $executed->name = $this->signature;
        $executed->is_multiple_called = 0;
        $executed->is_called = 1;
        $executed->executed_at = Carbon::now();
        $executed->save();
    }
}
