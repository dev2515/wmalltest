<?php

namespace App\Http\Resources\Restaurant;

use App\Models\DriverVehicle;
use App\Models\PaymentMethod;
use App\Models\QRPointer;
use App\Models\RestaurantCart;
use App\Models\RestaurantFood;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class OrderListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        $dt = Carbon::parse($this->created_at);
        //return parent::toArray($request);
        return[
            'hash_id' =>                        $this->order()->hash_id,
            'order_number' =>                   decode($this->order->order_number,'order-number'),
            'sub_total' =>                      centToPrice($this->sub_total),
            'status' =>                         $this->order()->status_info,
            'order_type' =>                     $this->order_type,
            'customer_hash_id' =>               $this->user()->hash_id,
            'customer' =>                       $this->user()->name,
            'mobile' =>                         $this->user()->mobile,
            'payment_method' =>                 $this->paymethod()->name,
            'order_type' =>                     $this->order()->order_type_info,
            'delivery_man' =>                   $this->deliveryman()->name,
            'vehicle_plate' =>                  $this->vehicle_plate()->plate_number,
            'delivery_address' =>               $this->order()->shipping_address,
            'order_date' =>                     $dt->year.'-'. $dt->month .'-'. $dt->day,
            'order_time' =>                     $dt->hour .':'. $dt->minute,
            'orders' =>                         $this->orderList(),
            'qrcode' =>                         $this->qrcode()->hash,
        ];
    }

    public function paymethod()
    {
        $order = RestaurantOrder::where('id', $this->order_id)->first();
        if($order != null)$paymethod = PaymentMethod::where('id', $order->payment_method_id)->where('status', 1)->first();
        
        return $paymethod;
    }

    public function user()
    {
        $order = RestaurantOrder::where('id', $this->order_id)->first();
        if($order != null) $cx = User::where('id', $order->customer_id)->where('type', 0)->first();
        return $cx;
    }

    public function deliveryman()
    {
        $order = RestaurantOrder::where('id', $this->order_id)->first();
        if($order == null)$cx = User::where('id', $order->customer_id)->where('type', 4)->first();
        return $cx;
    }

    public function vehicle_plate()
    {
        $$order = RestaurantOrder::where('id', $this->order_id)->first();
        if($order == null)$cx = DriverVehicle::where('user_id', $order->customer_id)->first();
        return $cx;
    }

    public function order()
    {
        $order = RestaurantOrder::where('id', $this->order_id)->first();
        return $order;
    }

    public function orderList()
    {   
        $orderList = RestaurantOrderFood::select('*')->where('order_list_id', $this->id)->get();
        $orderList->makeHidden(['id','order_list_id','food_id','commision', 'is_rate', 'status', 'created_at', 'updated_at']);
        return $orderList;
    }

    public function qrcode()
    {
        $c = RestaurantCart::where('id', $this->order()->cart_id)->pluck('transaction_number');
        $qr = QRPointer::where('transaction_number', $c)->first();

        return $qr;
    }
}