<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifiedComputerCardApprovalInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('stores', 'computer_card_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('computer_card_approval')->nullable()->change();
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('commercial_registration_approval')->nullable()->change();
            });
        }

        if(Schema::hasColumn('stores', 'commercial_permit_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('commercial_permit_approval')->nullable()->change();
            });
        }

        if(Schema::hasColumn('stores', 'sponsor_qid_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('sponsor_qid_approval')->nullable()->change();
            });
        }

        if(Schema::hasColumn('stores', 'signature_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('signature_approval')->nullable()->change();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
