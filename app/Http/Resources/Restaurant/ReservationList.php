<?php

namespace App\Http\Resources\Restaurant;

use App\Models\PaymentMethod;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\RestaurantTable;
use App\Models\StoreStaff;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ReservationList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $order_type = 'Dine In'; 
        if($this->order_type == 1) $order_type = 'Dine In';
        if($this->order_type == 2) $order_type = 'Delivery';
        if($this->order_type == 3) $order_type = 'Pickup';


        $is_completed = 'Pending'; 
        if($this->is_completed == 1) $is_completed = 'Pending';
        if($this->is_completed == 2) $is_completed = 'Completed';

        return[
            'reservation_hash_id' =>            $this->hash_id,
            'customer_hash_id' =>               $this->customer()->hash_id,
            'customer_name' =>                  $this->customer()->name,
            'table' =>                          $this->table_number,
            'number_of_gueat' =>                $this->guest,
            'time' =>                           date('H:i', $this->arrive_time),
            'date' =>                           date('Y-m-d', $this->arrive_date),
            'order_hash_id' =>                  $this->order()->hash_id,
            'order_status' =>                   $this->order()->status_info, //pending, pickup, delivered, served
            'waiter_name' =>                    $this->waiter() !== null ? $this->text : 'Waiter 1',
            'payment_status' =>                 $is_completed,
            'payment_method' =>                 $this->paymethod() !== null ? $this->text : 'Cash',
            'order_type' =>                     $order_type,
            'orders' =>                         $this->order_food(), 
            'total' =>                          $this->order()->sub_total,
        ];
    }

    public function customer()
    {
        $cx = User::where('id', $this->user_id)->where('status', 1)->where('type', 0)->first();
        return $cx;
    }
    
    public function paymethod()
    {
        $paymethod = PaymentMethod::where('id', $this->order()->payment_method)->where('status', 1)->first();
        return $paymethod;
    }

    public function waiter()
    {
       $waiter = User::select('id', 'name', 'type', 'status')->where('id', $this->assigned_waiter_id)->where('status', 1)->first();
        return $waiter;
    }
    
    public function order()
    {
        $order = RestaurantOrder::where('reservation_id', $this->id)->first();
        return $order;
    }

    public function order_food()
    {
        $order_list_id = RestaurantOrderList::where('order_id', $this->order()->id)->first();

        $food = RestaurantOrderFood::select('*')->where('order_list_id', $order_list_id->id)->get();
        $food->makeHidden(['id','order_list_id', 'food_id', 'commission', 'is_rate', 'created_at', 'updated_at']);
        return $food;
    }
}
