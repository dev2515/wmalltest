<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQrcodeStatusInTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('table_reservations', 'qrcode_status')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->boolean('qrcode_status')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_orders','qrcode_status')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->dropColumn('qrcode_status');
            });
        }
    }
}
