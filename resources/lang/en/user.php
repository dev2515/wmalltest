<?php

return [
    'not_found'                 => 'User Information not found',
    'not_allowed'               => 'You are not allowed for this operation',
    'required_bdate'            => 'Birthdate is required',
    'invalid_current_password'  => 'Invalid current password',
    'same_mobile'               => 'Unable to proceed due to same mobile number',
    'same_email'                => 'Unable to proceed due to same email address',
    'existing_mobile'           => 'Mobile Number is already used by other',
    'existing_email'            => 'Email Address is already used by other',
    'already_request'           => 'You already request, Please wait the admin decession',
    'declined_message'          => 'Please leave a message, Why you dis-approve this request',
    'storage_path_exists'       => 'User path not exists',
    'not_vendor'                => 'This user is not vendor',
    'required_hashid'           => 'User hashid is required',
    'fcm_not_added'             => 'User fcm device token not added'
];
