<?php

namespace App\Interfaces;

interface PayPalInterface
{
    public function createPayment($req);

    public function executePayment($req);
}