<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderTypeToRestaurantOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_orders', 'order_type')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->integer('order_type')->default(1); // [1] dine int
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_orders', 'order_type')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('order_type')->default(1); // [1] dine int
            });
        };
    }
}
