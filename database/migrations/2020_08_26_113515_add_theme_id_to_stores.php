<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThemeIdToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'theme_id')){
            Schema::table('stores', function (Blueprint $table) {
                $table->integer('theme_id')->default(1)->after('agent_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'theme_id')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('theme_id');
            });
        }
    }
}
