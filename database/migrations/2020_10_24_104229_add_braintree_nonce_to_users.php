<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBraintreeNonceToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('users', 'braintree_token')){
            Schema::table('users', function (Blueprint $table) {
                $table->string('braintree_token')->nullable()->after('type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('users', 'braintree_token')){
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('braintree_token');
            });
        }
    }
}
