<?php

namespace App\Http\Resources\Blogger;

use Illuminate\Http\Resources\Json\JsonResource;


class BloggerVideoResource extends JsonResource
{

     public function toArray($request)
     {
        
         return [
             'video_id' => $this->id,
             'video_title' => $this->video_title,
             'video_desc' => $this->video_description,
             'video_thumb' => asset('images/blogger').'/'.$this->video_thumbnail,
             'video_url' => asset('videos/blogger').'/'.$this->video_url,
             'date'      =>  $this->created_at->toDateString(),
             'time'      => $this->created_at->format('H:i')
          ];
        
     }

}