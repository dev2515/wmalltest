<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagerActivityLog extends Model
{
    protected $table = 'manager_activity_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'activity', 'url', 'method', 'ip', 'status'
    ];
}
