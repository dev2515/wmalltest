<?php

use Illuminate\Database\Seeder;
use App\Models\RestaurantService;

class SeedDefaultRestaurantServices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $count = RestaurantService::count();
        if($count == 0){
            $services = ['Dine-in', 'Take-out', 'Delivery'];
            foreach ($services as $service){
                RestaurantService::firstOrCreate(['name' => $service]);
            }
        }

        $update = RestaurantService::where('name', 'Take-out')->first();
        if($update){
            $update->update(['name' => 'Take-Away']);
        }
    }
}
