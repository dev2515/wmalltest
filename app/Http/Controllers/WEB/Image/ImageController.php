<?php

namespace App\Http\Controllers\WEB\Image;

use Illuminate\Http\Request;
use App\Interfaces\ImageInterface;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    protected $req;
    protected $image;

    public function __construct(Request $req, ImageInterface $image)
    {
        $this->req = $req;
        $this->image = $image;
    }

    public function avatar($hashid, $width = 256, $height = null)
    {
        return $this->image->avatar($this->req, $hashid, $width, $height);
    }

    public function product($hashid, $width = 256, $height = null)
    {
        return $this->image->product($this->req, $hashid, $width, $height);
    }

    public function banner($name, $width = 256, $height = null)
    {
        return $this->image->banner($this->req, $name, $width, $height);
    }

    public function store($name, $width = 256, $height = null)
    {
        return $this->image->store($this->req, $name, $width, $height);
    }

    public function productBase64s($hashid, $index, $width = 400, $height = null)
    {
        return $this->image->productBase64s($this->req, $hashid, $index, $width, $height);
    }

    public function downloadPdf($hashid, $documentType, $fileName)
    {
        return $this->image->downloadPdf($this->req, $hashid, $documentType, $fileName);
    }
}
