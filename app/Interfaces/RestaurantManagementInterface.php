<?php

namespace App\Interfaces;

interface RestaurantManagementInterface{

    /*****************************************************************************************
     * 
     *                  MANAGER INTERFACESS
     * 
     *****************************************************************************************/

     public function dine_in_order_list($req);
     public function manager_order_list($req);
    

     /*****************************************************************************************
     * 
     *                  WAITER INTERFACES
     * 
     *****************************************************************************************/

    public function dine_in_order_list_waiter($req);
    public function waiter_confirmation_order($req);
    public function complete_order($req);
    public function updateFoodStatus($req);
    public function waiter_confirm_order($req);
}