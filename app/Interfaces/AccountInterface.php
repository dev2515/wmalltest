<?php

namespace App\Interfaces;

interface AccountInterface
{
    public function refreshToken($req);

    public function update($req);

    public function changePassword($req);

    public function checkPassword($req);

    public function requestMobileChange($req);

    public function confirmMobileChange($req);

    public function requestEmailChange($req);

    public function confirmEmailChange($req);

    public function getSecurityQuestions($req);

    public function checkInfo($req);

    public function userProfile($req);

    public function createAgent($req);

    public function createAdminStaff($req);
    
    public function myProfile();

    public function userChangeDetails($req);

    public function agentList($req);

    public function agentChangeStatus($req);

    public function agentCount();

    public function staffChangeStatus($req);

    public function addFcmToken($req);

    public function getFcmToken($req);

}
