<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\FoodCategoryInterface;

class FoodCategoryController extends Controller
{
    protected $req;
    protected $category;

    public function __construct(Request $req, FoodCategoryInterface $category)
    {
        $this->req = $req;
        $this->category = $category;
    }

    public function addMyFoodType()
    {
        return $this->category->addMyFoodType($this->req);
    }

    public function updateMyFoodType()
    {
        return $this->category->updateMyFoodType($this->req);
    }
}
