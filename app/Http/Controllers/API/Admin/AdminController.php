<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\AdminInterface;

class AdminController extends Controller
{
    private $req;
    private $admin;

    public function __construct(Request $req, AdminInterface $admin)
    {
        $this->req = $req;
        $this->admin = $admin;
    }

    public function changeStatus()
    {
        return $this->admin->changeStatus($this->req);
    }

    public function totalRequest()
    {
        return $this->admin->totalRequest();
    }

    public function status()
    {
        return $this->admin->status($this->req);
    }

    public function showRequest()
    {
        return $this->admin->showRequest($this->req);
    }

    public function overAll()
    {
        return $this->admin->overAll();
    }

    public function marketplaceList()
    {
        return $this->admin->marketplaceList($this->req);
    }

    public function hotelList()
    {
        return $this->admin->hotelList($this->req);
    }

    public function restaurantList()
    {
        return $this->admin->restaurantList($this->req);
    }

    public function activitiesList()
    {
        return $this->admin->activitiesList($this->req);
    }

    public function approvedAllAddFood()
    {
        return $this->admin->approvedAllAddFood();
    }

    public function approvedAllEditFood()
    {
        return $this->admin->approvedAllEditFood();
    }

    public function dashboardRestaurant()
    {
        return $this->admin->dashboardRestaurant($this->req);
    }

    public function modifyActiveStatus()
    {
        return $this->admin->modifyActiveStatus($this->req);
    }
}
