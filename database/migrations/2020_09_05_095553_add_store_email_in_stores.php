<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreEmailInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'store_email')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('store_email')->nullable()->after('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'store_email')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('store_email');
            });
        }
    }
}
