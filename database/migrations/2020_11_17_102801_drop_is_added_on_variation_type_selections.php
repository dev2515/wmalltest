<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIsAddedOnVariationTypeSelections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('variation_type_selections', 'is_added')){
            Schema::table('variation_type_selections', function (Blueprint $table) {
                $table->dropColumn('is_added');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variation_type_selections', function (Blueprint $table) {
            //
        });
    }
}
