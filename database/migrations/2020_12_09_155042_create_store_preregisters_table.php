<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStorePreregistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('store_preregisters')){
            Schema::create('store_preregisters', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('business_type_id');
                $table->boolean('is_main_branch')->default(1);
                $table->string('registration_number');
                $table->string('store_name');
                $table->string('store_email');
                $table->string('store_number');
                $table->string('owner_name');
                $table->string('owner_email');
                $table->string('owner_mobile');
                $table->string('street');
                $table->string('city');
                $table->string('state');
                $table->string('latitude');
                $table->string('longitude');
                $table->string('other_address_info')->nullable();
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('store_preregisters')){
            Schema::dropIfExists('store_preregisters');
        }
    }
}
