<?php

namespace App\Interfaces;

interface CMSInterface
{
    public function getContent($req, $slug);
}
