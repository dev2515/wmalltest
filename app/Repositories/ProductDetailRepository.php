<?php
namespace App\Repositories;

use App\Interfaces\ProductDetailInterface;
use App\Models\ProductDetails;
use Illuminate\Support\Facades\Validator;

class ProductDetailRepository implements ProductDetailInterface
{
    
     public function create_product($req)
     {
         $permissions = ['blogger'];
         if(!in_array(auth()->user()->type_info, $permissions)) return res('Only blogger can do this process', null, 401);
         
          
        $validator = Validator::make($req->all(), [
           'product_name' => 'required',
           'price'        => 'required',
           'category_id'  =>  'required|exists:categories,id',
           'offer_price'  =>  'required',
           'delivery_time'=>  'required',
           'discount'     =>  'required',
           'can_customize'=> 'required',  
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }
      
         $productdetail = new ProductDetails();
         $productdetail->product_name      =  $req->product_name;
         $productdetail->price             =  $req->price;
         $productdetail->category_id       =  $req->category_id;
         $productdetail->offer_price       =  $req->offer_price;
         $productdetail->delivery_time     =  $req->delivery_time;
         $productdetail->discount          =  $req->discount;
         $productdetail->can_customize     =  $req->can_customize;
         $productdetail->user_id           =  2;
         $productdetail->save();
         
         return res('success','created product successfully',201);
        
     }

      public function get_product($req)
      {
         
      }
}