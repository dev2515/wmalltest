<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessTypeToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'business_type')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('business_type')->after('store_information')->default('marketplace');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'business_type')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('business_type');
            });
        }
    }
}
