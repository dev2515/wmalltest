<?php

namespace App\Http\Resources\Restaurant;

use App\Models\DriverCredits;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverCredit extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'driver_hash_id' => $this->hash_id,
            'name' => $this->name,
            'money_recieve' => $this->getTotalIncome(),
        ];
    }

    public function getTotalIncome()
    {
        $i = DriverCredits::where('driver_id', $this->id)->where('is_claim', 0)->sum('commision');
        return centToPrice($i);
    }
}
