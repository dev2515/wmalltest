<?php

namespace App\Http\Resources\Restaurant;
use App\Models\FoodCategory;
use stdClass;
use Illuminate\Http\Resources\Json\JsonResource;

class FoodType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' =>                     $this->hashid,
            'name' =>                       $this->food_category_name,
            'full' =>                       $this->photo()->full,
            'thumbnail' =>                  $this->photo()->thumbnail,
        ];
    }

    public function name()
    {
        $name = FoodCategory::where('id', $this->food_category_id)->first();

        return $name->name;
    }

    public function photo()
    {      
        $full = public_path() . '/images/food_type/' . $this->hashid . '-full.png';
        if(file_exists($full)){
            $full = '/images/food_type/' . $this->hashid . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food_type/' . $this->hashid . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food_type/' . $this->hashid . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

}
