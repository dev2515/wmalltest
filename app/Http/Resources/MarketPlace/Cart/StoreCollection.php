<?php

namespace App\Http\Resources\MarketPlace\Cart;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StoreCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total_items'   => $this->totalItems(),
            'total_ammount' => doubleVal(number_format($this->calculateTotalAmount(), 2, '.', '')),
            'items'         => parent::toArray($request),
        ];
    }

    public function calculateTotalAmount() {
        $total = 0;
        $user_id = auth()->user()->id;
        $cart_ids = \App\Models\Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        
        $items = \App\Models\CartItem::whereIn('cart_id', $cart_ids)
        ->where('is_deleted', 0)
            ->where('on_checkout', 0)
                ->where('is_ordered', 0)->get();

        foreach ($items as $item) {
            if ($item->product->getIsOnSaleAttribute() === 1) {
                $total += $item->product->sale_price * $item->quantity;
            } else {
                $total += $item->product->price * $item->quantity;
            }
        } 
        return $total;
    }

    public function totalItems()
    {
        $user_id = auth()->user()->id;
        $cart_ids = \App\Models\Cart::where('user_id', $user_id)->where('is_deleted', 0)->pluck('id');
        $items = \App\Models\CartItem::whereIn('cart_id', $cart_ids)
        ->where('is_deleted', 0)
            ->where('on_checkout', 0)->where('is_ordered', 0)->sum('quantity');;
        
        return (int) $items;
    }
}
