<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOnFoodVariationSelections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('food_variation_selections' ,['is_required', 'is_multiple', 'selection_max_number'])){
            Schema::table('food_variation_selections', function (Blueprint $table) {
                $table->boolean('is_required')->default(1)->after('name');
                $table->boolean('is_multiple')->default(1)->after('is_required');
                $table->integer('selection_max_number')->after('is_multiple');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('food_variation_selections' ,['is_required', 'is_multiple', 'selection_max_number'])){
            Schema::table('food_variation_selections', function (Blueprint $table) {
                $table->dropColumn('is_required');
                $table->dropColumn('is_multiple');
                $table->dropColumn('selection_max_number');
            });
        }
    }
}
