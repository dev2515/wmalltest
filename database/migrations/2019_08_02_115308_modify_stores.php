<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('stores', 'has_qid')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('has_qid');
            });
        }
        if (Schema::hasColumn('stores', 'has_eid')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('has_eid');
            });
        }
        if (Schema::hasColumn('stores', 'has_commercial_registration')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('has_commercial_registration');
            });
        }
        if (Schema::hasColumn('stores', 'has_commercial_permit')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('has_commercial_permit');
            });
        }

        if (!Schema::hasColumn('stores', 'qid')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('qid')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('stores', 'eid')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('eid')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('stores', 'commercial_registration')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('commercial_registration')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('stores', 'commercial_permit')) {
            Schema::table('stores', function (Blueprint $table) {
                $table->string('commercial_permit')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            //
        });
    }
}
