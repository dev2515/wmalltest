<?php

namespace App\Interfaces;

interface RestaurantDashboardInterface
{
    public function getOrderReceived($store_id);

    public function getTotalCustomer($store_id);

    public function getTotalDelivery($store_id);
}