<?php

namespace App\Http\Resources\Restaurant;

use App\Models\FoodReview;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class FoodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'hash_id' =>                    $this->hash_id,
            'restaurant' =>                 $this->restaurant->name,
            'food' =>                       $this->food->name,
            'description' =>                $this->food->description,
            'total_reviews' =>              $this->totalReview(),
            'ratings' =>                    $this->totalRate(),
            'full' =>                       asset($this->photo()->full),
            'thumbnail' =>                  asset($this->photo()->thumbnail),          
            'rating' =>                     $this->rate,
            'review' =>                     $this->review,
            'user' =>                       $this->user->name,
        ];
    }

    public function totalReview()
    {
        $id = decode($this->restaurant->hash_id, 'uuid');
        $food_id = decode($this->food->hash_id, 'uuid');
        $review = FoodReview::where('restaurant_id', $id)->where('food_id', $food_id)->where('status', 1)->count();
        return $review;
    }

    public function totalRate()
    {
        $id = decode($this->restaurant->hash_id, 'uuid');
        $food_id = decode($this->food->hash_id, 'uuid');
        $rate = FoodReview::where('restaurant_id',  $id)->where('food_id', $food_id)->avg('rate');
        return $rate;
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
