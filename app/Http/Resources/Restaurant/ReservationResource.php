<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class ReservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        $order = 'Yes';
        if($this->order == 2){$order = 'Yes';}
        else{$order= 'No';}

        $status = 'Open';
        if($this->status == 1){$status = 'Open';}
        elseif($this->status == 2){$status = 'accepted';}
        elseif($this->status == 1){$status = 'Cancelled';}
        elseif($this->status == 1){$status = 'Expired';}
        else{$status= 'Done';}

        return[
            'reservation_number' =>         decode($this->reservation_number, 'reservation-number'),
            'user_hash_id' =>               $this->user->hash_id,
            'user' =>                       $this->user->name,
            'note' =>                       $this->note,
            'time_in' =>                    date('H:i', $this->arrive_time),
            'date' =>                       date('Y-m-d', $this->arrive_date),
            'number_of_guest' =>            $this->guest,
            'with_order' =>                 $order,
            'status' =>                     $status,
        ];
    }

    public function order()
    {
        $order = RestaurantOrder::where('reservation_id', $this->id)->first();
        return $order;
    }
}
