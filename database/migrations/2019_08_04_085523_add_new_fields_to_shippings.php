<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToShippings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('shippings', 'max_rate')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->double('max_rate');
            });
        }
        if (!Schema::hasColumn('shippings', 'do_store_pickup')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->integer('do_store_pickup')->default(0);
            });
        }
        if (!Schema::hasColumn('shippings', 'do_oversea')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->integer('do_oversea')->default(0);
            });
        }
        if (!Schema::hasColumn('shippings', 'number')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->string('number')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('shippings', 'email')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->string('email')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('shippings', 'location')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->text('location');
            });
        }
        if (!Schema::hasColumn('shippings', 'website')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->string('website')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('shippings', 'iso')) {
            Schema::table('shippings', function (Blueprint $table) {
                $table->string('iso')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shippings', function (Blueprint $table) {
            //
        });
    }
}
