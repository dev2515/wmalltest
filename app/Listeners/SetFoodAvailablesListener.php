<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use App\Model\StoreBranch;
use App\Models\RestaurantFood;
use App\Models\RestaurantFoodAvailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetFoodAvailablesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data;
        if($store->is_main_branch == 1) return false;

        $store_branch = StoreBranch::where('store_child_id', $store->id)->first();
        if(!$store_branch) return false;

        $parent_food_ids = RestaurantFood::where('restaurant_id', $store_branch->store_id)->pluck('id');
        foreach ($parent_food_ids as $food_id){
            $check = RestaurantFoodAvailable::where('store_id', $store->id)->where('food_id', $food_id)->first();
            if(!$check){
                $food_available = new RestaurantFoodAvailable();
                $food_available->store_id = $store->id;
                $food_available->food_id  = $food_id;
                $food_available->save();
            }
        }
    }
}
