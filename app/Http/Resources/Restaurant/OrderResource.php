<?php

namespace App\Http\Resources\Restaurant;

use App\Model\StoreBranch;
use App\Models\DriverReview;
use App\Models\PaymentMethod;
use App\Models\QRPointer;
use App\Models\Restaurant;
use App\Models\RestaurantCart;
use App\Models\RestaurantFood;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $dt = Carbon::parse($this->created_at);

        return[
            'order_hash_id' =>                  $this->hash_id,
            'order_number' =>                   strval(decode($this->order_number,'order-number')),
            'qrcode' =>                         $this->qrcode()->hash,
            'customer_hash_id' =>               $this->user->hash_id,
            'customer_name' =>                  $this->user()->name,
            'customer_number' =>                $this->user()->mobile,
            'order_type' =>                     $this->order_type_info,
            'order_status' =>                   $this->status_info,
            'total_count_of_items' =>           $this->total_item_count(),
            'sub_total' =>                      $this->sub_total,
            'shipping_rate' =>                  $this->shipping_rate,
            'total' =>                          twoDecimalPlaces($this->sub_total + $this->shipping_rate),
            'billing_address_coordinates' =>    $this->shipping_address,
            'shipping_address_latitude' =>      strval($this->shipping_address_latitude),
            'shipping_address_longitude' =>     strval($this->shipping_address_longitude),
            'payment_method' =>                 $this->paymethod(),
            'restaurant_full' =>                asset($this->photo()->full),
            'restaurant_thumbnail' =>           asset($this->photo()->thumbnail),
            'restaurant' =>                     $this->restaurant()->name,
            'restaurant_latitude' =>            $this->restaurant()->latitude,
            'restaurant_longitude' =>           $this->restaurant()->longitude,
            'order_date' =>                     $dt->year.'-'. $dt->month .'-'. $dt->day,
            'order_time' =>                     $dt->hour .':'. $dt->minute,
            'food_details' =>                   $this->food_details(),
            'delivery_guy' =>                   $this->deliveryman()->name,
            'delivery_mobile_number' =>         $this->deliveryman()->mobile,
            'no_of_deliverires' =>              $this->numberdelivery(),
            'ratings' =>                        $this->driverrating(),
            'delivery_latitude' =>              $this->deliveryman()->latitude,
            'delivery_longitude' =>             $this->deliveryman()->longitude,
            'estimated_delivery_time' =>        '20',
            
        ];
    }

    public function user()
    {
        $user = User::select('id','name','mobile')->where('id', $this->customer_id)->first();
        return $user;
    }

    public function total_item_count()
    {
        $order_list = RestaurantOrderList::where('order_id', $this->id)->first();
        $order_item_count = RestaurantOrderFood::where('order_list_id', $order_list->id)->count();
        return $order_item_count;
    }

    public function paymethod()
    {
        $method = PaymentMethod::where('id', $this->payment_method_id)->first();
        return $method->name;
    }

    public function restaurant()
    {
        $order_list = RestaurantOrderList::where('order_id', $this->id)->first();
        $storebranch = Store::select('id','name','street','state','city','other_address_info','phone','store_email','latitude','longitude')
        ->where('id', $order_list->restaurant_id)->first();
        if($storebranch) $storebranch->makeHidden(['id','user_id','is_main_branch']);
        
        return $storebranch;
    }

    public function food_details()
    {
        $order_list = RestaurantOrderList::where('order_id', $this->id)->first();
        $orderList = RestaurantOrderFood::select('*')->where('order_list_id', $order_list->id)->get();
        if($orderList) $orderList->makeHidden(['id','order_list_id','food_id','commision', 'is_rate', 'status', 'created_at', 'updated_at']);

        return $orderList;
    }

    public function photo()
    {      
        $full = public_path() . '/images/stores/' . $this->restaurant()->hash_id . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->restaurant()->hash_id . '/profile-photo-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/stores/' . $this->restaurant()->hash_id . '/profile-photo-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/stores/' . $this->restaurant()->hash_id . '/profile-photo-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function deliveryman()
    {
        $c = RestaurantCart::select('id', 'transaction_number')->where('id', $this->cart_id)->first();
        $qrp = QRPointer::select('id','hash','transaction_number','assigned_driver_id')->where('transaction_number', $c->transaction_number)->first();
        $d = User::select('id','name','latitude','longitude','mobile')->where('id', $qrp->assigned_driver_id)->first();

        return $d;
    }

    public function qrcode()
    {
        $c = RestaurantCart::select('id', 'transaction_number')->where('id', $this->cart_id)->first();
        $qrp = QRPointer::select('id','hash','transaction_number','assigned_driver_id')->where('transaction_number', $c->transaction_number)->first();
        return $qrp;
    }
    
    public function numberdelivery()
    {
        $count = QRPointer::where('assigned_driver_id', $this->deliveryman()->id)->count();
        return $count;
    }

    public function driverrating()
    {
        $r = DriverReview::where('id', $this->deliveryman()->id)->average('rate');
        return $r;
    }
}
