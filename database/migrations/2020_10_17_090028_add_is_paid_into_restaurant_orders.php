<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPaidIntoRestaurantOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restaurant_orders', function (Blueprint $table) {
            $table->integer('is_paid')->default(0);
        });

        Schema::table('restaurant_order_lists', function (Blueprint $table) {
            $table->integer('is_paid')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_orders', function (Blueprint $table) {
            $table->dropColumn('is_paid');
        });

        Schema::table('restaurant_order_lists', function (Blueprint $table) {
            $table->dropColumn('is_paid');
        });
    }
}
