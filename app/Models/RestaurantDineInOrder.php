<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantDineInOrder extends Model
{
    protected $table = 'restaurant_dine_in_orders';
    protected $fillable = 'user_id';
    protected $appends = ['hash_id', 'parenthashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function getParentHashIdAttribute()
    {
        return encode($this->parent_id, 'uuid');
    }

    public function food()
    {
        return $this->belongsTo(\App\Models\RestaurantFood::class, 'food_id');
    }
}
