<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsClaimInDriverCredits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_credits', 'is_claim')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->boolean('is_claim')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_credits', 'is_claim')){
            Schema::table('driver_credits', function (Blueprint $table) {
                $table->dropColumn('is_claim');
            });
        }
    }
}
