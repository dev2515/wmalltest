<?php

namespace App\Listeners;

use App\Events\EditFoodEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\StoreChangeField;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use stdClass;
class EditFoodStoreRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EditFoodEvent  $event
     * @return void
     */
    public function handle(EditFoodEvent $event)
    {
        $data = $event->data;

        $add_request                        = new StoreChangeField();
        $add_request->store_id              = $data->food->restaurant_id;
        $add_request->restaurant_foods_id   = $data->food->id;
        $add_request->type                  = 'edit food';

        $req_data                           = new stdClass;
        $req_data->name                     = $data->req['name'];
        $req_data->price                    = priceToCent($data->req['price']);
        $req_data->description              = $data->req['description'];

        $add_request->data                  = $req_data;

        if($data->req['image'] != null){
            $add_request->base64  = $data->req['image'];
        }
    
        $add_request->request_by            = auth()->id();   
        $add_request->process_by            = auth()->user()->type_info === 'administrator' ? auth()->id() : 0;
        $add_request->is_approved           = auth()->user()->type_info === 'administrator' ? 1 : 0;
        $add_request->save();

        if($add_request->base64){
            $parent_path = public_path() . '/images/store-requests/' . encode($add_request->id, 'uuid');
            if(!file_exists($parent_path)){
                File::makeDirectory($parent_path, 0777, true, true);
            }

            $path = public_path() . '/images/store-requests/' . encode($add_request->id, 'uuid') . '/';

            $image = Image::make($add_request->base64);
            $image->save($path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path . 'thumbnail.png');
        }

        if(auth()->user()->type_info === 'administrator'){
            if($data->req['name'] != null){
                $data->food->name = $data->req['name'];
            }

            if($data->req['price'] != null){
                $data->food->price = priceToCent($data->req['price']);
            }

            if($data->req['description'] != null){
                $data->food->description = $data->req['description'];
            }

            if($data->req['image'] != null){
                $data->food->food_image = $data->req['image'];

                $path_full  = public_path() . '/images/food/' . encode($data->food->id, 'uuid') . '-full.png';
                $path_thumb = public_path() . '/images/food/' . encode($data->food->id, 'uuid') . '-thumbnail.png';

                if(file_exists($path_full) && file_exists($path_thumb)){
                    File::delete($path_full);
                    File::delete($path_thumb);
                }

                $children_path = public_path() . '/images/food/';

                $image  = Image::make($data->food->food_image);
                $image->save($children_path . encode($data->food->id, 'uuid') . '-full.png');
                $image->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                }); //@AES resizing
                $image->save($children_path . encode($data->food->id, 'uuid') . '-thumbnail.png');
            }
            $data->food->save();
        }
    }
}
