<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsMainToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'is_main_branch')){
            Schema::table('stores', function (Blueprint $table) {
                $table->boolean('is_main_branch')->default(1)->after('business_type_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'is_main_branch')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('is_main_branch');
            });
        }
    }
}
