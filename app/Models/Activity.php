<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';
    protected $fillable = ['vendor_id','description','price','pickup_time','pickup_date','pickup_address','drop_address','pickup_lat','pickup_lng','drop_lat','drop_lng'];


    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function activity_imgs()
    {
        return $this->hasMany(\App\Models\ActivityImage::class, 'activity_id');
    }

    public function activity_type()
    {
        return $this->belongsTo(\App\Models\ActivityType::class, 'type_id');
    }

    public function vendor()
    {
        return $this->belongsTo(\App\Models\Store::class, 'vendor_id');
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\ActivityReview::class, 'activity_id');
    }
}
