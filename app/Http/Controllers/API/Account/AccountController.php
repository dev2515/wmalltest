<?php

namespace App\Http\Controllers\API\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\AccountInterface;

class AccountController extends Controller
{
    protected $req;
    protected $account;

    public function __construct(Request $req, AccountInterface $account)
    {
        $this->req = $req;
        $this->account = $account;
    }

    public function refreshToken()
    {
        return $this->account->refreshToken($this->req);
    }

    public function update()
    {
        return $this->account->update($this->req);
    }

    public function changePassword()
    {
        return $this->account->changePassword($this->req);
    }

    public function checkPassword()
    {
        return $this->account->checkPassword($this->req);
    }

    public function requestMobileChange()
    {
        return $this->account->requestMobileChange($this->req);
    }

    public function confirmMobileChange()
    {
        return $this->account->confirmMobileChange($this->req);
    }

    public function requestEmailChange()
    {
        return $this->account->requestEmailChange($this->req);
    }

    public function confirmEmailChange()
    {
        return $this->account->confirmEmailChange($this->req);
    }

    public function checkInfo()
    {
        return $this->account->checkInfo($this->req);
    }

    public function userProfile()
    {
        return $this->account->userProfile($this->req);
    }

    public function createAgent()
    {
        return $this->account->createAgent($this->req);
    }

    public function createAdminStaff()
    {
        return $this->account->createAdminStaff($this->req);
    }

    public function myProfile()
    {
        return $this->account->myProfile($this->req);
    }

    public function userChangeDetails()
    {
        return $this->account->userChangeDetails($this->req);
    }

    public function agentList()
    {
        return $this->account->agentList($this->req);
    }

    public function agentChangeStatus()
    {
        return $this->account->agentChangeStatus($this->req);
    }

    public function agentCount()
    {
        return $this->account->agentCount();
    }

    public function staffChangeStatus()
    {
        return $this->account->staffChangeStatus($this->req);
    }

    public function addFcmToken()
    {
        return $this->account->addFcmToken($this->req);
    }

    public function getFcmToken()
    {
        return $this->account->getFcmToken($this->req);
    }
}
