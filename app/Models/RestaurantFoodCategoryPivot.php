<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantFoodCategoryPivot extends Model
{
    protected $table = 'restaurant_food_category_pivot';
    protected $fillable = ['restaurant_id', 'food_category_id'];
    protected $append = ['hashid'];

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }
}
