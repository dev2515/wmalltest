<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use App\Model\StoreBranch;
use App\Http\Resources\User\Vendor;
use App\Http\Resources\Images\CommercialPermitCollection;
use App\Http\Resources\Images\CommercialRegistrationCollection;
use App\Http\Resources\Images\ComputerCardCollection;
use App\Http\Resources\Images\SignatureCollection;
use App\Http\Resources\Images\SponsorQidCollection;
use Carbon\Carbon;
use App\Models\StoreLike;
use App\Models\StoreFollowing;
class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hashid'                    => $this->hashid,
            'is_actived'                => $this->is_active,
            'is_main_branch'            => $this->is_main_branch,
            'store_name'                => $this->name,
            'store_description'         => $this->store_information,
            'business_type'             => $this->businessType ? $this->businessType->name : null,
            'store_phone_number'        => $this->phone,
            'store_email'               => $this->store_email,
            'street'                    => $this->street,
            'latitude'                  => $this->latitude,
            'longitude'                 => $this->longitude,
            'state'                     => $this->state,
            'city'                      => $this->city,
            'other_address_info'        => $this->other_address_info,
            'vendor'                    => new Vendor($this->owner),
            'status_info'               => $this->status_info,
            'ratings'                   => $this->ratings,
            'timings'                   => $this->is_opened,
            'themes'                    => $this->theme(),
            'services'                  => $this->when($this->businessType->name == 'restaurant', new RestaurantServiceCollection($this->restaurantServices)),
            'total_staff'               => $this->total_staff,
            'total_product'             => $this->products->count(),
            'total_foods'               => $this->total_foods,
            'total_branch'              => $this->totalBranch(),
            'total_like'                => $this->likes->count(),
            'total_following'           => $this->follows->count(),
            'is_liked'                  => $this->when(auth()->user() ? auth()->user()->type_info === 'customer' : false, $this->isLiked()),
            'is_followed'               => $this->when(auth()->user() ? auth()->user()->type_info === 'customer' : false, $this->isFollowed()),
            'main_branch_hashid'        => $this->mainBranch(),
            'timestamp'                 =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'profile_photo_full'        => asset($this->local('/profile-photo')->full),
            'profile_photo_thumbnail'   => asset($this->local('/profile-photo')->thumbnail),
            'cover_photo_full'          => $this->local('/cover-photo')->full ? asset($this->local('/cover-photo')->full) : null,
            'cover_photo_thumbnail'     => $this->local('/cover-photo')->thumbnail ? asset($this->local('/cover-photo')->thumbnail) : null,
        ];
    }

    public function local($type)
    {
        $full = public_path().'/images/stores/' . $this->hashid . $type .'-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $this->hashid . $type .'-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . $this->hashid . $type .'-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . $this->hashid . $type .'-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }

    public function theme()
    {
        $data           = new stdClass;
        $data->id       = $this->theme->id;
        $data->name     = $this->theme->name;
        $data->color1   = $this->theme->codes[0];
        $data->color2   = $this->theme->codes[1];
        $data->color3   = $this->theme->codes[2];
        $data->color4   = $this->theme->codes[3];

        return $data;
    }

    public function totalBranch()
    {
        $data = StoreBranch::where('store_id', $this->id)->count();
        return $data;
    }

    public function mainBranch()
    {
        $main_branch = StoreBranch::where('store_id', $this->id)->where('is_main', 1)->first();
        if(!$main_branch) return null;

        return $main_branch->hashid;
    }

    public function isLiked()
    {
       $storeLiked = StoreLike::where('store_id', $this->id)->where('user_id', auth()->id())->where('is_liked', 1)->first();
       return $storeLiked ? 1 : 0;
    }

    public function isFollowed()
    {
       $storeFollowed = StoreFollowing::where('store_id', $this->id)->where('user_id', auth()->id())->where('is_followed', 1)->first();
       return $storeFollowed ? 1 : 0;
    }
}
