<?php

namespace App\Interfaces;

interface CustomerInterface
{
    public function list($req);

    public function show($req);

    public function listOfAllCustomer($req);

    public function brandList($req);

    public function brandListByName($req);

    public function brandSort($req);

    public function totalBrands();

    public function brandSearch($req);

    public function productList($req);

    public function productSearch($req);

    public function productSort($req);

    public function totalProducts();

    public function productListBytags($req);

    public function restaurantList($req);

    public function categoryList();
    
    public function customerAccount($req);

    public function orderList($req);

    public function orderShow($req);
    
    public function addAddress($req);

    public function addresses();

    public function showAddress($req);

    public function updateAddress($req);

    public function deleteAddress($req);

    public function orderStore($req);

    public function storeFollowing($req);

    public function isFollow($req);

    public function storefollowed($req);

    public function brandListPremium($req);

    public function addStoreRating($req);

    public function removeRating($req);

    public function addToWishList($req);

    public function wishLists($req);

    public function addressTypes();
}
