<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Stores\StoreList;
use App\Http\Resources\Stores\StoreListCollection;
use App\Models\Store;
use App\Models\StoreStaff;

use stdClass;
use Carbon\Carbon;
class Staff extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'id' => $this->hash_id,
            'name' => $this->name,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'status' => $this->status(),
            'role' => 'staff',
            //'timestamp' =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'store_working' => $this->store(),
            'full' => asset($this->profilePhoto()->full),
            'thumbnail' => asset($this->profilePhoto()->thumbnail),
        ];
    }

    public function store()
    {
        
        $ss = StoreStaff::where('user_id', $this->id)->where('status', 1)->first();
        if(!$ss) return 'Invalid Store';
        
        $store = Store::where('id', $ss->store_id)->first();
        if(!$store) return 'Invalid Store';

        $data = new stdClass;
        $data->hashid = $store->hash_id;
        $data->name = $store->name;
        
        return $data;
    }

    public function profilePhoto()
    {
        $full = public_path() . '/images/users/' . $this->hash_id . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . $this->hash_id . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . $this->hash_id . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . $this->hash_id . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }

    public function status()
    {
        if($this->status === 1 ) return 'active';

        if($this->status === 0 ) return 'inactive';
    }
}
