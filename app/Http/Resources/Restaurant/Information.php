<?php

namespace App\Http\Resources\Restaurant;

use App\Model\StoreBranch;
use Illuminate\Http\Resources\Json\JsonResource;

class Information extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'            => $this->hashid,
            'name'              => $this->name,
            'business_type_id'  => $this->business_type_id,
            'store_email'       => $this->store_email,
            'store_information' => $this->store_information,
            'order_duration'    => $this->order_duration,
            'phone'             => $this->phone,
            'ratings'           => $this->ratings,
            'followers'         => $this->followers,
            'branches'          => $this->branches(),
        ];
    }

    public function branches()
    {
        $b = StoreBranch::select('id','store_id','name','contact_name','street','city','other_address_info','contact_email','latitude','longitude','is_main')
            ->where('store_id', $this->id)->first();

        if($b) $b->makeHidden(['id','store_id','is_main']);
        
        if($b->is_main == 0) {return $b;}
        else{return 'Main branch';}
    
    }
}
