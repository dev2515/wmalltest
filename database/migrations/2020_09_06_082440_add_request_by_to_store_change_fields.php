<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestByToStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_change_fields', 'request_by')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->integer('request_by')->after('is_declined')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'request_by')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('request_by');
            });
        }
    }
}
