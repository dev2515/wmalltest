<?php

namespace App\Http\Resources\Message;

use Illuminate\Http\Resources\Json\JsonResource;

class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'    => $this->hashid,
            'type'      => $this->type,
            'message'   => $this->message(),
            'sent_at'   => $this->created_at,
            'is_from_user' => $this->is_from_user,
            'is_from_store' => $this->is_from_store,
            'is_seen'   => $this->is_new === 1 ? 0 : 1,
        ];
    }

    public function message()
    {
        switch ($this->type){
            case 'message' :
                return $this->text;
            break;

            case 'product' :
                $product = \App\Models\Product::where('id', $this->product_id)->first();
                if(!$product) return null;

                $full = public_path().'/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-full.png';
                if(file_exists($full)){
                    $full = '/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-full.png';
                } else { $full = null; }

                $thumbnail  = public_path().'/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-thumbnail.png';
                if(file_exists($thumbnail)){
                    $thumbnail  = '/images/products/' . encode($product->id, 'uuid') . '/' . encode($product->images[0]->id, 'uuid') .'-thumbnail.png';
                } else { $thumbnail = null; }

                $data = new \stdClass;
                $data->name = $product->name;
                $data->price = $product->price;
                $data->image_full = asset($full);
                $data->image_thumb = asset($thumbnail);

                return $data;
            break;

            case 'order' :
                return 'WIP';
            break;

            case 'photo' :
                return 'WIP';
            break;

            default :
                return 'Invalid type';
            break;
        }
    }
}
