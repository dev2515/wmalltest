<?php

namespace App\Http\Resources\Blogger;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use stdClass;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'hash_id'                   => encode($this->category_id,'uuid'),
            'name'                      => $this->name,
            'full'                      => asset($this->photo()->full),
            'thumbnail'                 => asset($this->photo()->thumbnail),
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/blogger/' . encode($this->store_id, 'uuid'). '/'. encode($this->category_id, 'uuid') . '-full.png';
        if(file_exists($full)){
            $full = '/images/blogger/' . encode($this->store_id, 'uuid'). '/'. encode($this->category_id, 'uuid') .  '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/blogger/' . encode($this->store_id, 'uuid'). '/'. encode($this->category_id, 'uuid') .  '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/blogger/' . encode($this->store_id, 'uuid'). '/'. encode($this->category_id, 'uuid') .  '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }  
}
