<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('restaurant_orders')){
            Schema::create('restaurant_orders', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('customer_id');
                $table->string('order_number');
                $table->longtext('qrcode');
                $table->longtext('qrcode_image');
                $table->double('sub_total');
                $table->double('shipping_rate');
                $table->text('billing_address');
                $table->text('shipping_address');
                $table->text('billing_address_coordinates');
                $table->text('shipping_address_coordinates');
                $table->text('payment_method_id');
                $table->integer('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_orders');
    }
}
