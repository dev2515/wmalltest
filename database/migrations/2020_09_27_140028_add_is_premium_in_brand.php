<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPremiumInBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('brands', 'is_premium')){
            Schema::table('brands', function (Blueprint $table) {
                $table->boolean('is_premium')->nullable()->after('profile_base64');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('brands', 'is_premium')){
            Schema::table('brands', function (Blueprint $table) {
                $table->dropColumn('is_premium');
            });
        }
    }
}
