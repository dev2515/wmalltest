<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Store;
use App\Models\RestaurantService;
use App\Models\RestaurantServicePivot;
use App\Models\BusinessType;

class AddDefaultRestaurantServicesToStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'old-store-add:restaurant-services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add default restaurant services to old store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $business_type = BusinessType::where('name', 'restaurant')->first();
        if(!$business_type) {
            $this->info('Business type not found');
            return false;
        }

        $stores = Store::where('business_type_id', $business_type->id)->get();
        $bar = $this->output->createProgressBar(count($stores));
        $bar->start();
        foreach ($stores as $store){
            $restaurant_services_ids = RestaurantService::pluck('id');
            foreach ($restaurant_services_ids as $id){
                $check = RestaurantServicePivot::where('store_id', $store->id)->where('restaurant_service_id', $id)->first();
                if(!$check){
                    $add_default = new RestaurantServicePivot;
                    $add_default->store_id = $store->id;
                    $add_default->restaurant_service_id = $id;
                    $add_default->save();
                }
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
