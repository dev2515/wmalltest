<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuickDetailsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('products', 'quick_details')){
            Schema::table('products', function (Blueprint $table) {
                $table->json('quick_details')->after('short_description')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('products', 'quick_details')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('quick_details');
            });
        }
    }
}
