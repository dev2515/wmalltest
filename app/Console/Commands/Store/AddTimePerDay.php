<?php

namespace App\Console\Commands\Store;

use Illuminate\Console\Command;
use App\Models\StoreSchedule;
use App\Models\StoreTiming;
use App\Models\Store;
use Carbon\Carbon;

class AddTimePerDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:time-to-store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stores = Store::get();

        $bar = $this->output->createProgressBar(count($stores));
        $bar->start();

        foreach ($stores as $store){
            $store_schedule_ids = StoreSchedule::where('store_id', $store->id)->pluck('id');
            foreach ($store_schedule_ids as $id){
                $check = StoreTiming::where('store_schedule_id', $id)->where('store_open', Carbon::now()->format('Y-m-d 09:00'))->where('store_close', Carbon::now()->format('Y-m-d 19:00'))->first();
                if(!$check){
                    $add_time = new StoreTiming();
                    $add_time->store_schedule_id = $id;
                    $add_time->store_open   = Carbon::now()->format('Y-m-d 09:00');
                    $add_time->store_close  = Carbon::now()->format('Y-m-d 19:00');
                    $add_time->save();
                }
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
