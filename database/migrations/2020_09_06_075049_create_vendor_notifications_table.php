<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vendor_notifications')){
            Schema::create('vendor_notifications', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('notify_to_user_id');
                $table->string('message');
                $table->integer('change_field_id');
                $table->string('status_type');
                $table->smallInteger('is_new')->default(1);
                $table->smallInteger('is_seen')->default(0);
                $table->smallInteger('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_notifications');
    }
}
