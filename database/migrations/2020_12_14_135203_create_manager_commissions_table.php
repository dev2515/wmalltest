<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('manager_commissions')){
            Schema::create('manager_commissions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('store_id');
                $table->integer('percentage');
                $table->integer('store_percentage');
                $table->integer('commission');
                $table->unsignedBigInteger('order_id');
                $table->boolean('status');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('manager_commissions')){
            Schema::dropIfExists('manager_commissions');
        }
        
    }
}
