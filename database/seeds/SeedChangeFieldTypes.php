<?php

use Illuminate\Database\Seeder;
use App\Models\ChangeFieldType;

class SeedChangeFieldTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = ChangeFieldType::count();
        if($count == 0){
            $path = public_path('data/change-field-types.json');
            $json = file_get_contents($path);
            $json = json_decode($json);

            foreach ($json->types as $type){
                ChangeFieldType::firstOrCreate(['name' => ucwords($type), 'value' => $type]);
            }
        }
    }
}
