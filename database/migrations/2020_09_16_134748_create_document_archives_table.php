<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('document_archives')){
            Schema::create('document_archives', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('store_id');
                $table->integer('user_id');
                $table->string('document_type');
                $table->string('file_type');
                $table->string('file_name')->nullable();
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_archives');
    }
}
