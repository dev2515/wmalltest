<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceToRestaurantFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_foods', 'price')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->integer('price')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_foods', 'price')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->double('price')->change();
            });
        }
    }
}
