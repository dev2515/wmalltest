<?php

namespace App\Interfaces;

interface CategoryInterface
{
    public function top($req);

    public function list($req);
}
