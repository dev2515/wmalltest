<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFoodStatusInRestaurantCartItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_cart_items', 'food_status')){
            Schema::table('restaurant_cart_items', function (Blueprint $table) {
                $table->integer('food_status')->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('restaurant_cart_items', 'food_status')){
            Schema::table('restaurant_cart_items', function (Blueprint $table) {
                $table->dropColumn('food_status');
            });
        }
    }
}
