<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $appends = ['hashid'];
    protected $hidden  = ['created_at', 'updated_at', 'id'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
