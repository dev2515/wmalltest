<?php

namespace App\Http\Controllers\API\Shipping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ShippingInterface;

class ShippingController extends Controller
{
    protected $req;
    protected $shipping;

    public function __construct(Request $req, ShippingInterface $shipping)
    {
        $this->req = $req;
        $this->shipping = $shipping;
    }

    public function list()
    {
        return $this->shipping->getProviders($this->req);
    }

    public function categories()
    {
        return $this->shipping->categories($this->req);
    }
}
