<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcessByToStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasCOlumn('store_branches', 'process_by')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->integer('process_by')->after('is_declined');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasCOlumn('store_branches', 'process_by')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('process_by');
            });
        }
    }
}
