<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderStatusToQRPointers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('q_r_pointers', 'order_status')){
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->integer('order_status')->nullable()->default(6)->after('transaction_status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('q_r_pointers', 'order_status')){
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->dropColumn('order_status');
            });
        }
    }
}
