<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreTiming extends Model
{
    protected $table    = 'store_timings';
    protected $fillable = ['store_schedule_id', 'name', 'store_open', 'store_close' ,'is_opened'];
    protected $append   = ['hash_id'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store_schedule_id');
    }
}
