<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    protected $table = 'activity_types';
    protected $fillable = ['type', 'description'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function activities()
    {
        return $this->hasMany(\App\Models\Activity::class, 'type_id');
    }
}
