<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameStoreIdInStoreTimings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumns('store_timings', ['store_id', 'name'])){
            Schema::table('store_timings', function (Blueprint $table) {
                $table->renameColumn('store_id', 'store_schedule_id');
                $table->string('name')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('store_timings', ['store_schedule_id', 'name'])){
            Schema::table('store_timings', function (Blueprint $table) {
                $table->renameColumn('store_schedule_id', 'store_id');
            });
        }
    }
}
