<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QRPointerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => encode($this->id, 'uuid'),
            'transaction_type' => $this->transctionType->name,
            'business_type' => $this->businessType->name,
            'transaction_number' => $this->transaction_number,
            'transaction_status' => $this->status($this->transaction_status),
            'transaction' => null, // TODO: new CartResource($this->transaction)
        ];
    }

    private function status($int)
    {
        // TODO: Complete the Status
        switch ($int) {
            case 0:
                return 'pending';
            default:
                return 'pending';
        }
    }
}
