<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('table_reservations')){
            Schema::create('table_reservations', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('restaurant_id');
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('table_id');
                $table->string('note')->nullable();
                $table->integer('arrive_time');
                $table->integer('arrive_date');
                $table->integer('guest'); //number of guess
                $table->integer('order')->default(1); //if with order
                $table->integer('status')->default(1); //[1] open [2] reserve [3] cancelled [expired]
                $table->longText('qrcode');
                $table->longText('qrcode_image');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_reservations');
    }
}
