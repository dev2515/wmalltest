<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsActiveToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'is_active')){
            Schema::table('stores', function (Blueprint $table) {
                $table->boolean('is_active')->default(1)->after('is_declined');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'is_active')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('is_active');
            });
        }
    }
}
