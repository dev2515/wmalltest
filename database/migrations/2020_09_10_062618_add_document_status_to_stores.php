<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentStatusToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'computer_card_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('computer_card_status')->default('pending')->after('ban');
            });
        }

        if(!Schema::hasColumn('stores', 'commercial_registration_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('commercial_registration_status')->default('pending')->after('computer_card_status');
            });
        }

        if(!Schema::hasColumn('stores', 'commercial_permit_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('commercial_permit_status')->default('pending')->after('commercial_registration_status');
            });
        }

        if(!Schema::hasColumn('stores', 'sponsor_qid_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('sponsor_qid_status')->default('pending')->after('commercial_permit_status');
            });
        }

        if(!Schema::hasColumn('stores', 'signature_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->string('signature_status')->default('pending')->after('sponsor_qid_status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'computer_card_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('computer_card_status');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_registration_status');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_permit_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_permit_status');
            });
        }

        if(Schema::hasColumn('stores', 'sponsor_qid_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('sponsor_qid_status');
            });
        }

        if(Schema::hasColumn('stores', 'signature_status')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_status');
            });
        }
    }
}
