<?php

namespace App\Repositories;

use App\Interfaces\FoodCategoryInterface;
use App\Models\RestaurantFoodCategoryPivot;
use App\Models\FoodCategory;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class FoodCategoryRepository implements FoodCategoryInterface
{
    
    public function addMyFoodType($req)
    {
        $validator = Validator::make($req->all(),[
            'name' => 'required',
            'restaurant_hashid' => 'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $restaurant_id = decode($req->restaurant_hashid, 'uuid');

        $ifExist = RestaurantFoodCategoryPivot::where('restaurant_id', $restaurant_id)
        ->where('food_category_name', $req->name)->first();
        if($ifExist)  return res('category already exist.', null, 402);

        $food_type = new RestaurantFoodCategoryPivot;
        $food_type->restaurant_id = $restaurant_id;
        $food_type->food_category_name = $req->name;
        $food_type->food_type_image = $req->food_type_image;

        $food_type->save();

        $category = FoodCategory::where('name', $food_type->food_category_name)->first();

            if(!$category){

            $cat = new FoodCategory;
            $cat->restaurant_id = $food_type->restaurant_id;
            $cat->name = $food_type->food_category_name;
            $cat->save();
            }

        if($food_type){

            $local_path = public_path() . '/images/food_type/';

            if(!file_exists($local_path)) {
                File::makeDirectory(base_path() . '/public/images/food_type', $mode = 0777, true, true);
            }

            $image = Image::make($food_type->food_type_image);
            $image->save($local_path . encode($food_type->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($food_type->id, 'uuid') . '-thumbnail.png');
        }

        return res('success');
    }

    public function updateMyFoodType($req)
    {
        $validator = Validator::make($req->all(),[
            'name' => 'required',
            'my_food_type_hashid' => 'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $food_type_id = decode($req->my_food_type_hashid, 'uuid');

        $food_type = RestaurantFoodCategoryPivot::where('id', $food_type_id)->first();
        if(!$food_type)  return res('category not found.', null, 402);

        if($req->name) $food_type->food_category_name = $req->name;
        if($req->food_type_image) { 
            $food_type->food_type_image = $req->food_type_image;

            $full = public_path() . '/images/food_type/' . $food_type->hashid . '-full.png';
            $thumbnail = public_path() . '/images/food_type/' . $food_type->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/food_type/';

            $image = Image::make($food_type->food_type_image);
            $image->save($local_path . encode($food_type->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($food_type->id, 'uuid') . '-thumbnail.png');

           
        }
        $food_type->save();

        return res('success');
    }
   
}