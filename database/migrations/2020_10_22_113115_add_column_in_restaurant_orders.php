<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInRestaurantOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_orders','table_id')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->string('table_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_orders','table_id')){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->dropColumn('table_id');
            });
        }
    }
}
