<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductIdInStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_change_fields', 'product_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->integer('product_id')->nullable()->after('brand_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'product_id')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('product_id');
            });
        }
    }
}
