<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use stdClass;

class TableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'hash_id' =>                    $this->hash_id,
            'name' =>                       $this->name,
            'description' =>                $this->description,
            'floor' =>                      $this->floor,
            'x_axis' =>                     $this->x_axis,
            'y_axis' =>                     $this->y_axis,
            'angle' =>                      $this->angle,
            'flr_animation' =>              asset($this->getFLR()->full),
        ];
    }

    public function getFLR()
    {      
        $full = public_path() . '/storage/flr/' . $this->hash_id . '/'.$this->table_bg;
        if(file_exists($full)){
            $full = '/storage/flr/' . $this->hash_id . '/'.$this->table_bg;
        } else { $full = null;}

        $data = new stdClass;
        $data->full = asset($full);

        return $data;
    }
}

