<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsInCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('carts', 'reservation_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->integer('reservation_id')->default(0)->change();
            });
        }

        if(Schema::hasColumn('carts', 'reservation_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->renameColumn('reservation_id', 'has_booking');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('carts', 'reservation_id')){
            Schema::table('carts', function (Blueprint $table) {
                $table->unsignedBigInteger('reservation_id')->change();
            });
        }

        if(Schema::hasColumn('carts', 'has_booking')){
            Schema::table('carts', function (Blueprint $table) {
                $table->renameColumn('has_booking', 'reservation_id');
            });
        }
    }
}
