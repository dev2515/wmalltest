<?php

namespace App\Repositories;

use App\Models\Inventory;
use App\Interfaces\InventoryInterface;
use Illuminate\Support\Facades\Validator;

class InventoryRepository implements InventoryInterface
{
    public function add($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'quantity' => 'required|min:1|numeric'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        if ($req->quantity <= 0) {
            return res(__('product.invalid_quantity'), null, 400);
        }

        $inventory = new Inventory();
        $inventory->product_id = $req->product_id;
        $inventory->actor_id = auth()->user()->id;
        $inventory->quantity = $req->quantity;
        $inventory->remarks = $req->remarks ?? 'Added to inventory';
        $inventory->save();

        return res('Success', $inventory, 201);
    }

    public function minus($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'quantity' => 'required|min:1|numeric'
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        if ($req->quantity <= 0) {
            return res(__('product.invalid_quantity'), null, 400);
        }

        $inventory = new Inventory();
        $inventory->product_id = $req->product_id;
        $inventory->actor_id = auth()->user()->id;
        $inventory->quantity = ($req->quantity * -1);
        $inventory->remarks = $req->remarks ?? 'Deducted from inventory';
        $inventory->save();

        return res('Success', $inventory, 201);
    }

    public function list($req)
    {
        if (!$req->has('product_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $list = Inventory::where('product_id', $req->product_id)->where('status', 1)->get();

        return res('Success', $list);
    }
}
