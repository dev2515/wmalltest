<?php

namespace App\Listeners;

use App\Events\AddDriver;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DriverLicense;

class AddDriverLicenseListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddDriver  $event
     * @return void
     */
    public function handle(AddDriver $event)
    {
        $info = auth()->user()->type_info;
        $data = $event->data;

        $license                        = new DriverLicense;
        $license->user_id               = $data['user']['id'];
        $license->requested_by          = auth()->id();
        $license->processed_by          = $info == 'administrator' ? auth()->id() : 0;
        $license->license_number        = $data['license_number'];
        $license->expired_at            = $data['license_expiration_date'];
        $license->file_type             = 'N/A';
        $license->image                 = 'N/A';
        $license->file_name             = 'N/A';
        $license->is_approved           = 1;
        $license->is_declined           = 0;
        $license->status                = 1;
        $license->save();
    }
}
