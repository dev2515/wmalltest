<?php

namespace App\Interfaces;

interface ProductInterface
{
    public function list($req);

    public function open_product_list($req);

    public function show($req);

    public function add($req);

    public function update($req);

    public function uploadImage($req);

    public function types($req);

    public function stockStatus($req);

    public function mostPopular($req);

    public function uploadImages($req);

    public function getProductPhotos($req);

    public function toggleToFavorite($req);

    public function details($req);

    public function items($req);

    public function updateStatus($req);

    public function customerReviews($req);

    public function searchProduct($req);

    public function listByTags($req);

    public function listByCategory($req);

    public function listByName($req);

    public function listByPrice($req);

    public function listBySort();

    public function listOfTags($req);

    public function addBrand($req);

    public function updateBrand($req);

    public function listBrand($req);

    public function searchBrand($req);

    public function brandListByName($req);

    public function brandListBySort();

    public function listByStatus($req);

    public function totalBrands();
    
    public function getBrands();

    public function addCategory($req);

    public function updateCategory($req);

    public function deleteCategory($req);

    public function searchTags($req);

    public function updateTags($req);

    public function addTags($req);

    public function brandShow($req);

    public function totalProducts($req);

    public function productChangesStatus($req);

    public function brandChangeStatus($req);

    public function productShowInOpenAPI($req);

    public function searchProductImage($req);

    public function productTypeList();

    public function add_image_product($req);
}
