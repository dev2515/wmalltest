<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use App\Models\Store;
use App\Models\StoreStaff;
use Carbon\Carbon;
class Agent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hashid' => $this->hash_id,
            'name' => $this->name,
            'email' => $this->email,
            'mobile_number' => $this->mobile,
            'status' => $this->status ? 'active' : 'inactive',
            'timestamp' =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'full' => asset($this->profilePhoto()->full),
            'thumbnail' => asset($this->profilePhoto()->thumbnail),
        ];
    }

    public function profilePhoto()
    {
        $full = public_path() . '/images/users/' . $this->hash_id . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . $this->hash_id . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . $this->hash_id . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . $this->hash_id . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }
}
