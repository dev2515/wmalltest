<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDoubleIntoIntergerRestaurantOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumns('restaurant_orders', ['sub_total','shipping_rate'])){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->integer('sub_total')->change();
                $table->integer('shipping_rate')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('restaurant_orders', ['sub_total','shipping_rate'])){
            Schema::table('restaurant_orders', function (Blueprint $table) {
                $table->double('sub_total')->change();
                $table->double('shipping_rate')->change();
            });
        }
    }
}
