<?php

namespace App\Repositories;

use App\Interfaces\PaymentMethodInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\User;

use App\Models\UserPaymentMethod;


class PaymentMethodRepository implements PaymentMethodInterface
{
    public function generateToken()
    {
        $token   = braintree()->ClientToken()->generate();
        
        return res('success', $token, 200);
    }

    public function register($req)
    {
        if(!$req->user_hashid) return res(__('user.required_hashid'), null, 401);

        $response = braintree()->customer()->create([
            'id'  => decode($req->user_hashid, 'uuid'),
        ]);

        return res('success', $response, 200);
    }

    public function checkout($req)
    {
        $customer  = Auth::user();

        $names = explode(" ", $customer->name);

        DB::beginTransaction();
        $validator = Validator::make($req->all(), [
            'amount'                => 'required',
            'payment_method_nonce'  => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $payment_method_nonce = $req->payment_method_nonce;


        if(!$customer->braintree_token){
            $gateway    = braintree()->paymentMethod()->create([
                'customerId'            => $customer->id,
                'paymentMethodNonce'    => $payment_method_nonce,
            ]);
    
            if($gateway->success === true){
                $customer->braintree_token   = $gateway->paymentMethod->token;
                $customer->save();
            }
        }

        $client_nonce = braintree()->paymentMethodNonce()->create($customer->braintree_token);
        $result = braintree()->transaction()->sale([
            'amount'                => $req->amount,
            'customer'              => [
                'firstName' => $names[0],
                'lastName'  => end($names),
            ],
            'paymentMethodNonce'    => $client_nonce->paymentMethodNonce->nonce,
            'options'               => [
                'submitForSettlement' => true
              ]
        ]);

        if($result->success) {
            DB::commit();
            return res(__('payment.success'), $result, 200);
        }

        $errorString = '';

        foreach ($result->errors->deepAll() as $error) {
            $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
        }

        return res('failed', 'An error occurred with the message: ' . $result->message, 401);
        
    }

    public function savedCardInfo($req)
    {
        $user_id = Auth::user()->id;

        $validator = Validator::make($req->all(), [
            'card_number'   => 'integer|min:16',
            'name_on_card'  => 'required',
            'expiration'    => 'required',
            'cvv'           => 'integer|min:3',
        ]);
        
        // TODO: Request data need decryption this data to avoid data leak.
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $check = UserPaymentMethod::where('user_id', $user_id)->where('card_number', $req->card_number)->where('status', 1)->first();

        if($check) return res(__('card.already_saved'), null, 401);

        $add_card   = new UserPaymentMethod();
        $add_card->user_id      = $user_id;
        $add_card->card_number  = $req->card_number;
        $add_card->name_on_card = $req->name_on_card;
        $add_card->expiration   = $req->expiration;
        $add_card->cvv          = $req->cvv;
        $add_card->braintree_token          = null;
        $add_card->save();

        return res('success', null, 201);
    }
}