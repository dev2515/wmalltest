<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityImage extends Model
{
    protected $table = 'activity_images';
    protected $fillable = ['activity_id', 'filename', 'activty_image'];

    public function activity()
    {
        return $this->belongsTo(\App\Models\Activity::class, 'activity_id');
    }
}
