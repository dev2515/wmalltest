<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTotalNetFromDriverSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('driver_settings','total_net')){
            Schema::table('driver_settings', function (Blueprint $table) {
                $table->dropColumn('total_net');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('driver_settings','total_net')){
            Schema::table('driver_settings', function (Blueprint $table) {
                $table->integer('total_net');
            });
        }
    }
}
