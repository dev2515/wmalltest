<?php

namespace App\Repositories;

use App\User;
use App\Models\Store;
use App\Models\VendorType;
use App\Models\StoreBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Interfaces\VendorInterface;
use App\Models\SecurityQuestionEntry;
use App\Repositories\AddressRepository;
use App\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Events\VendorImagesEvent;
use App\Http\Resources\Stores\StoreCollection;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use stdClass;
use App\Http\Resources\Stores\StoreListCollection;
use App\Http\Resources\Stores\Store as StoreResource;
use App\Models\ComputerCardImage;
use App\Models\StoreStaff;
use App\Http\Resources\User\UserData;
use App\Http\Resources\User\UserDataCollection;
use App\Models\StoreTheme;
use App\Models\RestaurantFoodCategoryPivot;
use App\Http\Resources\Stores\ThemeCollection;
use App\Http\Resources\Stores\Theme;
use App\Http\Resources\User\Staff;
use App\Http\Resources\User\StaffCollection;
use App\Models\StoreChangeField;
use App\Http\Resources\MyStore;
use App\Model\StoreBranch;
use App\Models\RestaurantFood;
use App\Models\FoodCategory;
use App\Http\Resources\Images\CommercialPermitCollection;
use App\Http\Resources\Images\CommercialRegistrationCollection;
use App\Http\Resources\Images\ComputerCardCollection;
use App\Http\Resources\Images\SponsorQidCollection;
use App\Models\VendorNotification;
use App\Http\Resources\Notification\NotificationCollection;
use App\Models\StoreRequestImages as StoreRequestImage;
use App\Http\Resources\Stores\BranchShow;
use App\Http\Resources\Stores\BranchList as BranchResource;
use App\Http\Resources\Stores\BranchListCollection;
use App\Http\Resources\Images\Signature;
use App\Http\Resources\Images\SignatureCollection;
use App\Http\Resources\Stores\StoreChangeFieldCollection;
use App\Models\DocumentArchive;
use App\Http\Resources\MyStoreCollection;
use App\Http\Resources\Stores\VendorWithdrawal;
use App\Http\Resources\Stores\VendorWithdrawalCollection;
use App\Models\BusinessType;
use App\Models\StoreLike;
use App\Models\StoreFollowing;
use App\Models\Meal;
use App\Models\State;
use App\Models\StorePreregister;
use App\Models\VendorRequestWithdrawal;
use Illuminate\Support\Facades\Mail;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class VendorRepository implements VendorInterface
{
    public function register($req)
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin or agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'is_main_branch'        => 'required|boolean',
            'store_name'            => 'required|unique:stores,name',
            'store_phone_number'    => 'required',
            'store_email'           => 'required',
            'business_type_id'      => 'required',

            'vendor_name'           => 'required',
            'vendor_email'          => 'required',
            'vendor_mobile'         => 'required',


            'street'                => 'required',
            'city'                  => 'required',
            'state'                 => 'required',
            'other_address_info'    => 'required',

            'profile_photo_img'     => 'required',
            'cover_photo_img'       => 'required'

        ], [
            'name.unique'           => 'The Store Name has already been taken',
        ]);

        $validator->sometimes('main_store_hashid', 'required|string', function ($input) {
            return $input->is_main_branch == 0;
        });

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $main_store_id  = decode($req->main_store_hashid, 'uuid');
        $main_branch    = Store::where('id', $main_store_id)->where('is_main_branch', 1)->first();
        if($req->is_main_branch == 0 && !$main_branch){
            DB::rollBack();
            return res(__('store.main_branch.not_found'), null, 412);
        }

        $store                      = new Store;
        $store->is_main_branch      = $req->is_main_branch ? 1 : 0;
        $store->name                = $req->store_name;
        $store->phone               = $req->store_phone_number;
        $store->store_email         = $req->store_email;
        $store->paypal_email        = '';
        $store->business_type_id    = $req->business_type_id;

        $store->street              = $req->street;
        $store->city                = $req->city;
        $store->state               = $req->state;
        $store->other_address_info  = $req->other_address_info;
        $store->latitude            = $req->latitude ? $req->latitude : null;
        $store->longitude           = $req->longitude ? $req->longitude : null;
        
        $theme  = StoreTheme::where('name', 'default')->first();

        if(!$theme) {
            DB::rollBack();
            return res('Theme not found', null, 401);
        }

        $store->theme_id            = $theme->id;
        $store->country             = 'qat';
        $store->zip                 = '';
        $store->company_type        = '';
        $store->established_type    = '';
        $store->date_established    = '';
        $store->sell_on_country     = '';

        //-- Vendor Credential images
        if($req->has('profile_photo_img')) $store->profile_photo_img = $req->profile_photo_img;
        if($req->has('cover_photo_img')) $store->cover_photo_img = $req->cover_photo_img;
        //--
        
        $password = explode("@", $req->vendor_email);
        if (strlen($password[0]) < 6) { 
            $password = $password[0].generateMobileToken();
        }

        $user = User::where('email', $req->vendor_email)->first();

        if($user){
            $store->user_id         = $user->id;
            $store->agent_id        = auth()->id(); // process by agent who created vendor
            $store->po_box          = '';
            $store->zone_number     = null;
            $store->store_branches  = $req->branches;
            $store->save();

            //-- Creating branch as main branch
            $this->createBranch($req->is_main_branch, $main_branch->id, $store, $user);

            //-- Creating local storage for vendor images
            event(new VendorImagesEvent($store));
            //

            activityLog('Create store' . $store->id);
            DB::commit();
            return res('Success', $store->hashid, 200);
        }

        $user                   = new User;
        $user->name             = $req->vendor_name;
        $user->profile_photo    = $req->profile_photo_img;
        $user->email            = $req->vendor_email;
        $user->password         = bcrypt($password[0]);
        $user->mobile           = $req->vendor_mobile;
        $user->iso              = session('iso', getIso());
        $user->type             = 2;
        $user->status           = 1;
        $user->save();

        $store->user_id         = $user->id;
        $store->agent_id        = auth()->id(); // process by agent who created vendor
        $store->po_box          = '';
        $store->zone_number     = null;
        $store->save();

    
        $this->createBranch($req->is_main_branch, $main_branch->id, $store, $user);
        event(new VendorImagesEvent($store));

        activityLog('Create store' . $store->id);
        DB::commit();

        return res('Success', $store->hashid, 200);
    }

    public function createBranch($is_main_branch, $main_branch_id, $store, $user)
    {
        $branch                     = new StoreBranch;
        $branch->store_id           = $is_main_branch ? $store->id : $main_branch_id;
        $branch->store_child_id     = $is_main_branch ? 0 : $store->id;
        $branch->name               = $is_main_branch ? 'Main Branch' : $store->name;
        $branch->contact_name       = $user->name;
        $branch->street             = $store->street;
        $branch->state              = $store->state;
        $branch->city               = $store->city;
        $branch->other_address_info = $store->other_address_info;
        $branch->contact_number     = $user->mobile;
        $branch->contact_email      = $user->email;
        $branch->latitude           = $store->latitude ? $store->latitude : null;
        $branch->longitude          = $store->longitude ? $store->longitude : null;
        $branch->is_approved        = 1;
        $branch->is_declined        = 0;
        $branch->process_by         = auth()->id();
        $branch->is_main            = $is_main_branch ? 1 : 0;
        $branch->status             = 1;
        $branch->save();
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addBranches($req)
    {

        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid'  => 'required',
            'name'          => 'required|unique:brands,name',
        ], [
            'name.unique' => 'The Brand Name has already been taken',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        if(!$req->store_hashid) return res('Store hashid is required', null, 401);

        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        $scf = StoreChangeField::where('type', 'add branches')
        ->where('store_id', $store->id)
            ->where('is_approved', 0)->first();

        $new_branch                     = new StoreBranch;
        $new_branch->store_id           = $store->id;
        $new_branch->name               = $req->name;
        $new_branch->contact_name       = $req->contact_name;
        $new_branch->street             = $req->street;
        $new_branch->state              = $req->state;
        $new_branch->city               = $req->city;
        $new_branch->other_address_info = $req->other_address_info;
        $new_branch->contact_number     = $req->contact_number;
        $new_branch->contact_email      = $req->contact_email;
        $new_branch->latitude           = $req->latitude;
        $new_branch->longitude          = $req->longitude;
        $new_branch->is_approved        = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new_branch->process_by         = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $new_branch->status             = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new_branch->is_declined        = 0;
        $new_branch->save();

        if(!$scf){
            $new                = new StoreChangeField;
            $new->store_id      = $store->id;
            $new->type          = 'add branches';
            $new->data          = $req->except('store_hashid');;
            $new->request_by    = auth()->id() == $store->user_id ? 0 : auth()->id();
            $new->branch_id     = $new_branch->id;
            $new->save();

            return res('success', $new->hash_id, 200);
        }

        $scf->data              = $req->except('store_hashid');
        $scf->store_id          = $new_branch->id;
        $scf->save();

        activityLog('Add branch entry number' . $new_branch->id);
        return res('success', $scf->hash_id, 200);
    }

    public function profile($req)
    {
        if (isset($req->vendor_id)) {
            $vendor_id = $req->vendor_id;
        } else {
            $vendor_id = auth()->user()->id;
        }
        $vendor = User::find($vendor_id);
        if (!$vendor) {
            return res(__('vendor.not_found'), null, 400);
        }
        
        $data = new UserData($vendor);

        return res('Success', $data, 200);
    }

    public function myStore()
    {
        if(auth()->user()->type_info != 'vendor') return res('You are not allowed here', null, 400);

        $stores = Store::where('user_id', auth()->id())->get();

        if(!$stores) return res('Store not found', null, 404);

        $data = new MyStoreCollection($stores);

        return res('success', $data, 200);
    }

    public function getSecurityQuestions($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }

        $list = SecurityQuestionEntry::where('user_id', $req->user_id)->where('status', 1)->limit(2)->get();

        return res('Success', compact('list'));
    }

    public function updateStore($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $confirm_owner = Store::where('id', $req->store_id)->where('user_id', $req->user_id)->count();
        if ($confirm_owner === 0) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'name'              => 'required|unique:stores,name,' . $req->store_id,
            'address'           => 'required',
            'city'              => 'required',
            'state'             => 'required',
            'zip'               => 'required',
            'country'           => 'required|min:3|max:3',
            'paypal_email'      => 'required|email',
            'company_type'      => 'required',
            'date_established'  => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $store                   = Store::find($req->store_id);
        $store->name             = $req->name;
        $store->address          = $req->address;
        $store->city             = $req->city;
        $store->state            = $req->state;
        $store->zip              = $req->zip;
        $store->paypal_email     = $req->paypal_email;
        $store->company_type     = $req->company_type;
        $store->established_type = $req->has('established_type') ? $req->established_type : '';
        $store->date_established = $req->date_established;

        if ($req->has('qid')) {
            // UPLOAD QID
            $store->has_qid = 0;
        }
        if ($req->has('eid')) {
            // UPLOAD EID
            $store->has_eid = 0;
        }
        if ($req->has('commercial_registration')) {
            // UPLOAD REGISTRATION
            $store->has_commercial_registration = 0;
        }
        if ($req->has('commercial_permit')) {
            // UPLOAD PERMIT
            $store->has_commercial_permit = 0;
        }

        if ($req->has('po_box')) {
            $store->po_box = $req->po_box;
        }
        if ($req->has('zone_number')) {
            $store->zone_number = $req->zone_number;
        }
        $store->save();

        return res('Success', $store);
    }

    public function updateBank($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $confirm_owner = Store::where('id', $req->store_id)->where('user_id', $req->user_id)->count();
        if ($confirm_owner === 0) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $store = Store::find($req->store_id);
        if ($req->has('bank_name') && $req->bank_name != '') {
            $validator = Validator::make($req->all(), [
                'bank_name'             => 'required',
                'bank_account_number'   => 'required',
                'bank_account_name'     => 'required',
            ]);
            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412);
            }
        }
        if ($req->has('bank_account_number')) {
            $store->bank_account_number = $req->bank_account_number;
        }
        if ($req->has('bank_account_name')) {
            $store->bank_account_name = $req->bank_account_name;
        }
        if ($req->has('bank_name')) {
            $store->bank_name = $req->bank_name;
        }
        $store->save();

        return res('Success', $store);
    }

    public function updateSecurity($req)
    {
        if (!isset($req->user_id)) {
            return res(__('validation.vendor_identifier'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'question_1'    => 'required',
            'question_2'    => 'required',
            'answer_1'      => 'required',
            'answer_2'      => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $sec = SecurityQuestionEntry::where('user_id', $req->user_id)->limit(2)->get();
        if (!$sec) {
            for ($i=0; $i < 2; $i++) { 
                $sq             = new SecurityQuestionEntry;
                $sq->user_id    = $req->user_id;
                $sq->question   = $req->get('question_' . ($i + 1));
                $sq->answer     = $req->get('answer_' . ($i + 1));
                $sq->save();
            }
        } else {
            $i = 1;
            foreach ($sec as $value) {
                $sq = SecurityQuestionEntry::find($value->id);
                if ($sq) {
                    $sq->question   = $req->get('question_' . $i);
                    $sq->answer     = $req->get('answer_' . $i);
                    $sq->save();
                }
                $i++;
            }
        }

        return res('Success');
    }

    public function getTypes($req)
    {
        $types = VendorType::where('status', 1)->get();
        $types->makeHidden(['status', 'created_at', 'updated_at']);

        return res('Success', $types);
    }

    public function list($req)
    {
        $pending = Store::where('is_approved', 0)
        ->where('ban', 0)
            ->where('is_declined', 0)
                ->orderBy('created_at', 'desc')
                    ->pagination();
        $pendingCollection = new StoreListCollection($pending);

        $approved           = Store::where('is_approved', 1)->where('ban', 0)->where('is_declined', 0)->orderBy('approved_at', 'desc')->pagination();
        $approvedCollection = new StoreListCollection($approved);

        $declined           = Store::where('ban', 0)->where('is_declined', 1)->orderBy('created_at', 'desc')->pagination();
        $declinedCollection = new StoreListCollection($declined);

        $banned             = Store::where('ban', 1)->orderBy('created_at', 'desc')->pagination();
        $bannedCollection   = new StoreListCollection($banned);

        $data           = new stdClass;
        $data->pending  = $pendingCollection;
        $data->approved = $approvedCollection;
        $data->declined = $declinedCollection;
        $data->banned   = $bannedCollection;

        return res('Success', $data, 200);
    }

    public function changeStatus($req)
    {
        if (!isset($req->store_hash_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $role = auth()->user()->type_info;
        if ($role != 'administrator') {
            return res('You are not allowed', null, 400);
        }
        
        if (!$req->has('action') || $req->action == '') {
            return res('Action is Required', null, 400);
        }
        
        $stored_id = decode($req->store_hash_id, 'uuid');
        $store = Store::find($stored_id);
        if (!$store) {
            return res('Unable to find store', null, 400);
        }

        switch ($req->action) {
            case 'approve':
                if ($store->is_approved == 1) {
                    $data = new StoreResource($store);
                    return res('Already approved', $data, 200);
                }
                if ($store->ban == 1) {
                    $data = new StoreResource($store);
                    return res('This store is banned', $data, 400);
                }
                $store->is_approved = 1;
                $store->approved_at = now();
                $store->is_declined = 0;
                $store->save();

                $vendor             = User::where('id', $store->user_id)->first();
                $vendor->status     = 1;
                $vendor->save();

                $data = new StoreResource($store);
                activityLog('Approved Store entry number ' . $store->id);
                return res('Success', $data, 200);
                break;
            case 'decline':
                $store->is_approved = 0;
                $store->is_declined = 1;
                $store->save();

                $vendor = User::where('id', $store->user_id)->first();
                $vendor->status = 0;
                $vendor->save();

                $data = new StoreResource($store);
                activityLog('Declined Store entry number ' . $store->id);
                return res('Success', $data, 200);
                break;
            case 'pending':
                $store->is_approved = 0;
                $store->is_declined = 0;
                $store->save();

                $vendor = User::where('id', $store->user_id)->first();
                $vendor->status = 0;
                $vendor->save();

                $data = new StoreResource($store);
                activityLog('Set to pending store entry number ' . $store->id);
                return res('Success', $data, 200);
                break;
            case 'ban':
                $store->ban = 1;
                $store->save();

                $vendor = User::where('id', $store->user_id)->first();
                $vendor->status = 0;
                $vendor->save();

                $data = new StoreResource($store);
                activityLog('Ban store entry number ' . $store->id);
                return res('Success', $data, 200);
                break;
            case 'unban':
                $store->ban = 0;
                $store->save();

                $vendor = User::where('id', $store->user_id)->first();
                $vendor->status = 1;
                $vendor->save();

                $data = new StoreResource($store);
                activityLog('Unban store entry number ' . $store->id);
                return res('Success', $data, 200);
                break;
            
            default:
                return res('Invalid action', null, 400);
                break;
        }
    }

    public function updateBanners($req)
    {
        if (!isset($req->store_id)) {
            return res(__('validation.identifier'), null, 400);
        }

        $confirm_owner = Store::where('id', $req->store_id)->where('user_id', auth()->id())->count();
        if ($confirm_owner === 0) {
            return res(__('vendor.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(), [
            'banners' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $s = Store::find($req->store_id);
        if (!$s) {
            return res('Invalid Store', null, 400);
        }

        StoreBanner::where('store_id', $req->store_id)->delete();

        foreach ($req->banners as $key => $value) {
            $name           = $s->name . ' - ' . encode(now()->timestamp);
            $sb             = new StoreBanner;
            $sb->store_id   = $req->store_id;
            $sb->base64     = $value;
            $sb->name       = $name;
            $sb->save();
        }
        
        return res('Success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function allStatusList($req)
    {
        $business_type = 'marketplace';
        if($req->business_type != null){
            $business_type = $req->business_type;
        }

        if($req->business_type == 'all'){
            $business_type_ids = BusinessType::pluck('id');
        }else {
            $business_type_ids = BusinessType::where('name', $business_type)->pluck('id');
        }
        
        switch($req->status){
            case 'all' :
                $stores = Store::whereIn('business_type_id', $business_type_ids)->where('is_main_branch', 1)->where('name', 'like', '%' . $req->key . '%')->orderBy('created_at', 'desc')->paginate($req->limit);
            break;
            case 'approved' :
                $stores = Store::whereIn('business_type_id', $business_type_ids)->where('is_main_branch', 1)->where('name', 'like', '%' . $req->key . '%')->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            break;

            case 'pending' :
                $stores = Store::whereIn('business_type_id', $business_type_ids)->where('is_main_branch', 1)->where('name', 'like', '%' . $req->key . '%')->where('is_approved', 0)->where('is_declined', 0)->orderBy('created_at', 'desc')->paginate($req->limit);
            break;

            case 'declined' :
                $stores = Store::whereIn('business_type_id', $business_type_ids)->where('is_main_branch', 1)->where('name', 'like', '%' . $req->key . '%')->where('is_declined', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            break;

            default :
                return res('Invalid status', null, 200);
            break;
        }
        $data = new StoreListCollection($stores);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function showStore($req)
    {

        if(!$req->store_hash_id) return res('store_hash_id is required', null, 401);

        $store_id = decode($req->store_hash_id, 'uuid');
        
        $store = Store::where('id', $store_id)->first();

        if(!$store) return res('Store not found', null, 404);

        $data = new StoreResource($store);

        return res('success', $data, 200);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addStaff($req)
    {
        DB::beginTransaction();

        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Agent or Vendor can create staff', null, 401);
            DB::rollback();
        }


        $validator = Validator::make($req->all(), [
            'store_hash_id' => 'required|string',
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => 'required|email',
            'profile_photo' => 'required',
            'password'      => 'required|min:6|alpha_num|confirmed'
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
            DB::rollback();
        }

        $user = User::where('email', $req->email)->first();
        if($user){
            return res('The email has already been taken', null, 400);
        }
        
        $user                   = new User;
        $user->name             = $req->first_name . ' ' . $req->last_name;
        $user->profile_photo    = $req->profile_photo;
        $user->email            = $req->email;
        $user->password         = bcrypt($req->password);
        $user->mobile           = $req->mobile ? $req->mobile : '';
        $user->iso              = session('iso', getIso());
        $user->type             = 1;
        $user->save();
        
        $store_id = decode($req->store_hash_id, 'uuid');
        $store = Store::where('id', $store_id)->first();

        if (!$store) {
            return res(__('store.not_found'), null, 404);
            DB::rollBack();
        }
        
        $new            = new StoreStaff;
        $new->store_id  = $store->id;
        $new->user_id   = $user->id;
        $new->save();

        if($user){
            $path = public_path() . '/images/users';
            if (!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/users', $mode = 0777, true, true);
            }
            File::makeDirectory(base_path() . '/public/images/users/' . encode($user->id, 'uuid'), $mode = 0777, true, true);

            $root_path = public_path() . '/images/users/' . encode($user->id, 'uuid') . '/';

            $image = Image::make($user->profile_photo);
            $image->save($root_path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($root_path . 'thumbnail.png');
        }

        DB::commit();
        activityLog('Add staff in store entry number ' . $store->id);
        return res('success', $user->hash_id, 201);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function showStaff($req)
    {
        if(!$req->hash_id) return res('hash_id is required', null, 401);

        $user_id = decode($req->hash_id, 'uuid');

        $user = User::where('id', $user_id)->where('status', 1)->first();
        if(!$user) return res('User not found', null, 404);

        $data = new UserData($user);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function listStaff($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        if(!$req->store_hash_id) return res('store hash ID is required', null, 400);

        $store_id = decode($req->store_hash_id, 'uuid');
        
        switch($req->status){
            case 'active' :
            $store = Store::where('id', $store_id)->first();
            if(!$store) return res(__('store.not_found'), null, 404);
            $ss = StoreStaff::where('store_id', $store->id)->pluck('user_id');
            $staff = User::whereIn('id', $ss)->where('name', 'like', '%' . $req->key . '%')->where('status', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
            $data = new StaffCollection($staff);
            return res('success', $data, 200);
            break;

            case 'inactive' :
            $store = Store::where('id', $store_id)->first();
            if(!$store) return res(__('store.not_found'), null, 404);
            $ss = StoreStaff::where('store_id', $store->id)->pluck('user_id');
            $staff = User::whereIn('id', $ss)->where('name', 'like', '%' . $req->key . '%')->where('status', 0)->orderBy('created_at', 'desc')->paginate($req->limit);
            $data = new StaffCollection($staff);
            return res('success', $data, 200);
            break;

            case 'all' :
            $store = Store::where('id', $store_id)->first();
            if(!$store) return res(__('store.not_found'), null, 404);
            $ss = StoreStaff::where('store_id', $store->id)->pluck('user_id');
            $staff = User::whereIn('id', $ss)->where('name', 'like', '%' . $req->key . '%')->orderBy('created_at', 'desc')->paginate($req->limit);
            $data = new StaffCollection($staff);
            return res('success', $data, 200);
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchStaff($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        if(!$req->store_hash_id) return res('store hash ID is required', null, 400);

        $store_id = decode($req->store_hash_id, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);
        
        $ss = StoreStaff::where('store_id', $store->id)->pluck('user_id');

        $user = User::whereIn('id', $ss)->where('name', 'like', '%' . $req->key . '%')->paginate($req->limit);
        $data = new StaffCollection($user);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentNewVendor($req)
    {
        $start = Carbon::now()->startOfDay();
        $end = Carbon::now();

        $stores = Store::where('agent_id', auth()->id())->where('name', 'like', '%' . $req->key . '%')->where('created_at', '>=', $start)->where('created_at', '<=', $end)->orderBy('created_at', 'desc')->paginate($req->limit);
        $data = new StoreListCollection($stores);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentPrime($req)
    {
        $start = Carbon::now()->startOfDay();
        $end = Carbon::now();

        $stores = Store::where('agent_id', auth()->id())
        ->where('name', 'like', '%' . $req->key . '%')
            ->where('ban', '!=', 1)
                ->orderBy('created_at', 'desc')->paginate($req->limit);
        $data = new StoreListCollection($stores);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentOracle($req)
    {
        $start = Carbon::now()->startOfDay();
        $end = Carbon::now();

        $stores = Store::where('agent_id', auth()->id())
        ->where('name', 'like', '%' . $req->key . '%')
            ->where('is_approved', 0)
                ->where('ban', 0)
                    ->orderBy('created_at', 'desc')->paginate($req->limit);

        $data = new StoreListCollection($stores);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentSailore($req)
    {
        $start = Carbon::now()->startOfDay();
        $end = Carbon::now();

        $stores = Store::where('agent_id', auth()->id()
        )->where('name', 'like', '%' . $req->key . '%')
            ->where('is_approved', 1)
                ->orderBy('created_at', 'desc')->paginate($req->limit);
                
        $data = new StoreListCollection($stores);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentOverAll()
    {
        $start          = Carbon::now()->startOfDay();
        $end            = Carbon::now();

        $newMine        = Store::where('agent_id', auth()->id())->where('created_at', '>=', $start)->where('created_at', '<=', $end)->count();
        $newOverAll     = Store::where('created_at', '>=', $start)->where('created_at', '<=', $end)->count();

        $primeMine      = Store::where('agent_id', auth()->id())->where('ban', '!=', 1)->count();
        $primeOverAll   = Store::where('ban', '!=', 1)->count();

        $oracleMine     = Store::where('agent_id', auth()->id())->where('is_approved', 0)->where('ban', 0)->count();
        $oracleOverAll  = Store::where('is_approved', 0)->where('ban', 0)->count();
        
        $sailorMine     = Store::where('agent_id', auth()->id())->where('is_approved', 1)->count();
        $sailoreOverAll = Store::where('is_approved', 1)->count();

        $data           = new stdClass;
        $data->new      = ['mine' => $newMine,'over_all' => $newOverAll];
        $data->prime    = ['mine' => $primeMine,'over_all' => $primeOverAll];
        $data->oracle   = ['mine' => $oracleMine,'over_all' => $oracleOverAll];
        $data->sailors  = ['mine' => $sailorMine,'over_all' => $sailoreOverAll];

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchStore($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $stores = Store::where('name', 'like', '%' . $req->key . '%')->paginate($req->limit);
        $data   = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchByStoreStatus($req)
    {
        if(!$req->status) return res('Status is required', null, 400);

        $limit = 10;

        if(isset($req->limit)) $limit = $req->limit;

        switch($req->status){
            case 'pending' :
            $stores     = Store::where('is_approved', 0)->where('is_declined', 0)->paginate($limit);
            $data       = new StoreListCollection($stores);
            return res('success', $data, 200);
            break;

            case 'approved' :
            $stores     = Store::where('is_approved', 1)->paginate($limit);;
            $data       = new StoreListCollection($stores);
            return res('success', $data, 200);
            break;

            case 'declined' :
            $stores     = Store::where('is_declined', 1)->paginate($limit);;
            $data       = new StoreListCollection($stores);
            return res('success', $data, 200);
            break;

            case 'ban' :
            $stores     = Store::where('ban', 1)->paginate($limit);;
            $data       = new StoreListCollection($stores);
            return res('success', $data, 200);
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchSailor($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $stores     = Store::where('name', 'like', '%' . $req->key . '%')->where('is_approved', 1)->paginate($req->limit);
        $data       = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchOracle($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $stores     = Store::where('name', 'like', '%' . $req->key . '%')->where('is_approved', 0)->where('ban', 0)->where('is_declined', 0)->paginate($req->limit);
        $data       = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchPrime($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $stores     = Store::where('name', 'like', '%' . $req->key . '%')->where('ban', '!=', 1)->paginate($req->limit);
        $data       = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchNew($req)
    {
        if(!$req->limit) return res('limit is required', null, 400);

        $start  = Carbon::now()->startOfDay();
        $end    = Carbon::now();

        $stores = Store::where('name', 'like', '%' . $req->key . '%')->where('created_at', '>=', $start)->where('created_at', '<=', $end)->paginate($req->limit);
        $data   = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function themeList($req)
    {
        if(!$req->store_hashid) return res('Store hashid is required', null, 401);

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        $store_themes = StoreTheme::where('status', 1)->orderBy('created_at', 'desc')->get();

        $arr = [];

        foreach ($store_themes as $theme){
            $obj            = new stdClass;
            $obj->hashid    = $theme->hashid;
            $obj->name      = $theme->name;
            $obj->colors    = $theme->codes;
            $obj->active    = $store->theme_id === $theme->id ? 1 : 0;

            array_push($arr, $obj);
        }

        return res('success', $arr, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function changeTheme($req)
    {
        $validator = Validator::make($req->all(), [
            'theme_hashid' => 'required',
            'store_hashid' => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $theme_id   = decode($req->theme_hashid, 'uuid');
        $store_id   = decode($req->store_hashid, 'uuid');

        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        $theme      = StoreTheme::where('id', $theme_id)->where('status', 1)->first();
        if(!$theme) return res('Theme not found', null, 400);


        $store->theme_id = $theme->id;
        $store->save();

        $data = new Theme($theme);
        activityLog('Change theme in store entry number ' . $store->id);
        return res('success', $data, 200);
    }

    /**
     * Change information of a store
     * @param $req
     * @return JsonResponse
     */
    public function changeInformations($req):object
    {
        DB::beginTransaction();
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }
        
        if(!$req->store_hashid) return res('Store hashid is required', null, 401);

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store) {
            DB::rollBack();
            return res(__('store.not_found'), null, 404);
        }
        

        $vendor = User::where('id', $store->user_id)->first();
        if(!$vendor){
            DB::rollBack();
            return res(__('Vendor not found'), null, 404);
        }

        if($req->store_name != null)          $store->name          = $req->store_name;
        if($req->store_phone_number != null)  $store->phone         = $req->store_phone_number;
        if($req->store_email != null)         $store->store_email   = $req->store_email;
        if($req->vendor_name != null)         $vendor->name         = $req->vendor_name;
        if($req->vendor_email != null)        $vendor->email        = $req->vendor_email;

        if($req->vendor_mobile != null) {
            $mobile = User::where('mobile', $req->vendor_mobile)->first();
            if($mobile) {
                DB::rollBack();
                return res('The vendor mobile number has already been taken', null, 400);
            }
            $vendor->mobile = $req->vendor_mobile;
        }

        $password = explode("@", $req->vendor_email);
        if (strlen($password[0]) < 6) { 
        $password = $password[0].generateMobileToken();
        }

        $vendor->password = bcrypt($password[0]);
        $vendor->save();
        $store->save();
        
        // =======================================For Future Feature Only ============================================
        // $scf = StoreChangeField::where('type', 'informations')->where('store_id', $store->id)->where('is_approved', 0)->where('is_declined', 0)->first();
        // if($scf) return res(__('store.request_already'), null, 401);
        
        // $new                = new StoreChangeField;
        // $new->store_id      = $store->id;
        // $new->type          = 'informations';
        // $new->data          = $req->except('store_hashid');
        // $new->request_by    = auth()->id() == $store->user_id ? 0 : auth()->id();
        // $new->is_approved   = auth()->user()->type_info == 'administrator' ? 1 : 0;
        // $new->process_by    = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        // $new->save();

        // if(auth()->user()->type_info === 'administrator') activityLog('Approved store request number ' . $new->id);

        
        
        // activityLog('Request to change information of store number ' . $store->id . ' and create new request of ' . $new->id);

        DB::commit();
        return res('success', null, 201);

    }

    /**
     * Change description of a store
     * @param $req
     * @return JsonResponse
     */
    public function changeDescription($req):object
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        
        $store->store_information = $req->description;
        $store->save();
        
        // =======================================For Future Feature Only ============================================
        // $scf = StoreChangeField::where('type', 'description')->where('store_id', $store->id)->where('is_approved', 0)->where('is_declined', 0)->first();
        // if($scf) return res(__('store.request_already'), null, 401);

        // $new                = new StoreChangeField;
        // $new->store_id      = $store->id;
        // $new->type          = 'description';
        // $new->data          = $req->description;
        // $new->request_by    = auth()->id() == $store->user_id ? 0 : auth()->id();
        // $new->is_approved   = auth()->user()->type_info == 'administrator' ? 1 : 0;
        // $new->process_by    = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        // $new->save();

        // if(auth()->user()->type_info === 'administrator') activityLog('Approved store request number ' . $new->id);
        // activityLog('Request to change description of store number ' . $store->id . ' and create new request of ' . $new->id);

        return res('success', null, 201);
    }

    /**
     * Change location or address of a store
     * @param $req
     * @return JsonResponse
     */
    public function changeLocations($req):object
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }
        
        if(!$req->store_hashid) return res('Store hashid is required', null, 401);
        
        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store) return res('invalid store', null, 404);

        // =======================================For Future Feature Only ============================================
        // $scf = StoreChangeField::where('type', 'locations')->where('store_id', $store->id)->where('is_approved', 0)->where('is_declined', 0)->first();
        // if($scf) return res(__('store.request_already'), null, 401);


        if($req->street != null)    $store->street      = $req->street;
        if($req->latitude != null)  $store->latitude    = $req->latitude;
        if($req->longitude != null) $store->longitude   = $req->longitude;
        if($req->state != null)     $store->state       = $req->state;
        if($req->city != null)      $store->city        = $req->city;
        if($req->other_address_info != null) $store->other_address_info = $req->other_address_info;

        $store->save();

        // =======================================For Future Feature Only ============================================
        // $new                = new StoreChangeField;
        // $new->store_id      = $store->id;
        // $new->type          = 'locations';
        // $new->data          = $req->except('store_hashid');
        // $new->request_by    = auth()->id() == $store->user_id ? 0 : auth()->id();
        // $new->is_approved   = auth()->user()->type_info == 'administrator' ? 1 : 0;
        // $new->process_by    = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        // $new->save();

        // if(auth()->user()->type_info === 'administrator') activityLog('Approved store request number ' . $new->id);

        // activityLog('Request to change locations of store number ' . $store->id . ' and create new request of ' . $new->id);
        return res('success', null, 200);
    }

    /**
     * Changing profile pcicture of a store
     * @param $req
     * @return JsonResponse
     */
    public function changeProfilePhoto($req):object
    {
        DB::beginTransaction();
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }
        
        if(!$req->store_hashid) return res('Store hashid is required', null, 401);
        if(!$req->base64) return res('base64 is required', null, 401);

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store) {
            DB::rollBack();
            return res(__('store.not_found'), null, 404);
        }

        $store->profile_photo_img = $req->base64;

        $store_parent_folder = public_path() . '/images/stores/' . $store->hashid;
        if(!file_exists($store_parent_folder)){
            File::makeDirectory(base_path() . '/public/images/stores/' . $store->hashid, $mode = 0777, true, true);
        }

        $fullPath = public_path() . '/images/stores/' . $store->hashid . '/profile-photo-full.png';
        $thumbnailPath = public_path() . '/images/stores/' . $store->hashid . '/profile-photo-thumbnail.png';
        
        // @AES Delete existing
        if(file_exists($fullPath) && file_exists($thumbnailPath)) {
            File::delete($fullPath);
            File::delete($thumbnailPath);
        }

        $store_path = public_path() . '/images/stores/' . $store->hashid . '/';

        $image = Image::make($store->profile_photo_img);
        $image->save($store_path . 'profile-photo-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($store_path . 'profile-photo-thumbnail.png');
        $store->save();

        DB::commit();
        return res('success', null, 200);
        // =======================================For Future Feature Only ============================================
        // $scf        = StoreChangeField::where('type', 'profile photo')->where('store_id', $store->id)->where('is_approved', 0)->where('is_declined', 0)->first();
    

        // if($scf) return res(__('store.request_already'), null, 401);

        // $new                = new StoreChangeField;
        // $new->store_id      = $store->id;
        // $new->type          = 'profile photo';
        // $new->base64        = $req->base64;
        // $new->request_by    = auth()->id() == $store->user_id ? 0 : auth()->id();
        // $new->save();

        // $path = public_path() . '/images/store-requests';
        // if (!file_exists($path)) {
        //     File::makeDirectory(base_path() . '/public/images/store-requests', $mode = 0777, true, true);
        // }
        // File::makeDirectory(base_path() . '/public/images/store-requests/' . encode($new->id, 'uuid'), $mode = 0777, true, true);
        
        // $request_path = public_path() . '/images/store-requests/' . encode($new->id, 'uuid') . '/';
        
        // $image = Image::make($new->base64);
        // $image->save($request_path . 'profile-photo-full.png');
        // $image->resize(150, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // }); //@AES resizing
        // $image->save($request_path . 'profile-photo-thumbnail.png');

        // activityLog('Request to change profile photo of store number ' . $store->id . ' and create new request of ' . $new->id);
        //===========================================================================================================
        return res('success', null, 200);
    }

    public function changeCoverPhoto($req)
    {
        DB::beginTransaction();
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }
        
        if(!$req->store_hashid) return res('Store hashid is required', null, 401);
        if(!$req->base64) return res('base64 is required', null, 401);

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->first();
        if(!$store) {
            DB::rollBack();
            return res(__('store.not_found'), null, 404);
        }

        $store->cover_photo_img = $req->base64;

        $store_parent_folder = public_path() . '/images/stores/' . encode($store_id, 'uuid') . '/';
        if(!file_exists($store_parent_folder)){
            File::makeDirectory($store_parent_folder, $mode = 0777, true, true);
        }
        
        $fullPath = public_path() . '/images/stores/' . encode($store_id, 'uuid') . '/cover-photo-full.png';
        $thumbnailPath = public_path() . '/images/stores/' . encode($store_id, 'uuid') . '/cover-photo-thumbnail.png';
        
        // @AES Delete existing
        if (File::exists($thumbnailPath) && File::exists($fullPath)) {
            File::delete($thumbnailPath);
            File::delete($fullPath);
        }

        $store_path = public_path() . '/images/stores/' . encode($store_id, 'uuid') . '/';

        $image = Image::make($store->cover_photo_img);
        $image->save($store_path . 'cover-photo-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing

        $image->save($store_path . 'cover-photo-thumbnail.png');
        $store->save();

        DB::commit();
        return res('success', null, 200);
        
        //===================For Future Feature Only=====================================
        // $scf        = StoreChangeField::where('type', 'cover photo')->where('store_id', $store->id)->where('is_approved', 0)->where('is_declined', 0)->first();

        // if($scf) return res(__('store.request_already'), null, 401);

        // $new                = new StoreChangeField;
        // $new->store_id      = $store->id;
        // $new->type          = 'cover photo';
        // $new->base64        = $req->base64;
        // $new->request_by    = auth()->id() == $store->user_id ? 0 : auth()->id();
        // $new->save();

        // $path = public_path() . '/images/store-requests';
        // if (!file_exists($path)) {
        //     File::makeDirectory(base_path() . '/public/images/store-requests', $mode = 0777, true, true);
        // }
        // File::makeDirectory(base_path() . '/public/images/store-requests/' . encode($new->id, 'uuid'), $mode = 0777, true, true);
        
        // $request_path = public_path() . '/images/store-requests/' . encode($new->id, 'uuid') . '/';
        
        // $image = Image::make($new->base64);
        // $image->save($request_path . 'cover-photo-full.png');
        // $image->resize(150, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // }); //@AES resizing
        // $image->save($request_path . 'cover-photo-thumbnail.png');
        // activityLog('Request to change cover photo of store number ' . $store->id . ' and create new request of ' . $new->id);
        // return res('success', null, 200);
        //=======================For Future Feature Only====================================
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentChangeProfilePhoto($req)
    {
        DB::beginTransaction();
        $validator = Validator::make($req->all(), [
            'user_hashid' => 'required',
            'base64' => 'required',
        ]);
        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }
        $user_id = decode($req->user_hashid, 'uuid');

        $user = User::where('id', $user_id)->where('status', 1)->first();
        if(!$user) {
            DB::rollBack();
            return res(__('user.not_found'), null, 404);
        }

        $user->profile_photo = $req->base64;
        $user->save();

        $path = public_path() . '/images/users/' . $user->hash_id . '/';
        if(!file_exists($path)){
            File::makeDirectory(base_path() . '/public/images/users/' . $user->hash_id, $mode = 0777, true, true);

            $image = Image::make($user->profile_photo);
            $image->save($path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path . 'thumbnail.png');
        }

        $fullPath = public_path() . '/images/users/' . $user->hash_id . '/full.png';
        $thumbnailPath = public_path() . '/images/users/' . $user->hash_id . '/thumbnail.png';
        
        // @AES Delete existing
        if (File::exists($thumbnailPath) && File::exists($fullPath)) {
            File::delete($thumbnailPath);
            File::delete($fullPath);
        }

        $image = Image::make($user->profile_photo);
        $image->save($path . 'full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($path . 'thumbnail.png');

        
        activityLog('Request to change user profile photo of user ' . $user->id);
        DB::commit();
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeBranches($req)
    {
        if(!$req->store_hashid) return res(__('validation.identifier'), null, 400);

        $store_id = decode($req->store_hashid, 'uuid');

        $branches = StoreBranch::where('store_id', $store_id)->where('name', 'like', '%' . $req->key . '%')->get();
        if ($req->status == 'active')   $branches = StoreBranch::where('store_id', $store_id)->where('name', 'like', '%' . $req->key . '%')->where('status', 1)->get();
        if ($req->status == 'inactive') $branches = StoreBranch::where('store_id', $store_id)->where('name', 'like', '%' . $req->key . '%')->where('status', 0)->get();
        $data = new BranchListCollection($branches);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function pendingStaffList()
    {
        $staff = User::where('status', 0)->where('type', 1)->orderBy('created_at', 'desc')->get();
        $data = new StaffCollection($staff);

        return res('success', $data, 200);
    }
    
    /**
     * @param $req
     * @return JsonResponse
     */
    public function documents($req)
    {
        if(!$req->store_hashid) return res(__('store.not_found'), null, 404);

        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();

        if(!$store) return res(__('store.not_found'), null, 400);

        
        $data = new stdClass;
        // @AES Signature
        $signature_pdf_path = Storage::disk('local')->exists('/pdf/stores/' . $store->hashid . '/signature/' . $store->signature_file_name);
        $signature_images_path = public_path() . '/images/stores/' . $store->hashid . '/signature-images';
        if($signature_pdf_path) {
            $data->signature = [
                'file_type'     => 'pdf',
                'file_name'     => $store->signature_file_name,
                'document_name' => 'signature',
                'status'        => $store->signature_status
            ];
        } else if (file_exists($signature_images_path)){
            $data->signature = [
                'file_type'     => 'img',
                'data'          => new SignatureCollection($store->signatureImages),
                'status'        => $store->signature_status
            ];
        } else { 
            $data->signature = [
                'file_type'     => null,
                'data'          => null,
                'status'        => 'Not submitted'
            ];
        }
        //



        // @AES Commercial permit
        $cp_pdf_path = Storage::disk('local')->exists('/pdf/stores/' . $store->hashid . '/commercial-permit/' . $store->cp_file_name);
        $cp_images_path = public_path() . '/images/stores/' . $store->hashid . '/commercial-permit-images';
        if($cp_pdf_path){
            $data->commercial_permit = [
                'file_type'      => 'pdf',
                'file_name'      => $store->cp_file_name,
                'document_name'  => 'commercial-permit',
                'status'         => $store->commercial_permit_status
            ]; 
        } else if (file_exists($cp_images_path)) {
            $data->commercial_permit = [
                'file_type'     => 'img',
                'data'          =>  new CommercialPermitCollection($store->commercialPermitImages),
                'status'        => $store->commercial_permit_status
            ];
        } else { 
            $data->commercial_permit = [
                'file_type'     => null,
                'data'          =>  null,
                'status'        => 'Not submitted'
            ];
        }
        //


        // @AES Commercial registration
        $cr_pdf_path = Storage::disk('local')->exists('/pdf/stores/' . $store->hashid . '/commercial-registration/' . $store->cr_file_name);
        $cr_images_path = public_path() . '/images/stores/' . $store->hashid . '/commercial-registration-images';
        if($cr_pdf_path){
            $data->commercial_registration = [
                'file_type'     => 'pdf',
                'file_name'     => $store->cr_file_name,
                'document_name' => 'commercial-registration',
                'status'        => $store->commercial_registration_status
            ]; 
        } else if (file_exists($cr_images_path)) {
            $data->commercial_registration = [
                'file_type'     => 'img',
                'data'          =>  new CommercialRegistrationCollection($store->commercialRegistrationImages),
                'status'        => $store->commercial_registration_status
            ];
        } else { 
            $data->commercial_registration = [
                'file_type'     => null,
                'data'          =>  null,
                'status'        => 'Not submitted'
            ];
        }
        //--


        // @AES Computer card
        $cc_images_path = public_path() . '/images/stores/' . $store->hashid . '/computer-card-images';
        $cc_pdf_path = Storage::disk('local')->exists('/pdf/stores/' . $store->hashid . '/computer-card/' . $store->cc_file_name);
        if($cc_pdf_path){
            $data->computer_card = [
                'file_type'         => 'pdf',
                'file_name'         => $store->cc_file_name,
                'document_name'     => 'computer-card',
                'status'            => $store->computer_card_status
            ]; 
        } else if (file_exists($cc_images_path)) {
            $data->computer_card = [
                'file_type'         => 'img',
                'data'              =>  new ComputerCardCollection($store->computerCardImages),
                'status'            => $store->computer_card_status
            ];
        } else { 
            $data->computer_card = [
                'file_type'         => null,
                'data'              =>  null,
                'status'            => 'Not submitted'
            ];
        }
        //--


        // @AES Sponsor qid
        $sqid_images_path = public_path() . '/images/stores/' . $store->hashid . '/sponsor-qid-images';
        $sqid_pdf_path = Storage::disk('local')->exists('/pdf/stores/' . $store->hashid . '/sponsor-qid/' . $store->sq_file_name);
        if($sqid_pdf_path){
            $data->sponsor_qid = [
                'file_type'         => 'pdf',
                'file_name'         => $store->sq_file_name,
                'document_name'     => 'sponsor-qid',
                'status'            => $store->sponsor_qid_status
            ]; 
        } else if (file_exists($sqid_images_path)) {
            $data->sponsor_qid = [
                'file_type'         => 'img',
                'data'              =>  new SponsorQidCollection($store->sponsorQidImages),
                'status'            => $store->sponsor_qid_status
            ];
        } else { 
            $data->sponsor_qid = [
                'file_type'         => null,
                'data'              =>  null,
                'status'            => 'Not submitted'
            ]; 
        }
        //--

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addDocumentImages($req)
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'type'          => 'required|string',
            'file_type'     => 'required|string',
            'store_hashid'  => 'required|string'
        ]);

        if($req->file_type === 'images'){
            $validator = Validator::make($req->all(), [
                'base64' => 'required',
            ]);

            if($validator->fails()) return res('Failed', $validator->errors(), 412);
        }

        if($validator->fails()) return res('Failed', $validator->errors(), 412);

        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        if($req->file_type == 'images'){
            $scf = StoreChangeField::where('type', 'legal document images')->where('store_id', $store->id)->where('document_name', $req->type)->where('is_approved', 0)->where('is_declined', 0)->first();
            if($scf) return res(__('store.request_already'), null, 401);
        }

        if($req->file_type == 'pdf'){
            $scf = StoreChangeField::where('type', 'legal document PDF')->where('store_id', $store->id)->where('document_name', $req->type)->where('is_approved', 0)->where('is_declined', 0)->first();
            if($scf) return res(__('store.request_already'), null, 401);
        }
        
        //----Archives
        $pdf_exists = Storage::disk('local')->exists('/pdf/stores/' . encode($store->id, 'uuid') . '/' . $req->type);

        $current_images_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/' . $req->type . '-images';

        $archive                = new DocumentArchive;
        $archive->store_id      = $store->id;
        $archive->user_id       = auth()->id();
        $archive->document_type = $req->type;

        if($pdf_exists){
            
            $archive->file_type = 'pdf';

            if ($req->type === 'computer-card')             $archive->file_name = $store->cc_file_name;
            if ($req->type === 'commercial-registration')   $archive->file_name = $store->cr_file_name;
            if ($req->type === 'commercial-permit')         $archive->file_name = $store->cp_file_name;
            if ($req->type === 'sponsor-qid')               $archive->file_name = $store->sq_file_name;
            if ($req->type === 'signature')                 $archive->file_name = $store->signature_file_name;

            $archive->save();
            Storage::move('pdf/stores/' . encode($store->id, 'uuid') . '/' . $req->type, 'archives/pdf/stores/' . encode($archive->id, 'uuid') . '/' . $req->type);
        
        }

        if(file_exists($current_images_path)){
            $archive->file_type = 'image';
            $archive->save();

            File::makeDirectory(base_path() . '/public/images/archives/documents/' . encode($archive->id, 'uuid'), $mode = 0777, true, true);
            $archive_path = public_path() . '/images/archives/documents/' . encode($archive->id, 'uuid');

            File::move($current_images_path, $archive_path . '/' . $req->type . '-images');
        }
        //--------------------


        switch ($req->type) {
        case 'computer-card' :

            $store->computer_card_status = auth()->user()->type_info == 'administrator' ? 'approved' : 'pending';

            if($req->file_type === 'pdf'){
                $validator = Validator::make($req->all(), [
                    'file' => 'required|mimes:pdf',
                ]);

                if($validator->fails()) return res('Failed', $validator->errors(), 412);
                if($scf) return res(__('store.request_already'), null, 401);
                
                $store->cc_file_name = $req->file('file')->getClientOriginalName();
                $this->savePdfProcess($store->id, 'computer card', $store->user_id, 'computer-card', $req->file('file'));
                
                $store->save();
                return res('success', null, 200);
            }

            $this->saveDocumentProcess($store->id, $store->user_id, $req->base64, 'computer_card_images', 'computer-card-images', 'computer-card');
            $store->save();
            return res('success', null, 200);
            break;

        case 'commercial-registration' :

            $store->commercial_registration_status = auth()->user()->type_info == 'administrator' ? 'approved' : 'pending';

            if($req->file_type === 'pdf'){
                $validator = Validator::make($req->all(), [
                    'file' => 'required|mimes:pdf',
                ]);

                if($validator->fails()) return res('Failed', $validator->errors(), 412);
                if($scf) return res(__('store.request_already'), null, 401);

                $store->cr_file_name = $req->file('file')->getClientOriginalName();
                $this->savePdfProcess($store->id, 'commercial registration', $store->user_id, 'commercial-registration', $req->file('file'));
                
                $store->save();
                return res('success', null, 200);
            }
            
            $this->saveDocumentProcess($store->id, $store->user_id, $req->base64, 'commercial_registration_images', 'commercial-registration-images', 'commercial-registration');
            $store->save();
            return res('success', null, 200);
            break;

        case 'commercial-permit' :

            $store->commercial_permit_status = auth()->user()->type_info == 'administrator' ? 'approved' : 'pending';

            if($req->file_type === 'pdf'){
                $validator = Validator::make($req->all(), [
                    'file' => 'required|mimes:pdf',
                ]);

                if($validator->fails()) return res('Failed', $validator->errors(), 412);
                if($scf) return res(__('store.request_already'), null, 401);

                $store->cp_file_name = $req->file('file')->getClientOriginalName();
                $this->savePdfProcess($store->id, 'commercial permit', $store->user_id, 'commercial-permit', $req->file('file'));
                
                $store->save();
                return res('success', null, 200);
            }

            $this->saveDocumentProcess($store->id, $store->user_id, $req->base64, 'commercial_permit_images', 'commercial-permit-images', 'commercial-permit');
            $store->save();
            return res('success', null, 200);
            break;

        case 'sponsor-qid' :

            $store->sponsor_qid_status = auth()->user()->type_info == 'administrator' ? 'approved' : 'pending';

            if($req->file_type === 'pdf'){
                $validator = Validator::make($req->all(), [
                    'file' => 'required|mimes:pdf',
                ]);

                if($validator->fails()) return res('Failed', $validator->errors(), 412);
                if($scf) return res(__('store.request_already'), null, 401);

                $store->sq_file_name = $req->file('file')->getClientOriginalName();
                $this->savePdfProcess($store->id, 'sponsor qid', $store->user_id, 'sponsor-qid', $req->file('file'));
                
                $store->save();
                return res('success', null, 200);
            }
            

            $this->saveDocumentProcess($store->id, $store->user_id, $req->base64, 'sponsor_qid_images', 'sponsor-qid-images', 'sponsor-qid');
            $store->save();
            return res('success', null, 200);
            break;

        case 'signature' :

            $store->signature_status = auth()->user()->type_info == 'administrator' ? 'approved' : 'pending';

            if($req->file_type === 'pdf'){
                $validator = Validator::make($req->all(), [
                    'file' => 'required|mimes:pdf',
                ]);

                if($validator->fails()) return res('Failed', $validator->errors(), 412);
                if($scf) return res(__('store.request_already'), null, 401);
                
                $store->signature_file_name = $req->file('file')->getClientOriginalName();
                $this->savePdfProcess($store->id, 'signature', $store->user_id, 'signature', $req->file('file'));
                
                $store->save();
                return res('success', null, 200);
            }

            $this->saveDocumentProcess($store->id, $store->user_id, $req->base64, 'signature_images', 'signature-images', 'signature');
            $store->save();
            return res('success', null, 200);
            break;


            default :
            return res('Invalid type', null, 400);
            break;
            
        }

    }

    /**
     * @param $store_id
     * @param $base64
     * @param $model_name
     * @param $parent_folder
     * @param $image_name
     * @return JsonResponse
     */
    public function saveDocumentProcess($store_id, $user_id, $base64, $model_name, $parent_folder, $image_name)
    {
        $base64_array = json_decode($base64, true);
        // $base64_array = $base64;

        $new                = new StoreChangeField;
        $new->store_id      = $store_id;
        $new->type          = 'legal document images';
        $new->document_name = $image_name;
        $new->data          = ['image_name' => $image_name,'model_name' => $model_name];
        $new->request_by    = auth()->id() == $user_id ? 0 : auth()->id();
        $new->is_approved   = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new->process_by    = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $new->save();

        File::makeDirectory(base_path() . '/public/images/store-requests/' . encode($new->id, 'uuid'), $mode = 0777, true, true);
        $store_request_folder = public_path() . '/images/store-requests/' . encode($new->id, 'uuid') . '/';
        File::makeDirectory($store_request_folder, $mode = 0777, true, true);

        $path = $store_request_folder . '/';

        DB::table($model_name)->where('store_id', $store_id)->where('status', '=', 1)->update(array('status' => 0));
        foreach ($base64_array as $value){

            $id = DB::table($model_name)->insertGetId(
                [
                    'store_id' => $store_id, 
                    'base64' => $value,
                    'file_name' => '',
                ]
            );
            
            $store_parent_folder = public_path() . '/images/stores/' . encode($store_id, 'uuid');
            if(!file_exists($store_parent_folder)){
                File::makeDirectory(base_path() . '/public/images/stores/' . encode($store_id, 'uuid'), $mode = 0777, true, true);
            }

            $origin_path = public_path() . '/images/stores/' . encode($store_id, 'uuid') . '/' . $parent_folder;
            if(!file_exists($origin_path)){
                File::makeDirectory(base_path() . '/public/images/stores/' . encode($store_id, 'uuid') . '/' . $parent_folder, $mode = 0777, true, true);
            }

            $origin_root_path = $origin_path . '/';

            $image = Image::make($value);
            $image->save($origin_root_path . $id . '-' . $image_name . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($origin_root_path . $id . '-' . $image_name . '-thumbnail.png');

            $create_request_image                           = new StoreRequestImage;
            $create_request_image->store_change_field_id    = $new->id;
            $create_request_image->document_type            = $image_name;
            $create_request_image->base64                   = $value;
            $create_request_image->save();

            $image = Image::make($value);
            $image->save($path . $create_request_image->id . '-' . $image_name . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path . $create_request_image->id . '-' . $image_name . '-thumbnail.png');
        }

    }

    /**
     * @param $store_id
     * @param $document_type
     * @param $store_user_id
     * @param $document_name
     * @param $file
     * @return JsonResponse
     */
    public function savePdfProcess($store_id, $document_type, $store_user_id, $document_name, $file)
    {
        $new                = new StoreChangeField;
        $new->store_id      = $store_id;
        $new->type          = 'legal document PDF';
        $new->document_name = $document_name;
        $new->data          = ['document_name' => $document_name];
        $new->request_by    = auth()->id() == $store_user_id ? 0 : auth()->id();
        $new->is_approved   = auth()->user()->type_info = 'administrator' ? 1 : 0;
        $new->process_by    = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $new->save();

        $storage = Storage::disk('local')->putFilesAs('/pdf/stores/' . encode($store_id, 'uuid') . '/' . $document_name, $file, $file->getClientOriginalName());
    }

    /**
     * @return JsonResponse
     */
    public function notificationList()
    {
        $notifications = VendorNotification::where('notify_to_user_id', auth()->id())->orderBy('created_at', 'desc')->get();
        $data = new NotificationCollection($notifications);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function vendorStores($req)
    {
        if(auth()->user()->type_info != 'vendor') return res(__('user.not_allowed'), null, 400);

        if($req->status === 'all') $stores = Store::where('user_id', auth()->id())->where('name', 'like', '%' . $req->key . '%')->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->status === 'approved') $stores = Store::where('user_id', auth()->id())->where('name', 'like', '%' . $req->key . '%')->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        if($req->status === 'pending') $stores = Store::where('user_id', auth()->id())->where('name', 'like', '%' . $req->key . '%')->where('is_approved', 0)->orderBy('created_at', 'desc')->where('is_declined', 0)->paginate($req->limit);
        if($req->status === 'declined') $stores = Store::where('user_id', auth()->id())->where('name', 'like', '%' . $req->key . '%')->where('is_declined', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        if(!$stores) return res('success', null, 200);
        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }


    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchOwnStores($req)
    {
        if(auth()->user()->type_info != 'vendor') return res(__('user.not_allowed'), null, 400);

        $stores = Store::where('user_id', auth()->id())->where('name', 'like', '%' . $req->key . '%')->get();
        $data = new StoreListCollection($stores);

        return res('success', $data, 200);

    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function notificationCount()
    {
        $count = VendorNotification::where('notify_to_user_id', auth()->id())->where('is_new', 1)->count();
        return res('succes', $count, 200);
    }

    public function notificationupdate($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can process this.', null, 401);
        }

        
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchApproveBranch($req)
    {
        if(!$req->store_hashid) return res(__('validation.identifier'), null, 400);

        $store_id = decode($req->store_hashid, 'uuid');

        $branches = StoreBranch::where('store_id', $store_id)->where('name', 'like', '%' . $req->name . '%')->where('status', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        $data = new BranchListCollection($branches);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchPendingBranch($req)
    {
        if(!$req->store_hashid) return res(__('validation.identifier'), null, 400);

        $store_id = decode($req->store_hashid, 'uuid');

        $scf = StoreChangeField::where('store_id', $store_id)->where('type', "add branches")->where('data->name', 'like', '%' .$req->name. '%')->where('is_approved', 0)->where('is_declined', 0)->paginate($req->limit);
        
        $data = new StoreChangeFieldCollection($scf);
        return res('success', $data, 200);
    
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function agentList()
    {
        $limit = 10;

        $user = User::orderBy('created_at', 'desc')->where('type', 7)->paginate($limit);
        
        $data = new UserDataCollection($user);
        return res('success', $data, 200);
    
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function allVendorApproved()
    {
        $limit = 10;

        $store = Store::orderBy('approved_at', 'desc')->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($limit);
        
        $data = new StoreListCollection($store);
        return res('success', $data, 200);
    
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function searchApprovedVendor($req)
    {
        $store = Store::where('name', 'like', '%' . $req->key . '%')->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);
        $data = new StoreListCollection($store);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function businessTypes()
    {
        $types = BusinessType::where('status', 1)->get();

        return res('success', $types, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addFood($req)
    {
        $permissions = ['vendor', 'vendor staff', 'administrator', 'agent'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid' => 'required',
            'price' => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $category_id = decode($req->category_hashid, 'uuid');
        $category = FoodCategory::where('id', $category_id)->where('status', 1)->first();
        if(!$category) return res(__('category.not_found'), null, 404);
        
        $meal_id = decode($req->meal_hashid, 'uuid');
        $meal = Meal::where('id', $meal_id)->where('status', 1)->first();
        if(!$meal) return res(__('meal.not_found'), null, 404);
        
        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);
        if($store->businessType->name != 'restaurant') return res(__('restaurant.not_restaurant'), null, 400);
        
        $ifExist = RestaurantFood::where(['restaurant_id' => $store->id, 'name' => $req->name])->first();
        if($ifExist) return res(__('food already exist'), null, 404);

        $new = new StoreChangeField;
        $new->type = 'add food';
        $new->data = $req->except('food_image');
        $new->base64 = $req->food_image;
        $new->request_by = auth()->id();
        $new->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new->process_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;

        $food = new RestaurantFood;
        $food->restaurant_id = $store->id;
        $food->name = $req->name;
        $food->meal = $meal->name;
        $food->price = $req->price;
        if(!$req->description)$food->description = "";
        if($req->description)$food->description = $req->description;
        $food->food_image = $req->food_image;
        $food->category_id = $category->id;
        $food->is_available = 1;
        $food->status = 1;
        $food->is_approved = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $food->is_declined = 0;
        $food->process_by = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $food->request_by = auth()->id();
        $food->status = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $food->save();
        $new->restaurant_foods_id = $food->id;
        $new->store_id = $food->restaurant_id;
        $new->save();

        $this->restaurantPerCategory(['restaurant_id' => $food->restaurant_id, 'food_category_id' => $food->category_id, 'food_type_image' => $req->food_type_image]);
        
        if($req->food_image){
            $food->food_image = $req->food_image;

            $full = public_path() . '/images/food/' . $food->hashid . '-full.png';
            $thumbnail = public_path() . '/images/food/' . $food->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/food/';

            $image = Image::make($food->food_image);
            $image->save($local_path . encode($food->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($food->id, 'uuid') . '-thumbnail.png');
        }

        return res('success', null, 200);
    }

    public function restaurantPerCategory($data)
    {
        $categort = FoodCategory::where('id', $data['food_category_id'])->where('status', 1)->first();

        $ifExists = RestaurantFoodCategoryPivot::where('restaurant_id', $data['restaurant_id'])
        ->where('food_category_name', $categort->name)
            ->where('status', 1)->first();

        if($ifExists) return false;
        
        $food_type = new RestaurantFoodCategoryPivot;
        $food_type->restaurant_id = $data['restaurant_id'];
        $food_type->food_category_name = $categort->name;
        $food_type->food_type_image = $data['food_type_image'];
        $food_type->save();

        if($food_type){

            $full = public_path() . '/images/food_type/' . $food_type->hashid . '-full.png';
            $thumbnail = public_path() . '/images/food_type/' . $food_type->hashid . '-thumbnail.png';

            if(file_exists($full) && file_exists($thumbnail)) {
                File::delete($full);
                File::delete($thumbnail);
            }

            $local_path = public_path() . '/images/food_type/';

            if(!file_exists($local_path)) {
                File::makeDirectory(base_path() . '/public/images/food_type', $mode = 0777, true, true);
            }

            $image = Image::make($food_type->food_type_image);
            $image->save($local_path . encode($food_type->id, 'uuid') . '-full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($local_path . encode($food_type->id, 'uuid') . '-thumbnail.png');
        }
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function branchList($req)
    {   
        if(!$req->store_hashid) return res(__('validation.identifier'), null, 400);

        $store_id       = decode($req->store_hashid, 'uuid');
        $main_branch    = Store::where('id', $store_id)->first();
        if(!$main_branch) return res(__('store.not_found'), null, 404);
        if($main_branch->is_main_branch != 1) return res(__('store.main_branch.not_main_branch'), null, 412);
        
        if($req->status == 'all'){
            $store_branches_ids = StoreBranch::where('store_id', $store_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            $store_branches = Store::whereIn('id', $store_branches_ids)->paginate($req->limit);
        }

        elseif($req->status == 'approved'){
            $store_branches_ids = StoreBranch::where('store_id', $store_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            $store_branches = Store::whereIn('id', $store_branches_ids)->where('is_approved', 1)->paginate($req->limit);
        }

        elseif($req->status == 'pending'){
            $store_branches_ids = StoreBranch::where('store_id', $store_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            $store_branches = Store::whereIn('id', $store_branches_ids)->where('is_approved', 0)->where('is_declined', 0)->paginate($req->limit);    
        }
        elseif($req->status == 'declined'){
            $store_branches_ids = StoreBranch::where('store_id', $store_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            $store_branches = Store::whereIn('id', $store_branches_ids)->where('is_declined', 1)->paginate($req->limit);   
        }
        elseif($req->status == 'active'){
            $store_branches_ids = StoreBranch::where('store_id', $store_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            $store_branches = Store::whereIn('id', $store_branches_ids)->where('is_active', 1)->paginate($req->limit);   
        }
        elseif($req->status == 'inactive'){
            $store_branches_ids = StoreBranch::where('store_id', $store_id)->where('store_child_id', '!=', 0)->pluck('store_child_id');
            $store_branches = Store::whereIn('id', $store_branches_ids)->where('is_active', 0)->paginate($req->limit);   
        }else{ return res('failed', 'no data found', 402);}
       

        $data = new StoreListCollection($store_branches);

        return res('success', $data, 200);
    }
    
    /**
     * @param $req
     * @return JsonResponse
     */
    public function branchChangesStatus($req)
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        if(!$req->branch_hashid) return res(__('validation.identifier'), null, 400);

        $branch_id = decode($req->branch_hashid, 'uuid');

        $branch = StoreBranch::where('id', $branch_id)->first();
        if(!$branch)  return res('invalid branch', null, 404);

        switch(auth()->user()->type_info){
            case 'administrator' :
            if($req->approve === 'approve'){ 
            $branch->is_approved = 1;
            $branch->is_declined = 0;
            }
            if($req->approve === 'decline'){ 
            $branch->is_declined = 1;
            $branch->is_approved = 0;
            }
            if($req->approve === 'pending'){ 
            $branch->is_approved = 0;
            $branch->is_declined = 0;
            }
            if($req->status === 'active') $branch->status = 1;
            if($req->status === 'inactive') $branch->status = 0;
            $branch->save();
            return res('success', null, 200);
            break;

            case 'vendor' :
            if($req->approve) return res('Only admin can approve', null, 200);
            if($req->status === 'active') $branch->status = 1;
            if($req->status === 'inactive') $branch->status = 0;
            $branch->save();
            return res('success', null, 200);
            break;

            case 'agent' :
            if($req->approve) return res('Only admin can approve', null, 200);
            if($req->status === 'active') $branch->status = 1;
            if($req->status === 'inactive') $branch->status = 0;
            $branch->save();
            return res('success', null, 200);
            break;

            default :
            return res('invalid key format', null, 401);
            break;
        }
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalBranch($req)
    {
        if(!$req->store_hashid) return res(__('validation.identifier'), null, 400);

        $store_id = decode($req->store_hashid, 'uuid');

        $data = StoreBranch::where('store_id', $store_id)->count();
        
        return res('success', $data, 200);
    }

    public function storeListFilter($req)
    {
        $validator = Validator::make($req->all(), [
            'business_type' => 'required',
            'limit' => 'required',
        ]);
        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $business_type = BusinessType::where('name', $req->business_type)->where('status', 1)->first();
        if(!$business_type) return res('Business type not found', null, 404);

        $stores = Store::where('name', 'LIKE', '%' . $req->key . '%')->where('business_type_id', $business_type->id)->where('is_approved', 1)->orderBy('created_at', 'desc')->paginate($req->limit);

        $data = new StoreListCollection($stores);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function totalStaff($req)
    {
        if(!$req->store_hashid) return res(__('store.identifier'), null, 401);

        $store_id = decode($req->store_hashid, 'uuid');

        $data = StoreStaff::where('store_id', $store_id)->count('id');
        
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function branchShow($req)
    {
        if(!$req->branch_hashid) return res(__('validation.identifier'), null, 400);

        $id = decode($req->branch_hashid, 'uuid');

        $branch = StoreBranch::where('id', $id)->first();
        if(!$branch)  return res('invalid branch', null, 404);
        $data = new BranchShow($branch);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function editBranch($req)
    {
        $permissions = ['agent', 'vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin, agent and vendor can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'branch_hashid' => 'required',
            'name' => 'required|unique:store_branches,name',
        ], [
            'name.unique' => 'The Brand Name has already been taken',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        if(!$req->branch_hashid) return res(__('validation.identifier'), null, 400);

        $branch_id = decode($req->branch_hashid, 'uuid');

        $branch = StoreBranch::where('id', $branch_id)->first();
        if(!$branch)  return res('invalid branch', null, 404);

        if($req->name != null)          $branch->name = $req->name;
        if($req->contact_name != null)  $branch->contact_name = $req->contact_name;
        if($req->street != null)        $branch->street = $req->street;
        if($req->state != null)         $branch->state = $req->state;
        if($req->city != null)          $branch->city = $req->city;
        if($req->other_address_info != null)    $branch->other_address_info = $req->other_address_info;
        if($req->contact_number != null)        $branch->contact_number = $req->contact_number;
        if($req->contact_email != null)         $branch->contact_email = $req->contact_email;
        if($req->latitude != null)              $branch->latitude = $req->latitude;
        if($req->longitude != null)             $branch->longitude = $req->longitude;
        $branch->save();
       
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function customerLikeOrUnlikeStore($req)
    {
        if(!$req->store_hashid) return res(__('store.identifier'), null, 401);

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->where('is_approved', 1)->first();

        if(!$store) return res(__('store.not_found'), null, 404);

        $likeOrUnlike   = StoreLike::where('store_id', $store->id)->where('user_id', auth()->id())->first();

        if(!$likeOrUnlike){
            $like               = new StoreLike;
            $like->store_id     = $store->id;
            $like->user_id      = auth()->id();
            $like->save();

            return res('success', null, 200);
        }
        
        $likeOrUnlike->store_id     = $store->id;
        $likeOrUnlike->user_id      = auth()->id();
        $likeOrUnlike->is_liked     = $likeOrUnlike->is_liked ? 0 : 1;
        $likeOrUnlike->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function customerfolloweOrUnfollowStore($req)
    {
        if(!$req->store_hashid) return res(__('store.identifier'), null, 401);

        $store_id = decode($req->store_hashid, 'uuid');
        $store = Store::where('id', $store_id)->where('is_approved', 1)->first();

        if(!$store) return res(__('store.not_found'), null, 404);

        $followOrUnfollow = StoreFollowing::where('store_id', $store->id)->where('user_id', auth()->id())->first();

        if(!$followOrUnfollow){
            $follow             = new StoreFollowing;
            $follow->store_id   = $store->id;
            $follow->user_id    = auth()->id();
            $follow->save();

            return res('success', null, 200);
        }
        
        $followOrUnfollow->store_id         = $store->id;
        $followOrUnfollow->user_id          = auth()->id();
        $followOrUnfollow->is_followed      = $followOrUnfollow->is_followed ? 0 : 1;
        $followOrUnfollow->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function showStoreOpenApi($req)
    {
        if(!$req->store_hash_id) return res('store_hash_id is required', null, 401);

        $store_id   = decode($req->store_hash_id, 'uuid');
        
        $store      = Store::where('id', $store_id)->first();

        if(!$store) return res('Store not found', null, 404);

        $data       = new StoreResource($store);

        return res('success', $data, 200);
    }
 
    public function store_registration($req)
    {
        $v = Validator::make($req->all(),[
            'business_type_id'                  => 'required',
            'store_name'                        => 'required',
            'store_email'                       => 'required',
            'store_number'                      => 'required',
            'owner_name'                        => 'required',
            'owner_email'                       => 'required',
            'owner_mobile'                      => 'required',
            'street'                            => 'required',
            'city'                              => 'required',
            'state'                             => 'required',
            'latitude'                          => 'required',
            'longitude'                         => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $bti = decode($req->business_type_id, 'uuid');
       // $state = decode($req->state,'uuid');
        $state = State::where('name', $req->state)->pluck('id');
        $city = decode($req->city,'uuid');

        $submission = true;
        $r = generateRegistrationNumber();

        while($submission){
            $count = StorePreregister::where('regnumber', $r)->count();
            if($count > 0) {
                $r = generateRegistrationNumber();
            } else {
                $submission = false;
            }
        }

        $s                                      = new StorePreregister();
        $s->regnumber                           = $r;
        $s->business_type_id                    = $bti;
        $s->store_name                          = $req->store_name;
        $s->store_email                         = $req->store_email;
        $s->store_number                        = $req->store_number;
        $s->owner_name                          = $req->owner_name;
        $s->owner_email                         = $req->owner_email;
        $s->owner_mobile                        = $req->owner_mobile;
        $s->street                              = $req->street;
        $s->city                                = $city;
        $s->state                               = $state;
        $s->latitude                            = $req->latitude;
        $s->longitude                           = $req->longitude;
        $s->other_address_info                  = $req->other_address_info;
        $s->save();

        $bti = BusinessType::select('id','name')->where('id',$bti)->first();
        $body = 'Registration Number:'.$s->regnumber.'Business Type:'.$bti->name.'Store Name:'.$s->store_name.' Owner Name:'.$s->owner_name.' Owner Email:'.$s->owner_email.' Owner Mobile:'.$s->owner_mobile;

        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
            ->setUsername('arisudarbe@gmail.com')
            ->setPassword('passwordnibeshh')
        ;   

        $mailer = new Swift_Mailer($transport);

        $message = (new Swift_Message('New Registration'))
            ->setFrom(['contact@wmall.qa' => $req->owner_name])
            ->setTo(['arisudarbe@gmail.com', 'contact@wmall.qa' => 'A name'])
            ->setBody($body)
        ;

        // Send the message
        $result = $mailer->send($message);

        return res('success', 'Basic Email Sent. Check your inbox.');
    }

    public function preregistration_list($req)
    {
        $permissions = ['administrator', 'admin Staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }
        
        $pr = StorePreregister::select('*')->where('status' , 1)->get();
        if($pr) $pr->makeHidden(['id','status','updated_at']);

        return res('success', $pr);
    }

    public function get_business_type($req)
    {
        $bt = BusinessType::select('id','name','description','status')->where('status', 1)->get();
        if($bt) $bt->makeHidden(['id','description','status']);

        return res('success', $bt);
    }

    public function get_state($req)
    {
        $s = State::select('*')->where('status', 1)->get();
        if($s) $s->makeHidden(['id','state_id','iso','status','created_at','updated_at','hash_id']);

        return res('success', $s);
    }

    public function get_city($req)
    {
        $v = Validator::make($req->all(),[
            'state_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $s = decode($req->state_hash_id, 'uuid');
        
        $c = State::select('*')->where('state_id', $s)->first();
        if($c) $c->makeHidden(['id','state_id','iso','status','created_at','updated_at','state_hash_id','hash_id']);

        return res('success', $c);
    }

    /**
     * Vendor can request income withdrawal
     * @param $req
     * @return JsonResponse
     */
    public function requestWithdrawal($req):object
    {
        DB::beginTransaction();
        $permissions = ['vendor', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can request', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'store_hashid'          => 'required|string',
            'type'                  => 'required|string',
            'amount'                => 'required|int',
        ]);

        $valid_datas = $req->type == 'cheque' ? ['receiver_name', 'qid_number'] : ['bank_name', 'iban'];
        $validator->sometimes($valid_datas, 'required', function ($input) {
            return true;
        });

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $store_id = decode($req->store_hashid, 'uuid');
        $store    = Store::where('id', $store_id)->first();
        if(!$store){
            DB::rollBack();
            return res(__('store.not_found'), null, 401);
        }

        if($store->user_id != auth()->id()){
            DB::rollBack();
            return res(__('store.not_owner'), null, 401);
        }

        $request = VendorRequestWithdrawal::where('store_id', $store_id)->where('type', $req->type)->where('is_approved', 0)->where('is_declined', 0)->where('status', 1)->first();
        if($request){
            DB::rollBack();
            return res(__('store.withdrawal.already_exists'), null, 401);
        }

        $req->request->add(['store_id' => $store_id, 'amount' => priceToCent($req->amount), 'company_name' => $store->name]);
        VendorRequestWithdrawal::create($req->all());
        
        DB::commit();
        return res('success', null, 200);
    }

    /**
     * Vendor withdrawal list
     * @param $req
     * @return JsonResponse
     */
    public function withDrawalList($req):object
    {
        $permissions = ['administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin or agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'limit'  => 'required|int',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $req->request->add(['type' => 'list']);
        $list = VendorRequestWithdrawal::where('status', 1)->orderBy('created_at', 'DESC')->paginate($req->limit);
        $data = new VendorWithdrawalCollection($list);

        return res('success', $data, 200);
    }

    /**
     * show Vendor Withdrawal
     * @param $req
     * @return JsonResponse
     */
    public function showWithDrawal($req):object
    {
        $permissions = ['administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin or agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'hashid'  => 'required|string',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $withdrawal_id  = decode($req->hashid, 'uuid');
        $withdrawal     = VendorRequestWithdrawal::where('id', $withdrawal_id)->where('status', 1)->first();
        if(!$withdrawal) return res(__('store.withdrawal.not_found'), null, 404);

        $req->request->add(['type' => 'show']);
        $data = new VendorWithdrawal($withdrawal);

        return res('success', $data, 200);
    }

    /**
     * show Vendor Withdrawal
     * @param $req
     * @return JsonResponse
     */
    public function withdrawalChangeStatus($req):object
    {
        $permissions = ['administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only admin can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'hashid'  => 'required|string',
            'status'  => 'required|string'  
        ]);
        
        $withdrawal_id  = decode($req->hashid, 'uuid');
        $withdrawal     = VendorRequestWithdrawal::where('id', $withdrawal_id)->where('status', 1)->first();
        if(!$withdrawal) return res(__('store.withdrawal.not_found'), null, 404);
        
        if($req->status == 'approved')  {
            $withdrawal->is_approved = 1;
            $withdrawal->is_declined = 0;
        }

        if($req->status == 'declined')  {
            $withdrawal->is_approved = 0;
            $withdrawal->is_declined = 1;
        }

        if($req->status == 'pending'){
            $withdrawal->is_approved = 0;
            $withdrawal->is_declined = 0;
        }

        $withdrawal->save();
        return res('success', null, 200);
    }
} 
