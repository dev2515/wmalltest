<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlaceIdToUserAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('user_addresses', 'place_id')){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->string('place_id')->after('id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_addresses', 'place_id')){
            Schema::table('user_addresses', function (Blueprint $table) {
                $table->dropColumn('place_id');
            });
        }
    }
}
