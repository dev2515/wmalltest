<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'other_address_info')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('other_address_info')->nullable()->after('city');
            });
        }

        if(!Schema::hasColumn('store_branches', 'contact_number')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('contact_number')->nullable()->after('other_address_info');
            });
        }

        if(!Schema::hasColumn('store_branches', 'contact_email')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->string('contact_email')->nullable()->after('contact_number');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_branches', 'other_address_info')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('other_address_info')->nullable()->after('city');
            });
        }

        if(Schema::hasColumn('store_branches', 'contact_number')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('contact_number')->nullable()->after('other_address_info');
            });
        }

        if(Schema::hasColumn('store_branches', 'contact_email')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('contact_email')->nullable()->after('contact_number');
            });
        }
    }
}
