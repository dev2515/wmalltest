<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use App\Models\Store;
use App\Models\UserAddress;
use Illuminate\Http\Resources\Json\JsonResource;

class CheckoutDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return[
            'transaction_number' => $this->transaction_number,
            'cart_hash_id' => $this->hash_id,
            'delivery_address' => $this->user()->street.', '.$this->user()->city.', '.$this->user()->state,
            'other_address_info' => $this->user()->other_address_info,
            'latitude' => $this->user()->latitude,
            'longitude' => $this->user()->longitude,
            'delivery_time' => $this->store()->order_duration,
            'sub_total' => $this->calculateTotalAmount(),
        ];
    }

    public function user()
    {
        $u = UserAddress::select('id','user_id', 'street', 'state', 'city', 'other_address_info','latitude','longitude')->where('user_id', $this->user)->first();
        return $u;
    }

    public function store()
    {
        $s = Store::select('id','order_duration')->where('id', $this->restaurant_id)->first();
        return $s;
    }


    public function calculateTotalAmount() 
    {
        $total = 0.00;
        $total_count = 0;
        $cart_ids = RestaurantCart::where('id', $this->id)->where('user_id', $this->user_id)->where('is_deleted', 0)->pluck('id');
        $items = RestaurantCartItem::whereIn('cart_id', $cart_ids)->where('is_deleted', 0)->where('is_ordered', 0)->get();
        
        foreach ($items as $item) {
    
           // return res($v_item)->send();
            $total += $item->totalamount;
            $total_count += $total_count + 1;
        }

        $t = collect(['total' => twoDecimalPlaces($total), 'total_count' => $total_count]);
        return $t;
    }
}
