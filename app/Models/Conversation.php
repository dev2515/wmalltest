<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table = 'conversations';
    protected $append =  ['hashId'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
}
