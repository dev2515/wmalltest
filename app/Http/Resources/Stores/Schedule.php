<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\StoreTiming;

class Schedule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'day_hashid'  => encode($this->pivot->id, 'uuid'),
            'day'       => $this->name,
            'is_open'   => $this->pivot->is_opened,
        ];
    }

    public function getTimeSchedules()
    {
        $time_schedules = StoreTiming::where('store_schedule_id', $this->pivot->id)->get();
        $data = new TimeScheduleCollection($time_schedules);
        return $data;
    }
}
