<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('carts')) {
            Schema::create('carts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('user_id')->unsigned();
                $table->integer('store_id')->unsigned();
                $table->double('voucher_value')->nullable()->default(null);
                $table->string('voucher_type')->nullable()->default(null);
                $table->json('voucher_condition')->nullable()->default(null);
                $table->integer('on_checkout')->default(0);
                $table->integer('is_ordered')->default(0);
                $table->integer('is_deleted')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
