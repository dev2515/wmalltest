<?php

namespace App\Repositories;

use App\Http\Resources\Driver\DriverReviewResource;
use App\Http\Resources\Restaurant\FoodReviewResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Store\StoreReview;
use App\Http\Resources\User\MyReview;
use App\Interfaces\ReviewInterface;
use App\Models\Review;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class ReviewRepository implements ReviewInterface
{
    // Store and List
    public function store_review_list($req)
    {   
        $v = Validator::make($req->all(),[
            'category' => 'required',
            'type_hash_id' => 'required',
        ]);
        
        if($v->fails()) return res('failed', $v->errors(), 402);

        $type_id = decode($req->type_hash_id, 'uuid');

        $category = ['Store','Food', 'Driver'];

        if(!in_array($req->category, $category)) return res('No category found.','null', 402);

        if($req->category == 'Store')
        {
            $r = DB::table('stores')
                ->select('id', 'name')
                ->where('id', $type_id)->first();
            
            if($r) $r = new StoreReview($r);
        }

        if($req->category == 'Food' )
        {
            $r = DB::table('restaurant_foods')
                ->select('id', 'name')
                ->where('id', $type_id)->where('status', 1)->first();

            if($r) $r = new FoodReviewResource($r);
        }

        if($req->category == 'Driver')
        {
            $r = DB::table('users')
                ->select('id', 'name')
                ->where('id', $type_id)->where('type', 4)->first();
            
            if($r) $r = new DriverReviewResource($r);
        }

        return res('success', $r);
    }

    public function store_review_add($req)
    {
        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)) return res('Only customer can do this process.', null, 401);
    
        $v = FacadesValidator::make($req->all(),[
            'category' => 'required',
            'type_hash_id'  => 'required',
            'rate'     => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $id = decode($req->type_hash_id, 'uuid');

        $category = ['Store','Food', 'Driver'];

        if(!in_array($req->category, $category)) return res('No category found.',null, 402);

        $r = Review::updateOrCreate(
            [
                'user_id'   => auth()->user()->id,
                'type_id'   => $id,
                'category'  => $req->category,
            ],
            [
                'category'                  => $req->category,
                'type_id'                   => $id,
                'rate'                      => $req->rate,
                'content'                   => $req->content,
                'status'                    => 1,
                'user_id'                   =>auth()->user()->id,
            ]
        );

        return res('success');
            
    }

    public function store_review_remove($req)
    {
        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)) return res('Only customer can do this process.', null, 402);

        $v = FacadesValidator::make($req->all(),[
            'type_hash_id' => 'required',
            'category'     => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $type = decode($req->type_hash_id, 'uuid');

        $category = ['Store', 'Driver', 'Food'];
        if(!in_array($req->category, $category)) return res('No cateegory found.', null, 402);

        $r = Review::where('category', $req->category)->where('type_id', $type)->where('user_id', auth()->user()->id)->first();
        if(!$r)
        {
            return res('failed','No review found in this '.$req->category.' category');
        }
        $r->status = 0;
        $r->save();

        return res('success','Your rating and review has been remove.');
    }

    public function my_review_and_rating($req)
    {
        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)) return res('Only customer can do this process.',null, 402);
        
        $v = Validator::make($req->all(),[
            'category' => 'required',
            'type_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $type = decode($req->type_hash_id, 'uuid');
        $category = ['Store', 'Driver', 'Food'];

        if(!in_array($req->category, $category)) return res('Failed!', 'No category found.', 402);

        if($req->category == 'Store')
        {
            $r = DB::table('reviews')->join('stores', 'reviews.type_id', '=', 'stores.id')
                ->select('stores.name','reviews.rate','reviews.content','reviews.updated_at')
                ->where('reviews.category', 'Store')
                ->where('reviews.type_id', $type)
                ->where('reviews.user_id', auth()->user()->id)
                ->where('reviews.status', 1)
                ->first();
        }
        
        if($req->category == 'Driver')
        {
            $r = DB::table('reviews')->join('users', 'reviews.type_id', '=', 'users.id')
                ->select('users.name','reviews.rate','reviews.content','reviews.created_at','reviews.updated_at')
                ->where('reviews.category', 'Driver')
                ->where('reviews.type_id', $type)
                ->where('reviews.user_id', auth()->user()->id)
                ->where('reviews.status', 1)
                ->where('users.type', 4)
                ->first();
        }

        if($req->category == 'Food')
        {
            $r = DB::table('reviews')->join('restaurant_foods', 'reviews.type_id', '=', 'restaurant_foods.id')
                ->select('restaurant_foods.name','reviews.rate', 'reviews.content', 'reviews.created_at','reviews.updated_at')
                ->where('reviews.category', 'Food')
                ->where('reviews.type_id', $type)
                ->where('reviews.user_id', auth()->user()->id)
                ->where('reviews.status', 1)
                ->first();
        }

        return res('Success', $r);

    }
}