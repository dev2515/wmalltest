<?php

namespace App\Http\Controllers\API\Checkout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CheckoutInterface;

class CheckoutController extends Controller
{
    protected $req;
    protected $checkout;

    public function __construct(Request $req, CheckoutInterface $checkout)
    {
        $this->req = $req;
        $this->checkout = $checkout;
    }

    public function list()
    {
        return $this->checkout->list($this->req);
    }

    public function placeOrder()
    {
        return $this->checkout->placeOrder($this->req);
    }
}
