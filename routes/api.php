<?php

use App\Models\RandomWord;
use Illuminate\Support\Facades\Route;
use App\Models\SocialMedia;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}




// OPEN ROUTE
Route::group(['namespace' => 'API'], function () {
    Route::get('/', function () {
        return res('API Server V1 is UP');
    });
    
   

  

    Route::get('/unauthenticated', function () {
        return res(__('auth.unauthenticated'), null, 401);
    })->name('unauthenticated');

    
  

    // OPEN
    Route::group([], function () {
        Route::get('/get-default-country', 'CountryController@default');
        Route::get('/get-countries', 'CountryController@list');
        Route::get('/get-states', 'CountryController@states');
        Route::get('/get-top-categories', 'CategoryController@top');
        Route::get('/get-vendor-types', 'OpenController@vendorTypes');
        Route::get('/get-security-question', 'OpenController@securityQuestions');
        Route::get('/get-product-types', 'OpenController@productTypes');
        Route::get('/get-categories', 'OpenController@categories');
        Route::get('/get-banks', 'OpenController@banks');
        Route::get('/get-vendor-terms-and-conditions', 'OpenController@getVendorTermsAndConditions');
        Route::get('/get-about-us', 'OpenController@getAboutUs');
        Route::get('/get-buyer-protection', 'OpenController@getBuyerProtection');
        Route::get('/get-shipping-providers', 'OpenController@getShippingProviders');
        Route::get('/get-stock-statuses', 'OpenController@getStockStatus');
        Route::get('/get-privacy-policy', 'OpenController@getPrivacyPolicy');
        Route::get('/get-banner-names', 'OpenController@getBannerNames');
        Route::get('/get-payment-methods', 'OpenController@getPaymentMethods');
        Route::post('/get-store-change-fields', 'OpenController@getStoreChangeFields');
        Route::post('/get-nationalities', 'OpenController@getNationalities');
        Route::post('/check-email-availability', 'OpenController@checkEmailAvailability');
        Route::post('/check-mobile-availability', 'OpenController@checkMobileAvailability');
        Route::post('/get-product-photos', 'OpenController@getProductPhotos');
        Route::post('/convert-currency', 'OpenController@convertCurrency');
        Route::post('/store-list', 'OpenController@storeListFilter');
        Route::post('/store-advertisement-list', 'OpenController@storeAdvertisementList');
        Route::post('/store-show', 'OpenController@showStoreOpenApi');
        Route::get('/test', function() {
            $words = RandomWord::get();
            return res('success', $words);
        });


        Route::group(['prefix' => 'restaurant', 'namespace' => 'Restaurant'], function(){
            Route::get('list', 'RestaurantController@list');
            Route::get('near-me', 'RestaurantController@nearme');

            Route::get('list-food-types', 'RestaurantController@listOfFoodType');
            Route::get('list-by-followers', 'RestaurantController@listByFollowers');
            Route::get('test-by-rate', 'RestaurantController@testlistbyrate');
            Route::get('by-rate', 'RestaurantController@listByRate');
            Route::get('by-restaurant', 'RestaurantController@listByRestaurant');
            Route::post('menu', 'RestaurantController@menulist');
            Route::post('category/list', 'RestaurantController@ListByCategory');
            Route::post('food/list', 'RestaurantController@foodlist');
            Route::post('/food-search-key', 'RestaurantController@search_food');
            Route::post('/food/detail', 'RestaurantController@showFood');
            Route::post('/location', 'RestaurantController@restaurantCoordinates');
            Route::post('/image/list', 'RestaurantController@RestaurantImgList');

            Route::post('/information', 'RestaurantController@RestaurantShow');
            Route::post('/list/all', 'RestaurantController@listByAllRestaurant');
            Route::post('/edit-order-duration', 'RestaurantController@editOrderDuration');
            Route::post('/search/key', 'RestaurantController@searchMonster');
            Route::post('/details', 'RestaurantController@restaurant_details');
            Route::post('/menu-list', 'RestaurantController@menuList');
            Route::post('/variation-list', 'ManagerController@listVariationSelection');
            Route::post('/search-key', 'RestaurantController@search_store');
            Route::post('/search-food-key', 'RestaurantController@search_store_food');
            Route::post('/get-distance','RestaurantOrderController@get_distance_store_to_customer');
            Route::post('/find-near-restaurants', 'RestaurantCartController@get_customer_currnet_location');
        });

        Route::group(['prefix' => 'vendor', 'namespace' => 'Vendor'], function(){
            Route::post('/business-types', 'VendorController@get_business_type');
            Route::post('/state-list', 'VendorController@get_state');
            Route::post('/city-list', 'VendorController@get_city');
            Route::post('/pre-register', 'VendorController@store_registration');
        });

        Route::group(['prefix' => 'wmall-email',  'namespace' => 'Message'], function(){
            Route::post('/send', 'MessageController@sendEmail');
        });

        Route::group(['prefix' => 'review', 'namespace' => 'Review'], function(){
            Route::post('/list', 'ReviewController@store_review_list');
        });

        Route::group(['prefix' => 'blogger', 'namespace' => 'Blogger'], function(){
            Route::post('/list', 'BloggerController@blogger_list');
            Route::post('/category-list', 'BloggerController@category_list');
            Route::post('/product-list', 'BloggerController@product_list');
        });

        // Route::group(['prefix' => 'vendor', 'namespace' => 'Vendor'], function () {
        //     Route::post('/register', 'VendorController@register');
        // });

        Route::group(['prefix' => 'product', 'namespace' => 'Product'], function () {
            Route::post('/most-popular', 'ProductController@mostPopular');
            Route::post('/details', 'ProductController@details');
            Route::post('/items', 'ProductController@items');
            Route::post('/show-in-open-api', 'ProductController@productShowInOpenAPI');
            Route::get('/brands', 'ProductController@getBrands');
            Route::post('/open-list', 'ProductController@open_product_list');
        });

        Route::group(['prefix' => 'file-transfer', 'namespace' => 'FileTransfer'], function () {
            Route::post('/upload', 'FileTransferController@upload');
            Route::post('/download', 'FileTransferController@download');
        });

        Route::group(['prefix' => 'store', 'namespace' => 'Store'], function () {
            Route::post('/find', 'StoreController@find');
            Route::post('/details', 'StoreController@details');
            Route::post('/show', 'StoreController@show');
        });

        Route::group(['prefix' => 'paypal', 'namespace' => 'CustomerPayment'], function() {
            Route::post('/create-payment', 'PayPalController@createPayment');
            Route::post('/execute-payment', 'PayPalController@executePayment');
        });
    });

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::group(['prefix' => 'register'], function () {
            Route::post('/email', 'RegisterController@email');
            Route::post('/mobile', 'RegisterController@mobile');
            Route::post('/blogger-email', 'RegisterController@blogger_registration');

            Route::post('/verify-email', 'RegisterController@verifyEmail');
            Route::post('/verify-mobile', 'RegisterController@verifyMobile');
            Route::post('/verify-mobile-otp', 'RegisterController@verifyMobileOtp');

            Route::post('/check-mobile', 'RegisterController@checkMobile');

            Route::post('/resend-email-token', 'RegisterController@sendEmailVerificationToken');
            Route::post('/resend-mobile-token', 'RegisterController@sendMobileVerificationToken');
        });

        Route::group(['prefix' => 'login'], function () {
            Route::post('/email', 'LoginController@email');
            Route::post('/mobile', 'LoginController@mobile');
            Route::post('/mobile-otp', 'LoginController@mobileOtp');
            Route::post('/agent', 'LoginController@agent');
            Route::post('/vendor', 'LoginController@vendor');
            Route::post('/driver', 'LoginController@driver');

            Route::post('/facebook', 'LoginController@facebook');
            Route::post('/twitter', 'LoginController@twitter');
            Route::post('/instagram', 'LoginController@instagram');
            Route::post('/apple', 'LoginController@apple');
            Route::post('/google', 'LoginController@google');
        });

        Route::group(['prefix' => 'request-password-token'], function () {
            Route::post('/request-email', 'PasswordController@requestEmail');
            Route::post('/request-mobile', 'PasswordController@requestMobile');
            Route::post('/change', 'PasswordController@change');
        });
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/check-authentication', function () {
            if (auth() && auth()->user()) {
                $u = auth()->user();
                $social = SocialMedia::where('id',$u->id)->where('status',1)->first();
                $user = [
                    'user_id'       => $u->id,
                    'hashid'        => encode($u->id, 'uuid'),
                    'social_id'     => $social ? $social->social_id : null,
                    'email'         => $u->email,
                    'mobile_prefix' => $u->mobile_prefix,
                    'mobile'        => $u->mobile,
                    'name'          => $u->name,
                    'type_info'     => $u->type_info,
                    'gender'        => $u->gender,
                    'bdate'         => $u->bdate,
                ];
                // NOTE: Also update on LoginRepository
                return res('Authenticated', compact('user'));
            }
        });

        Route::group(['prefix' => 'blogger', 'namespace' => 'Blogger'], function(){
            Route::post('add-product', 'BloggerController@add_product');
            Route::post('/add-category-image', 'BloggerController@add_product_category_image');
            Route::post('/create-my-shop', 'BloggerController@add_boutique_shop');
        });

        Route::group(['prefix' => 'qrcode', 'namespace' => 'QRCode'], function () {
            Route::post('/scan', 'QRCodeController@scan');
            Route::post('/bind', 'QRCodeController@bind');
        });

        Route::group(['namespace' => 'Auth'], function () {
            Route::post('/logout', 'LoginController@logout');
        });

        Route::group(['prefix' => 'vendor', 'namespace' => 'Vendor'], function () {
            
            Route::post('/preregistration-list', 'VendorController@preregistration_list');
            Route::group(['prefix' => 'profile'], function () {
                Route::post('/show', 'VendorController@profile');
                Route::post('/get-security-questions', 'VendorController@getSecurityQuestions');
                Route::post('/update-store', 'VendorController@updateStore');
                Route::post('/update-bank', 'VendorController@updateBank');
                Route::post('/update-security', 'VendorController@updateSecurity');
                Route::post('/register', 'VendorController@register');
                Route::post('/change-status', 'VendorController@changeStatus');
                Route::post('/update-banners', 'VendorController@updateBanners');
                Route::post('/business-types', 'VendorController@businessTypes');
            });

            Route::group(['prefix' => 'withdrawal'], function () {
                Route::post('/request', 'VendorController@requestWithdrawal');
                Route::post('/change-status', 'VendorController@withdrawalChangeStatus');
                Route::post('/list', 'VendorController@withDrawalList');
                Route::post('/show', 'VendorController@showWithDrawal');
            });
            
            Route::group(['prefix' => 'product'], function () {
                Route::post('/list', 'ProductController@list');
                Route::post('/show', 'ProductController@show');
                Route::post('/add', 'ProductController@add');
                Route::post('/update', 'ProductController@update');
                Route::post('/upload-images', 'ProductController@uploadImages');
                Route::post('/add-image', 'ProductController@add_image_product');
                Route::post('/total-products', 'ProductController@totalProducts');
            });

            Route::group(['prefix' => 'order'], function () {
                Route::post('/list', 'OrderController@list');
                Route::post('/show', 'OrderController@show');
                Route::post('/track', 'OrderController@trackOrder');
                Route::post('/history', 'OrderController@orderHistory');
            });

            Route::group(['prefix' => 'store'], function () {
                Route::post('/list', 'VendorController@allStatusList');
                Route::post('/show', 'VendorController@showStore');
                Route::get('/my-store', 'VendorController@myStore');
                Route::post('/search', 'VendorController@searchStore');
                Route::post('/search-by-status', 'VendorController@searchByStoreStatus');
                Route::post('/theme-list', 'VendorController@themeList');
                Route::post('/change-theme', 'VendorController@changeTheme');
                Route::post('/change-informations', 'VendorController@changeInformations');
                Route::post('/change-description', 'VendorController@changeDescription');
                Route::post('/change-locations', 'VendorController@changeLocations');
                Route::post('/change-profile-photo', 'VendorController@changeProfilePhoto');
                Route::post('/change-cover-photo', 'VendorController@changeCoverPhoto');
                Route::post('/add-branches', 'VendorController@addBranches');
                Route::post('/edit-branch', 'VendorController@editBranch');
                Route::post('/branch-change-status', 'VendorController@branchChangesStatus');
                Route::post('/branches', 'VendorController@storeBranches');
                Route::post('/branch-list', 'VendorController@branchList');
                Route::post('/total-branch', 'VendorController@totalBranch');
                Route::post('/documents', 'VendorController@documents');
                Route::post('/add-document-images', 'VendorController@addDocumentImages');
                Route::post('/stores', 'VendorController@vendorStores');
                Route::post('/search-own-stores', 'VendorController@searchOwnStores');
                Route::post('/add-food', 'VendorController@addFood');
                Route::post('/branch-show', 'VendorController@branchShow');
                Route::post('/customer-likeOrUnlike-store', 'VendorController@customerLikeOrUnlikeStore');
                Route::post('/customer-followeOrUnfollow-store', 'VendorController@customerfolloweOrUnfollowStore');
                Route::post('/pre-registration-list', 'VendorController@preregistration_list');
            });


            Route::group(['prefix' => 'staff'], function() {
                Route::post('/add', 'VendorController@addStaff');
                Route::post('/show', 'VendorController@showStaff');
                Route::post('/list', 'VendorController@listStaff');
                Route::post('/search-staff', 'VendorController@searchStaff');
                Route::get('/pending-staff-list', 'VendorController@pendingStaffList');
                Route::post('/total-staff', 'VendorController@totalStaff');
            });

            Route::group(['prefix' => 'agent'], function() {
                Route::post('/dashboard-new', 'VendorController@agentNewVendor');
                Route::post('/dashboard-prime', 'VendorController@agentPrime');
                Route::post('/dashboard-oracle', 'VendorController@agentOracle');
                Route::post('/dashboard-sailors', 'VendorController@agentSailore');
                Route::get('/dashboard-over-all', 'VendorController@agentOverAll');
            });

            Route::group(['prefix' => 'user'], function() {
                Route::post('/change-profile-photo', 'VendorController@agentChangeProfilePhoto');
                Route::get('/agent-list', 'VendorController@agentList');
            });

            Route::group(['prefix' => 'search'], function() {
                Route::post('/sailor', 'VendorController@searchSailor');
                Route::post('/oracle', 'VendorController@searchOracle');
                Route::post('/prime', 'VendorController@searchPrime');
                Route::post('/new', 'VendorController@searchNew');
                Route::post('/search-pending-branch', 'VendorController@searchPendingBranch');
                Route::post('/search-approve-branch', 'VendorController@searchApproveBranch');

            });

            Route::group(['prefix' => 'notification'], function() {
                Route::get('/list', 'VendorController@notificationList');
                Route::get('/count', 'VendorController@notificationCount');
            });

            Route::group(['prefix' => 'customer'], function() {
                Route::get('/all-vendor-approved', 'VendorController@allVendorApproved');
                Route::post('/search-approved-vendor', 'VendorController@searchApprovedVendor');
            });
        });

        Route::group(['prefix' => 'vendor/store', 'namespace' => 'Store'], function(){
            Route::get('/food-types', 'StoreController@foodTypes');
            Route::get('/change-field-types', 'StoreController@changeFieldTypes');
            Route::group(['prefix' => 'schedule'], function(){
                Route::post('/list', 'StoreController@listOfdays');
                Route::post('/show', 'StoreController@showTimeSchedule');
                Route::post('/add', 'StoreController@addTimeShedule');
                Route::post('/edit', 'StoreController@editTimeSchedule');
                Route::post('/delete', 'StoreController@deleteTimeSchedule');
                Route::post('/open-or-close', 'StoreController@openOrClose');
            });

            Route::group(['prefix' => 'advertisement'], function() {
                Route::post('/add', 'StoreController@addAdvertisement');
                Route::post('/edit', 'StoreController@editAdvertisement');
                Route::post('/delete', 'StoreController@deleteAdvertisement');
                Route::get('/duration-list', 'StoreController@durationList');
            });
        });

        Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
            Route::get('over-all', 'AdminController@overAll');
            Route::post('marketplace-list', 'AdminController@marketplaceList');
            Route::post('hotel-list', 'AdminController@hotelList');
            Route::post('restaurant-list', 'AdminController@restaurantList');
            Route::post('activities-list', 'AdminController@activitiesList');
            
            Route::group(['prefix' => 'approved'], function () {
                Route::post('all-add-food', 'AdminController@approvedAllAddFood');
                Route::post('all-edit-food', 'AdminController@approvedAllEditFood');
            });

            Route::group(['prefix' => 'dashboard'], function() {
                Route::post('/restaurant', 'AdminController@dashboardRestaurant');
            });

            Route::group(['prefix' => 'store-change-fields'], function () {
                Route::post('change-status', 'AdminController@changeStatus');
                Route::post('status', 'AdminController@status');
                Route::post('show', 'AdminController@showRequest');
                Route::get('total-request', 'AdminController@totalRequest');
                Route::get('total-vendor', 'AdminController@totalvendor');
            });

            Route::group(['prefix' => 'store'], function(){
                Route::post('modify-active-status', 'AdminController@modifyActiveStatus');
            });

            Route::group(['prefix' => 'order'], function(){
                Route::post('order-history', 'OrderController@orderHistory');
            });

           
        });
        
        Route::group(['prefix' => 'driver', 'namespace' => 'Driver'], function(){
            Route::post('/add', 'DriverController@add');
            Route::post('/edit', 'DriverController@edit');
            Route::post('/list', 'DriverController@list');
            Route::post('/admin-driver-account-approve', 'DriverController@driverAccountApprove');
            Route::post('/show', 'DriverController@show');
            Route::post('/near-by', 'DriverController@nearBy');
            Route::post('/transaction-history', 'DriverController@transactionHistory');

            Route::group(['prefix' => 'document'], function () {
                Route::post('add', 'DriverController@addDocument');
                Route::post('list', 'DriverController@listDocument');
            });

            Route::group(['prefix' => 'vehicle'], function () {
                Route::post('/admin-vehicle-approve', 'DriverController@adminVehicleApprove');
                Route::post('/vehicle-list', 'DriverController@vehicleList');
                Route::post('/vehicle-show', 'DriverController@vehicleShow');
            });

            Route::group(['prefix' => 'my'], function (){
                Route::get('/preferred-delivery', 'DriverController@myPreferredDelivery');
                Route::get('/account', 'DriverController@account');
                Route::post('/update-preferred-delivery', 'DriverController@updatePreferredDelivery');
                Route::post('/delivery-list', 'DriverController@deliveryList');
                Route::post('/current-location', 'DriverController@myCurrentLocation');
                Route::post('/order-list', 'DriverController@driver_accepted_list');
                Route::post('/available-orders', 'DriverController@driver_order_list');
                Route::post('/accept-order', 'DriverController@driver_accept_order');
                Route::post('/arrived-in-location', 'DriverController@driver_update_location');
                Route::post('/order/history', 'DriverController@driver_order_history');
                Route::post('/credits', 'DriverController@driver_net_income');
                Route::post('/settings/update', 'DriverController@driver_notification_settings');
                Route::post('/current-status', 'DriverController@driver_current_status');
                Route::post('/today-credits', 'DriverController@driver_credits_today');
            });

            Route::group(['prefix' => 'store'], function(){
                Route::post('/order-from-restaurant-to-driver', 'DriverController@driver_scan_order_restaurant');
            });

            Route::group(['prefix' => 'order'], function(){
                Route::post('/qrcode-scan', 'DriverController@customer_order_recieve');
                Route::post('/complete', 'DriverController@driver_confirm_order');
                Route::post('/delete-transaction', 'DriverController@delete_unnecessary');
                Route::post('/order-reciept', 'DriverController@order_reciept');
            });

            Route::post('admin/drivers-income', 'DriverController@drivers_collect_income');
        });

        Route::group(['prefix' => 'restaurant', 'namespace' => 'Restaurant'], function(){
            Route::post('near-me', 'RestaurantController@nearme');
            Route::post('food-types', 'RestaurantController@foodTypes');
            Route::post('food/food-type', 'RestaurantController@listByCategory');
            Route::post('list-by-followers', 'RestaurantController@listByFollowers');
            Route::post('by-rate', 'RestaurantController@listByRate');
            Route::post('total-category', 'RestaurantController@totalCategory');
            Route::post('add-location', 'RestaurantController@addLocation');
            Route::post('list-with-follow', 'RestaurantController@storeListwithFollow');
            Route::post('is-followed', 'RestaurantController@isFollowed');
            Route::post('store-list-food-types', 'RestaurantController@storeListOfFoodType');

            Route::group(['prefix' => 'favorites'], function(){
                Route::post('my-favorites', 'RestaurantController@favoriteList');
                Route::post('add-to-my-favorite', 'RestaurantController@addFavorite');
                Route::post('list', 'RestaurantController@restaurantFavorite');
                Route::post('remove', 'RestaurantController@removeFavorite');
            });

            Route::group(['prefix' => 'wmall-manager'], function(){

                Route::group(['prefix' => 'store-operation'], function() {
                    Route::post('add-busy-duration', 'ManagerController@addBusyduration');
                    Route::post('stop-busy-duration', 'ManagerController@stopBusyduration');
                    Route::post('close-open', 'ManagerController@closeOpen');
                });

                Route::group(['prefix' => 'services'], function(){
                    Route::post('list', 'ManagerController@listOfServices');
                    Route::post('update', 'ManagerController@updateOfServices');
                });

                Route::group(['prefix' => 'food-type'], function(){
                    Route::post('add-food', 'ManagerController@addFoodInFoodType');
                    Route::post('add-food-to-sub-branch', 'ManagerController@addFoodInFoodTypeSubBranch');
                    Route::post('remove-food', 'ManagerController@removeFoodInFoodType');
                    Route::post('food-list', 'ManagerController@foodList');
                    Route::post('added-food', 'ManagerController@addedFoods');
                    Route::post('not-added-food', 'ManagerController@notAddedFoods');
                });

                Route::group(['prefix' => 'food'], function() {
                    Route::post('add', 'ManagerController@addFood');
                    Route::post('edit', 'ManagerController@editFood');
                    Route::post('delete', 'ManagerController@deleteFood');
                    Route::post('show', 'ManagerController@showFood');
                    Route::post('list-variation-selection', 'ManagerController@listVariationSelection');
                    Route::post('add-variation-selection', 'ManagerController@addVariationSelection');
                    Route::post('edit-variation-selection', 'ManagerController@editVariationSelection');
                    Route::post('delete-variation-selection', 'ManagerController@deleteVariationSelection');
                    Route::post('add-child-variation', 'ManagerController@addChildVariation');
                    Route::post('edit-child-variation', 'ManagerController@editChildVariation');
                    Route::post('delete-child-variation', 'ManagerController@deleteChildVariation');
                });
            });

            Route::group(['prefix' => 'images'], function(){
                Route::post('add', 'RestaurantController@addRestaurantImage');
                Route::post('update', 'RestaurantController@updateRestaurantImage');
            });

            //Route::post('ratings', 'RestaurantController@listByRatings');

            Route::group(['prefix' => 'food-type'], function(){
                Route::post('add', 'FoodCategoryController@add');
                Route::post('update', 'FoodCategoryController@update');
                Route::post('list', 'FoodCategoryController@list');
                Route::post('add-my-food-type', 'FoodCategoryController@addMyFoodType');
                Route::post('update-my-food-type', 'FoodCategoryController@updateMyFoodType');
            });

            Route::group(['prefix' => 'food'], function(){
                Route::post('add', 'RestaurantController@addFood');
                Route::post('edit-food', 'RestaurantController@updateFood');
                Route::post('food-by-category', 'RestaurantController@foodByCategory');
                Route::post('list-search-status', 'RestaurantController@listSearchStatus');
                Route::post('show-food', 'RestaurantController@showFood');
                Route::post('food-change-status', 'RestaurantController@foodChangeStatus');
                Route::post('total-food', 'RestaurantController@totalFood');
                Route::post('my-food-type', 'RestaurantController@myFoodType');
                Route::post('food-type-list', 'RestaurantController@foodTypeList');
                Route::post('total-food-type', 'RestaurantController@totalFoodType');
                Route::get('food-category-list', 'RestaurantController@foodCategoryList');
                Route::post('admin-food-change-status', 'RestaurantController@adminFoodChangeStatus');
                Route::post('list-by-meals', 'RestaurantController@listByMeals');
                Route::get('meals-list', 'RestaurantController@mealsList');
                Route::post('list-of-food-by-my-food-type', 'RestaurantController@listOfFoodByMyFoodType');
                Route::post('total-of-food-by-my-food-type', 'RestaurantController@totalOfFoodByMyFoodType');
                Route::post('admin-change-name', 'RestaurantController@adminChangeName');
                Route::post('add-multiple-food', 'RestaurantController@addMultipleFood');

            });

            Route::group(['prefix' => 'table'],function(){
                Route::group(['prefix' => 'reservation'], function(){
                    Route::post('list', 'TableReservationController@list');
                    Route::post('qrcode', 'TableReservationController@myqrcode');
                    Route::post('my-history', 'TableReservationController@myreservationhistory');
                    Route::post('date-reserve', 'TableReservationController@listbydatereserve');
                    Route::post('list-by-date', 'TableReservationController@listbydate');
                    Route::post('add', 'TableReservationController@add');
                    Route::post('update', 'TableReservationController@update');
                    Route::post('cancel', 'TableReservationController@cancel');
                    Route::post('accept', 'TableReservationController@acceptreservation');
                    Route::post('myreservation', 'TableReservationController@myreservation');
                    Route::post('/scan-qrcode', 'TableReservationController@scanQRcode');
                    Route::post('/scanned/list', 'TableReservationController@scannedQRcode');
                    Route::post('/with-food', 'TableReservationController@reservation_info');
                    Route::post('/qrcode/list', 'TableReservationController@restaurant_qrcode_list');
                    Route::post('/table/assign', 'TableReservationController@assignTable');
                    Route::post('/cart-details', 'TableReservationController@scan_cart_detail');
                    Route::post('/checkout/details', 'TableReservationController@reservation_checkout_details');
                    Route::post('/checkout', 'RestaurantOrderController@reservation_checkout');
                    Route::post('/my-reservation-list', 'TableReservationController@my_reservation_list');
                });
            });

            Route::group(['prefix' => 'order'], function(){
                Route::post('list', 'RestaurantOrderController@list');
                Route::post('place', 'RestaurantOrderController@add');
                Route::post('open-list', 'RestaurantOrderController@restaurantOrderList');
                Route::post('status-update', 'RestaurantOrderController@order_status_update');
                Route::post('my-order', 'RestaurantOrderController@myOrderlist');
                Route::post('list-by-type', 'RestaurantOrderController@listOrderByType');
                Route::post('search-qrcode', 'RestaurantOrderController@orderQRCode');
                Route::post('show', 'RestaurantOrderController@show');
                Route::post('total', 'RestaurantOrderController@totalOrder');
                Route::post('details', 'RestaurantOrderController@orderFoodDetail');
                Route::post('checkout', 'RestaurantOrderController@checkout');
                Route::post('dine-in-order', 'RestaurantOrderController@dineInOrder');
                Route::post('qrcode/list', 'RestaurantOrderController@qrcodeList');
                Route::post('/my-qrcode', 'RestaurantCartController@getQRCode');
                Route::post('/add-to-reservation', 'RestaurantOrderController@add_order_in_reservation');
                Route::post('/confirm-order-details', 'RestaurantOrderController@confirm_order_details');
                Route::post('/order-progress', 'RestaurantOrderController@order_progress');
                Route::post('/pickup/list', 'RestaurantOrderController@get_pickup_orders');
                Route::post('/my-order-list', 'RestaurantOrderController@my_order_list');
                Route::post('/my-order-info', 'RestaurantOrderController@my_order_info');
                Route::post('/deliver-order-list', 'RestaurantOrderController@deliver_order_list');
                Route::post('/pickup-order-list', 'RestaurantOrderController@pickup_order_list');
                Route::post('/search-order-number', 'RestaurantOrderController@get_order_using_order_number');
                Route::post('/total-status-count', 'RestaurantOrderController@total_status_count');
            });

            Route::group(['prefix' => 'cart'], function(){
                Route::post('list', 'RestaurantCartController@list');
                Route::post('/my-cart-list', 'RestaurantCartController@mycartlist');
                Route::post('add', 'RestaurantCartController@add');
                Route::post('count', 'RestaurantCartController@count');
                Route::post('clear', 'RestaurantCartController@clear');
                Route::post('update-quantity', 'RestaurantCartController@updateQuantity');
                Route::post('toggle-checkout', 'RestaurantCartController@toggleCheckout');
                Route::post('remove', 'RestaurantCartController@remove');
                Route::post('checkout', 'RestaurantCartController@checkout');
                Route::post('total', 'RestaurantCartController@cartTotal');
                Route::post('/order-confirm', 'RestaurantCartController@CartConfirmation');
                Route::post('/view', 'RestaurantCartController@open_cart');
                Route::post('/checkout-details', 'RestaurantOrderController@checkout_details');
                Route::post('/delivery-address', 'RestaurantOrderController@default_address');
                Route::post('/add-to-cart', 'RestaurantCartController@addtoCart');
            });

            Route::group(['prefix' => 'manager'], function(){
                Route::post('order/list', 'RestaurantManagementController@dine_in_order_list');
                Route::post('/order/list/waiter', 'RestaurantManagementController@dine_in_order_list_waiter');
                Route::post('/order/status-update', 'RestaurantManagementController@waiter_confirmation_order');
                Route::post('/order/complete-order', 'RestaurantManagementController@complete_order');
                Route::post('/food/status/update', 'RestaurantManagementController@updateFoodStatus');
                Route::post('/cart/re-open', 'RestaurantCartController@reopenCart');
                Route::post('/add-to-cart', 'RestaurantCartController@addOrderInExistingCart');
                Route::post('/order-update', 'RestaurantOrderController@updateOrder');
                Route::post('/order-complete', 'RestaurantOrderController@complete_order');
                Route::post('/transaction/complete', 'RestaurantManagementController@waiter_confirm_order');
                Route::post('/order/check', 'RestaurantManagementController@manager_order_list');
            });
        }); //<<<<<<< --------------------- RESTAURANT END -------------------------->>>>>>>>>

        Route::group(['prefix' => 'activity', 'namespace' => 'Activity'], function(){
            Route::post('list', 'ActivityController@list');
            Route::post('add', 'ActivityController@add');
            Route::post('update', 'ActivityController@update');
            Route::post('add-image', 'ActivityController@addImage');
            Route::post('update-image', 'ActivityController@updateImage');

            Route::group(['prefix' => 'type'], function(){
                Route::post('list', ' ActivityController@typeList');
                Route::post('add', 'ActivityController@addType');
                Route::post('update', 'ActivityController@updateType');
            });
        });

        Route::group(['prefix' => 'category'], function(){
            Route::post('/add', 'CategoryController@add');
        });

        Route::group(['prefix' => 'inventory', 'namespace' => 'Product'], function () {
            Route::post('/add', 'InventoryController@add');
            Route::post('/minus', 'InventoryController@minus');
            Route::post('/list', 'InventoryController@list');
        });

        Route::group(['prefix' => 'product', 'namespace' => 'Product'], function () {
            Route::post('/toggle-to-favorite', 'ProductController@toggleToFavorite');
            Route::post('/customer-reviews', 'ProductController@customerReviews');
            Route::post('/search-product', 'ProductController@searchProduct');
            Route::post('/list-by-tags', 'ProductController@listByTags');
            Route::post('/list-by-category', 'ProductController@listByCategory');
            Route::post('/list-by-name', 'ProductController@listByName');
            Route::post('/list-by-price', 'ProductController@listByPrice');
            Route::get('/list-by-sort', 'ProductController@listBySort');
            Route::post('/list-of-tags', 'ProductController@listOfTags');
            Route::post('/update-tags', 'ProductController@updateTags');
            Route::post('/search-tags', 'ProductController@searchTags');
            Route::post('/add-tags', 'ProductController@addTags');
            Route::post('/product-change-status', 'ProductController@productChangesStatus');
            Route::get('/product-type-list', 'ProductController@productTypeList');

            Route::group(['prefix' => 'brand'], function () {
                Route::post('/add', 'ProductController@addBrand');
                Route::post('/update', 'ProductController@updateBrand');
                Route::post('/list', 'ProductController@listBrand');
                Route::post('/search-brand', 'ProductController@searchBrand');
                Route::post('/list-by-name', 'ProductController@brandListByName');
                Route::get('/list-by-sort', 'ProductController@brandListBySort');
                Route::post('/list-by-status', 'ProductController@listByStatus');
                Route::get('/total-brands', 'ProductController@totalBrands');
                Route::post('/show', 'ProductController@brandShow');
                Route::post('/brand-change-status', 'ProductController@brandChangeStatus');
            });

            Route::group(['prefix' => 'category'], function () {
                Route::post('/add-category', 'ProductController@addCategory');
                Route::post('/update-category', 'ProductController@updateCategory');
                Route::post('/delete-category', 'ProductController@deleteCategory');
            });
        });

        Route::group(['prefix' => 'customer', 'namespace' => 'Customer'], function () {
            Route::post('/list', 'CustomerController@list');
            Route::post('/show', 'CustomerController@show');
            Route::post('/list-of-all-customer', 'CustomerController@listOfAllCustomer');
            Route::post('/customer-account', 'CustomerController@customerAccount');

            Route::group(['prefix' => 'product'], function () {
                Route::post('/product-list', 'CustomerController@productList');
                Route::post('/product-search', 'CustomerController@productSearch');
                Route::post('/product-sort', 'CustomerController@productSort');
                Route::post('/product-list-by-tags', 'CustomerController@productListBytags');
                Route::get('/total-products', 'CustomerController@totalProducts');
                Route::get('/category-list', 'CustomerController@categoryList');
                Route::post('/add-to-wishlist', 'CustomerController@addToWishList');
                Route::post('/wish-lists', 'CustomerController@wishLists');
            });

            Route::group(['prefix' => 'brand'], function () {
                Route::post('/brand-list', 'CustomerController@brandList');
                Route::post('/brand-list-premium', 'CustomerController@brandListPremium');
                Route::post('/list-by-name', 'CustomerController@brandListByName');
                Route::post('/brand-search', 'CustomerController@brandSearch');
                Route::post('/sort', 'CustomerController@brandSort');
                Route::get('/total-brands', 'CustomerController@totalBrands');
            });

            Route::group(['prefix' => 'store'], function () {
                Route::post('/restaurant-list', 'CustomerController@restaurantList');
                Route::post('/following', 'CustomerController@storeFollowing');
                Route::post('/follow', 'CustomerController@isFollow');
                Route::post('/following/list', 'CustomerController@storefollowed');

                Route::group(['prefix' => 'ratings'], function(){
                    Route::post('/add', 'CustomerController@addStoreRating');
                    Route::post('/remove', 'CustomerController@removeRating');
                });
            });

            Route::group(['prefix' => 'order'], function () {
                Route::post('/order-list', 'CustomerController@orderList');
                Route::post('/order-show', 'CustomerController@orderShow');
                Route::post('/order-store', 'CustomerController@orderStore');
                Route::post('/order-history', 'CustomerController@orderHistory');
                Route::post('/order-track', 'CustomerController@trackOrder');
            });

            Route::group(['prefix' => 'address'], function () {
                Route::post('/add', 'CustomerController@addAddress');
                Route::post('/update', 'CustomerController@updateAddress');
                Route::post('/delete', 'CustomerController@deleteAddress');
                Route::post('/show', 'CustomerController@showAddress');
                Route::get('/list', 'CustomerController@addresses');
                Route::get('/types', 'CustomerController@addressTypes');
            });

        });

        Route::group(['prefix' => 'account', 'namespace' => 'Account'], function () {
            Route::post('/update', 'AccountController@update');
            Route::post('/change-password', 'AccountController@changePassword');
            Route::post('/check-password', 'AccountController@checkPassword');
            Route::post('/request-mobile-change', 'AccountController@requestMobileChange');
            Route::post('/confirm-mobile-change', 'AccountController@confirmMobileChange');
            Route::post('/request-email-change', 'AccountController@requestEmailChange');
            Route::post('/confirm-email-change', 'AccountController@confirmEmailChange');
            Route::post('/user-profile', 'AccountController@userProfile');
            Route::post('/user-change-details', 'AccountController@userChangeDetails');
            Route::post('/refresh-token', 'AccountController@refreshToken');
            Route::post('/add-fcm-token', 'AccountController@addFcmToken');
            Route::post('/get-fcm-token', 'AccountController@getFcmToken');
            Route::get('/my-profile', 'AccountController@myProfile');
            Route::group(['prefix' => 'address'], function () {
                Route::post('/list', 'AddressController@list');
                Route::post('/add', 'AddressController@add');
                Route::post('/update', 'AddressController@update');
                Route::post('/delete', 'AddressController@delete');
            });

            Route::group(['prefix' => 'agent'], function () {
                Route::post('/create', 'AccountController@createAgent');
                Route::post('/list', 'AccountController@agentList');
                Route::post('/change-status', 'AccountController@agentChangeStatus');
                Route::get('/count', 'AccountController@agentCount');
            });

            Route::group(['prefix' => 'staff'], function () {
                Route::post('/change-status', 'AccountController@staffChangeStatus');
            });

            Route::group(['prefix' => 'admin-staff'], function () {
                Route::post('/create', 'AccountController@createAdminStaff');
            });

            Route::post('/check-info', 'AccountController@checkInfo');
        });

        Route::group(['prefix' => 'shipping', 'namespace' => 'Shipping'], function () {
            Route::post('/list', 'ShippingController@list');
            Route::post('/categories', 'ShippingController@categories');
        });

        Route::group(['prefix' => 'cart', 'namespace' => 'Cart'], function () {
            Route::post('/list', 'CartController@list');
            Route::post('/add', 'CartController@add');
            Route::post('/count', 'CartController@count');
            Route::post('/clear', 'CartController@clear');
            Route::post('/add-quantity', 'CartController@addQuantity');
            Route::post('/subtract-quantity', 'CartController@subtractQuantity');
            Route::post('/toggle-checkout', 'CartController@toggleCheckout');
            Route::post('/remove', 'CartController@remove');

            Route::group(['prefix' => 'market-place'], function(){
                Route::post('/my-cart-list', 'CartController@mk_cart_list');
            });

            Route::group(['prefix' => 'restaurant'], function(){
                Route::post('/my-cart-list', 'CartController@restaurant_cart_list');
            });
        });

        Route::group(['prefix' => 'checkout', 'namespace' => 'Checkout'], function () {
            Route::post('/list', 'CheckoutController@list');
            Route::post('/place-order', 'CheckoutController@placeOrder');
        });

        Route::group(['prefix' => 'manager'], function () {
            Route::group(['prefix' => 'stores', 'namespace' => 'Vendor'], function () {
                Route::post('/list', 'VendorController@list');
            });

            Route::group(['prefix' => 'product', 'namespace' => 'Product'], function () {
                Route::post('/update-status', 'ProductController@updateStatus');
            });
        });

        Route::group(['prefix' => 'messages', 'namespace' => 'Message'], function () {
            Route::group(['prefix' => 'user'], function () {
                Route::post('/send-message', 'MessageController@userSendMessage');
                Route::post('/conversation-list', 'MessageController@userConversationList');
                Route::post('/message-list', 'MessageController@userMessageList');
            });

            Route::group(['prefix' => 'store'], function () {
                Route::post('/send-message', 'MessageController@storeSendMessage');
                Route::post('/conversation-list', 'MessageController@storeConversationList');
                Route::post('/message-list', 'MessageController@storeMessageList');
            });

            Route::group(['prefix' => 'wmall-email'], function(){
                Route::post('/send', 'MessageControoler@sendEmail');
            });
        });

        Route::group(['prefix' => 'payment', 'namespace' => 'CustomerPayment'], function () {
            Route::post('save-card-info', 'CustomerPaymentController@savedCardInfo');
            
            Route::group(['prefix' => 'paypal'], function() {
                Route::post('create-payment', 'PayPalController@createPayment');
            });
        });

        Route::group(['prefix' => 'review', 'namespace' => 'Review'], function(){
            Route::post('add', 'ReviewController@store_review_add');
            Route::post('remove', 'ReviewController@store_review_remove');
            Route::post('my-review', 'ReviewController@my_review_and_rating');
        });
    });
});

