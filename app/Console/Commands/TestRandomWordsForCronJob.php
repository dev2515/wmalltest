<?php

namespace App\Console\Commands;

use App\Models\RandomWord;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TestRandomWordsForCronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:random-word';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Command is for testing only';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $words = [
            'aberration'    => 'a state or condition markedly different from the norm',
            'convivial'     => 'occupied with or fond of the pleasures of good company',
            'diaphanous'    => 'so thin as to transmit light',
            'elegy'         => 'a mournful poem; a lament for the dead',
            'ostensible'    => 'appearing as such but not necessarily so'
        ];

        $key = array_rand($words);
        $value = $words[$key];
        $count = RandomWord::count();

        if($count >= 5) DB::table('random_words')->delete();
        

        $add = new RandomWord();
        $add->name = $value;
        $add->save();
        
        $this->info('Success :)');
    }
}
