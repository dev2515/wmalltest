<?php

namespace App\Models;

use App\Http\Resources\Restaurant\FoodResource;
use Illuminate\Database\Eloquent\Model;

class RestaurantOrderList extends Model
{
    protected $table = 'restaurant_order_lists';
    protected $appends = ['hash_id'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

    public function foods()
    {
        return $this->hasMany(\App\Models\RestaurantOrderFood::class, 'order_list_id');
    }

    public function order()
    {
        return $this->belongsTo(\App\Models\RestaurantOrder::class, 'order_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Restaurant::class, 'restaurant_id');
    }
    
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}
