<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodTypePivot extends Model
{
    protected $table = 'food_type_pivot';
}
