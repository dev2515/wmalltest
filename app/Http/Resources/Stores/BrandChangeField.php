<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;
use stdClass;

class BrandChangeField extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'    => $this->hashid,
            'name'      => $this->data(),
            'status'    => $this->status(),
            'added by'  => $this->requestBy(),
            'full'      => $this->photo()->full,
            'thumbnail' => $this->photo()->thumbnail

        ];
    }

    public function photo()
    {
        $full = public_path() . '/images/brands/' . $this->hashid . '-full.png';
        if(file_exists($full)){
            $full = '/images/brands/' . $this->hashid . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/brands/' . $this->hashid . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/brands/' . $this->hashid . '-thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = asset($full);
        $size->thumbnail = asset($thumb);

        return $size;
    }

    public function requestBy()
    {
        $user = User::where('id', $this->request_by)->first();
        if(!$user) return null;
        
        $data = new stdClass;
        $data->name = $user->name;
        $data->role_type = $user->type_info;
        $data->contact_number = $user->mobile;
        $data->email = $user->email;
        
        return $data;

        
    }

    public function data()
    {
        if($this->type == 'add brand'){
            return $this->data->name;
            }
        
        return $this->name;
        
    }

    public function status()
    {
        if($this->type == 'add brand'){
        if($this->is_approved === 1) return 'approved';

        if($this->is_declined === 1) return 'declined';

        if($this->is_approved === 0 && $this->is_declined === 0) return 'pending';
        }

        if($this->status === 1 ) return 'active';

        if($this->status === 0 ) return 'inactive';
    }
}
