<?php

namespace App\Interfaces;

interface BloggerVideoInterface
{
    public function save_video($req);

    public function video_list($req);

    public function delete_video($req,$id);

}