<?php

namespace App\Http\Resources\Driver;

use App\Models\Review;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class DriverReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        ///return parent::toArray($request);
        return[
            'hash_id' => encode($this->id, 'uuid'),
            'name' => $this->name,
            'number_of_reviews' =>  $this->ratings()['total_reviews'],
            'ratings' =>  $this->ratings()['total_rate'],
            'review' => $this->reviews(),
        ];
    }

    public function ratings()
    {
        $c = DB::table('reviews')->select('type_id')->where('type_id',$this->id)->count();
        $r = DB::table('reviews')->select('type_id')->where('type_id',$this->id)->average('rate');

        $collect = collect(['total_reviews' => $c, 'total_rate' => $r]);

        return $collect;
    }

    public function reviews()
    {
        $c = Review::select('id','type_id','rate','content','user_id')->where('type_id',$this->id)->where('status', 1)->get();
        if($c) $c->makeHidden(['id','type_id','user_id']);

        return $c;
    }
}
