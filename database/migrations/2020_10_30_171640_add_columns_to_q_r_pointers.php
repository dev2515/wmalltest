<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToQRPointers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumns('q_r_pointers', ['product_id', 'user_id', 'business_link', 'transaction_status'])) {
            Schema::table('q_r_pointers', function (Blueprint $table) {
                $table->integer('product_id')->default(0);
                $table->integer('user_id')->default(0);
                $table->string('business_link')->nullable();
                $table->integer('transaction_status')->default(0)->after('transaction_number');
                $table->text('receiver_hash')->nullable()->after('transaction_status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('q_r_pointers', function (Blueprint $table) {
            //
        });
    }
}
