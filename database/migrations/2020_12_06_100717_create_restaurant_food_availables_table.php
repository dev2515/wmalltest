<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantFoodAvailablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('restaurant_food_availables')){
            Schema::create('restaurant_food_availables', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('store_id');
                $table->unsignedBigInteger('food_id');
                $table->boolean('is_available')->default(1);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_food_availables');
    }
}
