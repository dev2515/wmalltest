<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'computer_card_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('computer_card_approval')->after('ban');
            });
        }

        if(!Schema::hasColumn('stores', 'commercial_registration_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('commercial_registration_approval')->after('computer_card_approval');
            });
        }

        if(!Schema::hasColumn('stores', 'commercial_permit_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('commercial_permit_approval')->after('commercial_registration_approval');
            });
        }

        if(!Schema::hasColumn('stores', 'sponsor_qid_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('sponsor_qid_approval')->after('commercial_permit_approval');
            });
        }

        if(!Schema::hasColumn('stores', 'signature_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->smallInteger('signature_approval')->after('sponsor_qid_approval');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'computer_card_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('computer_card_approval');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_registration_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_registration_approval');
            });
        }

        if(Schema::hasColumn('stores', 'commercial_permit_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('commercial_permit_approval');
            });
        }

        if(Schema::hasColumn('stores', 'sponsor_qid_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('sponsor_qid_approval');
            });
        }

        if(Schema::hasColumn('stores', 'signature_approval')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('signature_approval');
            });
        }
    }
}
