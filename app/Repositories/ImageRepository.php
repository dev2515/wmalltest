<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductImage;
use App\Interfaces\ImageInterface;
use Illuminate\Support\Facades\Redis;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\StoreChangeField;
use App\Models\Store;

class ImageRepository implements ImageInterface
{
    private function returnImg($path, $width, $height = null)
    {
        if ($height) {
            return Image::make($path)->resize($width, $height)->response();
        } else {
            return Image::make($path)->resize($width, $height, function ($const) {
                $const->aspectRatio();
            })->response();
        }
    }

    public function avatar($req, $hashid, $width = 256, $height = null)
    {
        $id = decode($hashid, 'uuid');
        $path = storage_path('/app/public/images/avatars/' . $id . '.png');
        if (file_exists($path)) {
            $path = storage_path('/app/public/images/avatars/' . $id . '.png');
        } else {
            $path = storage_path('/app/public/images/avatar.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function product($req, $hashid, $width = 256, $height = null)
    {
        $id = decode($hashid, 'uuid');
        $product = Product::find($id);
        if ($product) {
            $data = Redis::get('images:products:' . $product->uuid);
            if ($data) {
                $images = json_decode($data);
                if (count($images) > 0) {
                    $path = $images[0]->data;
                }
            } else {
                $path = storage_path('/app/public/images/product.png');
            }
        } else {
            $path = storage_path('/app/public/images/product.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function banner($req, $name, $width = 256, $height = null)
    {
        $path = storage_path('/app/public/images/banners/' . $name);
        if (file_exists($path)) {
            $path = storage_path('/app/public/images/banners/' . $name);
        } else {
            $path = storage_path('/app/public/images/banner.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function bannerNames($req)
    {
        $files = Storage::disk('public')->files('/images/banners');
        $names = [];
        foreach ($files as $key => $value) {
            $filename = basename($value);
            array_push($names, $filename);
        }
        return res('Success', $names);
    }

    public function store($req, $hashid, $width = 256, $height = null)
    {
        $id = decode($hashid, 'uuid');
        $path = storage_path('/app/public/images/store/' . $id . '.png');
        if (file_exists($path)) {
            $path = storage_path('/app/public/images/store/' . $id . '.png');
        } else {
            $path = storage_path('/app/public/images/store.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function productBase64s($req, $hashid, $index, $width = 400, $height)
    {
        $id = decode($hashid, 'uuid');
        $product = Product::find($id);
        if ($product) {
            $images = ProductImage::where('product_id', $product->id)->get();
            if (count($images) > 0) {
                $path = $images[$index]->base64;
            } else {
                $path = storage_path('/app/public/images/product.png');
            }
        } else {
            $path = storage_path('/app/public/images/product.png');
        }
        return $this->returnImg($path, $width, $height);
    }

    public function downloadPdf($req, $hashid, $documentType, $fileName)
    {
        $store_id = decode($hashid, 'uuid');
        $store = Store::where('id', $store_id)->first();
        
        if(!$store) return res(__('store.not_found'), null, 404);
        
        $path = Storage::disk('local')->exists('/pdf/stores/' . $store->hashid . '/' . $documentType . '/' . $fileName);
        if(!$path) return res(__('storage.file_not_found'), null, 404);

            $headers = array(
                'Content-Type: application/pdf',
                );

            return Storage::download('/pdf/stores/' . $store->hashid . '/' . $documentType . '/' . $fileName, $fileName, $headers);
    }

    public function createUserProfilePhoto(Int $user_id, String $base64)
    {
        $path = public_path() . '/images/users';
            if (!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/users', $mode = 0777, true, true);
            }
            File::makeDirectory(base_path() . '/public/images/users/' . encode($user_id, 'uuid'), $mode = 0777, true, true);

            $root_path = public_path() . '/images/users/' . encode($user_id, 'uuid') . '/';

            $image = Image::make($base64);
            $image->save($root_path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $image->save($root_path . 'thumbnail.png');
    }

    public function createDocumentArchives(Type $var = null)
    {
        # code...
    }
}
