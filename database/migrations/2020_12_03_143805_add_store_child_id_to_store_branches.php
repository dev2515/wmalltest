<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreChildIdToStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'store_child_id')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->unsignedBigInteger('store_child_id')->nullable()->default(0)->after('store_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_branches', 'store_child_id')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('store_child_id');
            });
        }
    }
}
