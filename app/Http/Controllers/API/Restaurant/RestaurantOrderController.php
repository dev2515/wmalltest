<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RestaurantOrderInterface;

class RestaurantOrderController extends Controller
{
    protected $req;
    protected $order;

    public function __construct(Request $req, RestaurantOrderInterface $order)
    {
        $this->req = $req;
        $this->order = $order;
    }
 
    public function add()
    {
        return $this->order->add($this->req);
    }

    public function paymentMethods()
    {
        return $this->order->paymentMethods($this->req);
    }

    public function restaurantOrderList()
    {
        return $this->order->restaurantOrderList($this->req);
    }

    public function order_status_update()
    {
        return $this->order->order_status_update($this->req);
    }

    public function myOrderlist()
    {
        return $this->order->myOrderlist($this->req);
    }

    public function listOrderByType()
    {
        return $this->order->listOrderByType($this->req);
    }

    public function orderQRCode()
    {
        return $this->order->orderQRCode($this->req);
    }

    public function show()
    {
        return $this->order->show($this->req);
    }

    public function totalOrder()
    {
        return $this->order->totalOrder($this->req);
    }

    public function orderFoodDetail()
    {
        return $this->order->orderFoodDetail($this->req);
    }

    public function checkout()
    {
        return $this->order->checkout($this->req);
    }

    public function qrcodeList()
    {
        return $this->order->qrcodeList($this->req);
    }

    public function updateOrder()
    {
        return $this->order->updateOrder($this->req);
    }

    public function add_order_in_reservation()
    {
        return $this->order->add_order_in_reservation($this->req);
    }
    
    public function complete_order()
    {
        return $this->order->complete_order($this->req);
    }

    public function reservation_checkout()
    {
        return $this->order->reservation_checkout($this->req);
    }

    public function get_distance_store_to_customer()
    {
        return $this->order->get_distance_store_to_customer($this->req);
    }

    public function checkout_details()
    {
        return $this->order->checkout_details($this->req);
    }

    public function default_address()
    {
        return $this->order->default_address($this->req);
    }

    public function confirm_order_details()
    {
        return $this->order->confirm_order_details($this->req);
    }

    public function order_progress()
    {
        return $this->order->order_progress($this->req);
    }

    public function get_pickup_orders()
    {
        return $this->order->get_pickup_orders($this->req);
    }

    public function my_order_list()
    {
        return $this->order->my_order_list($this->req);
    }

    public function my_order_info()
    {
        return $this->order->my_order_info($this->req);
    }

    public function deliver_order_list()
    {
        return $this->order->deliver_order_list($this->req);
    }

    public function pickup_order_list()
    {
        return $this->order->pickup_order_list($this->req);
    }

    public function get_order_using_order_number()
    {
        return $this->order->get_order_using_order_number($this->req);
    }

    public function total_status_count()
    {
        return $this->order->total_status_count($this->req);
    }
}
