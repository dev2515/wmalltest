<?php

namespace App\Repositories;

use App\User;
use stdClass;
use Illuminate\Support\Arr;
use App\Models\BusinessType;
use App\Models\DriverReview;
use App\Models\VehicleImage;
use App\Models\DriverLicense;
use App\Models\DriverVehicle;
use App\Models\DriverPreference;
use App\Models\StoreChangeField;
use Illuminate\Support\Facades\DB;
use App\Interfaces\DriverInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Driver\VehicleResource;
use App\Http\Resources\Driver\DriverInfoCollection;
use App\Http\Resources\Driver\DriverReviewResource;
use App\Http\Resources\Driver\DriverDeliveryPreference;
use App\Http\Resources\Driver\DriverInfo as DriverResource;
use App\Http\Resources\QRPointerResource;
use App\Http\Resources\Restaurant\DriverNetIncome;
use App\Http\Resources\Restaurant\DriverOrderHistory;
use App\Http\Resources\Restaurant\DriverOrderList;
use App\Http\Resources\Restaurant\ScanOrder;
use App\Models\DriverSetting;
use App\Models\QRPointer;
use App\Models\Restaurant;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;

use App\Models\DriverActivity;
use App\Models\Store;
use Carbon\Carbon;
use App\Events\AddDriver;
use App\Models\DriverStatus;
use App\Http\Resources\Driver\DriverInfoListCollection;
use App\Http\Resources\Driver\DriverHistoryListCollection;
use App\Http\Resources\Restaurant\DeliveryResource;
use App\Http\Resources\Restaurant\DriverCredit;
use App\Models\DriverCredits;
use App\Models\ManagerCommission;
use App\Models\RestaurantCartItem;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\Review;
use Intervention\Image\Gd\Decoder;

class DriverRepository implements DriverInterface
{
    /**
     * Add Driver
     * @param $req
     * @return JsonResponse
     */
    public function add($req):object
    {
        DB::beginTransaction(); // open transaction
        $info = auth()->user()->type_info;

        $permissions = ['agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('Only admin and agent can do this process', null, 401);
        }

        // validation
        $validator = Validator::make($req->all(), [
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'email'                 => 'required|email',
            'qid_number'            => 'required|string',
            'qid_expiration'        => 'required|string',
            'mobile'                => 'required|phone:' . geoip()->getLocation()->iso_code . ',' . iso2ToIso3(session('iso'), true) . '|unique:users',
        ]);

        if ($validator->fails()) {
            DB::rollBack(); // rollback transaction
            return res('Failed', $validator->errors(), 412);
        }

        $user = User::where('email', $req->email)->first();

        if ($user) {
            DB::rollBack(); // rollback transaction
            return res(__('user.existing_email'), null, 401);
        }

        $user = new User;
        $user->name           = $req->first_name . ' ' . $req->last_name;
        $user->email          = $req->email;
        $user->mobile         = $req->mobile;
        $user->password       = bcrypt(preg_replace("/\s+/", "", $user->name));
        $user->profile_photo  = $req->profile_photo;
        $user->iso            = session('iso', getIso());
        $user->type           = 4; // Driver,
        $user->qid_number     = $req->qid_number;
        $user->qid_expiration = $req->qid_expiration;
        $user->nationality    = $req->nationality;
        $user->status         = 1;  
        $user->save();
        

        $data = array_merge($req->only(
            ['profile_photo', 'license_number', 'plate_number', 'license_expiration_date', 'vehicle_expiration_date']), 
            ['user' => $user]
        );
        
        event(new AddDriver($data));

        DB::commit(); // commit transaction data to database
        return res('success', null, 201);
    }

    /**
     * Edit Driver info
     * @param $req
     * @return JsonResponse
     */
    public function edit($req):object
    {
        DB::beginTransaction(); // open transaction
        $permissions = ['agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('Only admin and agent can do this process', null, 401);
        }

        // validation
        $validator = Validator::make($req->all(), [
            'user_hashid' => 'required|string',
        ]);

        if ($validator->fails()) {
            DB::rollBack(); // rollback transaction
            return res('Failed', $validator->errors(), 412);
        }

        $user_id    = decode($req->user_hashid, 'uuid');
        $user       = User::where('id', $user_id)->first();
        if(!$user){
            DB::rollBack();
            return res(__('user.not_found'), null, 404);
        }

        if($req->name != null)             $user->name   = $req->name;
        if($req->email != null)            $user->email  = $req->email;
        if($req->mobile != null)           $user->mobile = $req->mobile;
        if($req->qid_number != null)       $user->qid_number = $req->qid_number;
        if($req->qid_expiration != null)   $user->qid_expiration = $req->qid_expiration;
        if($req->nationality != null)      $user->nationality    = $req->nationality;

        $user->save();

        $driver_vehicle = DriverVehicle::where('user_id', $user_id)->first();
        if(!$driver_vehicle){
            DB::rollBack();
            return res(__('driver.vehicle_not_found'), null, 404);
        }

        if($req->plate_number != null) $driver_vehicle->plate_number = $req->plate_number;
        if($req->vehicle_expiration_date != null) $driver_vehicle->expiration_date = $req->vehicle_expiration_date;

        $driver_vehicle->save();

        $driver_license = DriverLicense::where('user_id', $user_id)->first();
        if(!$driver_license){
            DB::rollBack();
            return res(__('driver.license_not_found'), null, 404);
        }

        if($req->license_number != null) $driver_license->license_number = $req->license_number;
        if($req->license_expiration_date != null) $driver_license->expired_at = $req->license_expiration_date;
        
        $driver_license->save();

        DB::commit();
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function list($req):object
    {
        $validator = Validator::make($req->all(), [
            'status'    => 'required',
            'limit'     => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $status     = DriverStatus::where('name', $req->status)->first(); 

        $user_ids   = User::where('name', 'LIKE', '%' . $req->key . '%')->where('type', 4)->pluck('id');

        if(!$status){
            $activities = DriverActivity::whereIn('user_id', $user_ids)->paginate($req->limit);
            $data = new DriverInfoListCollection($activities);
            return res('success', $data, 200);
        }

        $activities = DriverActivity::whereIn('user_id', $user_ids)->where('status_id', $status->id)->paginate($req->limit);
        $data = new DriverInfoListCollection($activities);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function addDocument($req)
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('Only admin and agent can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'user_hashid'   => 'required',
            'document_type' => 'required',
            'file_type'     => 'required',
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $user_id = decode($req->user_hashid, 'uuid');
        $user = User::where('id', $user_id)->first();

        if(!$user){
            DB::rollBack();
            return res(__('user.not_found'), null, 401);
        }

        if($user->type_info !== 'driver'){
            DB::rollBack();
            return res(__('driver.not_driver'), null, 401);
        }


        switch ($req->document_type) {

            // Licence
            case 'license' :

                $request_field = StoreChangeField::where('type', 'driver-licenses')->where('user_id', $user->id)->where('is_approved', 0)->where('is_declined', 0)->first();
                if($request_field) {
                    DB::rollBack();
                    return res(__('store.request_already'), null, 401);
                }

                $add_license                = new DriverLicense;
                $add_license->user_id       = $user->id;
                $add_license->requested_by  = auth()->id();
                $add_license->processed_by  = auth()->user()->type_info === 'administrator' ? auth()->id() : 0;
                $add_license->is_approved   = auth()->user()->type_info === 'administrator' ? 1 : 0;
                $add_license->file_type     = $req->file_type;

                if($req->file_type === 'pdf'){

                    $validator = Validator::make($req->all(), [
                        'pdf' => 'required|mimes:pdf',
                    ]);

                    if($validator->fails()) {
                        DB::rollBack();
                        return res('Failed', $validator->errors(), 412);
                    }

                    $add_license->file_name     = $req->file('pdf')->getClientOriginalName();
                    $add_license->save();

                    $this->pdfProcess($user->id, $req->file('pdf'), 'driver-licenses');

                    DB::commit();
                    return res('success', null, 201);
                }

                if($req->file_type === 'img'){

                    $validator = Validator::make($req->all(), [
                        'img' => 'required',
                    ]);

                    if($validator->fails()) {
                        DB::rollBack();
                        return res('Failed', $validator->errors(), 412);
                    }

                    $add_license->image = $req->img;
                    $add_license->save();

                    $this->imgProcess($user->id, $req->img, 'driver-licenses');

                    DB::commit();
                    return res('success', null, 201);
                }

            break;

            // Vehicle registration
            case 'vehicle-registration' :
                $request_field = StoreChangeField::where('type', 'vehicle-registrations')->where('user_id', $user->id)->where('is_approved', 0)->where('is_declined', 0)->first();
                if($request_field) {
                    DB::rollBack();
                    return res(__('store.request_already'), null, 401);
                }

                $driver_vehicle = DriverVehicle::where('user_id', $user->id)->first();

                if(!$driver_vehicle){
                    DB::rollBack();
                    return res(__('driver.vehicle_not_found'), null, 401);
                }

                $driver_vehicle->file_type      = $req->file_type;

                if($req->file_type === 'pdf'){
                    $validator = Validator::make($req->all(), [
                        'pdf' => 'required|mimes:pdf',
                    ]);

                    if($validator->fails()) {
                        DB::rollBack();
                        return res('Failed', $validator->errors(), 412);
                    }

                    $driver_vehicle->registration_file_name     = $req->file('pdf')->getClientOriginalName();
                    $driver_vehicle->registration__status       = auth()->user()->type_info === 'administrator' ? 'approved' : 'pending';
                    $driver_vehicle->save();

                    $this->pdfProcess($user->id, $req->file('pdf'), 'vehicle-registrations');

                    DB::commit();
                    return res('success', null, 201);
                }

                if($req->file_type === 'img'){
                    $validator = Validator::make($req->all(), [
                        'img' => 'required',
                    ]);

                    if($validator->fails()) {
                        DB::rollBack();
                        return res('Failed', $validator->errors(), 412);
                    }

                    $driver_vehicle->registration_image     = $req->img;
                    $driver_vehicle->save();

                    $this->imgProcess($user->id, $req->img, 'vehicle-registrations');

                    DB::commit();
                    return res('success', null, 201);
                }
            break;
        }
    }

    /**
     * @param $user_id
     * @param $file
     * @param $document_type
     * @return JsonResponse
     */
    public function pdfProcess(Int $user_id, $file, String $document_type):void
    {
        $request_data               = new StoreChangeField;
        $request_data->user_id      = $user_id;
        $request_data->type         = $document_type;
        $request_data->request_by   = auth()->id();
        $request_data->process_by   = auth()->user()->type_info === 'administrator' ? auth()->id() : 0;
        $request_data->is_approved  = auth()->user()->type_info === 'administrator' ? 1 : 0;
        $request_data->save();

        Storage::disk('local')->putFileAs('/pdf/' . $document_type . '/' . encode($user_id, 'uuid'), $file, $file->getClientOriginalName());
    }

    /**
     * @param $user_id
     * @param $base64
     * @param $document_type
     * @return JsonResponse
     */
    public function imgProcess(Int $user_id, $base64, String $document_type):void
    {
        $request_data               = new StoreChangeField;
        $request_data->user_id      = $user_id;
        $request_data->type         = $document_type;
        $request_data->request_by   = auth()->id();
        $request_data->process_by   = auth()->user()->type_info === 'administrator' ? auth()->id() : 0;
        $request_data->is_approved  = auth()->user()->type_info === 'administrator' ? 1 : 0;
        $request_data->save();

        $user_folder = public_path() . '/images/' . $document_type . '/' . encode($user_id, 'uuid') . '/';

        if(!file_exists($user_folder)){
            File::makeDirectory(base_path() . '/public/images/' . $document_type . '/' . encode($user_id, 'uuid'), $mode = 0777, true, true);
        }

        $image = Image::make($base64);
        $image->save($user_folder . 'full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($user_folder . 'thumbnail.png');
    }
    
    /**
     * @param $user_id
     * @param $base64
     * @param $document_type
     * @return JsonResponse
     */
    public function listDocument($req)
    {
        if(!$req->user_hashid) return res(__('validation.identifier'), null, 401);


        $user_id     = decode($req->user_hashid, 'uuid');
        $user        = User::where('id', $user_id)->first();

        if(!$user)  return res(__('user.not_found'), null, 404);

        $data        = new stdClass;

        $data->license              = $user->driver_license_status;
        $data->vehicle_registration = $user->vehicle_registration_status;

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function vehicleList($req)
    {
        if (!$req->status) return res('status is required', null, 401);

        $limit = 10;
        if($req->limit) $limit = $req->limit;

        if($req->status == 'approved') {
            $vehicle = DriverVehicle::where('owner', 'LIKE', '%' . $req->key . '%')->where('is_approved', 1)->paginate($limit);
        }else if ($req->status == 'pending'){
            $vehicle = DriverVehicle::where('owner', 'LIKE', '%' . $req->key . '%')->where(['is_approved' => 0, 'is_declined' => 0])->paginate($limit);
        }else if ($req->status == 'declined'){
            $vehicle = DriverVehicle::where('owner', 'LIKE', '%' . $req->key . '%')->where('is_declined', 0)->paginate($limit);
        }else {
            $vehicle = DriverVehicle::where('owner', 'LIKE', '%' . $req->key . '%')->paginate($limit);
        }

        $data = VehicleResource::collection($vehicle);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function adminVehicleApprove($req)
    {
        if (!auth()->user()->type_info === 'administrator')
            return res('Only admin can do this transaction', null, 401);

        $validator = Validator::make($req->all(), [
            'vehicle_hashid' => 'required',
            'action'        => 'required',
        ]);

        if($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $id = decode($req->vehicle_hashid, 'uuid');

        $driver = DriverVehicle::where('id', $id)->first();
            if(!$driver) return res('Driver not found', null, 401);

        if($req->action === 'approve') {
            $driver->is_approved = 1;
            $driver->is_declined = 0;
        }
        if($req->action === 'decline') {
            $driver->is_declined = 1;
            $driver->is_approved = 0;
        }

        $driver->processed_by = auth()->id();
        $driver->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function driverAccountApprove($req)
    {
        if (!auth()->user()->type_info === 'administrator')
            return res('Only admin can do this transaction', null, 401);

        $validator = Validator::make($req->all(), [
            'driver_hashid' => 'required',
            'action'        => 'required',
        ]);

        if($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $id = decode($req->driver_hashid, 'uuid');

        $driver = User::where('id', $id)->first();
            if(!$driver) return res('Driver not found', null, 401);

        if($req->action === 'approve') {
            $driver->status = 1;
        }
        if($req->action === 'decline') {
            $driver->status = 0;
        }

        $driver->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function show($req)
    {
        $validator = Validator::make($req->all(), [
            'driver_hashid' => 'required',
        ]);

        if($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $user_id    = decode($req->driver_hashid, 'uuid');
        $user       = User::where('id', $user_id)->where('type', 4)->first();
        if(!$user) return res(__('user.not_found'), null, 404);

        $activity   = DriverActivity::where('user_id', $user_id)->first();

        $data       = new DriverResource($activity);
        
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function vehicleShow($req)
    {
        if (!$req->vehicle_hashid) return res('vehicle_hashid is required', null, 401);

        $id = decode($req->vehicle_hashid, 'uuid');

        $vehicle = DriverVehicle::where('id', $id)->first();
            if(!$vehicle) return res('Vehicle not found', null, 401);

        $data = new VehicleResource($vehicle);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function myPreferredDelivery($req)
    {
        $u = Auth::user();
        if (!$u) return res('User not found');

        $services = ['marketplace', 'restaurant', 'hotel', 'activity'];
        $to_search = [];
        foreach ($services as $service) {
            if (config('settings.driver_delivery_preferences.' . $service) == 1) array_push($to_search, $service);
        }

        $to_search_ids = [];
        foreach ($to_search as $search) {
            $bt = BusinessType::where('name', $search)->where('status', 1)->first();
            if ($bt) {
                array_push($to_search_ids, $bt->id);
                DriverPreference::firstOrCreate(['driver_id' => $u->id, 'business_type_id' => $bt->id], ['status' => 1]);
            }
        }

        $prefs = DriverPreference::where('driver_id', $u->id)->whereIn('business_type_id', $to_search_ids)->get();

        $data = DriverDeliveryPreference::collection($prefs);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function account($req)
    {
        $u = Auth::user();
        $driver = User::where('id', $u->id)->first();
        if(!$driver) return res('Driver not found', null, 401);

        $data = new DriverResource($driver);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function updatePreferredDelivery($req)
    {
        if (!$req->pref_hashid) return res('pref_hashid is required', null, 401);

        $id = decode($req->pref_hashid, 'uuid');

        $df = DriverPreference::where('id', $id)->first();
        if(!$df) return res('Preference not found', null, 401);

        $df->status = $req->status == 1 ? 1 : 0;
        $df->save();

        return res('success');
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function deliveryList($req)
    {
        $v = Validator::make($req->all(), [
            'filter' => 'required',
        ]);
        if ($v->fails()) return res('Failed', $v->errors(), 412);

        $did = Auth::user();

        // TODO: Complete the filter status
        switch ($req->filter) {
            case 'pending':
                $qrp = QRPointer::where('assigned_driver_id', $did)->where('transaction_type_id', 4)->where('transaction_status', 0)->where('status', 1)->get();
            default:
                $qrp = QRPointer::where('assigned_driver_id', $did)->where('transaction_type_id', 4)->where('status', 1)->get();
        }

        $data = QRPointerResource::collection($qrp);

        return res('Success', $data);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function myCurrentLocation($req):object
    {
        DB::beginTransaction(); // open transaction
        $permissions = ['administrator'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            DB::rollBack();
            return res('You are not driver', null, 401);
        }

        $driver  = Auth::user();

        $location   = geoip()->getLocation(\Request::ip());

        $driver->latitude   = $location->lat;
        $driver->longitude  = $location->lon;
        $driver->save();

        DB::commit();
        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function nearBy($req):object
    {
        $validator = Validator::make($req->all(), [
            'store_hashid' => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $store_id   = decode($req->store_hashid, 'uuid');
        $store      = Store::where('id', $store_id)->where('is_active', 1)->first();
        if(!$store) return res(__('store.not_found'), null, 404);
        
        $drivers = [];

        $radius_attempts = [5, 10, 15, 20, 25];
        foreach ($radius_attempts as $attempt){
            $near_drivers = User::closeTo($store->latitude, $store->longitude, $attempt);
            if(count($near_drivers) > 0 ) {
                foreach ($near_drivers as $driver){
                    $obj = [
                        'id'        => $driver->id,
                        'name'      => $driver->name,
                        'latitude'  => $driver->latitude,
                        'longitude' => $driver->longitude,
                        'radius'    => $attempt
                    ];
                    array_push($drivers, $obj);
                }
                break;
            }
        }

        return res('success', $drivers, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function transactionHistory($req):object
    {
        $validator = Validator::make($req->all(), [
            'status'    => 'required',
            'limit'     => 'required'
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);
        
        $driver_ids     = User::where('type', 4)->where('name', 'LIKE', '%' . $req->key . '%')->pluck('id');
        $transactions   = QRPointer::whereIn('assigned_driver_id', $driver_ids)->where('order_status', $req->status)->paginate($req->limit); 

        $data = new DriverHistoryListCollection($transactions);
        
        return res('success', $data, 200);
    }

    public function driver_accept_order($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }
        
        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
        ]);

        if($v->fails()) {return res('failed', $v->errors(), 402); DB::rollBack();}

        $o = decode($req->order_hash_id, 'uuid');
        
        $order = RestaurantOrder::find($o);
        if(!$order) {return res('no order found'); DB::rollBack();}
        
        $cart = RestaurantCart::find($order->cart_id);

        $t = QRPointer::where('transaction_number', $cart->transaction_number)->where('assigned_driver_id', 0)->first();
        if(!$t) {return res('order not exist or already accepted.'); DB::rollBack();}
        
        $t->assigned_driver_id = auth()->user()->id;
        $t->driver_accepted_time = Carbon::now();
        $t->save();

        $order = RestaurantOrder::select('id','cart_id','status')->where('cart_id', $cart->id)->first();
        $order->status = 8;
        $order->save();
        
        DB::commit();
        return res('success', 'order accepted');
    }

    public function driver_order_list($req)
    {
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $r = QRPointer::where('assigned_driver_id', auth()->user()->id)->where('status', 1)->first();

        if($r)
        {
            return res('please complete your delivery in order to see new orders',[]);
        }

        $t = QRPointer::where('assigned_driver_id', 0)->where('status', 1)->orderBy('created_at', 'DESC')->get();
        $t = DriverOrderList::collection($t);

        return res('success', $t);
    }

    public function driver_accepted_list($req)
    {
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }
        
        $t = QRPointer::where('assigned_driver_id', auth()->user()->id)->where('status', 1)->get();
        $t = DriverOrderList::collection($t);

        return res('success', $t);
    }

    public function driver_scan_order_restaurant($req)
    {
        DB::beginTransaction();
        
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $v = Validator::make($req->all(),[
            'qrcode' => 'required',
        ]);

        if($v->fails()) {return res('failed', $v->errors(), 402); DB::rollBack();}
        
        $qrcode = QRPointer::select('id','hash','transaction_number','assigned_driver_id')->where('hash', $req->qrcode)->where('assigned_driver_id', auth()->user()->id)->first();
        if(!$qrcode) return res('failed','no order found.');
        $cart = RestaurantCart::select('id','transaction_number')->where('transaction_number', $qrcode->transaction_number)->first();
        
        $order = RestaurantOrder::where('cart_id', $cart->id)->first();
      
        $order->status = 4;
        $order->save();

        DB::commit();
        return res('success');
    }

    public function driver_update_location($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
        ]);

        if($v->fails()) {return res('failed', $v->errors(), 402); DB::rollBack();}
        
        $o = decode($req->order_hash_id, 'uuid');

        $order = RestaurantOrder::find($o);
        if(!$order) {return res('no order found'); DB::rollBack();}
        
        $cart = RestaurantCart::find($order->cart_id);

        $t = QRPointer::where('transaction_number', $cart->transaction_number)->where('assigned_driver_id', auth()->user()->id)->first();
        if(!$t) {return res('order not exist or already accepted.'); DB::rollBack();}

        $order = RestaurantOrder::select('id','cart_id','status')->where('cart_id', $cart->id)->first();
        $order->status = 7;
        $order->save();
        
        DB::commit();
        return res('success', 'status updated');
    }

    public function customer_order_recieve($req)
    {
        DB::beginTransaction();
        $b = false;
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        if($req->transaction_number != null)
        {
            $t = QRPointer::where('transaction_number', $req->transaction_number)->where('assigned_driver_id', auth()->user()->id)->first();
            if(!$t) {return res('order not exist or already accepted.'); DB::rollBack();}

            $cart = RestaurantCart::where('transaction_number', $t->transaction_number)->first();

            $order = RestaurantOrder::select('id','order_number','cart_id')->where('cart_id', $cart->id)->first();
            if(!$order) {return res('no order found'); DB::rollBack();}

            if($order->payment_type_id == 1)
            {
                if($t)
                {
                    $order->status = 5;
                    $order->save();
                }
            }
            else
            {
                if($t)
                {
                    $order->status = 6;
                    $order->save();
                }
            }
            $b = true;          
        }

        if($req->qrcode != null)
        {
            
            $t = QRPointer::where('hash', $req->qrcode)->where('assigned_driver_id', auth()->user()->id)->first();
            if(!$t) {return res('order not exist or already accepted.'); DB::rollBack();}

            $cart = RestaurantCart::where('transaction_number', $t->transaction_number)->first();

            $order = RestaurantOrder::select('id','order_number','cart_id')->where('cart_id', $cart->id)->first();
            if(!$order) {return res('no order found'); DB::rollBack();}

            if($order->payment_type_id == 1)
            {
                if($t)
                {
                    $order->status = 5;
                    $order->save();
                }
            }
            else
            {
                if($t)
                {
                    $order->status = 6;
                    $order->save();
                }
            }
            $b = true;
        }

        if($b == true)
        {
            DB::commit();
            $b = false;
            $y = $this->customer_details($order->id);
            return res('success',$y);
        }
        else
        {
            return res('please pass order hash id or qrcode');
        }
    }

    public function driver_confirm_order($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
        ]);

        if($v->fails()) {return res('failed', $v->errors(), 402); DB::rollBack();}
        
        $o = decode($req->order_hash_id, 'uuid');

        $order = RestaurantOrder::find($o);
        if(!$order) {return res('no order found'); DB::rollBack();}
        
        $cart = RestaurantCart::find($order->cart_id);
        if(!$cart) return res('failed','no cart found');

        $t = QRPointer::where('transaction_number', $cart->transaction_number)->where('assigned_driver_id', auth()->user()->id)->first();
        if(!$t) {return res('order not exist or already accepted.'); DB::rollBack();}

        $t->status = 2;
        $t->save();

        $order = RestaurantOrder::select('id','cart_id','shipping_rate','status', 'payment_method_id')->where('cart_id', $cart->id)->first();
        $order->status = 6;
        $order->save();
        
        $this->total_income($order->shipping_rate);
        $this->driver_credits($order->shipping_rate, $order->payment_method_id);

        DB::commit();
        return res('success', 'status updated');
    }

    public function customer_details($req)
    {
        $order = RestaurantOrder::select('id','customer_id','order_number','sub_total','shipping_address_latitude','shipping_address_longitude'
        ,'payment_method_id','note')->where('id', $req)->first();
        $order = new ScanOrder($order);
        return $order;
    }

    public function driver_order_history($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $q = QRPointer::select('id','hash','transaction_number','assigned_driver_id','updated_at','driver_accepted_time')->where('assigned_driver_id', auth()->user()->id)->get();
        
        $q = DriverOrderHistory::collection($q);

        DB::commit();
        return res('success', $q);
    }

    public function driver_net_income($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];
        
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $q = DriverActivity::where('user_id', auth()->user()->id)->first();
        
        DB::commit();    
        return res('success', $q->total_income);
    }

    public function driver_notification_settings($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];
        
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $turnItOff = 1;
        $msg = 'notification is on.';
        $d = DriverSetting::where('user_id', auth()->user()->id)->first();
        if($d->notification_is_activated == 1){$turnItOff = 0; $msg = 'notification is off.';}else{$turnItOff = 1; $msg = 'notification is on.';}
        
        $d = DriverSetting::updateOrCreate(
        [
            'user_id' =>    auth()->user()->id,
        ],
        [
            'user_id' =>  auth()->user()->id,
            'notification_is_activated' =>  $turnItOff,
        ]);

        DB::commit();    
        return res($msg);
    }

    public function total_income($shipping_rate)
    {
        $percentage = 30;
        $i = ($percentage/100) * $shipping_rate;
        $i = $shipping_rate - $i;
        $s = DriverActivity::updateOrCreate(
            [
                'user_id' => auth()->user()->id,
            ],
            [
                'total_income' => $i,
            ]);
    }

    public function driver_settings_view($req)
    {
        DB::beginTransaction();
        $permissions = ['driver'];
        
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
            DB::rollBack();
        }

        $s = DriverSetting::where('user_id', auth()->user()->id)->first();

        DB::commit();
        return res('success', $s->notification_is_activated);
    }

    public function driver_current_status($req)
    {
        $permissions = ['driver'];
        $m = '';

        if (! in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }
        
        $t = QRPointer::select('id','transaction_number','assigned_driver_id')->where('status', 1)->where('assigned_driver_id', auth()->user()->id)->first();
        if(!$t) return res('success', 'driver available');
        
        $c = RestaurantCart::select('id','transaction_number')->where('transaction_number', $t->transaction_number)->first();
        $o = RestaurantOrder::select('id','status','cart_id')->where('cart_id', $c->id)->first();

        if($o->status == 4) {$m = 'on delivering';}
        else{$m = 'driver avaiable';}

        return res('success', $m);
    }

    public function driver_credits($income, $paytype)
    {
        $percentage = 30;
        $i =  ($percentage/100) * $income;
        $dc                                 = new DriverCredits;
        $dc->driver_id                      = auth()->user()->id;
        $dc->commision                      = $i;
        $dc->payment_type_id                = $paytype;
        $dc->delivery_charge                = $income;
        $dc->save();
    }

    public function driver_credits_today($req)
    {
        $permissions = ['driver'];

        if (! in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $cod = DriverCredits::where('driver_id', auth()->user()->id)->where('created_at', Carbon::now())->where('payment_type_id', 1)->sum('delivery_charge');
        if($cod) $cod = CentToPrice($cod);
        if(!$cod) $cod = 0;

        $c = DriverCredits::where('driver_id', auth()->user()->id)->where('created_at', Carbon::now())->where('payment_type_id', '!=', 1)->sum('delivery_charge');
        if($c) $c = CentToPrice($c);
        if(!$c) $c = 0;

        $comm = DriverCredits::where('driver_id', auth()->user()->id)->sum('commision');
        if($comm) $comm = CentToPrice($comm);
        if(!$comm) $comm = 0;

        $tc = DriverCredits::where('driver_id', auth()->user()->id)->where('payment_type_id', 1)->sum('delivery_charge');
        if($tc) $tc = CentToPrice($tc);
        if(!$tc) $tc = 0;

        $op = DriverCredits::where('driver_id', auth()->user()->id)->where('payment_type_id', '!=', 1)->sum('delivery_charge');
        if(!$op) $op = 0;

        $collect = collect(['cash_collected_today' => $cod, 'online_payment_today' => $c, 'total_collected_cash' => $tc, 'total_online_payment' => centToPrice($op),'total_commissions' => $comm]);
        
        return res('success', $collect);
    }

    public function delete_unnecessary($req)
    {
        $v = Validator::make($req->all(),[
            'transaction_number' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $cart = RestaurantCart::where('transaction_number', $req->transaction_number)->first();
        $o = RestaurantOrder::where('cart_id', $cart->id)->first();
        $ol = RestaurantOrderList::where('order_id', $o->id)->first();

        $d = QRPointer::where('transaction_number', $req->transaction_number)->delete();

        $c = RestaurantCart::where('transaction_number', $req->transaction_number)->delete();
        $ci = RestaurantCartItem::where('cart_id', $cart->id)->delete();

        $order = RestaurantOrder::where('cart_id', $cart->id)->delete();
        $ol = RestaurantOrderList::where('order_id', $o->id)->delete();
        $ol = RestaurantOrderFood::where('order_list_id', $ol->id)->delete();
        
        if($d) return 'success';
        else{return 'failed';}
    }

    public function drivers_collect_income($req)
    {
        $permissions = ['administrator', 'admin Staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $limit = 0;
        if($req->limit != 0 || $req->limit != null) $limit = $req->limit;

        $drivers_list = User::select('id','name','type')->where('type', 4)->where('status', 1)->limit($limit)->paginate($req->limit);
        if($drivers_list) $drivers_list->makeHidden(['type','current_address','status_info','mobile_prefix','type_info','default_billing_address','default_shipping_address','driver_license_status','vehicle_registration_status','store_info']);
       
        $drivers_list = DriverCredit::collection($drivers_list);

        return res('success', $drivers_list);
    }

    public function driver_notify_collection($req)
    {
        $permissions = ['driver'];

        if (! in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $d = DriverCredits::where('driver_id', auth()->user()->id)->first();

        return res('success', $d);
    }

    public function claim_commission($req)
    {
        $permissions = ['administrator', 'admin Staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'drier_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $driver_id = decode($req->driver_hash_id, 'uuid');
    
        $d = DriverCredits::where('driver_id', $driver_id)->first();
        $d->is_claim = 1;
        $d->save();

        return res('commission has been claimed');
    }

    public function get_driver_notify($req)
    {
        $permissions = ['administrator', 'admin Staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_allowed'), null, 400);
        };

        $v = Validator::make($req->all(),[
            'driver_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $d = decode($req->driver_hash_id, 'uuid');

        $dc = DriverCredits::where('driver_id', $d)->first();
    }

    public function manager_commission($req, $store_id)
    {
        $c = ManagerCommission::where('order_id', $req)->first();
        if(!$c)
        {
            $o = RestaurantOrder::where('id', $req)->first();
            
            $mc = new ManagerCommission;
            $mc->store_id = $store_id;
            $mc->percentage = 10;
            $mc->store_percentage = $o->sub_total * 0.1;
        }
    }

    public function order_reciept($req)
    {
        $permissions = ['driver', 'vendor', 'vendor staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_allowed', null, 400));
        }

        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);
        
        $o = decode($req->order_hash_id, 'uuid');

        $order = DB::table('restaurant_orders')
        ->select('restaurant_orders.id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_rate','payment_method_id','shipping_address','restaurant_orders.status','restaurant_orders.order_type','restaurant_orders.customer_id','restaurant_orders.created_at',
        'restaurant_orders.payment_method_id','restaurant_orders.cart_id','restaurant_order_lists.restaurant_id')
            ->join('restaurant_order_lists', 'restaurant_order_lists.order_id', '=', 'restaurant_orders.id')
            ->where('restaurant_orders.id', $o)
            ->first();

        $order = new DeliveryResource($order);

        return res('success', $order);
    }
}
