<?php

namespace App\Interfaces;

interface RestaurantCartInterface
{
    public function list($req);
   
    public function add($req);

    public function count($req);

    public function clear($req);

    public function updateQuantity($req);
    
    public function toggleCheckout($req);

    public function remove($req);

    public function checkout($req);

    public function cartTotal($req);

    public function CartConfirmation($req);

    public function reopenCart($req);

    public function addOrderInExistingCart($req);

    public function mycartlist($req);

    public function getQRCode($req);

    public function addtoCart($req);

    public function open_cart($req);

    public function get_customer_currnet_location($req);
}