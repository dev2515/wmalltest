<?php

namespace App\Http\Controllers\API\Cart;

use Illuminate\Http\Request;
use App\Interfaces\CartInterface;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    protected $req;
    protected $cart;

    public function __construct(Request $req, CartInterface $cart)
    {
        $this->req = $req;
        $this->cart = $cart;
    }

    public function list()
    {
        return $this->cart->list($this->req);
    }

    public function add()
    {
        return $this->cart->add($this->req);
    }

    public function count()
    {
        return $this->cart->count($this->req);
    }

    public function clear()
    {
        return $this->cart->clear($this->req);
    }

    public function addQuantity()
    {
        return $this->cart->addQuantity($this->req);
    }

    public function subtractQuantity()
    {
        return $this->cart->subtractQuantity($this->req);
    }

    public function toggleCheckout()
    {
        return $this->cart->toggleCheckout($this->req);
    }

    public function remove()
    {
        return $this->cart->remove($this->req);
    }

    // Market Place Lists

    public function mk_cart_list()
    {
        return $this->cart->mk_cart_list($this->req);
    }

    // Restaurant Lists
    
    public function restaurant_cart_list()
    {
        return $this->cart->restaurant_cart_list($this->req);
    }

}
