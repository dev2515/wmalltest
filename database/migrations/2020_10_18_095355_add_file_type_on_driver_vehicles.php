<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileTypeOnDriverVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_vehicles', 'file_type')){
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->string('file_type')->nullable()->after('qid');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_vehicles', 'file_type')){
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->dropColumn('file_type');
            });
        }
    }
}
