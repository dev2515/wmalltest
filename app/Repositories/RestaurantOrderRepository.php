<?php

namespace App\Repositories;

use App\Http\Resources\Restaurant\CheckoutDetails;
use App\Http\Resources\Restaurant\ConfirmOrder;
use App\Http\Resources\Restaurant\DeliveryResource;
use App\Http\Resources\restaurant\MyOrderInfo;
use App\Http\Resources\Restaurant\MyOrderList;
use App\Http\Resources\Restaurant\OrderFoodsResource;
use App\Http\Resources\Restaurant\OrderInProgress;
use App\Http\Resources\Restaurant\OrderListResource;
use App\Http\Resources\Restaurant\OrderResource;
use App\Http\Resources\Restaurant\QRListResource;
use App\Interfaces\RestaurantOrderInterface;
use App\Models\PaymentMethod;
use App\Models\QRPointer;
use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;
use App\Models\RestaurantCartItemVariation;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderFoodVariation;
use App\Models\RestaurantOrderList;
use App\Models\Store;
use App\Models\TableReservation;
use App\Models\UserAddress;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RestaurantOrderRepository implements RestaurantOrderInterface
{
    public function orderQRCode($req)
    {
        $qr = QRPointer::where('hash', $req->qrcode)
        ->first();

        if(!$qr) res('failed, qrcode does not exist');

        $cart_id = RestaurantCart::where('transaction_number', $qr->transaction_number)->pluck('id');
        $order = RestaurantOrder::where('cart_id', $cart_id)->get();

        $collection = OrderResource::collection($order);

        return res('success', $collection); 
    }
    
    public function paymentMethods($req)
    {
        $methods = PaymentMethod::where('status', 1)->get();
        $methods->makeHidden(['created_at', 'updated_at', 'status']);

        return res('Success', $methods);
    }

    public function checkout($req)
    {
        $permissions = ['customer'];
        
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }
        
        if (!$req->has('order_number')) {
            return res(__('validation.identifier'), null, 400);
        }

        $order = RestaurantOrder::where('order_number', $req->order_number)->where('is_paid', 0)->first();
        $order->is_paid = 1;
        $order->save();

        $orderlist = RestaurantOrderList::where('order_id', $order->id)->where('is_paid', 0)->first();
        $orderlist->is_paid = 1;
        $orderlist->save();

        return res('success');
    }

    public function add($req)
    {
        DB::beginTransaction();
        try{
            $reservation_id = 0;
            $shipping_address_latitude = 0.0;
            $shipping_address_longitude = 0.0;
            $dc = 10;
            $note = 'null';

            $permissions = ['customer'];
        
            if (!in_array(auth()->user()->type_info, $permissions)) {
                return res(__('user.not_allowed'), null, 400);
            }
                
            $validator = Validator::make($req->all(), [
                'payment_method_id' =>      'required',
            ]);

            if ($validator->fails()) {
                return res('Failed', $validator->errors(), 412)->send();
            }

            $carts = RestaurantCart::where('user_id', auth()->user()->id)->where('on_checkout', 0)->where('is_ordered', 0)
                    ->where('is_deleted', 0)->first();
            
            if(!$carts->has_booking) {
                $shipping_address_latitude = $req->shipping_address_latitude;
                $shipping_address_longitude = $req->shipping_address_longitude;
            }

            if($req->note) $note = $req->note;

            if($carts == '2' && $req->shipping_rate) $dc = $req->shipping_rate;

            $submission = true;

            $on = generateOrderNumber();
            while($submission){
                $count = RestaurantOrder::where('order_number', $on)->count();
                if($count > 0) {
                    $on = generateOrderNumber();
                } else {
                    $submission = false;
                }
            }

            $order =                                new RestaurantOrder; 
            $order->customer_id =                   auth()->user()->id;
            $order->order_number =                  $on;
            $order->sub_total =                     0;
            $order->shipping_rate =                 priceToCent($dc);
            $order->payment_method_id =             $req->payment_method_id;
            $order->order_type =                    $carts->order_type;

            $order->billing_address =               json_encode(auth()->user()->default_billing_address);
            $order->shipping_address =              $req->shipping_address;
            $order->billing_address_coordinates =   json_encode(auth()->user()->default_billing_address->latlng);
            $order->shipping_address_coordinates =  'love your self';
            $order->shipping_address_latitude =     $shipping_address_latitude;
            $order->shipping_address_longitude =    $shipping_address_longitude;
            $order->note =                          $note;
            $order->is_paid =                       0;
            $order->is_completed =                  0;
            $order->reservation_id =                0;
            $order->cart_id =                       $carts->id;
    
            if($carts)
            {
                $order->save();
                if($carts->has_booking == 1)  $reservation_id = $this->addTableReservation($req, $carts);
                $this->saveOrderRestaurant($order->id, $reservation_id);
            }
            else
            {
                return res('no cart or order already exist');
            }

            DB::commit();

            $myorder = RestaurantOrder::where('customer_id', auth()->user()->id)
            ->where('id', $order->id)
            ->first();
    
            if($myorder) $myorder = new ConfirmOrder($myorder);

            return res('success', $myorder);

        } catch (\Throwable $e){
            DB::rollback();
            throw $e;
        }   
    }

    public function addTableReservation($req, $cart)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'time_arrive' =>        'required',
            'date_arrive' =>        'required',
            'guest' =>              'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402)->send();
        }
        
        $time = strtotime($req->time_arrive);
        $date = strtotime($req->date_arrive);

        $carts = RestaurantCart::where('user_id', auth()->user()->id)        
            ->where('order_type', 1)->where('on_checkout', 0)
                ->where('is_ordered', 0)->where('is_deleted', 0)->first();
        
        $exist = TableReservation::where('table_number', $req->table_number)
            ->where('restaurant_id', $carts['restaurant_id'])
                ->where('arrive_time', $time)
                    ->where('arrive_date', $date)
                        ->where('status', 1) //means reserve
                            ->first();

        if($exist) return res('this table is already reserved');

        $reserve_number = encode(Carbon::now()->timestamp, 'reservation-number');

        $c = TableReservation::where('reservation_number', $reserve_number)->first();

        if($c) $reserve_number = encode(Carbon::now()->timestamp, 'reservation-number');

        $reserve =                              new TableReservation;
        $reserve->restaurant_id =               $cart->restaurant_id;
        $reserve->user_id =                     auth()->user()->id;
        $reserve->table_number =                0;
        $reserve->note =                        $req->note;
        $reserve->arrive_time =                 $time;
        $reserve->arrive_date =                 $date;
        $reserve->order =                       1;
        $reserve->guest =                       $req->guest;
        $reserve->qrcode =                      Str::random(32);
        $reserve->reservation_number =          $reserve_number; 
        $reserve->save();
        
        $cart = RestaurantCart::find($cart->id);
        $cart->reservation_id = $reserve->id;
        $cart->save();

        return $reserve->id;
    }

    private function saveOrderRestaurant(int $order_id, $reservation_id)
    {
        DB::beginTransaction();
        try{
            $type = 'Dine in'; 
            $carts = RestaurantCart::where('user_id', auth()->user()->id)
                ->where('on_checkout', 0)->where('is_ordered', 0)
                    ->where('is_deleted', 0)->first();


            if($carts->order_type == 1) {$type ='Dine In';} if($carts->order_type == 2) {$type = 'Delivery';} if($carts->order_type == 3) {$type == 'Pickup';}            
            
            $carts = array($carts);
           
            foreach ($carts as $value){
                $sub_total = 0;
                foreach ($value->foods as $item) {
                    $sub_total += $item->food->price * $item->quantity;     
                }
                
                $os =                                   new RestaurantOrderList;
                $os->order_id =                         $order_id;
                $os->restaurant_id =                    $value->restaurant_id;
                $os->sub_total =                        priceToCent($sub_total);
                $os->order_type =                       $type;
                
                $os->save();

                $this->saveOrderFood($value, $os->id, $os->restaurant_id);
                $this->updateSub($order_id, $sub_total, $reservation_id, $value->id);
            }

            DB::commit();
        }catch (\Throwable $e){
            DB::rollback();
            throw $e;
        }
    }

    private function saveOrderFood(RestaurantCart $cart, int $order_list_id, $restaurant_id)
    {
        $v_total = 0;
        $p = 0;
        $i = 0;
        $var_id = 0;
        $i = RestaurantCartItem::where('cart_id', $cart->id)->first();
        foreach ($cart->foods as $value) {           
            $v = RestaurantCartItemVariation::where('cart_item_id', $i->id)->get();
            if($v){
                foreach($v as $var)
                {
                    $v_total += $var->amount; 
                    $var_id = $var->id;
                }
                
            }   
           
            $comm_settings = settings('commission');
            $osi =                                  new RestaurantOrderFood;
            $osi->order_list_id =                   $order_list_id;
            $osi->food_id =                         $value->food_id;
            $osi->name =                            $value->food->name;
            $p =                                    $value->food->price + $v_total;
            $osi->price =                           $p;
            $osi->quantity =                        $value->quantity;
            $osi->request =                         $value->request;

            if ($comm_settings->mode == 'percentage') {
                $i = ($osi->foodvariation->price * $osi->quantity) * $comm_settings->value / 100;
            } else {
                $i = priceToCent($comm_settings->value);
            }

            $osi->commission = priceToCent($i);
            $osi->is_rate = 0;
            $osi->save();

            $this->add_order_food_variation($var_id, $osi->food_id);

            $item = RestaurantCart::find($cart->id);
            $item->restaurant_id = $restaurant_id;
            $item->on_checkout =1;
            $item->is_ordered = 1;
            $item->save();

            $item = RestaurantCartItem::find($value->id);
            $item->is_ordered = 1;
            $item->save();
        }
    }

    public function updateSub($order_id, $sub_total, $reservation_id, $cart_id)
    {
        $total = RestaurantOrder::where('id', $order_id)->first();
        $total -> sub_total = $sub_total;
        $total -> reservation_id = $reservation_id;
        $total -> cart_id = $cart_id;
        $total ->save();
    }

    public function add_order_food_variation($order_variation_id, $food_id)
    {
        $v = RestaurantCartItemVariation::where('id', $order_variation_id)->first();
        $f =                            new RestaurantOrderFoodVariation;
        $f->food_item_id =              $food_id;           
        $f->food_variation_id =         $v->id;
        $f->save();

    }

    public function restaurantOrderList($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        $status = 1;
        if($req->status == 'All' && $req->order_type == 'All'){

            $status = 10; 

            $orderlist = RestaurantOrderList::where('restaurant_id', $restaurant_id)
            ->where('order_type', '!=' , 'Dine In')
            //->where('status', $status)
            ->limit($req->limit)
            ->orderBy('updated_at', 'DESC')
            ->paginate($req->limit);

            $collection = OrderListResource::collection($orderlist);
        } 
        else
        {
            if($req->status == 'Cancelled') $status = 0; if($req->status == 'Pending') $status = 1; if($req->status == 'Preparing') $status = 2; if($req->status == 'Ready to Serve') $status = 3; if($req->status == 'Completed') $status = 4;
            if($req->status == 'Ready to Pickup') $status = 5; if($req->status == 'Delivered') $status = 6;

            if($req->order_type == 'All')
            {
                $orderlist = RestaurantOrderList::where('restaurant_id', $restaurant_id)
                //->where('status', $status)
                ->where('order_type', '!=' , 'Dine In')
                ->limit($req->limit)
                ->orderBy('updated_at', 'DESC')
                ->paginate($req->limit);
            }

            if($req->order_type == 'Delivery')
            {
                if($req->status == 'All')
                {
                    $orderlist = RestaurantOrderList::where('restaurant_id', $restaurant_id)
                    ->where('order_type', 'Delivery')
                    ->limit($req->limit)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate($req->limit);
                }
                else
                {
                    $orderlist = RestaurantOrderList::where('restaurant_id', $restaurant_id)
                    ->where('status', $status)
                    ->where('order_type', 'Delivery')
                    ->limit($req->limit)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate($req->limit);
                }
            }

            if($req->order_type == 'Pickup')
            {
                if($req->status == 'All')
                {
                    $orderlist = RestaurantOrderList::where('restaurant_id', $restaurant_id)
                    ->where('order_type', 'Pickup')
                    ->limit($req->limit)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate($req->limit);
                }
                else
                {
                    $orderlist = RestaurantOrderList::with('order','restaurant')->where('restaurant_id', $restaurant_id)
                    ->where('status', $status)
                    ->where('order_type', 'Pickup')
                    ->limit($req->limit)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate($req->limit);
                }
            }
            if($orderlist != null)  $collection = OrderListResource::collection($orderlist);
            else{return res('success', 'no data available');}
        }
      
        return res('success', $collection);
    }

    public function myOrderlist($req)
    {
        $permissions = ['customer'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        if (!$req->has('status')) {
            return res(__('validation.identifier'), null, 400);
        }

        //$restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        if($req->status == 'All')
        {  
            $orderlist = RestaurantOrder::where('customer_id', auth()->user()->id)
            ->orderBy('created_at', 'DESC')
            ->limit($req->limit)
            ->paginate($req->limit);
        }
        else
        {
            $orderlist = RestaurantOrder::where('customer_id', auth()->user()->id)
            ->where('order_type', '!=', 1)
            ->where('status', $req->status)
            ->orderBy('created_at', 'DESC')
            ->paginate($req->limit);
        }
        
        $collection = OrderResource::collection($orderlist);
        return res('success', $collection);
    }

    public function order_status_update($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
            'order_hash_id' => 'required',
            'status' => 'required',
        ]);
        
        if($v->fails()) return res('failed', $v->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        $order_id = decode($req->order_hash_id, 'uuid');

        $order_id = RestaurantOrderList::where('order_id', $order_id)
        ->where('restaurant_id', $restaurant_id)
        ->first()
        ->order_id;

        if(!$order_id) return res('no order found.');

        $order = RestaurantOrder::where('id', $order_id)
        ->first();

        if(!$order) return res('no order found.');

        $order->status = $req->status;
        $order->save();

        if($req->status == 6)
        {
            $order_list = RestaurantOrderList::where('order_id', $order_id)
            ->where('restaurant_id', $restaurant_id)
            ->first();
        
            $order_list->is_completed = 1;
            $order_list->save();
        }

        return res('success', decode($order->order_number,'order-number'));
    }

    public function listOrderByType($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        if (!$req->has('order_type')) {
            return res(__('validation.identifier'), null, 400);
        }

        if (!$req->has('status')) {
            return res(__('validation.identifier'), null, 400);
        }

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
        $order = RestaurantOrder::where('restaurant_id', $restaurant_id)
        ->where('id', $req->order_type)
        ->where('status', $req->status)
        ->get();    

        return res('success', $order);
    }

    public function show($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        if (!$req->has('order_hash_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $order_hash_id = decode($req->order_hash_id, 'uuid');
        $orderlist = RestaurantOrderList::with('order','restaurant','user')->where('id', $order_hash_id)
        ->get();

        $collection = OrderResource::collection($orderlist);
      
        return res('success', $collection);
    }

    public function totalOrder($req)
    {
        $permissions = ['vendor', 'vendor staff',];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        if (!$req->has('restaurant_hash_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $restaurant_hash_id = decode($req->restaurant_hash_id, 'uuid');
        $total = RestaurantOrderList::with('order','restaurant','user')->where('restaurant_id', $restaurant_hash_id)
        ->count();

        return res('success', $total);
    }

    public function orderFoodDetail($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        if (!$req->has('order_hash_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        $order_hash_id = decode($req->order_hash_id, 'uuid');     
        $orderlist = RestaurantOrderFood::with('list')->where('order_list_id', $order_hash_id)
        ->get();

        $orderlist = OrderFoodsResource::collection($orderlist);

        return res('success', $orderlist);
    }

    public function qrcodeList($req)
    {
        $permissions = ['customer'];
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $myOrders = RestaurantOrder::where('customer_id', auth()->user()->id)->get();
        $myOrders = QRListResource::collection($myOrders);

        return res('success', $myOrders);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    
    public function qrcodeRestaurantList($req)
    {
        $permissions = ['customer'];
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $restuarant_id = decode($req->restaurant_hash_id, 'uuid');

        $myOrders = RestaurantOrder::where('restaurant_id' , $restuarant_id)->get();
        $myOrders = QRListResource::collection($myOrders);

        return res('success', $myOrders);
    }

    public function updateOrder($req)
    {
        $permissions = ['customer', 'vendor staff'];
        
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'cart_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $c = decode($req->cart_hash_id, 'uuid');

        $order = RestaurantOrder::where('cart_id', $c)->first();
        if(!$order) return res('order not exist');

        DB::beginTransaction();
        try{
            $carts = RestaurantCart::where('user_id', auth()->user()->id)->where('on_checkout', 0)->where('is_ordered', 0)->where('is_deleted', 0)->get();
            
            foreach ($carts as $value){
                $sub_total = 0;
                foreach ($value->foods as $item) {
                    $sub_total += centToPrice($item->food->price) * $item->quantity;     
                    
                }
                      
                $os =                                   new RestaurantOrderList;
                $os->order_id =                         $order->id;
                $os->restaurant_id =                    $value->restaurant_id;
                $os->sub_total =                        priceToCent($sub_total);
                $os->order_type =                       1;
                
                $os->save();

                $this->saveOrderFood($value, $os->id, $os->restaurant_id);
                $this->updateSub($order->id, $sub_total, $order->reservation_id, $c);
            }

            DB::commit();

            return res('successfully ordered');
        }catch (\Throwable $e){
            DB::rollback();
            throw $e;
        }
    }

    public function add_order_in_reservation($req)
    {
        DB::beginTransaction();
        try{
            $permissions = ['customer', 'vendor', 'vendor staff'];
            if (!in_array(auth()->user()->type_info, $permissions)) {
                return res(__('user.not_allowed'), null, 400);
            }

            $v =Validator::make($req->all(),[
                'restaurant_hash_id' =>             'required',
                'reservation_hash_id' =>            'required',
                'cart_hash_id' =>                   'required',
                'user_hash_id' =>                   'required',
                'payment_method_id' =>              'required',
            ]);

            if($v->fails()) return res('failed', $v->errors(), 402);

            $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
            $reservation_id = decode($req->reservation_hash_id, 'uuid');
            $cart_id = decode($req->cart_hash_id, 'uuid');
            $user_id = decode($req->user_hash_id, 'uuid');

            $carts = RestaurantCart::where('id', $cart_id)->where('user_id', $user_id)
                ->where('restaurant_id', $restaurant_id)->where('order_type', 1)
                    ->where('on_checkout', 0)->where('is_ordered', 0)
                        ->where('is_deleted', 0)->first();
            
            if(!$carts) return res('no cart found');

            $reservation = TableReservation::where('id', $reservation_id)->where('restaurant_id', $restaurant_id)->where('user_id', $user_id)->first();
            if(!$reservation) return res('no reservation found');

            $order_number = encode(now()->timestamp, 'order-number');
            $c = RestaurantOrder::where('order_number', $order_number)->count();

            if($c > 0) $order_number = encode(now()->timestamp, 'order-number');

            $order =                                new RestaurantOrder; 
            $order->customer_id =                   $user_id;
            $order->order_number =                  $order_number;
            $order->sub_total =                     0;
            $order->shipping_rate =                 0;
            $order->payment_method_id =             $req->payment_method_id;
            $order->order_type =                    1;

            $order->billing_address =               'no data';
            $order->shipping_address =              'no data';
            $order->billing_address_coordinates =   'no data';
            $order->shipping_address_coordinates =  'love your self';
            $order->shipping_address_latitude =     0.0;
            $order->shipping_address_longitude =    0.0;
            $order->note =                          'null';
            $order->is_paid =                       0;
            $order->is_completed =                  0;
            $order->reservation_id =                $reservation_id;
            $order->cart_id =                       $cart_id;
            
            if($carts)
            {
                $order->save();
                //if($carts->has_booking == 1)  $reservation_id = $this->addTableReservation($req);
                $this->saveOrderRestaurant($order->id, $reservation_id,0);
            }
            else
            {
                return res('no cart or order already exist');
            }

            DB::commit();

            return res('success', 'order number:'.$order_number);

        } catch (\Throwable $e){
            DB::rollback();
            throw $e;
        }   
    }

    public function complete_order($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $order_id = decode($req->order_hash_id, 'uuid');
        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        $order_exist = RestaurantOrderList::where('order_id', $order_id)->where('restaurant_id', $restaurant_id)->first();
        if(!$order_exist) return res('failed..,order does not exist');

        $order = RestaurantOrder::find($order_id);
        $order->status = 6;
        $order->save();

        return res('success...order completed');
    }

    public function reservation_checkout($req)
    { 
        DB::beginTransaction();
        try{
            $permissions = ['customer'];
        
            if(!in_array(auth()->user()->type_info, $permissions)){
                return res('Only vendor can do this process', null, 401);
            }
    
            $v = Validator::make($req->all(),[
                'restaurant_hash_id' =>     'required',
                'reservation_hash_id' =>    'required',
                'cart_hash_id' =>           'required',
                'payment_method_id' =>      'required'
            ]);
    
            if($v->fails()) return res('failed', $v->errors(), 402);
    
            $restaurant_id = decode($req->restaurant_hash_id, 'uuid');
            $reservation_id = decode($req->reservation_hash_id,'uuid');
            $cart_id = decode($req->cart_hash_id, 'uuid');

            $carts = RestaurantCart::where('id', $cart_id)
                    ->where('user_id', auth()->user()->id)
                    ->where('restaurant_id', $restaurant_id)
                    ->where('reservation_id', $reservation_id)->first();


            $order_number = encode(now()->timestamp, 'order-number');
            $c = RestaurantOrder::where('order_number', $order_number)->count();

            if($c > 0) $order_number = encode(now()->timestamp, 'order-number');

            $order =                                new RestaurantOrder; 
            $order->customer_id =                   auth()->user()->id;
            $order->order_number =                  $order_number;
            $order->sub_total =                     0;
            $order->shipping_rate =                 0;
            $order->payment_method_id =             $req->payment_method_id;
            $order->order_type =                    1;

            $order->billing_address =               json_encode(auth()->user()->default_billing_address);
            $order->shipping_address =              'qatar';
            $order->billing_address_coordinates =   json_encode(auth()->user()->default_billing_address->latlng);
            $order->shipping_address_coordinates =  'love your self';
            $order->shipping_address_latitude =     0.0;
            $order->shipping_address_longitude =    0.0;
            $order->note =                          'null';
            $order->is_paid =                       0;
            $order->is_completed =                  0;
            $order->reservation_id =                0;
            $order->cart_id =                       $carts->id;
            
            if($carts)
            {
                $order->save();
                $this->saveOrderRestaurant($order->id,   $reservation_id,0);
            }
            else
            {
                return res('no cart or order already exist');
            }

            DB::commit();

            return res('success', 'order complete');
        } catch (\Throwable $e){
            DB::rollBack();
            throw $e;
        }
    }

    public function get_distance_store_to_customer($req)
    {    
        $radius = 100;
        $lat = $req->latitude;
        $lon = $req->longitude;

        $id = decode($req->restaurant_hash_id,'uuid');

        $haversine = "(6371 * acos(cos(radians($lat)) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) 
        - radians($lon)) 
        + sin(radians($lat)) 
        * sin(radians(latitude))))";
        
        return Store::select('id','name','latitude','longitude','street', 'city') //pick the columns you want here.
        ->selectRaw("{$haversine} AS distance")
        ->where('id', $id)
        ->whereRaw("{$haversine} < ?", [$radius])->orderBy('distance')->limit(1)->first();
    }

    public function checkout_details($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'cart_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);
        
        $id = decode($req->cart_hash_id, 'uuid');
        
        $c = RestaurantCart::select('id','user_id','restaurant_id','transaction_number')
        ->where('id', $id)->first();

        $c = new CheckoutDetails($c);

        return res('success', $c);
    }
    
    public function default_address($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $a = UserAddress::select('id','user_id', 'street', 'state', 'city', 'other_address_info','latitude','longitude')
        ->where('user_id', auth()->user()->id)->first();

        if($a) $a->makeHidden(['id','user_id']);

        return res('success', $a);
    }
    public function confirm_order_details($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $id = decode($req->order_hash_id, 'uuid');

        $myorder = RestaurantOrder::where('customer_id', auth()->user()->id)
        ->where('id', $id)
        ->first();

        if($myorder) $myorder = new ConfirmOrder($myorder);
        
        return res('success', $myorder);
    }

    public function order_progress($req)
    {
        $permissions = ['customer'];
        
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        if($req->all_order == 1)
        {
            if($req->status == 'All' || $req->status == null){
                $o = RestaurantOrder::select('id','customer_id','shipping_rate','order_number','sub_total','shipping_address','status')
                    ->where('customer_id', auth()->user()->id)
                    ->limit($req->limit)
                    ->orderBy('created_at','DESC')->paginate($req->limit);
            }
            else
            {
                $o = RestaurantOrder::select('id','customer_id','shipping_rate','order_number','sub_total','shipping_address','status')
                    ->where('customer_id', auth()->user()->id)
                    ->where('status',  $req->status)
                    ->limit($req->limit)
                    ->orderBy('created_at','DESC')->paginate($req->limit);
            }

            $o = OrderInProgress::collection($o);
        }
        else if($req->all_order == 2)
        {
            $o = RestaurantOrder::select('id','customer_id','order_number','sub_total','shipping_rate','shipping_address','status')
                ->where('customer_id', auth()->user()->id)->latest('created_at')->first();
            
            if($o) {$o = new OrderInProgress($o);} else{$o = 'no order found';}
        }
        return res('success', $o);
    }

    public function get_pickup_orders($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'store_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);
        
        $s = decode($req->store_hash_id, 'uuid');

        $ol = RestaurantOrderList::where('restaurant_id', $s)
        ->where('order_type', 'Pickup')
        ->limit($req->limit)
        ->orderBy('updated_at', 'DESC')
        ->paginate($req->limit);

        return res('success', $ol);
    }

    public function my_order_list($req)
    {
        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $ol = RestaurantOrder::select('id', 'customer_id','sub_total', 'shipping_rate','shipping_address','shipping_address_latitude','shipping_address_longitude','status','created_at')->where('customer_id', auth()->user()->id)->orderBy('created_at', 'DESC')->limit($req->limit)->paginate($req->limit);
        $ol = MyOrderList::collection($ol);

        return res('success', $ol);
    }

    public function my_order_info($req)
    {
        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only customer can do this process', null, 401);
        }

        $v = Validator::make($req->all(),[
            'order_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $o = decode($req->order_hash_id, 'uuid');

        $ol = RestaurantOrder::where('id', $o)->where('customer_id', auth()->user()->id)->first();
        if($ol) $ol = new MyOrderInfo($ol);
    
        return res('success', $ol);
    }
    
    public function deliver_order_list($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        $status = 1;

        if(!in_array(auth()->user()->type_info, $permissions))
        {
            return res('Only vendor and vendor staf can process this', null, 402);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        
        if($v->fails()) return res('failed', $v->errors(), 402);

        $r = decode($req->restaurant_hash_id, 'uuid');
        if($req->status) $status = $req->status;
        
        $check = DB::table('stores')->select('id','user_id')->where('id', $r)->first();
        if($check->user_id != auth()->user()->id) return res('failed', 'You are not allowed here', 402);
        
        if($status == 'All')
        {
            $ol = DB::table('restaurant_orders')
            ->select('restaurant_orders.id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_rate','payment_method_id','shipping_address','restaurant_orders.status','restaurant_orders.order_type','restaurant_orders.customer_id','restaurant_orders.created_at',
            'restaurant_orders.payment_method_id','restaurant_orders.cart_id','restaurant_order_lists.restaurant_id')
                ->join('restaurant_order_lists', 'restaurant_order_lists.order_id', '=', 'restaurant_orders.id')
                ->where('restaurant_order_lists.restaurant_id', $r)
                ->limit($req->limit)
                ->orderBy('created_at', 'DESC')
                ->paginate($req->limit);

        }else{

            $ol = DB::table('restaurant_orders')
            ->select('restaurant_orders.id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_rate','payment_method_id','shipping_address','restaurant_orders.status','restaurant_orders.order_type','restaurant_orders.customer_id','restaurant_orders.created_at',
            'restaurant_orders.payment_method_id','restaurant_orders.cart_id','restaurant_order_lists.restaurant_id')
                ->join('restaurant_order_lists', 'restaurant_order_lists.order_id', '=', 'restaurant_orders.id')
                ->where('restaurant_order_lists.restaurant_id', $r)
                ->where('restaurant_orders.status', $status)
                ->limit($req->limit)
                ->orderBy('created_at', 'DESC')
                ->paginate($req->limit);
        }

        $ol = DeliveryResource::collection($ol);
        //$ol = new DeliveryResource($ol);

        return res('success', $ol);
    }

    public function pickup_order_list($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        $status = 1;

        if(!in_array(auth()->user()->type_info, $permissions))
        {
            return res('Only vendor and vendor staf can process this', null, 402);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        
        if($v->fails()) return res('failed', $v->errors(), 402);

        $r = decode($req->restaurant_hash_id, 'uuid');
        if($req->status) $status = $req->status;
        
        $check = DB::table('stores')->select('id','user_id')->where('id', $r)->first();
        if($check->user_id != auth()->user()->id) return res('failed', 'You are not allowed here', 402);
        
        if($status == 'All')
        {
            $ol = DB::table('restaurant_orders')
                ->select('restaurant_orders.id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_rate','payment_method_id','shipping_address','restaurant_orders.status','restaurant_orders.order_type','restaurant_orders.customer_id','restaurant_orders.created_at',
                'restaurant_orders.payment_method_id','restaurant_orders.cart_id','restaurant_order_lists.restaurant_id')
                ->join('restaurant_order_lists', 'restaurant_order_lists.order_id', '=', 'restaurant_orders.id')
                ->where('restaurant_order_lists.restaurant_id', $r)
                ->where('restaurant_orders.order_type' , 3)
                ->limit($req->limit)
                ->orderBy('created_at', 'DESC')
                ->paginate($req->limit);
        }else{

            $ol = DB::table('restaurant_orders')
                ->select('restaurant_orders.id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_rate','payment_method_id','shipping_address','restaurant_orders.status','restaurant_orders.order_type','restaurant_orders.customer_id','restaurant_orders.created_at',
                'restaurant_orders.payment_method_id','restaurant_orders.cart_id','restaurant_order_lists.restaurant_id')
                ->join('restaurant_order_lists', 'restaurant_order_lists.order_id', '=', 'restaurant_orders.id')
                ->where('restaurant_order_lists.restaurant_id', $r)
                ->where('restaurant_orders.status', $status)
                ->where('restaurant_orders.order_type' , 3)
                ->limit($req->limit)
                ->orderBy('created_at', 'DESC')
                ->paginate($req->limit);
        }

        $ol = DeliveryResource::collection($ol);
        //$ol = new DeliveryResource($ol);

        return res('success', $ol);
    }

    public function get_order_using_order_number($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_allowed', null, 400));
        }

        $v = Validator::make($req->all(),[
            'order_number' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $on = encode($req->order_number,'order-number');

        $order = DB::table('restaurant_orders')
        ->select('restaurant_orders.id','restaurant_orders.order_number','restaurant_orders.sub_total','restaurant_orders.shipping_rate','payment_method_id','shipping_address','restaurant_orders.status','restaurant_orders.order_type','restaurant_orders.customer_id','restaurant_orders.created_at',
        'restaurant_orders.payment_method_id','restaurant_orders.cart_id','restaurant_order_lists.restaurant_id')
            ->join('restaurant_order_lists', 'restaurant_order_lists.order_id', '=', 'restaurant_orders.id')
            ->where('restaurant_orders.order_number', $on)
            ->first();

        if($order) $order = new DeliveryResource($order);

        return res('success', $order);
    }

    public function total_status_count($req)
    {
        $permissions = ['vendor', 'vendro staff'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor and vendor staf can do this process.', null, 400);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed!', $v->errors(), 402);

        $restaurant = decode($req->restaurant_hash_id, 'uuid');

        $s = DB::table('stores')->select('id')->where('id', $restaurant)->first();
        if(!$s) return res('failed!','Store not found.', 402);

        $count = DB::table('restaurant_orders')->join('restaurant_order_lists', 'restaurant_orders.id', '=', 'restaurant_order_lists.id')
                ->select(DB::raw('SUM(status = 1 && reservation_id = 0) as dine_in'),DB::raw('SUM(status = 2) as delvery'), DB::raw('SUM(status = 3) as take_away'), DB::raw('SUM(status = 1 and reservation_id != 0) as table_booking'))
                ->where('restaurant_order_lists.restaurant_id', $restaurant)->first();
    
        return res('success', $count);
    }
}