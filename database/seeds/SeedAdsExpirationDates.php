<?php

use Illuminate\Database\Seeder;
use App\Models\AdsExpirationDate;

class SeedAdsExpirationDates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = AdsExpirationDate::count();
        if($count == 0){
            AdsExpirationDate::firstOrCreate(['duration_name' => '3 days', 'duration_type' => 'day', 'duration_value' => 3, 'status' => 1]);
            AdsExpirationDate::firstOrCreate(['duration_name' => '1 week', 'duration_type' => 'week', 'duration_value' => 1, 'status' => 1]);
            AdsExpirationDate::firstOrCreate(['duration_name' => '2 weeks', 'duration_type' => 'week', 'duration_value' => 2, 'status' => 1]);
            AdsExpirationDate::firstOrCreate(['duration_name' => '1 month', 'duration_type' => 'month', 'duration_value' => 1, 'status' => 1]);
        }
    }
}
