<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchShow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'                => $this->hashid,
            'name'                  => $this->name,
            'street'                => $this->street,
            'state'                 => $this->state,
            'city'                  => $this->city,
            'contact_name'          => $this->contact_name,
            'other_address_info'    => $this->other_address_info,
            'contact_number'        => $this->contact_number,
            'contact_email'         => $this->contact_email,
            'latitude'              => $this->latitude,
            'longitude'             => $this->longitude,
            'status'                => $this->status ? 'active' : 'inactive',
            'is_approved'           => $this->approve(),
            //'main_branch' => $this->store()
        ];
    }

    public function status()
    {
        if($this->status === 1)return 'active';
        if($this->status === 0)return 'inactive';
    }

    public function approve()
    {
        if($this->is_approved === 1)return 'approved';
        if($this->is_declined === 0)return 'declined';
        if($this->is_declined === 0 && $this->is_approved === 0)return 'pending';
    }
}
