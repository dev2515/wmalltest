<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFoodTypeImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_food_category_pivot', 'food_type_image')){
            Schema::table('restaurant_food_category_pivot', function (Blueprint $table) {
                $table->longtext('food_type_image')->nullable()->after('status');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_food_category_pivot', 'food_type_image')){
            Schema::table('restaurant_food_category_pivot', function (Blueprint $table) {
                $table->dropColumn('food_type_image');
            });
        }
    }
}
