<?php

use Illuminate\Database\Seeder;
use App\Models\ShippingCategory;

class SeedShippingCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShippingCategory::firstOrCreate(['slug' => 'w-express', 'name' => 'WExpress']);
        ShippingCategory::firstOrCreate(['slug' => 'economy', 'name' => 'Economy']);
        ShippingCategory::firstOrCreate(['slug' => 'standard', 'name' => 'Standard']);
        ShippingCategory::firstOrCreate(['slug' => 'express', 'name' => 'Express']);
    }
}
