<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('driver_activities')){
            Schema::create('driver_activities', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->boolean('on_duty')->default(0)->nullable();
                $table->integer('daily_trip')->default(0)->nullable();
                $table->integer('total_income')->default(0)->nullable();
                $table->integer('total_working_hour')->default(0)->nullable();
                $table->integer('total_distance')->default(0)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_activities');
    }
}
