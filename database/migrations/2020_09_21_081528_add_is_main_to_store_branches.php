<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Model\StoreBranch;

class AddIsMainToStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'is_main')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->boolean('is_main')->after('is_declined')->default(0);
            });
        }

        $store_branches = StoreBranch::where('name', 'Main Branch')->get();
        foreach ($store_branches as $branch){
            $branch->is_main = 1;
            $branch->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_branches', 'is_main')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('is_main');
            });
        }
    }
}
