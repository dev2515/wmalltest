<?php

namespace App\Repositories;

use App\Models\Category;
use App\Interfaces\CategoryInterface;

class CategoryRepository implements CategoryInterface
{
    public function top($req)
    {
        $count = 4;
        if ($req->has('count')) {
            $count = $req->count;
        }
        // TODO: Get algo for most sales
        $list = Category::where('parent_id', 0)->where('status', 1)->get()->shuffle()->pluck('name')->toArray();
        $items = array_slice($list, 0, $count);

        return res('Success', $items);
    }

    public function list($req)
    {
        $list = Category::where('parent_id', 0)->where('status', 1)->orderBy('name', 'ASC')->get();
        $list->makeHidden(['created_at', 'updated_at', 'status', 'parent_id']);

        return res('Success', $list);
    }
}
