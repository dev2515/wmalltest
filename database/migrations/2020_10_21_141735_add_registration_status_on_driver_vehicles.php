<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegistrationStatusOnDriverVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_vehicles', 'registration_status')){
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->string('registration_status')->default('pending')->nullable()->after('file_type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_vehicles', 'registration_status')){
            Schema::table('driver_vehicles', function (Blueprint $table) {
                $table->dropColumn('registration_status');
            });
        }
    }
}
