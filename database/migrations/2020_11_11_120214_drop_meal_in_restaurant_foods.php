<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropMealInRestaurantFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('restaurant_foods', 'meal')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('meal');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restaurant_foods', function (Blueprint $table) {
            //
        });
    }
}
