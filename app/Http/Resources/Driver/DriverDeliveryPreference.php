<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverDeliveryPreference extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => $this->hash_id,
            'driver' => $this->driver->name,
            'type' => $this->type->name,
            'status' => $this->status == 1 ? 'on' : 'off',
        ];
    }
}
