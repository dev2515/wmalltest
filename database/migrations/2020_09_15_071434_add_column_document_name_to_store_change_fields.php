<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDocumentNameToStoreChangeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_change_fields', 'document_name')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->string('document_name')->nullable()->after('type');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_change_fields', 'document_name')){
            Schema::table('store_change_fields', function (Blueprint $table) {
                $table->dropColumn('document_name');
            });
        }
    }
}
