<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInDineInOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_dine_in_orders','order_number')){
            Schema::table('restaurant_dine_in_orders', function (Blueprint $table) {
                $table->integer('order_number')->default(45);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_dine_in_orders', 'order_number')){
            Schema::table('restaurant_dine_in_orders', function (Blueprint $table) {
                $table->dropColumn('order_number');
            });
        }
    }
}
