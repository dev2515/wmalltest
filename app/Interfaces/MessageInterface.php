<?php 

namespace App\Interfaces;

interface MessageInterface 
{
    public function userSendMessage($req);

    public function userConversationList($req);

    public function userMessageList($req);

    public function storeMessageList($req);

    public function storeSendMessage($req);

    public function storeConversationList($req);

    public function sendEmail($req);
}