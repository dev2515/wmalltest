<?php

return [
    'not_found'         => 'Conversation not found',
    'identifier'        => 'Conversation hashid is required',
];