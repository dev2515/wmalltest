<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class RestaurantFood extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       //return parent::toArray($request);
        return[
            'hash_id' =>                    $this->hash_id,
            'restaurant' =>                 $this->name,
            'email' =>                      $this->email,
            'phone_number' =>               $this->phone_number,
            'information' =>                $this->information,
            'full' =>                       asset($this->photo()->full),
            'thumbnail' =>                  asset($this->photo()->thumbnail), 
            'cover_full' =>                 asset($this->cover()->full),
            'cover_thumbnail' =>            asset($this->cover()->thumbnail),
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/restaurant/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/restaurant/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/restaurant/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/restaurant/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
    
    public function cover()
    {      
        $full = public_path() . '/images/restaurant_cover/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/restaurant_cover/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/restaurant_cover/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/restaurant_cover/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
    

}
