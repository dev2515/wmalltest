<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAndUpdateColumnsInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('orders',['shipping_address_latitude','shipping_address_longitude','order_type','note','reservation_id','is_paid'])){
            Schema::table('orders', function (Blueprint $table) {
                $table->integer('shipping_address_latitude');
                $table->integer('shipping_address_longitude');
                $table->integer('order_type')->default(0);
                $table->longText('note')->nullable();
                $table->unsignedBigInteger('reservation_id')->default(0);
                $table->boolean('is_paid')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('orders',['shipping_address_latitude','shipping_address_longitude','order_type','note','reservation_id','is_paid'])){
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn('shipping_address_latitude');
                $table->dropColumn('shipping_address_longitude');
                $table->dropColumn('order_type')->default(0);
                $table->dropColumn('note')->nullable();
                $table->dropColumn('reservation_id')->default(0);
                $table->dropColumn('is_paid')->default(0);
            });
        }
    }
}
