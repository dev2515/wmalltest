<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;

class TimeSchedule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'time_hashid'   => encode($this->id, 'uuid'),
            'title'         => $this->name,
            'open_at'       => $this->store_open,
            'close_at'      => $this->store_close,
        ];
    }
}
