<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsApprovedToStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'is_approved')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->boolean('is_approved')->after('longitude');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_branches', 'is_approved')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('is_approved');
            });
        }
    }
}
