<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('reviews')){
            Schema::create('reviews', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('category');
                $table->unsignedBigInteger('type_id');
                $table->integer('rate');
                $table->longText('content');
                $table->integer('flag')->nullable();
                $table->longText('comment')->nullable();
                $table->boolean('status');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('reviews'))
        {
            Schema::dropIfExists('reviews');
        }
    }
}
