<?php

namespace App\Repositories;

use App\Http\Resources\Restaurant\OrderListResource;
use App\Http\Resources\Restaurant\ReservationList;
use App\Http\Resources\Restaurant\ReservationResource;
use App\Http\Resources\Restaurant\WaiterOrderListResource;
use App\Interfaces\RestaurantManagementInterface;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\TableReservation;
use Illuminate\Support\Facades\Validator;

class RestaurantManagementRepository implements RestaurantManagementInterface
{
    /*****************************************************************************************
     * 
     *                  MANAGER FUNCTIONALITIES
     * 
     *****************************************************************************************/
    public function dine_in_order_list($req)
    {
        $permissions = ['vendor', 'vendor staff','administrator'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
            'limit' => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');

        if($req->status == 'All')
        {
            $order_list = TableReservation::where('restaurant_id', $restaurant_id)
            ->limit($req->limit)
            ->orderBy('updated_at', 'DESC')
            ->paginate($req->limit);

        }        
        elseif($req->status >= 0 )
        {
            $order_list = TableReservation::where('restaurant_id', $restaurant_id)
            ->limit($req->limit)
            ->where('status', $req->status)
            ->orderBy('updated_at', 'DESC')
            ->paginate($req->limit);
        }
        else{return res('no record found.');}

        $order_list = ReservationList::collection($order_list);

        return res('success', $order_list);
    }

    public function manager_order_list($req)
    {
        $permissions = ['vendor', 'vendor staff','administrator'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $v = Validator::make($req->all(),[
            'restaurant_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $r = decode($req->restaurant_hash_id, 'uuid');
        
        $o = RestaurantOrderList::where('restaurant_id', $r)->get();

        if($req->order_hash_id)
        {
            $order_id = decode($req->order_hash_id, 'uuid');
            if($req->dateFrom && $req->dateTo)
            {   
                $dateFrom = $req->dateFrom.'00:00:00';        
                $dateTo = $req->dateTo.'00:00:00';
                
                $o = RestaurantOrder::where('id', $order_id)->where('restaurant_id', $r)
                    ->whereBetween('created_at', [$dateFrom, $dateTo])->get();
            }
        }

        if($req->dateFrom && $req->dateTo)
        {   
            $dateFrom = $req->dateFrom.'00:00:00';        
            $dateTo = $req->dateTo.'00:00:00';
            
            $o = RestaurantOrder::where('restaurant_id', $r)
                ->whereBetween('created_at', [$dateFrom, $dateTo])->get();
        }
        
        $o = OrderListResource::collection($o);
        return res('success', $o);
    }

     /*****************************************************************************************
     * 
     *                  WAITER FUNCTIONALITIES
     * 
     *****************************************************************************************/
    public function dine_in_order_list_waiter($req)
    {
        $validator = Validator::make($req->all(),[
            'reservation_hash_id' =>    'required',
            'restaurant_hash_id' =>     'required',
        ]);

        if($validator->fails())
        {
            return res('failed', $validator->errors(), 402);
        }

        $reservation_id = decode($req->reservation_hash_id, 'uuid');
        $restaurant_id = decode($req->restaurant_hash_id, 'uuid');


        $reserved = TableReservation::where('id', $reservation_id)
        ->where('restaurant_id', $restaurant_id)
        ->orderBy('updated_at', 'DESC')
        ->get();

        if(!$reserved) return res('failed...qrcode not exist.');
        
        $reserved = WaiterOrderListResource::collection($reserved);

        return res('success', $reserved);
    }

    public function waiter_confirmation_order($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'order_hash_id' =>           'required',
            'status' =>                 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $cart_id = decode($req->order_hash_id, 'uuid');
        $cart = RestaurantOrder::where('id', $cart_id)->first();

        if(!$cart) return res('order not found');

        $cart->status =             $req->status;
        $cart->save();

        return res('order has been updated.');
    }

    public function complete_order($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'cart_hash_id' =>           'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $cart_id = decode($req->cart_hash_id, 'uuid');

        $cart =                     RestaurantCart::where('id', $cart_id)->first();
        
        if($cart == null) return res('cart not found.');
        $cart->is_completed =       2;
        $cart->status =             6;
        $cart->save();
        
        return res('order has been completed.');
    }

    public function updateFoodStatus($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'order_hash_id' =>           'required',
            'order_food_hash_id' =>      'required',
            'status' =>                  'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $order_id = decode($req->order_hash_id, 'uuid');
        $food_id = decode($req->order_food_hash_id, 'uuid');
        
        $order_list = RestaurantOrderList::where('order_id', $order_id)->first();
        if(!$order_list) return res('order not exist');

        $order_list = RestaurantOrderFood::where('id', $food_id)->first();
        $order_list -> status = $req->status;
        $order_list -> save();

        return res('successfully updated');
    }

    public function waiter_confirm_order($req)
    {
        $permissions = ['vendor', 'vendor staff'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('user.not_allowed'), null, 400);
        }

        $validator = Validator::make($req->all(),[
            'reservation_hash_id' =>     'required'
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $reservation_id = decode($req->reservation_hash_id,'uuid');
        $reservation = TableReservation::where('id', $reservation_id)->first();

        $reservation->qrcode_status = 2;
        $reservation->status = 2;
        $reservation->save();

        $order = RestaurantOrder::where('reservation_id', $reservation->id)->first();
        $order->status = 6;
        $order->save();

        return res('transaction confirm');
    }
}