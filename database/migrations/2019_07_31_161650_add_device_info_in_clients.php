<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceInfoInClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('clients', 'uuid')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->string('uuid')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('clients', 'manufacturer')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->string('manufacturer')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('clients', 'version')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->string('version')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('clients', 'model')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->string('model')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('clients', 'serial')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->string('serial')->nullable()->default(null);
            });
        }
        if (!Schema::hasColumn('clients', 'platform')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->string('platform')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            //
        });
    }
}
