<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RestaurantManagerInterface;

class ManagerController extends Controller
{
    private $req;
    private $restaurant_manager;

    public function __construct(Request $req, RestaurantManagerInterface $restaurant_manager)
    {
        $this->req = $req;
        $this->restaurant_manager = $restaurant_manager;
    }

    public function addFoodInFoodType()
    {
       return $this->restaurant_manager->addFoodInFoodType($this->req);
    }

    public function addFoodInFoodTypeSubBranch()
    {
       return $this->restaurant_manager->addFoodInFoodTypeSubBranch($this->req);
    }

    public function foodList()
    {
        return $this->restaurant_manager->foodList($this->req);
    }

    public function addedFoods()
    {
        return $this->restaurant_manager->addedFoods($this->req);
    }

    public function notAddedFoods()
    {
        return $this->restaurant_manager->notAddedFoods($this->req);
    }

    public function removeFoodInFoodType()
    {
        return $this->restaurant_manager->removeFoodInFoodType($this->req);
    }

    public function addFood()
    {
        return $this->restaurant_manager->addFood($this->req);
    }

    public function editFood()
    {
        return $this->restaurant_manager->editFood($this->req);
    }

    public function addVariationSelection()
    {
        return $this->restaurant_manager->addVariationSelection($this->req);
    }

    public function editVariationSelection()
    {
        return $this->restaurant_manager->editVariationSelection($this->req);
    }

    public function listVariationSelection()
    {
        return $this->restaurant_manager->listVariationSelection($this->req);
    }

    public function deleteVariationSelection()
    {
        return $this->restaurant_manager->deleteVariationSelection($this->req);
    }

    public function listOfServices()
    {
        return $this->restaurant_manager->listOfServices($this->req);
    }

    public function updateOfServices()
    {
        return $this->restaurant_manager->updateOfServices($this->req);
    }

    public function showFood()
    {
        return $this->restaurant_manager->showFood($this->req);
    }

    public function addChildVariation()
    {
        return $this->restaurant_manager->addChildVariation($this->req);
    }

    public function editChildVariation()
    {
        return $this->restaurant_manager->editChildVariation($this->req);
    }

    public function deleteChildVariation()
    {
        return $this->restaurant_manager->deleteChildVariation($this->req);
    }

    public function deleteFood()
    {
        return $this->restaurant_manager->deleteFood($this->req);
    }

    public function addBusyduration()
    {
        return $this->restaurant_manager->addBusyduration($this->req);
    }

    public function stopBusyduration()
    {
        return $this->restaurant_manager->stopBusyduration($this->req);
    }

    public function closeOpen()
    {
        return $this->restaurant_manager->closeOpen($this->req);
    }
}
