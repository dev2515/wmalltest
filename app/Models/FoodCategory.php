<?php

namespace App\Models;

use App\Http\Resources\Restaurant\FoodResource;
use App\Http\Resources\Restaurant\MenuListResource;
use Illuminate\Database\Eloquent\Model;

class FoodCategory extends Model
{
    protected $appends = ['hash_id', 'foods'];
    protected $table = 'food_categories';
    protected $fillable = ['name', 'category_image', 'restaurant_id'];

    public function getHashIdAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function foods()
    {
        return $this->hasMany(\App\Models\RestaurantFood::class, 'category_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(\App\Models\Store::class, 'restaurant_id');
    }

    public function getFoodsAttribute()
    {
        // $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        //$restaurant_id = decode($req->restaurant_id, 'uuid');
        $restaurant_id = isset($this->attributes['restaurant_id']) ? $this->attributes['restaurant_id'] : 0;

        $food = RestaurantFood::select('id','restaurant_id','meal','name', 'price', 'description', 'food_image','category_id')
        ->where('category_id', $id)
        ->where('restaurant_id', $restaurant_id)
        ->where('is_approved', 1)
        ->get();

        $food = MenuListResource::collection($food);

        return $food;
    }
}
