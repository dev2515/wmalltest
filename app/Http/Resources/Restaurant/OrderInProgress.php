<?php

namespace App\Http\Resources\Restaurant;

use App\Models\RestaurantOrderList;
use App\Models\Store;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderInProgress extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'order_hash_id' => $this->hash_id,
            'order_number' => decode($this->order_number,'order-number'),
            'sub_total' => $this->sub_total,
            'shipping_rate' => $this->shipping_rate,
            'total' => twoDecimalPlaces($this->sub_total + $this->shipping_rate),
            'status_info' => $this->status_info,
            'restaurant' => $this->store(),
        ];
    }

    public function store()
    {
        $o = RestaurantOrderList::select('id','order_id','restaurant_id')->where('order_id', $this->id)->first();
        $o = Store::select('id','name','street','state','city','other_address_info','phone','store_email','latitude','longitude')
        ->where('id', $o->restaurant_id)->first();
        if($o) $o->makeHidden(['id','user_id','is_main_branch','main_store_id','status_info','ratings','chat_performance','ship_out_time'
        ,'cancellation_rate','product_count','categories','banners','total_staff','timings','followers','total_foods','is_opened']);

        return $o;
    }
}
