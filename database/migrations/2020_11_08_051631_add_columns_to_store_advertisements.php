<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToStoreAdvertisements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumns('store_advertisements', ['duration_id', 'start_at'])){
            Schema::table('store_advertisements', function (Blueprint $table) {
                $table->integer('duration_id')->after('store_id');
                $table->string('start_at')->after('image');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumns('store_advertisements', ['duration_id', 'start_at'])){
            Schema::table('store_advertisements', function (Blueprint $table) {
                $table->dropColumn('duration_id');
                $table->dropColumn('start_at');
            });
        }
    }
}
