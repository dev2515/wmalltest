<?php

namespace App\Http\Resources\Product;
use stdClass;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandShow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid' => $this->hashid,
            'name' => $this->name,
            'isPremium' => $this->is_premium,
            'added_by' => $this->addedBy(),
            'status' => $this->status(),
            'full' => $this->photo()->full,
            'thumbnail' => $this->photo()->thumbnail,
            'created_at' => $this->created_at,
        ];
    }

    public function photo()
    {
        $full = public_path() . '/images/brands/' . $this->hashid . '-full.png';
        if(file_exists($full)){
            $full = '/images/brands/' . $this->hashid . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/brands/' . $this->hashid . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/brands/' . $this->hashid . '-thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = asset($full);
        $size->thumbnail = asset($thumb);

        return $size;
    }

    public function status()
    {
        if($this->is_approved === 1) return 'approved';
        if($this->is_declined === 1) return 'declined';
        if($this->is_approved === 0 && $this->is_declined === 0) return 'pending';
    }

    public function addedBy()
    {
        $user = User::where('id', $this->request_by)->first();
        if(!$user) return null;
            
        return $user->name;
    }
}
