<?php

namespace App\Repositories;

use App\Events\VendorImagesEvent;
use App\Http\Resources\Blogger\BloggerListResource;
use App\Http\Resources\Blogger\CategoryResource;
use App\Http\Resources\Blogger\ProductLisResource;
use App\Interfaces\BloggerInterface;
use App\Model\StoreBranch;
use App\Models\BloggerFollowing;
use App\Models\BusinessType;
use App\Models\Product;
use App\Models\ProductCategoryImage;
use App\Models\ProductImage;
use App\Models\State;
use App\Models\Store;
use App\Models\StoreChangeField;
use App\Models\StorePreregister;
use App\Models\StoreTheme;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class BloggerRepository implements BloggerInterface
{
    public function blogger_list($req)
    {   
        $u = Store::where('is_approved', 1)->where('business_type_id', 5)->get();
        $u = BloggerListResource::collection($u);
        return res('success', $u, 200);
    }

    public function category_list($req)
    {
        $v = Validator::make($req->all(),[
            'store_hash_id' => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $store_id = decode($req->store_hash_id, 'uuid');

        $c = DB::table('product_categories')->join('products', 'product_categories.product_id', '=', 'products.id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->select('product_categories.category_id','categories.name', 'products.store_id')->where('products.store_id', $store_id)->get();

        $c = CategoryResource::collection($c);
        
        return res('success', $c, 200);
    }

    public function product_list($req)
    {
        $v = Validator::make($req->all(),[
            'store_hash_id'         => 'required',
            'category_hash_id'      => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $shid = decode($req->store_hash_id, 'uuid');
        $chid = decode($req->category_hash_id, 'uuid');
        
        $p = DB::table('products')->join('stores', 'products.store_id', '=', 'stores.id')
            ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
            ->join('categories', 'product_categories.category_id', '=', 'categories.id')
            ->select('products.id','products.name','products.price','products.width', 'products.height', 'products.stock_status_id')
            ->where('products.store_id', $shid)->where('product_categories.category_id', $chid)->get();
        
        $p = ProductLisResource::collection($p);

        return res('success', $p, 200);
    }

    public function blogger_follower($req)
    {

    }

    public function add_product($req)
    {
        DB::beginTransaction();

        $permissions = ['blogger'];

        if (!in_array(auth()->user()->type_info, $permissions)) {
            return res(__('vendor.not_allowed'), null, 400);
            DB::rollBack();
        }
    
        $validator = Validator::make($req->all(), [
            'store_hash_id'     => 'required',
            'product_type_id'   => 'required',
            'category_id'      => 'required',
            'name'              => 'required',
            'price'             => 'required',
            'short_description' => 'required|max:500',
            'tags'              => 'required',
            'delivery_option'   => 'required',
            'length'            => 'required',
            'width'             => 'required',
            'height'            => 'required',
            'weight'            => 'required'
        ]);

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        $store_id       = decode($req->store_hash_id, 'uuid');
        $brand_id       = 0;
        $branch_ids     = [];

        $store_check = DB::table('stores')->select('id','business_type_id')->where('id', $store_id)->first();
        if($store_check->business_type_id != 5) return res('failed','store not exist!', 401);

        $has_product = Product::where('store_id', $store_id)->where('name', $req->name)->count();

        if ($has_product > 0) {
            DB::rollBack();
            return res(__('product.existed'), null, 400);
        }

        $new                        = new StoreChangeField;
        $new->store_id              = $store_id;
        $new->type                  = 'add product';
        $new->data                  = $req->only('store_id', 'name', 'price', 'on_sale', 'sale_price', 'short_description');
        $new->request_by            = auth()->id();
        $new->is_approved           = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $new->process_by            = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;

        $product                    = new Product;
        $product->store_id          = $store_id;
        $product->brand_id          = $brand_id;
        $product->product_type_id   = $req->product_type_id;
        $product->branch_ids        = $branch_ids;
        $product->category_ids      = $req->category_id;
        $product->name              = $req->name;
        $product->price             = $req->price;
        $product->short_description = $req->short_description;
        $product->quick_details     = $req->quick_details;
        $product->tags              = $req->tags;
        $product->uuid              = $req->uuid;
        $product->days_of_return    = 7; // @AES Default wblue days of return;
        $product->years_of_warranty = 1; // @AES Default years of waranty
        $product->length            = $req->length;
        $product->width             = $req->width;
        $product->height            = $req->height;
        $product->weight            = $req->weight;
        $product->delivery_option   = $req->delivery_option;
        $product->dimensions        = 0;
        $product->is_approved       = auth()->user()->type_info == 'administrator' ? 1 : 0;
        $product->process_by        = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $product->request_by        = auth()->user()->type_info == 'administrator' ? auth()->id() : 0;
        $product->status            = auth()->user()->type_info == 'administrator' ? 1 : 0;

        if ($req->has('used_owned') && $req->used_owned){
            $product->days_of_return    = $req->days_of_return;
            $product->years_of_waranty  = $req->years_of_waranty;
        }

        if ($req->has('on_sale') && $req->on_sale) {
            if ($req->sale_price <= 0) {
                DB::rollBack();
                return res(__('product.invalid_price'), null, 400);
            }

            $validator = Validator::make($req->all(), [
                'sale_date_from'    => 'required',
                'sale_date_to'      => 'required',
            ]);

            if ($validator->fails()) {
                DB::rollBack();
                return res('Failed', $validator->errors(), 412);
            }

            $product->sale_price        = $req->sale_price;
            $product->sale_date_from    = $req->sale_date_from;
            $product->sale_date_to      = $req->sale_date_to;
        }
        

        if ($req->has('stock_status_id')) {
            $product->stock_status_id = $req->stock_status_id;
        }
        if ($req->has('sku')) {
            $product->sku = $req->sku;
        }
        
        if ($req->has('processing_time')) {
            $product->processing_time = $req->processing_time;
        }
        $product->save();
        $new->product_id = $product->id;
        $new->save();
        //$this->saveCategoryRelation($product->id, $req->category_ids);
        //$this->saveTagRelation($product->id, $req->tags);
        // $this->saveDeliveryDetails($product->id, $req->delivery_options); @AES comment out for big changes

        if ($req->has('quantity')) {
            $req->request->add(['product_id' => $product->id]);
            (new InventoryRepository)->add($req);
        }

        if ($req->has('variations')) {
           // $this->saveVariations($product->id, $req->variations);
        }
        $images_base64 = json_decode($req->images_base64, true); // ------------- comment for testing--------
        // $images_base64 = $req->images_base64; // ------------- uncomment for testing--------
        if ($req->has('images_base64') && count($images_base64) > 0) {

            $path = public_path() . '/images/products';
            if(!file_exists($path)){
                File::makeDirectory(base_path() . '/public/images/products');
            }
            File::makeDirectory(base_path() . '/public/images/products/' . encode($product->id, 'uuid'), $mode = 0777, true, true);
            foreach ($images_base64 as $key => $value) {
                $pi                 = new ProductImage;
                $pi->product_id     = $product->id;
                $pi->base64         = $value;
                $pi->file_name      = $product->name . '-' . $key;
                $pi->save();

                $product_path = public_path() . '/images/products/' . encode($product->id, 'uuid') . '/';

                $make = Image::make($value);
                $make->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $make->save($product_path . encode($pi->id, 'uuid') . '-full.png');

                $make->resize(150, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $make->save($product_path . encode($pi->id, 'uuid') . '-thumbnail.png');
            }
        }
        DB::commit();
        return res('Success', $product->hashid, 201);
    }

    public function add_boutique_shop($req)
    {
        DB::beginTransaction();
        $permissions = ['blogger'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only bloger admin can do this process', null, 401);
        }

        $validator = Validator::make($req->all(), [
            'is_main_branch'        => 'required|boolean',
            'store_name'            => 'required|unique:stores,name',
            'store_phone_number'    => 'required',
            'store_email'           => 'required',

            'street'                => 'required',
            'city'                  => 'required',
            'state'                 => 'required',
            'other_address_info'    => 'required',

            'profile_photo_img'     => 'required',
            'cover_photo_img'       => 'required'

        ], [
            'name.unique'           => 'The Store Name has already been taken',
        ]);

        // $validator->sometimes('main_store_hashid', 'required|string', function ($input) {
        //     return $input->is_main_branch == 0;
        // });

        if ($validator->fails()) {
            DB::rollBack();
            return res('Failed', $validator->errors(), 412);
        }

        // $main_store_id  = decode($req->main_store_hashid, 'uuid');
        // $main_branch    = Store::where('id', $main_store_id)->where('is_main_branch', 1)->first();
        // if($req->is_main_branch == 0 && !$main_branch){
        //     DB::rollBack();
        //     return res(__('store.main_branch.not_found'), null, 412);
        // }

        $user = User::where('id', auth()->user()->id)->where('status', 1)->first();
        if(!$user) return res('user not found', null, 402);

        $store                      = new Store;
        //$store->is_main_branch      = $req->is_main_branch ? 1 : 0;
        $store->name                = $req->store_name;
        $store->phone               = $req->store_phone_number;
        $store->store_email         = $req->store_email;
        $store->paypal_email        = '';
        $store->business_type_id    = 5;

        $store->street              = $req->street;
        $store->city                = $req->city;
        $store->state               = $req->state;
        $store->other_address_info  = $req->other_address_info;
        $store->latitude            = $req->latitude ? $req->latitude : null;
        $store->longitude           = $req->longitude ? $req->longitude : null;
        
        $theme  = StoreTheme::where('name', 'default')->first();

        if(!$theme) {
            DB::rollBack();
            return res('Theme not found', null, 401);
        }

        $store->theme_id            = $theme->id;
        $store->country             = 'qat';
        $store->zip                 = '';
        $store->company_type        = '';
        $store->established_type    = '';
        $store->date_established    = '';
        $store->sell_on_country     = '';

        //-- Vendor Credential images
        if($req->has('profile_photo_img')) $store->profile_photo_img = $req->profile_photo_img;
        if($req->has('cover_photo_img')) $store->cover_photo_img = $req->cover_photo_img;
        //--
        
        $password = explode("@", $req->vendor_email);
        if (strlen($password[0]) < 6) { 
            $password = $password[0].generateMobileToken();
        }

        $user = User::where('email', $req->vendor_email)->first();

        if($user){
            $store->user_id         = $user->id;
            $store->agent_id        = auth()->id(); // process by agent who created vendor
            $store->po_box          = '';
            $store->zone_number     = null;
            $store->store_branches  = $req->branches;
            $store->save();

            //-- Creating branch as main branch
            //$this->createBranch($req->is_main_branch, $main_branch->id, $store, $user);

            //-- Creating local storage for vendor images
            event(new VendorImagesEvent($store));
            //

            activityLog('Create blogger shop' . $store->id);
            DB::commit();
            return res('Success', $store->hashid, 200);
        }
    
        //$this->createBranch($req->is_main_branch, $main_branch->id, $store, $user);
        event(new VendorImagesEvent($store));

        activityLog('Create store' . $store->id);
        DB::commit();

        return res('Success', $store->hashid, 200);
    }

    public function createBranch($is_main_branch, $main_branch_id, $store, $user)
    {
        $branch                     = new StoreBranch;
        $branch->store_id           = $is_main_branch ? $store->id : $main_branch_id;
        $branch->store_child_id     = $is_main_branch ? 0 : $store->id;
        $branch->name               = $is_main_branch ? 'Main Branch' : $store->name;
        $branch->contact_name       = $user->name;
        $branch->street             = $store->street;
        $branch->state              = $store->state;
        $branch->city               = $store->city;
        $branch->other_address_info = $store->other_address_info;
        $branch->contact_number     = $user->mobile;
        $branch->contact_email      = $user->email;
        $branch->latitude           = $store->latitude ? $store->latitude : null;
        $branch->longitude          = $store->longitude ? $store->longitude : null;
        $branch->is_approved        = 1;
        $branch->is_declined        = 0;
        $branch->process_by         = auth()->id();
        $branch->is_main            = $is_main_branch ? 1 : 0;
        $branch->status             = 1;
        $branch->save();
    }

    public function add_product_category_image($req)
    {
        $permissions = ['administrator', 'afmin Staff', 'vendor', 'vendor staff', 'blogger'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_found'), null, 402);
        }

        $v = Validator::make($req->all(),[
            'store_hash_id'             => 'required',
            'category_hash_id'          => 'required',
            'base64_image'              => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 402);

        $store_id = decode($req->store_hash_id, 'uuid');
        $category_id = decode($req->category_hash_id, 'uuid');

        $img                        = new ProductCategoryImage;
        $img->store_id              = $store_id;
        $img->category_id           = $category_id;
        $img->base64_image          = $req->base64_image;
        $img->save();

        $full = public_path() . '/images/blogger/'. $req->store_hash_id .'/'. $req->category_hash_id . '-full.png';
        $thumbnail = public_path() . '/images/blogger/' . $req->store_hash_id .'/'. $req->category_hash_id . '-thumbnail.png';

        if(file_exists($full) && file_exists($thumbnail)) {
            File::delete($full);
            File::delete($thumbnail);
        }

        $local_path = public_path() . '/images/blogger/'.$req->store_hash_id.'/';
        File::makeDirectory($local_path, 0777, true, true);

        $image = Image::make($img->base64_image);
        $image->save($local_path . $req->category_hash_id . '-full.png');
        $image->resize(150, null, function ($constraint) {
            $constraint->aspectRatio();
        }); //@AES resizing
        $image->save($local_path . $req->category_hash_id . '-thumbnail.png');

        return res('success', null, 200);
    }

    public function follow_blogger($req)
    {
        $permissions = ['customer'];

        if(!in_array(auth()->user()->type_info, $permissions)){
            return res(__('user.not_found'), null, 402);
        }

        $v= Validator::make($req->all(),[
            'blogger_hash_id'   => 'required',
        ]);

        if($v->fails()) return res('failed', $v->errors(), 401);

        $blogger_id = decode($req->blogger_hash_id, 'uuid');

        $u = DB::table('users')->select('id')->where('id', $blogger_id)->where('type', 3)->first();
        if(!$u) return res('failed', 'blogger not exist', 401);

        //$bl = BloggerFollowing;
    }
}