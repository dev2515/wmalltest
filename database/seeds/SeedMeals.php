<?php

use Illuminate\Database\Seeder;
use App\Models\Meal;

class SeedMeals extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Meal::count();

        $names = ['Breakfast', 'Lunch', 'Dinner', 'All'];
        if($count === 0){
            foreach ($names as $name){
                Meal::firstOrCreate(['name' => $name]);
            }
        }
    }
}
