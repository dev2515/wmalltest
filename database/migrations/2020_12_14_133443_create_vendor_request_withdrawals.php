<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorRequestWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vendor_request_withdrawals')){
            Schema::create('vendor_request_withdrawals', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('store_id');
                $table->string('type');
                $table->string('company_name')->nullable();
                $table->string('receiver_name')->nullable();
                $table->string('bank_name')->nullable();
                $table->string('account_number')->nullable();
                $table->string('iban')->nullable();
                $table->string('qid_number')->nullable();
                $table->integer('amount')->default(0);
                $table->boolean('is_approved')->default(0);
                $table->boolean('is_declined')->default(0);
                $table->integer('modified_by')->default(0);
                $table->string('modified_at')->nullable();
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_request_withdrawals');
    }
}
