<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAndRenameTableIdDatatypeInTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('table_reservations', 'table_id')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->renameColumn('table_id', 'table_number');
            });
        }

        if(Schema::hasColumn('table_reservations', 'table_number')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->string('table_number')->nullable()->change();
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'table_id')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('table_id', 'has_booking')->default(0);
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'has_booking')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->boolean('has_booking')->default(0)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('table_reservations', 'table_number')){
            Schema::table('table_reservations', function (Blueprint $table) {
                $table->renameColumn('table_number', 'table_id');
            });
        }

        if(Schema::hasColumn('restaurant_carts', 'has_booking')){
            Schema::table('restaurant_carts', function (Blueprint $table) {
                $table->renameColumn('has_booking', 'table_id');
            });
        }
    }
}
