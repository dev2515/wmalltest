<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgentIdInStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'agent_id')){
            Schema::table('stores', function (Blueprint $table) {
                $table->integer('agent_id')->after('user_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'agent_id')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('agent_id');
            });
        }
    }
}
