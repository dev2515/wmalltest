<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDeclinedToStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('store_branches', 'is_declined')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->boolean('is_declined')->after('is_approved');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_branches', 'is_declined')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('is_declined');
            });
        }
    }
}
