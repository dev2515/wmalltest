<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreChangeField extends Model
{
    protected $appends = ['hashid'];

    protected $casts = ['data' => 'object'];

    public function getHashidAttribute()
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($id, 'uuid');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function documentRequestImages()
    {
        return $this->hasMany(StoreRequestImages::class, 'store_change_field_id');
    }
}
