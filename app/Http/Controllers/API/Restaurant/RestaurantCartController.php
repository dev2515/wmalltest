<?php

namespace App\Http\Controllers\API\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RestaurantCartInterface;

class RestaurantCartController extends Controller
{
    protected $req;
    protected $cart;

    public function __construct(Request $req, RestaurantCartInterface $cart)
    {
        $this->req = $req;
        $this->cart = $cart;
    }

    public function list()
    {
        return $this->cart->list($this->req);
    }

    public function add()
    {
        return $this->cart->add($this->req);
    }

    public function count()
    {
        return $this->cart->count($this->req);
    }

    public function clear()
    {
        return $this->cart->clear($this->req);
    }

    public function updateQuantity()
    {
        return $this->cart->updateQuantity($this->req);
    }

    public function toggleCheckout()
    {
        return $this->cart->toggleCheckout($this->req);
    }

    public function remove()
    {
        return $this->cart->remove($this->req);
    }

    public function checkout()
    {
        return $this->cart->checkout($this->req);
    }

    public function cartTotal()
    {
        return $this->cart->cartTotal($this->req);
    }

    public function CartConfirmation()
    {
        return $this->cart->CartConfirmation($this->req);
    }

    public function reopenCart()
    {
        return $this->cart->reopenCart($this->req);
    }

    public function addOrderInExistingCart()
    {
        return $this->cart->addOrderInExistingCart($this->req);
    }

    public function mycartlist()
    {
        return $this->cart->mycartlist($this->req);
    }

    public function getQRCode()
    {
        return $this->cart->getQRCode($this->req);
    }

    public function addtoCart()
    {
        return $this->cart->addtoCart($this->req);
    }

    public function open_cart()
    {
        return $this->cart->open_cart($this->req);
    }

    public function get_customer_currnet_location()
    {
        return $this->cart->get_customer_currnet_location($this->req);
    }
}
