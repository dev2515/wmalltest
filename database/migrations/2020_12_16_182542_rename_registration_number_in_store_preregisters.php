<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRegistrationNumberInStorePreregisters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('store_preregisters','registration_number')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->renameColumn('registration_number', 'regnumber');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('store_preregisters','regnumber')){
            Schema::table('store_preregisters', function (Blueprint $table) {
                $table->renameColumn('regnumber', 'registration_number');
            });
        }
    }
}
