<?php

namespace App\Http\Resources\Driver;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use Carbon\Carbon;
use App\Models\UserAddress;
use App\Models\DriverVehicle;
use App\Models\DriverLicense;
class DriverInfo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hashid'            => $this->user->hashid,
            'on_duty'           => $this->on_duty,
            'qid_number'        => $this->user->qid_number,
            'qid_expiration'    => $this->user->qid_expiration,
            'name'              => $this->user->name,
            'email'             => $this->user->email,
            'mobile'            => $this->user->mobile,
            'address'           => $this->address(),
            'age'               => null,
            'birthday'          => null,
            'nationality'       => $this->user->nationality,
            'vehicle'           => $this->vehicle(),
            'order_reference'   => null,
            'declined_clients'  => 0,
            'total_delivery'    => 0,
            'total_income'      => centToPrice($this->total_income),
            'total_trip'        => 0,
            'total_distance'    => $this->total_distance,
            'total_income'      => $this->total_income,
            'daily_trips'       => $this->daily_trip,
            'status'            => $this->status->name,
            'declined_clients'  => 0,
            'total_working_hour'=> $this->total_working_hour,
            'timestamp'         => $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'full'              => asset($this->profilePhoto()->full),
            'thumbnail'         => asset($this->profilePhoto()->thumbnail),
        ];
    }

    public function profilePhoto()
    {
        $full = public_path() . '/images/users/' . $this->user->hashid . '/full.png';
        if(file_exists($full)){
            $full = '/images/users/' . $this->user->hashid . '/full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/users/' . $this->user->hashid . '/thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/users/' . $this->user->hashid . '/thumbnail.png';
        } else { $thumb = null;}

        $size = new stdClass;
        $size->full = $full;
        $size->thumbnail = $thumb;

        return $size;
    }

    public function address()
    {
        $address = UserAddress::where('user_id', $this->user->id)->first();
        if(!$address) return null;

        return $address->street . ' ' . $address->state . ' ' . $address->city;
    }

    public function vehicle()
    {
        $vehicle = DriverVehicle::where('user_id', $this->user->id)->first();
        if(!$vehicle) return null;

        $license = DriverLicense::where('user_id', $this->user->id)->first();
        if(!$license) return null;

        return [
            'make'              => $vehicle->make,
            'plate_number'      => $vehicle->plate_number,
            'model'             => $vehicle->model,
            'transmission'      => $vehicle->transmission,
            'vehicle_expiry'    => $vehicle->expiration_date,
            'license_number'    => $license->license_number,
            'license_expiry'    => $license->expired_at,
        ];
    }
}
