<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorType extends Model
{
    public function getDetailsAttribute()
    {
        return castsToArray($this->attributes['details']);
    }
}
