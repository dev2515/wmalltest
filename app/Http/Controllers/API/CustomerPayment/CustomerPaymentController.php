<?php

namespace App\Http\Controllers\API\CustomerPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\PaymentMethodInterface;

class CustomerPaymentController extends Controller
{
    protected $req;
    protected $payment;

    public function __construct(Request $req, PaymentMethodInterface $payment)
    {
        $this->req      = $req;
        $this->payment  = $payment;
    }

    public function generateToken()
    {
        return $this->payment->generateToken();
    }

    public function register()
    {
        return $this->payment->register($this->req);
    }

    public function checkout()
    {
        return $this->payment->checkout($this->req);
    }

    public function savedCardInfo()
    {
        return $this->payment->savedCardInfo($this->req);
    }
}
