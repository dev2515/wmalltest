<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariationTypeSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_variation_type_selections')){
            Schema::create('product_variation_type_selections', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('produce_variation_id');
                $table->string('name');
                $table->integer('amount')->default(0);
                $table->smallInteger('is_added_price')->default(1);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('product_variation_type_selections')){
            Schema::dropIfExists('product_variation_type_selections');
        }
    }
}
