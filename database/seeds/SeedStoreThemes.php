<?php

use Illuminate\Database\Seeder;
use App\Models\StoreTheme;

class SeedStoreThemes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('data/store-theme.json');
        $json = file_get_contents($path);
        $json = json_decode($json);

        $delete = DB::table('store_themes')->delete();
        foreach ($json->themes as $theme){
            StoreTheme::firstOrCreate(['id' => $theme->id, 'name' => $theme->name, 'codes' => $theme->codes]);
        }
    }
}
