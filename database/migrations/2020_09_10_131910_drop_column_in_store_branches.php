<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnInStoreBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('store_branches', 'other_info')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('other_info');
            });
        }

        if(Schema::hasColumn('store_branches', 'contact')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('contact');
            });
        }

        if(Schema::hasColumn('store_branches', 'email')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('email');
            });
        }

        if(Schema::hasColumn('store_branches', 'mobile')){
            Schema::table('store_branches', function (Blueprint $table) {
                $table->dropColumn('mobile');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_branches', function (Blueprint $table) {
            //
        });
    }
}
