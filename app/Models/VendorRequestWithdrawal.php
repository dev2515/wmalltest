<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorRequestWithdrawal extends Model
{
    protected $fillable = ['store_id', 'type', 'company_name','receiver_name', 'bank_name', 'iban', 'qid_number', 'amount'];
    protected $appends = ['hashid'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }

}
