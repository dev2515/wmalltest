<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFromUserIdToIsFromUserInUserConversation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('user_conversations', 'from_user_id')){
            Schema::table('user_conversations', function (Blueprint $table) {
                $table->renameColumn('from_user_id', 'is_from_user');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_conversations', 'is_from_user')){
            Schema::table('user_conversations', function (Blueprint $table) {
                $table->renameColumn('is_from_user', 'from_user_id');
            });
        }
    }
}
