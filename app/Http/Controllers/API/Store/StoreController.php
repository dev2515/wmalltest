<?php

namespace App\Http\Controllers\API\Store;

use Illuminate\Http\Request;
use App\Interfaces\StoreInterface;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    protected $req;
    protected $store;

    public function __construct(Request $req, StoreInterface $store)
    {
        $this->req = $req;
        $this->store = $store;
    }

    public function find()
    {
        return $this->store->find($this->req);
    }

    public function details()
    {
        return $this->store->details($this->req);
    }

    public function listOfdays()
    {
        return $this->store->listOfdays($this->req);
    }

    public function showTimeSchedule()
    {
        return $this->store->showTimeSchedule($this->req);
    }

    public function addTimeShedule()
    {
        return $this->store->addTimeShedule($this->req);
    }

    public function deleteTimeSchedule()
    {
        return $this->store->deleteTimeSchedule($this->req);
    }

    public function openOrClose()
    {
        return $this->store->openOrClose($this->req);
    }

    public function editTimeSchedule()
    {
        return $this->store->editTimeSchedule($this->req);
    }

    public function addAdvertisement()
    {
        return $this->store->addAdvertisement($this->req);
    }

    public function editAdvertisement()
    {
        return $this->store->editAdvertisement($this->req);
    }

    public function deleteAdvertisement()
    {
        return $this->store->deleteAdvertisement($this->req);
    }

    public function durationList()
    {
        return $this->store->durationList();
    }

    public function changeFieldTypes()
    {
        return $this->store->changeFieldTypes();
    }
}
