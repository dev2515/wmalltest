<?php

namespace App\Http\Resources\RestaurantManager;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
use App\Models\FoodVariationSelection;
class FoodList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'        => $this->hash_id,
            'name'          => $this->name,
            'description'   => $this->description,
            'price'         => centToPrice($this->price),
            'variations'    => new FoodVariationCollection($this->getVariations()),
            'is_have_variation' => $this->is_have_variation,
            'full'          => asset($this->photo()->full),
            'thumbnail'     => asset($this->photo()->thumbnail), 
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . $this->hash_id . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . $this->hash_id . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . $this->hash_id . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . $this->hash_id . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }

    public function getVariations(Type $var = null)
    {
        return $variations = FoodVariationSelection::where('food_id', $this->id)->where('status', 1)->get();
    }
}
