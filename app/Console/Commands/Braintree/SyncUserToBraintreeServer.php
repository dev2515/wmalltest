<?php

namespace App\Console\Commands\Braintree;

use Illuminate\Console\Command;
use App\User;

class SyncUserToBraintreeServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:user_to_braintree_server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * This command is to sync our user customer data to braintree server.
     * @return mixed
     */
    public function handle()
    {
        $users  = User::where('type', 0)->where('status', 1)->get();

        $bar = $this->output->createProgressBar(count($users));
        $bar->start();

        foreach ($users as $user){

            $names = explode(" ", $user->name);

            braintree()->customer()->create([
                'id'        => $user->id,
                'firstName' => $names[0],
                'lastName'  => end($names),
                'phone'     => $user->mobile ? $user->mobile : null,
            ]);

            $bar->advance();
        }


        $bar->finish();
    }
}
