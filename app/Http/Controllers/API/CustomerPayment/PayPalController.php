<?php

namespace App\Http\Controllers\API\CustomerPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\PayPalInterface;

class PayPalController extends Controller
{
    protected $req;
    protected $paypal;

    public function __construct(Request $req, PayPalInterface $paypal)
    {
        $this->req      = $req;
        $this->paypal   = $paypal;
    }

    public function createPayment()
    {
        return $this->paypal->createPayment($this->req);
    }

    public function executePayment()
    {
        return $this->paypal->executePayment($this->req);
    }
}
