<?php

namespace App\Listeners;

use App\Events\VendorImagesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use App\Models\ComputerCardImage;
use Illuminate\Support\Facades\File;
class ComputerCardImgListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VendorImagesEvent  $event
     * @return void
     */
    public function handle(VendorImagesEvent $event)
    {
        $store = $event->data->model;

        $computer_card_images = json_decode($event->data->computer_card_images, true);
        
        foreach ($computer_card_images as $image){
            $new = new ComputerCardImage;
            $new->store_id = $store->id;
            $new->base64 = $image;
            $new->save();

            $store_path = public_path() . '/images/stores/' . encode($store->id, 'uuid') . '/';
        
            $path = $store_path . 'computer-card-images/';

            if(!file_exists($path)) {
                File::makeDirectory(base_path() . '/public/images/stores/' . encode($store->id, 'uuid') . '/computer-card-images', $mode = 0777, true, true);
            }

            $image = Image::make($new->base64);
            $image->save($path . $new->id .'-computer-card-full.png');
            $image->resize(150, 150);
            $image->save($path . $new->id .'-computer-card-thumbnail.png');
        }

    }
}
