<?php

namespace App\Http\Resources\Message;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Stores\StoreList;
use App\Models\Store;
use stdClass;
use App\Models\UserConversation;
use App\User;
class Conversation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashid'    =>      $this->hashid,
            'time'      =>      $this->time(),
            'store'     =>      $this->store(),
            'is_seen'   =>      $this->isNew() > 0 ? 0 : 1,
            'new_message_total'     =>      $this->isNew(),
            'last_message'          =>      $this->lastMessage()
        ];
    }

    public function store()
    {
        $store = Store::where('id', $this->store_id)->first();
        if(!$store) return null;

        $full = public_path().'/images/stores/' . $store->hashid . '/profile-photo-full.png';
        if(file_exists($full)){
            $full = '/images/stores/' . $store->hashid . '/profile-photo-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/stores/' . $store->hashid . '/profile-photo-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/stores/' . $store->hashid . '/profile-photo-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->name = $store->name;
        $data->profile_full = asset($full);
        $data->profile_thumbnail = asset($thumbnail);

        return $data;
    }

    public function time()
    {
        $user_conversation = UserConversation::where('conversation_id', $this->id)->where('created_at', 'DESC')->first();
        if(!$user_conversation) return $this->created_at;

        return $user_conversation->created_at;
    }

    public function user()
    {
        $user = User::where('id', $this->user_id)->first();
        if(!$user) return null;

        $data = new stdClass;
        $data->hashid = $user->hash_id;
        $data->name = $user->name;

        return $data;
    }

    public function isNew()
    {
        $count = UserConversation::where('conversation_id', $this->id)->where('is_from_store', 1)->where('is_new', 1)->where('deleted_store', '!=', 1)->count();
        return $count;
    }

    public function lastMessage()
    {
        $last_message = UserConversation::where('conversation_id', $this->id)->orderBy('created_at', 'DESC')->first();
        if(!$last_message) return null;

        return $last_message->text;
    }
}
