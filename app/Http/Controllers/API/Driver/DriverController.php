<?php

namespace App\Http\Controllers\API\Driver;

use Illuminate\Http\Request;
use App\Interfaces\DriverInterface;
use App\Http\Controllers\Controller;

class DriverController extends Controller
{
    protected $req;
    protected $driver;

    public function __construct(Request $req, DriverInterface $driver)
    {
        $this->req = $req;
        $this->driver = $driver;
    }

    public function add()
    {
        return $this->driver->add($this->req);
    }

    public function edit()
    {
        return $this->driver->edit($this->req);
    }

    public function list()
    {
        return $this->driver->list($this->req);
    }

    public function addDocument()
    {
        return $this->driver->addDocument($this->req);
    }

    public function listDocument()
    {
        return $this->driver->listDocument($this->req);
    }

    public function adminVehicleApprove()
    {
        return $this->driver->adminVehicleApprove($this->req);
    }

    public function vehicleList()
    {
        return $this->driver->vehicleList($this->req);
    }

    public function driverAccountApprove()
    {
        return $this->driver->driverAccountApprove($this->req);
    }

    public function show()
    {
        return $this->driver->show($this->req);
    }

    public function vehicleShow()
    {
        return $this->driver->vehicleShow($this->req);
    }

    public function myPreferredDelivery()
    {
        return $this->driver->myPreferredDelivery($this->req);
    }

    public function account()
    {
        return $this->driver->account($this->req);
    }

    public function updatePreferredDelivery()
    {
        return $this->driver->updatePreferredDelivery($this->req);
    }

    public function deliveryList()
    {
        return $this->driver->deliveryList($this->req);
    }

    public function myCurrentLocation()
    {
        return $this->driver->myCurrentLocation($this->req);
    }

    public function nearBy()
    {
        return $this->driver->nearBy($this->req);
    }

    public function transactionHistory()
    {
        return $this->driver->transactionHistory($this->req);
    }

    public function driver_order_list()
    {
        return $this->driver->driver_order_list($this->req);
    }

    public function driver_accepted_list()
    {
        return $this->driver->driver_accepted_list($this->req);
    }

    public function driver_accept_order()
    {
        return $this->driver->driver_accept_order($this->req);
    }

    public function driver_update_location()
    {
        return $this->driver->driver_update_location($this->req);
    }

    public function customer_order_recieve()
    {
        return $this->driver->customer_order_recieve($this->req);
    }

    public function driver_confirm_order()
    {
        return $this->driver->driver_confirm_order($this->req);
    }

    public function driver_scan_order_restaurant()
    {
        return $this->driver->driver_scan_order_restaurant($this->req);
    }

    public function driver_order_history()
    {
        return $this->driver->driver_order_history($this->req);
    }

    public function driver_net_income()
    {
        return $this->driver->driver_net_income($this->req);
    }

    public function driver_notification_settings()
    {
        return $this->driver->driver_notification_settings($this->req);
    }

    public function driver_current_status()
    {
        return $this->driver->driver_current_status($this->req);
    }

    public function driver_credits_today()
    {
        return $this->driver->driver_credits_today($this->req);
    }

    public function delete_unnecessary()
    {
        return $this->driver->delete_unnecessary($this->req);
    }

    public function drivers_collect_income()
    {
        return $this->driver->drivers_collect_income($this->req);
    }

    public function driver_notify_collection()
    {
        return $this->driver->driver_notify_collection($this->req);
    }

    public function get_driver_notify()
    {
        return $this->driver->get_driver_notify($this->req);
    }

    public function order_reciept()
    {
        return $this->driver->order_reciept($this->req);
    }
}
