<?php

namespace App\Http\Resources\Restaurant;

use Illuminate\Http\Resources\Json\JsonResource;

class QRListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'order_number'=>                $this->order_number,
            'qrcode'=>                      $this->qrcode,
            'order_type'=>                  $this->order_type_info,
            'total'=>                       centToPrice($this->sub_total),
            'date'=>                        $this->created_at,
        ];
    }
}