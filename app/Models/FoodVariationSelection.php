<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodVariationSelection extends Model
{
    protected $table    = 'food_variation_selections';
    protected $hidden   = ['status', 'created_at', 'updated_at', 'food_id', 'id'];
    protected $appends  = ['hashid'];

    public function getHashIdAttribute():string
    {
        $id = isset($this->attributes['id']) ? $this->attributes['id'] : 0;
        return encode($this->id, 'uuid');
    }

    //=== ONE TO MANY====//
    public function variationTypeSelections()
    {
        return $this->hasMany(VariationTypeSelection::class, 'food_variation_id')->where('status', 1);
    }
}
