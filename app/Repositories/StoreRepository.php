<?php

namespace App\Repositories;
use Illuminate\Support\Facades\DB;

use App\Models\Store;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Interfaces\StoreInterface;
use App\Models\StoreRating;
use App\Models\StoreTiming;
use Illuminate\Support\Facades\Validator;
use App\Models\Day;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\Http\Resources\Stores\ScheduleCollection;
use App\Http\Resources\Stores\TimeScheduleCollection;
use App\Models\StoreSchedule;
use App\Models\StoreAdvertisement;
use App\Http\Resources\Stores\StoreListCollection;

use App\Models\AdsExpirationDate;
use App\Http\Resources\Stores\AdvertisementCollection;
use App\Http\Resources\Stores\DurationCollection;
use App\Models\ChangeFieldType;
use App\Models\BusinessType;
use App\Http\Resources\Restaurant\RestaurantResource;
use App\Models\FoodTypePivot;

class StoreRepository implements StoreInterface
{
    public function find($req)
    {
        $limit = 10;
        if ($req->has('limit')) {
            $limit = $req->limit;
        }

        $city = 'All';
        $category = 'All';
        if ($req->has('city')) {
            $city = $req->city;
        }
        if ($req->has('category')) {
            $category = $req->category;
        }

        if (strtolower($category) == 'all') {
            $category_ids = Category::pluck('id');
        } else {
            $category_ids = Category::where('name', $category)->pluck('id');
        }
        $product_ids = ProductCategory::whereIn('category_id', $category_ids)->pluck('product_id')->unique();
        $store_ids = Product::whereIn('id', $product_ids)->pluck('store_id')->unique();

        if (strtolower($city) == 'all') {
            $stores = Store::whereIn('id', $store_ids)->where('sell_on_country', session('iso'))->where('is_approved', 1)->where('ban', 0)->limit($limit)->get()->shuffle();
        } else {
            $stores = Store::whereIn('id', $store_ids)->where('city', $city)->where('sell_on_country', session('iso'))->where('is_approved', 1)->where('ban', 0)->limit($limit)->get()->shuffle();
        }
        $stores->makeHidden(['qid', 'eid', 'commercial_registration', 'commercial_permit', 'updated_at', 'is_approved', 'ban', 'approved_at']);

        $found_store_ids = $stores->pluck('id');
        $other_stores = Store::whereNotIn('id', $found_store_ids)->where('sell_on_country', session('iso'))->where('is_approved', 1)->where('ban', 0)->limit($limit - $stores->count())->get()->shuffle();
        $other_stores->makeHidden(['qid', 'eid', 'commercial_registration', 'commercial_permit', 'updated_at', 'is_approved', 'ban', 'approved_at']);

        return res('Success', [
            'found' => $stores,
            'alternative' => $other_stores
        ]);
    }

    public function details($req)
    {
        if (!$req->has('store_id')) {
            return res(__('validation.identifier'), null, 400);
        }

        if ($req->store_id == 0 && $req->has('user_id')) {
            $store = Store::where('user_id', $req->user_id)->where('ban', 0)->where('sell_on_country', session('iso'))->first();
        } else {
            $store = Store::where('id', $req->store_id)->where('ban', 0)->where('sell_on_country', session('iso'))->first();
        }
        if (!$store) {
            return res(__('validation.not_found'), null, 400);
        }

        return res('Success', $store);
    }

    public function listOfdays($req)
    {
        if(!$req->store_hashid) return res(__('store.identifier'), null, 401);

        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        $data = new ScheduleCollection($store->schedules);

        return res('success', $data);
    }

    public function showTimeSchedule($req)
    {
        $carbon = Carbon::now();
        $permissions = ['agent', 'administrator', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        if(!$req->day_hashid) return res('hashid is required', null, 401);

        $store_schedule_id = decode($req->day_hashid, 'uuid');
        
        $schedules = StoreTiming::where('store_schedule_id', $store_schedule_id)->where('status', 1)->get();
        $data = new TimeScheduleCollection($schedules);

        return res('success', $data, 200);
    }

    public function addTimeShedule($req)
    {
        $permissions = ['agent', 'administrator', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'day_hashid'    => 'required',
            'title'         => 'required',
            'store_open'    => 'required',
            'store_close'   => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $store_schedule_id = decode($req->day_hashid, 'uuid');
        
        $add = new StoreTiming();
        $add->store_schedule_id = $store_schedule_id;
        $add->name              = $req->title;
        $add->store_open        = $req->store_open;
        $add->store_close       = $req->store_close;
        $add->save();

        return res('success', null, 200);
    }

    public function editTimeSchedule($req)
    {
        $permissions = ['agent', 'administrator', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'time_hashid'   => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $store_timing_id = decode($req->time_hashid, 'uuid');

        $time_schedule = StoreTiming::where('id', $store_timing_id)->where('status', 1)->first();
        if(!$time_schedule) return res('Time schedule not found', null, 404);

        if($req->store_open)  $time_schedule->store_open  = $req->store_open;
        if($req->store_close) $time_schedule->store_close = $req->store_close;
        if($req->title)       $time_schedule->name        = $req->title;

        $time_schedule->save();

        return res('success', null, 201);

    }

    public function deleteTimeSchedule($req)
    {
        $permissions = ['agent', 'administrator', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'time_hashid'   => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $store_timing_id = decode($req->time_hashid, 'uuid');

        $time_schedule = StoreTiming::where('id', $store_timing_id)->where('status', 1)->first();
        if(!$time_schedule) return res('Time schedule not found', null, 404);

        $time_schedule->status = 0;
        $time_schedule->save();

        return res('success', null, 201);

    }

    public function openOrClose($req)
    {
        $permissions = ['agent', 'administrator', 'vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'day_hashid'    => 'required',
            'is_open'       => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);
        
        $store_schedule_id = decode($req->day_hashid, 'uuid');

        $schedule = StoreSchedule::where('id', $store_schedule_id)->first();
        if(!$schedule) return res('Schedule not found', null, 404);

        $schedule->is_opened = $req->is_open;
        $schedule->save();
        
        return res('success', null, 201);
    }

    public function addAdvertisement($req)
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'store_hashid'      => 'required',
            'duration_hashid'   => 'required',
        ]);

        if($validator->fails()) {
            DB::rollBack();
            return res('failed', $validator->errors(), 402);
        }

        $store_id       = decode($req->store_hashid, 'uuid');
        $duration_id    = decode($req->duration_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) {
            DB::rollBack();
            return res(__('store.not_found'), null, 404);
        }

        $duration = AdsExpirationDate::where('id', $duration_id)->first();
        if(!$duration) {
            DB::rollBack();
            return res('Duration not found', null, 404);
        }
        
        $store_advertisement = StoreAdvertisement::where('store_id', $store_id)->where('status', 1)->first();
        if(!$store_advertisement){
            $add_advertisement = new StoreAdvertisement();
            $add_advertisement->store_id    = $store_id;
            $add_advertisement->duration_id = $duration->id;
            $add_advertisement->image       = $req->image;
            $add_advertisement->start_at    = Carbon::now();
            $add_advertisement->end_at      = getExpirationDate($duration);
            $add_advertisement->save();

            $parent_folder = public_path() . '/images/store-advertisements';
            if(!file_exists($parent_folder)){
                File::makeDirectory($parent_folder, $mode = 0777, true, true);
            }

            $child_folder = public_path() . '/images/store-advertisements/' . encode($add_advertisement->store_id, 'uuid');
            if(!file_exists($child_folder)){
                File::makeDirectory($child_folder, $mode = 0777, true, true);
            }

            $path = public_path() . '/images/store-advertisements/' . encode($add_advertisement->store_id, 'uuid') . '/';

            $image = Image::make($req->image);
            $image->save($path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path .'thumbnail.png');

            DB::commit();
            return res('success', null, 201);
        }

        return res('Already exist', null, 409);
    }

    public function editAdvertisement($req)
    {
        DB::beginTransaction();
        $permissions = ['agent', 'administrator', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'store_hashid'  => 'required',
            'image'         => 'required',
        ]);

        if($validator->fails()) {
            DB::rollBack();
            return res('failed', $validator->errors(), 402);
        }
        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) {
            DB::rollBack();
            return res(__('store.not_found'), null, 404);
        }

        $add_advertisement = StoreAdvertisement::where('store_id', $store_id)->first();
        if(!$add_advertisement) {
            DB::rollBack();
            return res(__('store.advertise_not_found'), null, 404);
        }

        $add_advertisement->image = $req->image;
        $add_advertisement->save();
        
        $child_folder = public_path() . '/images/store-advertisements/' . encode($add_advertisement->store_id, 'uuid');
        if(file_exists($child_folder)){
            $path_full  = public_path() . '/images/store-advertisements/' . encode($add_advertisement->store_id, 'uuid') . '/full.png';
            $path_thumb = public_path() . '/images/store-advertisements/' . encode($add_advertisement->store_id, 'uuid') . '/thumbnail.png';

            if (File::exists($path_full) && File::exists($path_thumb)) {
                File::delete($path_full);
                File::delete($path_thumb);
            }

            $path = public_path() . '/images/store-advertisements/' . encode($add_advertisement->store_id, 'uuid') . '/';

            $image = Image::make($req->image);
            $image->save($path . 'full.png');
            $image->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            }); //@AES resizing
            $image->save($path .'thumbnail.png');
        }

        DB::commit();
        return res('success', null, 201);
    }

    public function deleteAdvertisement($req)
    {
        $permissions = ['agent', 'administrator', 'vendor'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('You are not allowed to process this.', null, 401);
        }

        $validator = Validator::make($req->all(),[
            'store_hashid'  => 'required',
        ]);

        if($validator->fails()) return res('failed', $validator->errors(), 402);

        $store_id = decode($req->store_hashid, 'uuid');

        $store = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 404);

        $store_advertisement = StoreAdvertisement::where('store_id', $store_id)->where('status', 1)->first();

        if(!$store_advertisement) return res(__('store.advertise_not_found'), null, 404);
        
        $store_advertisement->status = 0;
        $store_advertisement->save();

        return res('success', null, 201);
    }

    public function storeAdvertisementList($req)
    {
        $limit = 10;
        if($req->limit) $limit = $req->limit;
        $now = Carbon::now();

        $store_ids      = StoreAdvertisement::where('end_at', '>=', $now)->where('status', 1)->pluck('store_id');
        $current_ids    = [];

        foreach ($store_ids as $store_id){
            $food_types = FoodTypePivot::where('store_id', $store_id)->where('status', 1)->count();
            if($food_types > 0) array_push($current_ids, $store_id);
        }

        $business_type  = BusinessType::where('name', 'restaurant')->first();  
        $stores         = Store::whereIn('id', $current_ids)->where('is_active', 1)->where('business_type_id', $business_type->id)->paginate($limit);

        $data = new AdvertisementCollection($stores);
        return res('success', $data, 200);
    }

    public function durationList()
    {
        $durations  = AdsExpirationDate::get();
        $data       = new DurationCollection($durations);

        return res('success', $data, 200);
    }

    public function changeFieldTypes()
    {
        $types = ChangeFieldType::get();
        return res('success', $types, 200);
    }

}
