<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeColumnsInStoreTimings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumns('store_timings', ['store_open', 'store_close'])){
            Schema::table('store_timings', function (Blueprint $table) {
                $table->string('store_open')->change();
                $table->string('store_close')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_timings', function (Blueprint $table) {
            //
        });
    }
}
