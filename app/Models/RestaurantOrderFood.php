<?php

namespace App\Models;

use App\Http\Resources\Restaurant\OrderFoodImage;
use Illuminate\Database\Eloquent\Model;
use stdClass;

class RestaurantOrderFood extends Model
{
    protected $table = 'restaurant_order_food';
    protected $appends = ['hash_id', 'food_info', 'price'];

    public function getHashIdAttribute()
    {
        return encode($this->id, 'uuid');
    }
    
    public function list()
    {
        return $this->belongsTo(\App\Models\RestaurantOrderFood::class, 'order_list_id');
    }

    public function getFoodInfoAttribute()
    {
        $id = isset($this->attributes['food_id']) ? $this->attributes['food_id'] : 0;
        $food = RestaurantFood::where('id', $id)->first();
        $c = collect(['full' => asset($this->photo()->full), 'thumbnail' => asset($this->photo()->thumbnail)]);

        $food = new OrderFoodImage($food);
        return $c;
    }

    public function getPriceAttribute()
    {
        return centToPrice($this->attributes['price']);
    }

    public function photo()
    {      
        $full = public_path() . '/images/food/' . encode($this->id, 'uuid') . '-full.png';
        if(file_exists($full)){
            $full = '/images/food/' . encode($this->id, 'uuid') . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/food/' . encode($this->id, 'uuid') . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/food/' . encode($this->id, 'uuid') . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }
}
