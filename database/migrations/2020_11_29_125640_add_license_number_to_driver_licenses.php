<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseNumberToDriverLicenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('driver_licenses', 'license_number')){
            Schema::table('driver_licenses', function (Blueprint $table) {
                $table->string('license_number')->default(0)->after('processed_by');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('driver_licenses', 'license_number')){
            Schema::table('driver_licenses', function (Blueprint $table) {
                $table->dropColumn('license_number');
            });
        }
    }
}
