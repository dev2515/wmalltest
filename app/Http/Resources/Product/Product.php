<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Stores\StoreList;
use App\Http\Resources\Images\ProductImageCollection;
use App\Models\ProductReview;
use Carbon\Carbon;
class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $dt = Carbon::parse($this->updated_at);

        return [
            'hash_id'                   => $this->hashid,
            'store_id'                  => $this->storeHashId(),
            'product_type'              => $this->product_type_info,
            'category'                  => $this->category_info,
            'tags'                      => json_decode($this->tags, true),
            'name'                      => $this->name,
            'price'                     => $this->price,
            'short_description'         => $this->short_description,
            'quick_details'             => json_decode($this->quick_details, true),
            'brand_id'                  => $this->brand_id,
            'days_of_return'            => $this->days_of_return,
            'years_of_waranty'          => $this->years_of_waranty,
            'on_sale'                   => $this->is_on_sale,
            'sale_price'                => $this->sale_price,
            'sale_date_from'            => $this->sale_date_from,
            'sale_date_to'              => $this->sale_date_to,
            'stock_status'              => $this->stock_status_info,
            'sku'                       => $this->sku,
            'dimensions'                => $this->dimensions_info,
            'quantity'                  => $this->inventory_info,
            'processing_time'           => $this->processing_time,
            'variations'                => $this->variations_info,
            'total_reviews'             => $this->totalReview(),
            'branch_id'                 => $this->branch,
            'uuid'                      => $this->uuid,
            'delivery_option'           => $this->delivery_option,
            'timestamp'                 =>  $dt->year. $dt->month . $dt->day . $dt->hour . $dt->minute . $dt->second . $dt->micro,
            'images'                    => new ProductImageCollection($this->images),
        ];
    }

    public function totalReview(){

        $id = decode($this->hashid, 'uuid');
        $review = ProductReview::where('product_id', $id)->where('status', 1)->count();

        return $review;
    }

    public function storeHashId(){

        $id = encode($this->store_id, 'uuid');

        return $id;
    }
}
