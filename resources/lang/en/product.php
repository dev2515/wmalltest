<?php

return [
    'identifier'        => 'Product hashid is required',
    'existed'           => 'Product is already existed',
    'not_found'         => 'Product not found',
    'invalid_quantity'  => 'Please input valid quantity, should be more that or equal to 1',
    'shipping_required' => 'Please select at least 1 shipping provider',
    'tags'              => 'Product tags should be 1 or 5 tags only',
    'invalid_price'     => 'Please input valid price, should be more than 0',
    'brand_not_found'   => 'Brand not found',
    'out_of_stock'      => 'Out of stock'
];
