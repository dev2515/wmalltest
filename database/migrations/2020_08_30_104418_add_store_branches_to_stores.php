<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreBranchesToStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('stores', 'store_branches')){
            Schema::table('stores', function (Blueprint $table) {
                $table->json('store_branches')->nullable()->after('store_information');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('stores', 'store_branches')){
            Schema::table('stores', function (Blueprint $table) {
                $table->dropColumn('store_branches');
            });
        }
    }
}
