<?php

namespace App\Repositories;

use App\Models\Bank;
use App\Interfaces\BankInterface;
use Illuminate\Support\Facades\Validator;

class BankRepository implements BankInterface
{
    public function list($req)
    {
        $list = Bank::where('iso', session('iso'))->where('status', 1)->get();
        $list->makeHidden(['iso', 'status', 'created_at', 'updated_at']);

        return res('Success', $list);
    }

    public function convertCurrency($req)
    {
        $validator = Validator::make($req->all(), [
            'from' => 'required',
            'to' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $pair = $req->from . '_' . $req->to;
        $json = conversions($pair);

        $value = $json->$pair * $req->amount;

        return res('Success', $value);
    }


}
