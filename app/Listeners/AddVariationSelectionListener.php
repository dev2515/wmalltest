<?php

namespace App\Listeners;

use App\Events\AddFoodEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\FoodVariationSelection;

class AddVariationSelectionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddFoodEvent  $event
     * @return void
     */
    public function handle(AddFoodEvent $event)
    {
        $data = $event->data;

        foreach ($data->variation_selections as $selection){
            $new            = new FoodVariationSelection();
            $new->food_id   = $data->model->id;
            $new->name      = $selection['name'];
            $new->price     = $selection['price'];
            $new->save();
        }
    }
}
