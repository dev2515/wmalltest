<?php

namespace App\Http\Resources\Blogger;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

class ProductLisResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return[
            'product_hash_id'                       => encode($this->id, 'uuid'),
            'name'                                  => $this->name,
            'price'                                 => $this->price,
            'width'                                 => $this->width,
            'height'                                => $this->height,
            'full'                                  => asset($this->photo()->full),
            'thumbnail'                             => asset($this->photo()->thumbnail),
        ];
    }

    public function photo()
    {      
        $full = public_path() . '/images/products/' . encode($this->id, 'uuid') . '-full.png';
        if(file_exists($full)){
            $full = '/images/products/' . encode($this->id, 'uuid') . '-full.png';
        } else { $full = null;}

        $thumb = public_path() . '/images/products/' . encode($this->id, 'uuid') . '-thumbnail.png';

        if(file_exists($thumb)){
            $thumb = '/images/products/' . encode($this->id, 'uuid') . '-thumbnail.png';
        } else { $thumb = null;}

        $data = new stdClass;
        $data->full = asset($full);
        $data->thumbnail = asset($thumb);

        return $data;
    }  
}
