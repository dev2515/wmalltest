<?php

namespace App\Repositories;
use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use PayPal\Api\WebProfile;
use PayPal\Api\InputFields;
use PayPal\Api\Transaction;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use App\Interfaces\PayPalInterface;
use App\Models\RestaurantCart;
use App\Models\RestaurantCartItem;

use Illuminate\Support\Facades\Validator;
use App\Models\RestaurantFood;
use App\Models\RestaurantCartItemVariation;
class PayPalRepository implements PayPalInterface
{
    /**
     * @param $req
     * @return JsonResponse
     */
    public function createPayment($req)
    {   
        $validator = Validator::make($req->all(), [
            'cart_hashid' => 'required',
        ]);

        if ($validator->fails()) {
            return res('Failed', $validator->errors(), 412);
        }

        $cart_id    = decode($req->cart_hashid, 'uuid');
        $cart       = RestaurantCart::where('id', $cart_id)->where('is_deleted', 0)->first();
        if(!$cart){
            return res(__('cart.not_found'), null, 404);
        }

        $cart_items = RestaurantCartItem::where('cart_id', $cart->id)->get();
        
        $paypal_items = [];

        $total = 0; // total for all food;

        foreach ($cart_items as $cart_item){

            $total_cart_items  = 0; // total of a food
            $food   = RestaurantFood::where('id', $cart_item->food_id)->first();

            $total_cart_items += $food->price; // food price
            $total_variation = RestaurantCartItemVariation::where('cart_item_id', $cart_item->id)->sum('np');

            $total_cart_items += $total_variation; // food price + variation over all total price

            $total += $total_cart_items * $cart_item->quantity; // over all total of a food

            $item = new Item();
            $item->setName($food->name)
                ->setCurrency('USD')
                ->setQuantity($cart_item->quantity)
                ->setPrice($total_cart_items);
            
                array_push($paypal_items, $item);
        }

        

        $api_context = payPalApiContext();

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");
    
    
        $itemList = new ItemList();
        $itemList->setItems($paypal_items);
        
        

        $details = new Details();
        $details->setShipping(0)->setTax(0)->setSubtotal($total);
        
        
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($total)
            ->setDetails($details);
        
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        
    
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://laravel-paypal-example.test")
            ->setCancelUrl("http://laravel-paypal-example.test");
    
        // Add NO SHIPPING OPTION
        $inputFields = new InputFields();
        $inputFields->setNoShipping(1);
    
        $webProfile = new WebProfile();
        $webProfile->setName('test' . uniqid())->setInputFields($inputFields);
    
        $webProfileId = $webProfile->create($api_context)->getId();
    
        $payment = new Payment();
        $payment->setExperienceProfileId($webProfileId); // no shipping
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
        
        try {

            $payment->create($api_context);

        } catch (Exception $ex) {
            echo $ex;

            exit(1);
        }
    
        return $payment;
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function executePayment($req)
    {
        $api_context = payPalApiContext();
    
        $paymentId = $req->paymentID;
        $payment = Payment::get($paymentId, $api_context);
    
        $execution = new PaymentExecution();
        $execution->setPayerId($req->payerID);
    
        // $transaction = new Transaction();
        // $amount = new Amount();
        // $details = new Details();
    
        // $details->setShipping(2.2)
        //     ->setTax(1.3)
        //     ->setSubtotal(17.50);
    
        // $amount->setCurrency('USD');
        // $amount->setTotal(21);
        // $amount->setDetails($details);
        // $transaction->setAmount($amount);
    
        // $execution->addTransaction($transaction);
    
        try {
            $result = $payment->execute($execution, $api_context);
        } catch (Exception $ex) {
            echo $ex;
            exit(1);
        }
    
        return $result;
    }
}