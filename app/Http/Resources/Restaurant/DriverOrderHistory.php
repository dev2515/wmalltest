<?php

namespace App\Http\Resources\Restaurant;

use App\Models\QRPointer;
use App\Models\RestaurantCart;
use App\Models\RestaurantOrder;
use App\Models\RestaurantOrderFood;
use App\Models\RestaurantOrderList;
use App\Models\Store;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverOrderHistory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'transaction_number' => $this->transaction_number,
            'store_name' => $this->store()->name,
            'store_address' => $this->store()->street.', '.$this->store()->city.', '.$this->store()->state,
            'customer_name' => $this->customer()->name,
            'delivery_address' => $this->order()->shipping_address,
            'sub_total' => $this->order()->sub_total,
            'shipping_rate' => $this->order()->shipping_rate,
            'total_amount' => twoDecimalPlaces($this->order()->sub_total + $this->order()->shipping_rate),
            'driver_accepted_order' => $this->driver_accepted_time,
            'deliver_at' => $this->updated_at,
            'order_number' => decode($this->order()->order_number, 'order-number'),
            'order_date' => $this->order()->created_at,
            'total_delivered' => $this->count(),
        ];
    }

    public function order()
    {
        $c = RestaurantCart::select('id', 'transaction_number')->where('transaction_number', $this->transaction_number)->first();
        $o = RestaurantOrder::select('id','customer_id','order_number','payment_method_id','sub_total','shipping_rate','shipping_address','created_at','updated_at','note','status','shipping_address_latitude','shipping_address_longitude')
            ->where('cart_id', $c->id)->first();

        if($o) $o = $o->makeHidden(['id','order_type_info','user_name','']);
        
        return $o;
    }

    public function order_list()
    {
        $ol = RestaurantOrderList::where('id', $this->order()->id)->first();
        $f = RestaurantOrderFood::select('id','')->where('order_list_id', $ol->id)->get();

        return $f;
    }

    public function store()
    {
        $ol = RestaurantOrderList::select('id','order_id','restaurant_id')->where('id', $this->order()->id)->first();
        $r = Store::select('id','name','street', 'city', 'state', 'other_address_info','latitude', 'longitude')
        ->where('id',$ol->restaurant_id)->first();

        return $r;
    }

    public function customer()
    {
        $c = User::select('id','name')->where('id', $this->order()->customer_id)->first();
        return $c;
    }

    public function count()
    {
        $c = QRPointer::select('id','assigned_driver_id')->where('assigned_driver_id', $this->assigned_driver_id)->count();
        return $c;
    }
}
