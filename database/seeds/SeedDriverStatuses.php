<?php

use Illuminate\Database\Seeder;
use App\Models\DriverStatus;

class SeedDriverStatuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DriverStatus::count();
        if($count == 0){
            $statuses = ['booked', 'day off', 'idle'];
            foreach ($statuses as $status){
                DriverStatus::firstOrCreate(['name' => $status]);
            }
        }
    }
}
