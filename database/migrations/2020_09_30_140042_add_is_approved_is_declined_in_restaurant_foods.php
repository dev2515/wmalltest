<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsApprovedIsDeclinedInRestaurantFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('restaurant_foods', 'is_approved')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->boolean('is_approved')->nullable()->after('description');
            });
        }

        if(!Schema::hasColumn('restaurant_foods', 'is_declined')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->boolean('is_declined')->nullable()->after('is_approved');
            });
        }

        if(!Schema::hasColumn('restaurant_foods', 'request_by')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->integer('request_by')->nullable()->after('is_declined');
            });
        }

        if(!Schema::hasCOlumn('restaurant_foods', 'process_by')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->integer('process_by')->nullable()->after('request_by');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('restaurant_foods', 'is_approved')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('is_approved');
            });
        }

        if(Schema::hasColumn('restaurant_foods', 'is_declined')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('is_declined');
            });
        }

        if(Schema::hasCOlumn('restaurant_foods', 'process_by')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('process_by');
            });
        }

        if(Schema::hasColumn('restaurant_foods', 'request_by')){
            Schema::table('restaurant_foods', function (Blueprint $table) {
                $table->dropColumn('request_by');
            });
        }
    }
}
