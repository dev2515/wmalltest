<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestByInProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('products', 'request_by')){
            Schema::table('products', function (Blueprint $table) {
                $table->integer('request_by')->nullable()->after('is_declined');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('products', 'request_by')){
            Schema::table('products', function (Blueprint $table) {
                $table->dropColumn('request_by');
            });
        }
    }
}
