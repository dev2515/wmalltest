<?php

namespace App\Http\Resources\Images;

use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;
class ProductImage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'full' => asset($this->local()->full),
            'thumbnail' => asset($this->local()->thumbnail),
        ];
    }

    public function local()
    {
        $full = public_path().'/images/products/' . encode($this->product_id, 'uuid') . '/' . encode($this->id, 'uuid') .'-full.png';
        if(file_exists($full)){
            $full = '/images/products/' . encode($this->product_id, 'uuid') . '/' . encode($this->id, 'uuid') .'-full.png';
        } else { $full = null; }

        $thumbnail  = public_path().'/images/products/' . encode($this->product_id, 'uuid') . '/' . encode($this->id, 'uuid') .'-thumbnail.png';
        if(file_exists($thumbnail)){
            $thumbnail  = '/images/products/' . encode($this->product_id, 'uuid') . '/' . encode($this->id, 'uuid') .'-thumbnail.png';
        } else { $thumbnail = null; }

        $data = new stdClass;
        $data->full = $full;
        $data->thumbnail = $thumbnail;

        return $data;
    }
}
