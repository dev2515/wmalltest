<?php

namespace App\Repositories;

use App\Interfaces\MessageInterface;
use App\Models\Conversation;
use App\Models\Store;
use Carbon\Carbon;
use App\Models\UserConversation;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Message\ConversationCollection;
use App\Http\Resources\Message\MessageCollection;
use App\User;
use App\Http\Resources\Message\StoreConversationCollection;
use Illuminate\Support\Facades\Mail;

class MessageRepository implements MessageInterface
{
    /**
     * @param $req
     * @return JsonResponse
     */
    public function userSendMessage($req)
    {
        $validator = Validator::make($req->all(), [
            'store_hashid'  => 'required',
            'message'       => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $store_id   = decode($req->store_hashid, 'uuid');

        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 401);
        

        $val    = Carbon::now();
        $date   = Carbon::parse($val);
        $now    = (int)$date->format('ymdhis');


        $conversation = Conversation::where('store_id', $store->id)->where('user_id', auth()->id())->first();
        if(!$conversation) {
            $new_convo              = new Conversation;
            $new_convo->store_id    = $store->id;
            $new_convo->user_id     = auth()->id();
            $new_convo->sent_at     = $now;
            $new_convo->save();

            $conversation = $new_convo;
        }

        $user_conversation                      = new UserConversation;
        $user_conversation->conversation_id     = $conversation->id;
        $user_conversation->text                = $req->message;
        $user_conversation->is_from_user        = 1; 
        $user_conversation->is_from_store       = 0; 
        $user_conversation->type                = 'message';
        $user_conversation->save();

        $conversation->sent_at                  = $now;
        $conversation->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeSendMessage($req)
    {
        $validator = Validator::make($req->all(), [
            'user_hashid' => 'required',
            'store_hashid' => 'required',
            'message' => 'required',
        ]);

        if ($validator->fails()) return res('Failed', $validator->errors(), 412);

        $user_id    = decode($req->user_hashid, 'uuid');
        $store_id   = decode($req->store_hashid, 'uuid');

        $user       = User::where('id', $user_id)->first();
        if(!$user) return res(__('user.not_found'), null, 401);
        
        $store      = Store::where('id', $store_id)->first();
        if(!$store) return res(__('store.not_found'), null, 401);

        $val        = Carbon::now();
        $date       = Carbon::parse($val);
        $now        = (int)$date->format('ymdhis');


        $conversation = Conversation::where('store_id', $store->id)->where('user_id', $user->id)->first();
        if(!$conversation) {
            $new_convo              = new Conversation;
            $new_convo->store_id    = $store->id;
            $new_convo->user_id     = $user->id;
            $new_convo->sent_at     = $now;
            $new_convo->save();

            $conversation = $new_convo;
        }

        $user_conversation                  = new UserConversation;
        $user_conversation->conversation_id = $conversation->id;
        $user_conversation->text            = $req->message;
        $user_conversation->is_from_user    = 0;
        $user_conversation->is_from_store   = 1;
        $user_conversation->type            = 'message';
        $user_conversation->save();

        $conversation->sent_at              = $now;
        $conversation->save();

        return res('success', null, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function userConversationList($req)
    {
        $limit = 10;
        if($req->limit) $limit = $req->limit;

        $conversations = Conversation::where('user_id', auth()->id())
            ->where('deleted_user', '!=', 1)
                ->orderBy('sent_at', 'DESC')->paginate($limit);

        $data = new ConversationCollection($conversations);

        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeConversationList($req)
    {
        if(!$req->store_hashid) return res(__('store.identifier'), null, 401);

        $limit = 10;
        if($req->limit) $limit = $req->limit;

        $store_id = decode($req->store_hashid, 'uuid');

        $conversations = Conversation::where('store_id', $store_id)
            ->where('deleted_store', '!=', 1)
                ->orderBy('sent_at', 'DESC')->paginate($limit);

        $data = new StoreConversationCollection($conversations);

        return res('success', $data, 200);
        
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function userMessageList($req)
    {
        if(!$req->conversation_hashid) return res(__('conversation.identifier'), null, 401);

        $limit = 10;
        if($req->limit) $limit = $req->limit;

        $conversation_id = decode($req->conversation_hashid, 'uuid');

        $update_new_message = UserConversation::where('conversation_id', $conversation_id)
        ->where('deleted_user', '!=', 1)
            ->where('is_from_store', 1)
                ->where('is_new', 1)->update(array('is_new' => 0));

        $messages = UserConversation::where('conversation_id', $conversation_id)
        ->where('deleted_user', '!=', 1)
            ->orderBy('created_at', 'DESC')->paginate($limit);
        
        $data = new MessageCollection($messages);
        return res('success', $data, 200);
    }

    /**
     * @param $req
     * @return JsonResponse
     */
    public function storeMessageList($req)
    {
        $permissions = ['vendor', 'vendor staff'];
        if(!in_array(auth()->user()->type_info, $permissions)){
            return res('Only vendor and vendor staff', null, 401);
        }

        if(!$req->conversation_hashid) return res(__('conversation.identifier'), null, 401);

        $limit = 10;
        if($req->limit) $limit = $req->limit;

        $conversation_id = decode($req->conversation_hashid, 'uuid');

        $update_new_message = UserConversation::where('conversation_id', $conversation_id)
        ->where('deleted_user', '!=', 1)
            ->where('is_from_user', 1)
                ->where('is_new', 1)->update(array('is_new' => 0));

        $messages = UserConversation::where('conversation_id', $conversation_id)
        ->where('deleted_store', '!=', 1)
            ->orderBy('created_at', 'DESC')->paginate($limit);
        
        $data = new MessageCollection($messages);
        return res('success', $data, 200);
    }

    public function sendEmail($req)
    {
        $data = array('name'=>"Virat Gandhi");
   
        Mail::send([], $data, function($message) {
            $message->to('arisudarbe@gmail.com', 'Bayot')->subject
            ('Laravel Basic Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        
        return res('Basic Email Sent. Check your inbox.');
    }
}